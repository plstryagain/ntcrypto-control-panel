#ifndef __CERTIFICATE__PARSER__H__
#define __CERTIFICATE__PARSER__H__

#include "Asn1Types.h"

class Asn1Decoder;

class CertificateParser 
{
protected:
	CertificateParser() = delete;
	CertificateParser(const byte* data, const uint32 size);
	CertificateParser(std::vector<byte>& data);
	CertificateParser(const CertificateParser& other) = delete;
	virtual ~CertificateParser() {}

public:
	virtual std::string GetCertificateVersionStr() = 0;
	virtual std::wstring GetCertificateSerialNumberStr() const = 0;
	virtual std::string GetSignatureAlgorithmIdentifierStr() = 0;
	virtual std::string GetNotBeforeStr() const = 0;
	virtual std::string GetNotAfterStr() const = 0;
	virtual std::vector<byte> GetSignatureValue() const = 0;
	virtual std::vector<byte> GetAuthorityKeyIdValue() const = 0;

protected:
	ASN1_ERROR ParseIssuerSerial(uint32* idx, ISSUER_SERIAL* issuer_serial);
	ASN1_ERROR ParseGeneralNames(uint32* idx, GENERAL_NAMES* gnames);
	ASN1_ERROR ParseAlgorithmIdentifier(uint32* idx, ALGORITHM_IDENTIFIER* aid);
	ASN1_ERROR ParseCertificateSerialNumber(uint32* idx, CERTIFICATE_SERIAL_NUMBER* csn);
	ASN1_ERROR ParseGeneralizedTime(uint32* idx, GENERALIZED_TIME* gt);
	ASN1_ERROR ParseAttribute(uint32* idx, ATTRIBUTE* att);
	ASN1_ERROR ParseExtensions(uint32* idx, EXTENSIONS* exts);
	std::string ByteArrayToHexString(const std::vector<byte>& ba, const bool is_use_uppercase) const;
	std::wstring ByteArrayToHexWString(const std::vector<byte>& ba, const bool is_use_uppercase) const;
	std::wstring DirectoryNameToStr(const NAME& dname);
	std::wstring RDNameOidToStr(const OBJECT_IDENTIFIER& oid);
	std::wstring GetOidName(const OBJECT_IDENTIFIER& oid) const;
	std::wstring ExtensionValueToString(const OBJECT_IDENTIFIER& oid, const std::vector<byte>& value) const;
	std::wstring GetAuthorityKeyIdStr(const std::vector<byte>& value) const;

protected:
	std::unique_ptr<Asn1Decoder> decoder_;
};

#endif // !__CERTIFICATE__PARSER__H__

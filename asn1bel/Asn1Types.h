#ifndef __ASN1__TYPES__H__
#define __ASN1__TYPES__H__

#include <vector>
#include <memory>
#include <string>

using byte = unsigned char;
using uint8 = byte;
using uint16 = unsigned short;
using uint32 = unsigned int;
using uint64 = unsigned long long;

enum class ASN1_ENCODING
{
	INVALID,
	DER,
	BER
};

enum class ASN1_OBJ_CLASS
{
	INVALID = -1,
	UNIVERSAL_CLASS = 0,
	APPLICATION = 1,
	CONTEXT_SPECIFIC = 2,
	PRIVATE = 3,
};

enum class ASN1_UNIVERSAL_TAG_NUMBER : unsigned int
{
	INVALID = 0xffffffff,
	BOOLEAN = 0x01,
	INTEGER = 0x02,
	BIT_STRING = 0x03,
	OCTET_STRING = 0x04,
	NULL_TAG = 0x05,
	OBJECT_IDENTIFIER = 0x06,
	ObjectDescriptor = 0x07,
	INSTANCE_OF = 0x08, 
	EXTERNAL = 0x08, 
	REAL = 0x09,
	ENUMERATED = 0x0a,
	EMBEDDED_PDV = 0x0b,
	UTF8String = 0x0c,
	RELATIVE_OID = 0x0d,
	SEQUENCE = 0x10, 
	SEQUENCE_OF = 0x10, 
	SET = 0x11, 
	SET_OF = 0x11,
	NumericString = 0x12,
	PrintableString = 0x13,
	TeletexString = 0x14,
	T61String = 0x14,
	VideotexString = 0x15,
	IA5String = 0x16,
	UTCTime = 0x17,
	GeneralizedTime = 0x18,
	GraphicString = 0x19,
	VisibleString = 0x1a,
	ISO646String = 0x1a,
	GeneralString = 0x1b,
	UniversalString = 0x1c,
	CHARACTER_STRING = 0x1d,
	BMPString = 0x1e,
};

enum class ASN1_ERROR
{
	success,
	unknown_error,
	unsupported_encoding,
	unexpected_end_of_file,
	unsupported_asn_tag,
	index_out_of_bound,
	unexpected_tag,
	invalid_parameter,
	unsupported_certificate_version,
	unsupported_content_type
};

enum class CERTIFICATE_VERSION
{
	v1 = 0,
	v2 = 1,
	v3 = 2
};

struct ASN1_TAG
{
	ASN1_OBJ_CLASS obj_class = ASN1_OBJ_CLASS::INVALID;
	bool is_primitive = true;
	union {
		ASN1_UNIVERSAL_TAG_NUMBER utag;
		uint32 choice;
	};
};

struct ASN1_NODE
{
	ASN1_TAG tag;
	uint32 idx = 0;
	uint32 deepness = 0;
	uint32 size = 0;
	byte* content = nullptr;
};


using ASNBOOLEAN = bool;
using INTEGER = std::vector<byte>;
using OBJECT_IDENTIFIER = std::vector<byte>;
using OCTET_STRING = std::vector<byte>;
using ATT_CERT_VERSION = std::vector<byte>;
using CERTIFICATE_SERIAL_NUMBER = INTEGER;
using TELETEX_STRING = std::vector<byte>;
using PRINTABLE_STRING = std::vector<byte>;
using UNIVERSAL_STRING = std::vector<byte>;
using UTF8STRING = std::vector<byte>;
using BMP_STRING = std::vector<byte>;
using GENERALIZED_TIME = std::vector<byte>;
using UTC_TIME = std::vector<byte>;
using OTHER_NAME = std::vector<byte>;
using IA5STRING = std::vector<byte>;
using ORADDRESS = std::vector<byte>;
using EDI_PARTY_NAME = std::vector<byte>;

struct BIT_STRING {
	uint32 unused_bits;
	std::vector<byte> content;
};

using UNIQUE_IDENTIFIER = BIT_STRING;

enum class DIGESTED_OBJECT_TYPE 
{
	publicKey = 0,
	publicKeyCert = 1,
	otherObjectTypes = 2
};

enum class DIRECTORY_STRING_CHOICE
{
	notDirectoryString = -1,
	teletexString = 0,
	printableString = 1,
	universalString = 2,
	utf8String = 3,
	bmpString = 4
};


struct DIRECTORY_STRING {
	DIRECTORY_STRING_CHOICE choice;
	TELETEX_STRING teletex_string;
	PRINTABLE_STRING printable_string;
	UNIVERSAL_STRING universal_string;
	UTF8STRING utf8string;
	BMP_STRING bmp_string;
};

using ATTRIBUTE_TYPE = OBJECT_IDENTIFIER;
//using ATTRIBUTE_VALUE = DIRECTORY_STRING;// std::vector<byte>;
struct ATTRIBUTE_VALUE
{
	ASN1_UNIVERSAL_TAG_NUMBER str_type;
	std::vector<byte> value;
};

struct ATTRIBUTE_TYPE_AND_VALUE {
	ATTRIBUTE_TYPE type;
	ATTRIBUTE_VALUE value;
};

using ATTRIBUTE_VALUES = std::vector<ATTRIBUTE_VALUE>;

using RELATIVE_DISTINGUISHED_NAME = std::vector<ATTRIBUTE_TYPE_AND_VALUE>;
using RDN_SEQUENCE = std::vector<RELATIVE_DISTINGUISHED_NAME>;

struct NAME {
	RDN_SEQUENCE rdn_sequence;
};

struct GENERAL_NAME {
	uint32 choice;
	OTHER_NAME other_name;
	IA5STRING rfc822name;
	IA5STRING dns_name;
	ORADDRESS x400_address;
	NAME directory_name;
	EDI_PARTY_NAME edi_party_name;
	IA5STRING uniform_resource_identifier;
	OCTET_STRING ip_address;
	OBJECT_IDENTIFIER registered_id;
};

using GENERAL_NAMES = std::vector<GENERAL_NAME>;

struct ALGORITHM_IDENTIFIER {
	OBJECT_IDENTIFIER algorithm;
	std::vector<byte> parameters;
};

struct EXTENSION {
	OBJECT_IDENTIFIER ext_id;
	ASNBOOLEAN critical = false;
	OCTET_STRING value;
};

using EXTENSIONS = std::vector<EXTENSION>;

struct TIME {
	UTC_TIME utc_time;
	GENERALIZED_TIME generalized_time;
};

struct ISSUER_SERIAL {
	GENERAL_NAMES issuer;
	CERTIFICATE_SERIAL_NUMBER serial;
	UNIQUE_IDENTIFIER issuer_uid;
};

struct ATT_CERT_VALIDITY_PERIOD {
	GENERALIZED_TIME not_before_time;
	GENERALIZED_TIME not_after_time;
};

struct ATTRIBUTE {
	ATTRIBUTE_TYPE type;
	ATTRIBUTE_VALUES values;
};

using ATTRIBUTES = std::vector<ATTRIBUTE>;

struct OBJECT_DIGEST_INFO {
	DIGESTED_OBJECT_TYPE digested_object_type;
	OBJECT_IDENTIFIER other_object_type_id;
	ALGORITHM_IDENTIFIER digest_algorithm;
	BIT_STRING object_digest;
};

struct ATT_CERT_ISSUER {
	GENERAL_NAMES issuer_name;
	ISSUER_SERIAL base_certificate_id;
	OBJECT_DIGEST_INFO object_digest_info;
};

struct HOLDER {
	ISSUER_SERIAL base_certificate_id;
	GENERAL_NAMES entity_name;
	OBJECT_DIGEST_INFO object_digest_info;
};

struct ATTRIBUTE_CERTIFICATE_INFO
{
	ATT_CERT_VERSION version;
	HOLDER holder;
	ATT_CERT_ISSUER issuer;
	ALGORITHM_IDENTIFIER signature;
	CERTIFICATE_SERIAL_NUMBER serial_number;
	ATT_CERT_VALIDITY_PERIOD attr_cert_validity_period;
	ATTRIBUTES attributes;
	UNIQUE_IDENTIFIER issuer_unique_id;
	EXTENSIONS extensions;
};

struct ATTRIBUTE_CERTIFICATE {
	ATTRIBUTE_CERTIFICATE_INFO attr_cert_info;
	ALGORITHM_IDENTIFIER signature_algorithm;
	BIT_STRING signature_value;
};



#endif // __ASN1__TYPES__H__
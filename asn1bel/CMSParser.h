#ifndef __CMS__PARSER__H__
#define __CMS__PARSER__H__

#include "Asn1Types.h"

class Asn1Decoder;

using DIGEST_ALGORTIM_IDENTIFIER = ALGORITHM_IDENTIFIER;

struct ENCAPSULATED_CONTENT_INFO
{
	OBJECT_IDENTIFIER e_content_type;
	OCTET_STRING e_content;
};

struct CMS_DATA
{
	CMS_DATA() {}
	virtual ~CMS_DATA() = 0 {}
};

struct DATA
{
	byte* ptr;
	uint32 size;
};

struct SIGNER_INFO
{

};

struct SIGNED_DATA : public CMS_DATA
{
	INTEGER cms_version;
	std::vector<DIGEST_ALGORTIM_IDENTIFIER> digest_algorithm_identifiers;
	ENCAPSULATED_CONTENT_INFO encap_content_info;
	std::vector<DATA> certificates;
	std::vector<DATA> att_certificates;
	std::vector<DATA> crls;
	std::vector<SIGNER_INFO> signer_infos;
};

struct CMS
{
	OBJECT_IDENTIFIER content_type;
	std::unique_ptr<CMS_DATA> content;
};

class CmsParser
{
public:
	CmsParser() = delete;
	CmsParser(const byte* data, const uint32 size);
	CmsParser(const std::vector<byte>& data);
	CmsParser(const CmsParser& other) = delete;
	~CmsParser();

public:
	ASN1_ERROR Parse();
	std::vector<std::vector<byte>> GetAttributeCertificates();

private:
	ASN1_ERROR ParseSignedData(uint32* idx);
	ASN1_ERROR ParseDigestAlgortihmIdentifiers(uint32* idx, 
		std::vector<DIGEST_ALGORTIM_IDENTIFIER>& digest_algorithm_identifiers);
	ASN1_ERROR ParseEncapsulatedContentInfo(uint32* idx, ENCAPSULATED_CONTENT_INFO* econtent_info);
	ASN1_ERROR ParseCertificates(uint32* idx, SIGNED_DATA* sdata);

private:
	std::unique_ptr<Asn1Decoder> decoder_;
	CMS cms_;
};

#endif // __CMS__PARSER__H__
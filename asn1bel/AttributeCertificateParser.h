#ifndef __ATTRIBUTE__CERTIFICATE__PARSER__H__
#define __ATTRIBUTE__CERTIFICATE__PARSER__H__

#include "CertificateParser.h"

class AttributeCertificateParser : public CertificateParser
{
public:
	AttributeCertificateParser(const byte* data, const uint32 size);
	AttributeCertificateParser(std::vector<byte>& data);
	~AttributeCertificateParser();
	AttributeCertificateParser() = delete;
	AttributeCertificateParser(const AttributeCertificateParser& other) = delete;

public:
	ASN1_ERROR Parse();
	std::string GetCertificateVersionStr() override;
	std::wstring GetCertificateSerialNumberStr() const override;
	std::wstring GetHolderCertificateSerialNumberStr() const;
	std::string GetSignatureAlgorithmIdentifierStr() override;
	std::wstring GetHolderNameStr();
	std::wstring GetIssuerNameStr();
	std::string GetNotBeforeStr() const override;
	std::string GetNotAfterStr() const override;
	std::vector<byte> GetSignatureValue() const override;
	std::vector<byte> GetAuthorityKeyIdValue() const override;

	

public:
	std::vector<std::wstring> GetAttributesStr() const;
	std::vector<OBJECT_IDENTIFIER> GetAttributeOidList() const;
	std::wstring GetAttributeNameStr(const OBJECT_IDENTIFIER & oid) const;
	std::wstring GetAttributeValueStr(const OBJECT_IDENTIFIER & oid) const;
	std::vector<std::wstring> GetExtensionsStr() const;
	std::vector<OBJECT_IDENTIFIER> GetExtensionsOidList() const;
	bool IsExtensionCritical(const OBJECT_IDENTIFIER& oid) const;
	std::wstring GetExtensionNameStr(const OBJECT_IDENTIFIER& oid) const;
	std::wstring GetExtensionValueStr(const OBJECT_IDENTIFIER& oid) const;
	std::vector<byte> GetToBeSignedValue() const;


private:
	ASN1_ERROR ParseAttributeCertificateInfo(uint32* idx);
	ASN1_ERROR ParseSignatureAlgorithm(uint32* idx);
	ASN1_ERROR ParseSignatureValue(uint32* idx);
	ASN1_ERROR ParseHolder(uint32* idx);
	ASN1_ERROR ParseIssuer(uint32* idx);
	ASN1_ERROR ParseSignature(uint32* idx);
	ASN1_ERROR ParseSerialNumber(uint32* idx);
	ASN1_ERROR ParseAttrCertValidityPeriod(uint32* idx);
	ASN1_ERROR ParseAttributes(uint32* idx);

private:
	ATTRIBUTE_CERTIFICATE att_cert_;
	byte* tbs_val_ = nullptr;
	uint32 tbs_len_ = 0;
};

#endif // __ATTRIBUTE__CERTIFICATE__PARSER__H__
#pragma once
#include <vector>
#include <string>
#include <map>

using byte = unsigned char;

namespace OID
{
	extern const std::string OBJECT_CLASS_STR;	
	extern const std::string ALLIASED_ENTRY_NAME_STR_OID;
	extern const std::string KNOWLEDGE_INFORMATION_STR_OID;
	extern const std::string COMMON_NAME_STR_OID;
	extern const std::string SURNAME_STR_OID;
	extern const std::string SERIAL_NUMBER_STR_OID;
	extern const std::string COUNTRY_NAME_STR_OID;
	extern const std::string LOCALITY_NAME_STR_OID;
	extern const std::string STATE_OR_PROVINCE_NAME_STR_OID;
	extern const std::string STREET_ADDRESS_STR_OID;
	extern const std::string ORGANIZATION_NAME_STR_OID;
	extern const std::string ORGANIZATION_UNIT_NAME_STR_OID;
	extern const std::string TITLE_STR_OID;
	extern const std::string DESCRIPTION_STR_OID;
	extern const std::string SEARCH_GUIDE_STR_OID;
	extern const std::string BUSINESS_CATEGORY_STR_OID;
	extern const std::string POSTAL_ADDRESS_STR_OID;
	extern const std::string POSTAL_CODE_STR_OID;
	extern const std::string POST_OFFICE_BOX_STR_OID;
	extern const std::string PHYSICAL_DELIVERY_OFFICE_NAME_STR_OID;
	extern const std::string TELEPHONE_NUMBER_STR_OID;
	extern const std::string TELEX_NUMBER_STR_OID;
	extern const std::string TELETEX_TERMINAL_IDENTIFIER_STR_OID;
	extern const std::string EMAIL_ADDRESS_STR_OID;





	extern const std::vector<byte> OBJECT_CLASS_OID;
	extern const std::vector<byte> ALLIASED_ENTRY_NAME_OID;
	extern const std::vector<byte> KNOWLEDGE_INFORMATION_OID;
	extern const std::vector<byte> COMMON_NAME_OID;
	extern const std::vector<byte> SURNAME_OID;
	extern const std::vector<byte> SERIAL_NUMBER_OID;
	extern const std::vector<byte> COUNTRY_NAME_OID;
	extern const std::vector<byte> LOCALITY_NAME_OID;
	extern const std::vector<byte> STATE_OR_PROVINCE_NAME_OID;
	extern const std::vector<byte> STREET_ADDRESS_OID;
	extern const std::vector<byte> ORGANIZATION_NAME_OID;
	extern const std::vector<byte> ORGANIZATION_UNIT_NAME_OID;
	extern const std::vector<byte> TITLE_OID;
	extern const std::vector<byte> DESCRIPTION_OID;
	extern const std::vector<byte> SEARCH_GUIDE_OID;
	extern const std::vector<byte> BUSINESS_CATEGORY_OID;
	extern const std::vector<byte> POSTAL_ADDRESS_OID;
	extern const std::vector<byte> POSTAL_CODE_OID;
	extern const std::vector<byte> POST_OFFICE_BOX_OID;
	extern const std::vector<byte> PHYSICAL_DELIVERY_OFFICE_NAME_OID;
	extern const std::vector<byte> TELEPHONE_NUMBER_OID;
	extern const std::vector<byte> TELEX_NUMBER_OID;
	extern const std::vector<byte> TELETEX_TERMINAL_IDENTIFIER_OID;
	extern const std::vector<byte> EMAIL_ADDRESS_OID;


	namespace EXT {
		/* Certificate Extensions */
		extern const std::string AUTHORITY_KEY_ID_STR_OID;
		extern const std::string SUBJECT_KEY_ID_STR_OID;
		extern const std::string KEY_USAGE_STR_OID;
		extern const std::string CERTIFICATE_POLICIES_STR_OID;
		extern const std::string POLICY_MAPPINGS_STR_OID;
		extern const std::string SUBJECT_ALT_NAME_STR_OID;
		extern const std::string ISSUER_ALT_NAME_STR_OID;
		extern const std::string SUBJECT_DIRECTORY_ATTRIBUTES_STR_OID;
		extern const std::string BASIC_CONSTRAINTS_STR_OID;
		extern const std::string NAME_CONSTRAINTS_STR_OID;
		extern const std::string POLICY_CONSTRAINTS_STR_OID;
		extern const std::string EXT_KEY_USAGE_SYNTAX_STR_OID;
		extern const std::string CRL_DISTRIBUTION_POINTS_STR_OID;
		extern const std::string INHIBIT_ANY_POLICY_STR_OID;
		extern const std::string FRESHEST_CRL_STR_OID;
		extern const std::string AUTHORITY_INFO_ACCESS_SYNTAX_STR_OID;
		extern const std::string SUBJECT_INFO_ACCESS_SYNTAX_STR_OID;

		/* Attribute certificates */
		extern const std::string UNP_STR_OID;
		extern const std::string UNPF_STR_OID;
		extern const std::string USER_DOCUMENT_DATA_STR_OID;
		extern const std::string UNP_STR_OID;
		extern const std::string UNPF_STR_OID ;
		extern const std::string BLANK_CARD_PKI_STR_OID;
		extern const std::string SURNAME_BEL_STR_OID;
		extern const std::string NAME_BEL_STR_OID;
		extern const std::string MIDDLE_NAME_STR_OID;
		extern const std::string WORK_AND_POS_STR_OID;
		extern const std::string WORK_UNIT_STR_OID;
		extern const std::string IDENTIFIER_ATT_CA_STR_OID;
		extern const std::string NAME_AND_MIDDLE_NAME_STR_OID;

		/* Certificate Extensions */
		extern const std::vector<byte> UNP_OID;
		extern const std::vector<byte> UNPF_OID;
		extern const std::vector<byte> AUTHORITY_KEY_ID_OID;
		extern const std::vector<byte> SUBJECT_KEY_ID_OID;
		extern const std::vector<byte> KEY_USAGE_OID;
		extern const std::vector<byte> CERTIFICATE_POLICIES_OID;
		extern const std::vector<byte> POLICY_MAPPINGS_OID;
		extern const std::vector<byte> SUBJECT_ALT_NAME_OID;
		extern const std::vector<byte> ISSUER_ALT_NAME_OID;
		extern const std::vector<byte> SUBJECT_DIRECTORY_ATTRIBUTES_OID;
		extern const std::vector<byte> BASIC_CONSTRAINTS_OID;
		extern const std::vector<byte> NAME_CONSTRAINTS_OID;
		extern const std::vector<byte> POLICY_CONSTRAINTS_OID;
		extern const std::vector<byte> EXT_KEY_USAGE_SYNTAX_OID;
		extern const std::vector<byte> CRL_DISTRIBUTION_POINTS_OID;
		extern const std::vector<byte> INHIBIT_ANY_POLICY_OID;
		extern const std::vector<byte> FRESHEST_CRL_OID;
		extern const std::vector<byte> AUTHORITY_INFO_ACCESS_SYNTAX_OID;
		extern const std::vector<byte> SUBJECT_INFO_ACCESS_SYNTAX_OID;
	}

	namespace CMS_OID
	{
		extern const std::vector<byte> SIGNED_DATA_OID;
	}
}


//std::map<std::vector<byte>, std::pair<std::string, std::string>> RDN_TABLE = {
//	{ OID::ORGANIZATION_NAME_OID, std::pair<std::string, std::string>("O","OrganizationName") }
//};
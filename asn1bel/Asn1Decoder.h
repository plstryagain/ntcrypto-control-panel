#ifndef __ASN1__DECODER__H__
#define __ASN1__DECODER__H__


#include "Asn1Types.h"

class Asn1Decoder
{
public:
	Asn1Decoder(const Asn1Decoder& other) = delete;
	static std::unique_ptr<Asn1Decoder> Create(const byte* data, const uint32 size, const ASN1_ENCODING encoding);
	static std::unique_ptr<Asn1Decoder> Create(const std::vector<byte>& data, const ASN1_ENCODING encoding);
	virtual ~Asn1Decoder() {}

public:
	ASN1_ERROR ParseAsnTree();
	ASN1_ERROR PrintAsnTree();
	const ASN1_TAG* GetTagByIndex(const uint32 idx);
	const ASN1_NODE * GetNodeByIndex(const uint32 idx) const;
	bool IsTagAsExpected(const uint32 idx, const ASN1_UNIVERSAL_TAG_NUMBER exp_tag) const;
	uint32 GetContextSpecificChoice(const uint32 idx) const;
	uint32 GetDeepness(const uint32 idx) const;
	ASN1_ERROR GetOctetString(const uint32 idx, OCTET_STRING* ostr) const;
	ASN1_ERROR GetInteger(const uint32 idx, INTEGER* integer) const;
	ASN1_ERROR GetOid(const uint32 idx, OBJECT_IDENTIFIER* oid) const;
	ASN1_ERROR GetAttributeValue(const uint32 idx, ATTRIBUTE_VALUE* av) const;
	ASN1_ERROR GetBitString(const uint32 idx, BIT_STRING* bs) const;
	ASN1_ERROR GetBmpString(const uint32 idx, BMP_STRING*bs) const;
	ASN1_ERROR GetRawContent(const uint32 idx, std::vector<byte>* content) const;
	ASN1_ERROR GetRawContent(const uint32 idx, byte ** content, uint32 * len) const;
	ASN1_ERROR GetGeneralizedTime(const uint32 idx, GENERALIZED_TIME* gt) const;
	ASN1_ERROR GetBoolean(const uint32 idx, ASNBOOLEAN* bl) const;
	std::string OidToStr(const OBJECT_IDENTIFIER& oid);

public:
	virtual ASN1_ERROR DecodeBoolean(uint32* idx, ASN1_NODE* node) = 0;
	//virtual ASN1_ERROR DecodeOctetString(uint32* idx, ASN1_NODE* node) = 0;
	virtual ASN1_ERROR DecodeOctetString(const uint32 idx, OCTET_STRING* ostring) = 0;
	virtual ASN1_ERROR DecodeInteger(const uint32 idx, INTEGER* integer) = 0;
	virtual ASN1_ERROR DecodeOid(const uint32 idx, OBJECT_IDENTIFIER* oid) = 0;
	virtual std::wstring DecodeBmpString(const std::vector<byte>& val) = 0;
	virtual std::wstring DecodePrintableString(const std::vector<byte>& val) = 0;
	virtual std::wstring DecodeIA5String(const std::vector<byte>& val) const = 0;
	virtual std::wstring DecodeUTF8String(const std::vector<byte>& val) const = 0;
	virtual std::string GeneralizedTimeToString(const GENERALIZED_TIME& gt) const = 0;

protected:
	Asn1Decoder() {};
	ASN1_ERROR GetTag(uint32* idx, ASN1_TAG* tag);
	byte GetBit(const byte b, const byte num);
	ASN1_ERROR GetByte(const uint32 idx, byte* b);
	ASN1_ERROR GetLength(uint32* idx, uint32* len);
	uint32 GetLength(const std::vector<byte>& val);
	std::string TagToString(const ASN1_OBJ_CLASS obj_class, const ASN1_UNIVERSAL_TAG_NUMBER tag,
		const uint32 choice);

protected:
	std::vector<byte> enc_data_;
	std::vector<ASN1_NODE> asn_tree_;
};

class Asn1DERDecoder : public Asn1Decoder
{
public:
	Asn1DERDecoder(const byte* data, const uint32 size);
	Asn1DERDecoder(const std::vector<byte>& data);
	~Asn1DERDecoder();

public:
	ASN1_ERROR DecodeBoolean(uint32* idx, ASN1_NODE* node) override;
	//ASN1_ERROR DecodeOctetString(uint32* idx, ASN1_NODE* node) override;
	ASN1_ERROR DecodeOctetString(const uint32 idx, OCTET_STRING* ostring) override;
	ASN1_ERROR DecodeInteger(const uint32 idx, INTEGER* integer) override;
	ASN1_ERROR DecodeOid(const uint32 idx, OBJECT_IDENTIFIER* oid) override;
	std::wstring DecodeBmpString(const std::vector<byte>& val) override;
	std::wstring DecodePrintableString(const std::vector<byte>& val) override;
	std::wstring DecodeIA5String(const std::vector<byte>& val) const override;
	std::wstring DecodeUTF8String(const std::vector<byte>& val) const override;
	std::string GeneralizedTimeToString(const GENERALIZED_TIME& gt) const override;

private:
	void ChangeByteOrder(const std::vector<byte>& one, std::vector<byte>* two);
};

#endif // __ASN1__DECODER__H__
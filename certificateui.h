#ifndef CERTIFICATEUI_H
#define CERTIFICATEUI_H

#include <QWidget>
#include <memory>

class ICertificateManager;
class QTreeWidgetItem;

namespace Ui {
class CertificateUI;
}

enum class CERT_UI_TYPE
{
    InvalidType = -1,
    X509Certificate = 0,
    AttributeCertificate = 1
};

class CertificateUI : public QWidget
{
    Q_OBJECT

public:
    CertificateUI(QWidget *parent, const CERT_UI_TYPE ct,
                           const std::unique_ptr<ICertificateManager>& cmgr);
    ~CertificateUI();

protected:
    void showEvent(QShowEvent *event) override;

private slots:
    void on_btn_ok_clicked();
    void on_tabw_about_cert_currentChanged(int index);
    void on_tw_composition_itemSelectionChanged();


    void on_tree_cert_chain_itemSelectionChanged();

    void on_btn_view_cert_clicked();

private:
    void FillCompositeTable();
    void FillCertificateChainTree();
    void FillAttributeCertCompositeTable();
    void FillX509CertCompositeTable();
    QString GetSimpleSubjectName(const void* pCert) const;

private:
    Ui::CertificateUI *ui;
    CERT_UI_TYPE ct_;
    const std::unique_ptr<ICertificateManager>& cmgr_;
    bool is_compos_table_already_fill_ = false;
    bool is_chain_already_build_ = false;
    const void* pChain_;
};

#endif // CERTIFICATEUI_H

#include "encryptoperationwizard.h"
#include "ui_encryptoperationwizard.h"
#include "Constants.h"
#include "selectcertwizard.h"
#include "icertificatemanager.h"
#include "dialogpleasewait.h"
#include "cryptooperationsmanager.h"
#include <Windows.h>
#include <QFileDialog>
#include <QToolTip>
#include <QFile>
#include <QMessageBox>
#include <QThread>
#include <QDebug>

EncryptOperationWizard::EncryptOperationWizard(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EncryptOperationWizard)
{
    ui->setupUi(this);
    ui->rb_enc->setChecked(true);
    ui->rb_cert_from_file->setChecked(true);

    ui->cb_alg->addItem("belt-ecb256", QVariant(CALG_BELT_ECB_256));
    ui->cb_alg->addItem("belt-cbc256", QVariant(CALG_BELT_CBC_256));
    ui->cb_alg->addItem("belt-cfb256", QVariant(CALG_BELT_CFB_256));
    ui->cb_alg->addItem("belt-ctr256", QVariant(CALG_BELT_CTR_256));

    ui->cb_impl->addItem("Программная", QVariant(static_cast<const uint>(Constants::SOFT_IMPL_FLAG)));
    ui->cb_impl->addItem("СКЗИ \"Сигма\"", QVariant(static_cast<const uint>(Constants::SIGMA_IMPL_FLAG)));
    ui->cb_impl->addItem("СКЗИ NTStore", QVariant(static_cast<const uint>(Constants::NTSTORE_IMPL_FLAG)));
    ui->cb_impl->setVisible(false);
    ui->lb_impl->setVisible(false);
    ui->btn_enc_dec->setIcon(QIcon(":/images/images/Lock_32px.png"));
}

EncryptOperationWizard::~EncryptOperationWizard()
{
    for(const auto& cert : certs_){
        CertFreeCertificateContext(cert);
    }
    delete ui;
}

void EncryptOperationWizard::on_rb_dec_toggled(bool checked)
{
    if(checked){
        ui->gb_cert->setEnabled(false);
        ui->cb_alg->setEnabled(false);
        ui->lb_alg->setEnabled(false);
        //ui->cb_impl->setEnabled(false);
        //ui->lb_impl->setEnabled(false);
        ui->chk_base64->setEnabled(false);
        ui->btn_enc_dec->setText("Расшифрование");
        ui->btn_enc_dec->setIcon(QIcon(":/images/images/Padlock_32px.png"));
        ui->lb_enc_dec_txt->setText("Файлы для\nрасшифрования");
    } else {
        ui->gb_cert->setEnabled(true);
        ui->cb_alg->setEnabled(true);
        ui->lb_alg->setEnabled(true);
        //ui->cb_impl->setEnabled(true);
        //ui->lb_impl->setEnabled(true);
        ui->chk_base64->setEnabled(true);
        ui->btn_enc_dec->setText("Зашифрование");
        ui->btn_enc_dec->setIcon(QIcon(":/images/images/Lock_32px.png"));
        ui->lb_enc_dec_txt->setText("Файлы для\nзашифрования");
    }
    int count = ui->lw_files->count();
    for(int i = 0; i < count; ++i){
        delete ui->lw_files->takeItem(0);
    }
    ui->ln_out_file->setText("");
}

void EncryptOperationWizard::on_btn_add_file_clicked()
{
    QString mask;
    QString caption;
    if(ui->rb_enc->isChecked()){
        mask = "(*.*)";
        caption = "Выберите файлы, которые необходимо зашифровать";
    } else {
        mask = "(*.enc);;(*.*)";
        caption = "Выберите файлы, которые необходимо расшифровать";
    }
    QStringList files = QFileDialog::getOpenFileNames(this,
                                                      caption,
                                                      "",
                                                      mask);
    if(files.isEmpty()){
        return;
    } else {
        for(const auto& file :files){
            ui->lw_files->addItem(file);
        }
    }
}

void EncryptOperationWizard::on_btn_del_file_clicked()
{
    int row = ui->lw_files->currentRow();
    if(row < 0){
        return;
    }
    delete ui->lw_files->takeItem(row);
}

void EncryptOperationWizard::on_btn_enc_dec_clicked()
{
    if(ui->lw_files->count() == 0){
        ShowEmptyLineNotification(ui->lw_files, "Выберите файлы");
        return;
    }
    if(ui->ln_out_file->text().isEmpty()){
        ShowEmptyLineNotification(ui->ln_out_file, "Выберите куда сохранить результат");
        return;
    }
    if(ui->rb_enc->isChecked() && (ui->lw_certs->count() == 0)){
        ShowEmptyLineNotification(ui->lw_certs, "Выберите сертификат шифрования");
        return;
    }
    report_.clear();
    int count = ui->lw_files->count();
    QStringList files;
    for(int i = 0; i < count; ++i){
        files.append(ui->lw_files->item(i)->text());
    }
    bool use_base64 = ui->chk_base64->isChecked();
    QString out_path = ui->ln_out_file->text();
    DialogPleaseWait dpw(this);
    dpw.SetAmountOfFiles(count);
    QThread* thread = new QThread();
    std::unique_ptr<CryptoOperationsManager> comgr =
            std::unique_ptr<CryptoOperationsManager>(new CryptoOperationsManager());
    if(ui->rb_enc->isChecked()){
        ALG_ID alg_id = ui->cb_alg->currentData().value<ALG_ID>();
        DWORD impl = ui->cb_impl->currentData().value<uint>();
        comgr->InitEncryptOperation(files, certs_, alg_id, impl, out_path, use_base64);
    } else {
        comgr->InitDecryptOperation(files, out_path);
    }
    comgr->moveToThread(thread);
    connect(comgr.get(), SIGNAL(signalFinish()), thread, SLOT(quit()));
    connect(comgr.get(), SIGNAL(signalReportError(QString)), this, SLOT(Error(QString)));
    if(ui->rb_enc->isChecked()){
        connect(thread, SIGNAL(started()), comgr.get(), SLOT(Encrypt()));
    } else {
        connect(thread, SIGNAL(started()), comgr.get(), SLOT(Decrypt()));
    }
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(comgr.get(), SIGNAL(signalFileProcessed()), &dpw, SLOT(ProgressUpdate()));
    connect(thread, SIGNAL(finished()), &dpw, SLOT(accept()));
    thread->start();
    dpw.exec();
    if(report_.isEmpty()){
        if(ui->rb_enc->isChecked()){
            QMessageBox::information(this, "Информация", "Все файлы успешно зашифрованы", QMessageBox::Ok);
        } else {
            QMessageBox::information(this, "Информация", "Все файлы успешно расшифрованы", QMessageBox::Ok);

        }
    } else {
        QMessageBox::warning(this, "Ошибка", report_, QMessageBox::Ok);
    }
}

void EncryptOperationWizard::ShowEmptyLineNotification(QWidget* w, const QString& text)
{
    w->setFocus();
    QToolTip::showText(w->mapToGlobal(QPoint()), text);
    return;
}

void EncryptOperationWizard::on_btn_out_file_clicked()
{
    QString path_to_save = QFileDialog::getExistingDirectory(this,
                                                            tr("Выберите куда вы хотите сохранить результат"),
                                                            "");
    if(!path_to_save.isEmpty()){
        ui->ln_out_file->setText(path_to_save);
    }
}

void EncryptOperationWizard::on_btn_add_cert_clicked()
{
    if(ui->rb_cert_from_store->isChecked()){
        SelectCertWizard scw(this, "MY", false);
        scw.show();
        QEventLoop loop;
        connect(&scw, SIGNAL(selected()), &loop, SLOT(quit()));
        loop.exec();
        const SystemCertificateManager* cert = dynamic_cast<const SystemCertificateManager*>(scw.GetSelectedCertificate());
        if(!cert){
            return;
        } else {
            ui->lw_certs->addItem(cert->GetCertSubjectSimpleName());
            PCCERT_CONTEXT pCert = CertDuplicateCertificateContext(static_cast<PCCERT_CONTEXT>(cert->GetRawContextPtr()));
            certs_.push_back(pCert);
        }
    } else {
        QString cert_name = QFileDialog::getOpenFileName(this, tr("Выберите файл сертификата"),
                                                         "",
                                                         "Сертификат (*.crt *.pem *.der *.cer);;Все файлы (*.*)");
        if(cert_name.isEmpty()){
            return;
        }
        QFile file(cert_name);
        if(!file.open(QFile::ReadOnly)){
           QMessageBox::critical(this, "Ошибка", "Не удалось открыть файл", QMessageBox::Ok);
           return;
        }
        QByteArray content = file.readAll();
        if(content.isEmpty()){
            QMessageBox::critical(this, "Ошибка", "Не удалось прочитать файл", QMessageBox::Ok);
            return;
        }
        if(content.contains("-----BEGIN CERTIFICATE-----"))
        {
            content = content.mid(28, content.size() - 53);
            content = QByteArray::fromBase64(content);
        }
        PCCERT_CONTEXT cert = CertCreateCertificateContext(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
                                             reinterpret_cast<BYTE*>(content.data()),
                                             static_cast<DWORD>(content.size()));
        if(!cert){
            QMessageBox::critical(this, "Ошибка", "Не удалось создать контекст сертификата", QMessageBox::Ok);
            return;
        }
        certs_.push_back(cert);
        auto cmgr = ICertificateManager::Create(CERTIFICATE_TYPE::SystemCertificate, cert, false);
        ui->lw_certs->addItem(dynamic_cast<SystemCertificateManager*>(cmgr.get())->GetCertSubjectSimpleName());
    }
}

void EncryptOperationWizard::on_btn_del_cert_clicked()
{
    int row = ui->lw_certs->currentRow();
    if(row < 0){
        return;
    }
    CertFreeCertificateContext(certs_.at(row));
    certs_.erase(certs_.begin() + row);
    delete ui->lw_certs->takeItem(row);
}

void EncryptOperationWizard::Error(QString error)
{
    report_.append(error+"\n");
}

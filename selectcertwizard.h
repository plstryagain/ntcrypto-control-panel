#ifndef SELECTCERTWIZARD_H
#define SELECTCERTWIZARD_H

#include <QWidget>
#include <memory>

class CertificateStoreManager;
class ICertificateManager;

namespace Ui {
class SelectCertWizard;
}

class SelectCertWizard : public QWidget
{
    Q_OBJECT

public:
    SelectCertWizard(QWidget *parent, const QString& cert_store_name, const bool show_only_with_key);
    ~SelectCertWizard();
    const ICertificateManager *GetSelectedCertificate() const;

protected:
    void SelectCertWizard::showEvent(QShowEvent *event) override;

signals:
    void selected();

private slots:
    void on_lw_certs_currentRowChanged(int currentRow);

    void on_btn_show_inf_clicked();

    void on_btn_cancel_clicked();

    void on_btn_ok_clicked();

private:
    Ui::SelectCertWizard *ui;
    QWidget* parent_;
    std::unique_ptr<CertificateStoreManager> csmgr_;
    int selected_row_;
    QString cert_store_name_;
    bool show_only_with_key_;
};

#endif // SELECTCERTWIZARD_H

#ifndef INTEGRITYCONTROL_H
#define INTEGRITYCONTROL_H

#include <QObject>
#include <string>

#define SERVICE_CONTROL_INTEGRITY_CHECK	129
#define SERVICE_CONTROL_CLOSE_SHARED_HANDLE 130

extern const QString key_iso_srvc_name;
extern const QString cng_key_iso_srvc_name;

extern const std::wstring shared_mem_name;
extern const std::wstring cng_shared_mem_name;


struct COMPARE_RESULT
{
    int ntcrypto = 0;
    int ntcrypt32 = 0;
    int ntcryptocore = 0;
    int ntcryptoext = 0;
    int ntkeyisosrvc = 0;
    int libnki = 0;
    int ntstore = 0;
    int ntcryptoui = 0;
    int ntcrypto64 = 0;
    int ntcrypt3264 = 0;
    int ntcryptocore64 = 0;
    int ntcryptoext64 = 0;
    int ntkeyisosrvc64 = 0;
    int libnki64 = 0;
    int ntstore64 = 0;
    int ntcryptoui64 = 0;
};


struct CNG_COMPARE_RESULT
{
    int ntcryptocng = 0;
    int ntcryptoksp = 0;
    int ntcryptocngext = 0;
    int ntcngkeyisosrvc = 0;
    int libnki = 0;
    int ntstore = 0;
    int ntcryptoui = 0;
    int ntcryptocng64 = 0;
    int ntcryptoksp64 = 0;
    int ntcryptocngext64 = 0;
    int ntcngkeyisosrvc64 = 0;
    int libnki64 = 0;
    int ntstore64 = 0;
    int ntcryptoui64 = 0;
};


class IntegrityControl : public QObject
{
    Q_OBJECT
public:
    explicit IntegrityControl(QObject *parent = 0);
    COMPARE_RESULT GetResult() const;
    CNG_COMPARE_RESULT GetCngResult() const;
    unsigned long GetStatus() const;

signals:
    void signalFinished();

public slots:
    void CheckIntegrity();

private:
    unsigned long SubmitRequestToSrvc(const QString& srvc_name, const wchar_t* mem_name, void* cr,
                                      const int cr_len);
    unsigned long ReadSharedMemory(const wchar_t* mem_name, void* cr, const int cr_len);

private:
    COMPARE_RESULT cr_;
    CNG_COMPARE_RESULT ccr_;
    unsigned long status_ = 0;
};

#endif // INTEGRITYCONTROL_H

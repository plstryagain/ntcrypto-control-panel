#include "cryptooperationsmanager.h"
#include "ntcrypto.h"
#include "Constants.h"
#include "icertificatemanager.h"
#include <QFile>
#include <QDirIterator>

CryptoOperationsManager::CryptoOperationsManager(QObject *parent) : QObject(parent)
{

}

CryptoOperationsManager::~CryptoOperationsManager()
{
    if(hStore_){
        CertCloseStore(hStore_, 0);
    }
}

void CryptoOperationsManager::InitCalculateHashOperation(const ALG_ID alg_id, const DWORD impl, const QString &path, bool is_dir)
{
    alg_id_ = alg_id;
    impl_ = impl;
    path_ = path;
    is_dir_ = is_dir;
    is_calc_hash_op_init_ = true;
}

void CryptoOperationsManager::InitCompareOperation(const ALG_ID alg_id, const DWORD impl,
                                                   const QString& path1, const QString& path2)
{
    alg_id_ = alg_id;
    impl_ = impl;
    path_ = path1;
    path2_ = path2;
    is_compare_op_init_ = true;
}

void CryptoOperationsManager::InitSigOperation(const ALG_ID alg_id, const QStringList& files_to_sig,
                                               PCCERT_CONTEXT cert, const QString& out_file_path,
                                               const bool is_detached, const bool use_base64)
{
    alg_id_ = alg_id;
    files_ = files_to_sig;
    cert_ = cert;
    out_file_path_ = out_file_path;
    is_detached_ = is_detached;
    use_base64_ = use_base64;
}

void CryptoOperationsManager::InitSigVerifyOperation(const QString& sig_file_name,
                                                     const QString& data_file_name,
                                                     const bool is_detached)
{
    is_detached_ = is_detached;
    sig_file_name_ = sig_file_name;
    data_file_name_ = data_file_name;
}

void CryptoOperationsManager::InitEncryptOperation(const QStringList& files,
                                                   const std::vector<PCCERT_CONTEXT> &certs,
                                                   const ALG_ID alg_id, const DWORD impl,
                                                   const QString& out_path, const bool use_base64)
{
    files_ = files;
    certs_ = certs;
    alg_id_ = alg_id;
    impl_ = impl;
    out_file_path_ = out_path;
    use_base64_ = use_base64;
}

void CryptoOperationsManager::InitDecryptOperation(const QStringList& files, const QString& out_path)
{
    files_ = files;
    out_file_path_ = out_path;
}

void CryptoOperationsManager::CalculateHash()
{
    if(!is_calc_hash_op_init_){
        emit signalFinish();
        return;
    }
    if(!is_dir_){
        QFile file(path_);
        if(!file.open(QFile::ReadOnly)){
            error_ = "Не удалось открыть файл \""+path_+"\"";
            emit signalReportError(error_);
            emit signalFinish();
            return;
        }
        QByteArray content = file.readAll();
        file.close();
        QByteArray out;
        if(!CalculateHash(alg_id_, impl_, content, &out)){
            emit signalReportError(error_);
            emit signalFinish();
            return;
        } else {
            emit signalCalculated(path_, out);
            emit signalFileProcessed();
        }
    } else {
        QDirIterator it(path_, QDir::Files);
        while(it.hasNext()){
            QString fname = it.next();
            QFile file(fname);
            if(!file.open(QFile::ReadOnly)){
                error_ = "Не удалось открыть файл \""+fname+"\"";
                emit signalReportError(error_);
                emit signalFinish();
                return;
            }
            QByteArray content = file.readAll();
            file.close();
            QByteArray out;
            if(!CalculateHash(alg_id_, impl_, content, &out)){
                emit signalReportError(error_);
                emit signalFinish();
                return;
            } else {
                emit signalCalculated(fname, out);
                emit signalFileProcessed();
            }
        }
    }
    emit signalFinish();
}

void CryptoOperationsManager::CompareFiles()
{
    if(!is_compare_op_init_){
        emit signalFinish();
        return;
    }
    QFile file1(path_);
    if(!file1.open(QFile::ReadOnly)){
        error_ = "Не удалось открыть файл \""+path_+"\"";
        emit signalReportError(error_);
        emit signalFinish();
        return;
    }
    QByteArray content1 = file1.readAll();
    file1.close();
    QFile file2(path2_);
    if(!file2.open(QFile::ReadOnly)){
        error_ = "Не удалось открыть файл \""+path2_+"\"";
        emit signalReportError(error_);
        emit signalFinish();
        return;
    }
    QByteArray content2 = file2.readAll();
    file2.close();
    QByteArray out1, out2;
    if(!CalculateHash(alg_id_, impl_, content1, &out1)){
        emit signalReportError(error_);
        emit signalFinish();
        return;
    }
    if(!CalculateHash(alg_id_, impl_, content2, &out2)){
        emit signalReportError(error_);
        emit signalFinish();
        return;
    }
    QString result;
    if(out1 != out2){
        result = "Файлы не совпадают!\n";
        result.append("Хэш-значение файла "+path_+": "+QString(out1.toHex())+"\n\r");
        result.append("Хэш-значение файла "+path2_+": "+QString(out2.toHex())+"\n\r");
    } else {
        result = "Файлы одинаковы!\n";
        result.append("Хэш-значение: "+QString(out1.toHex())+"\n");
    }
    emit signalCompared(result);
    emit signalFinish();
    return;
}

bool CryptoOperationsManager::CalculateHash(const ALG_ID alg_id, const DWORD impl, const QByteArray& content, QByteArray* out)
{
    NTCryptoManager mgr("", Constants::BIGN_CSP_TYPE, CRYPT_VERIFYCONTEXT | impl);
    if(!mgr.Init()){
        error_ = "Не удалось проинициализировать криптопровайдер, код ошибки: "+
                QString::number(GetLastError());
        return false;
    }
    DWORD out_len = 0;
    if(!mgr.CalculateHash(alg_id, reinterpret_cast<const BYTE*>(content.data()),
                          content.size(), nullptr, &out_len)){
        error_ = "Не удалось вычислить хэш-значение, код ошибки: "+QString::number(GetLastError());
        return false;
    }
    out->resize(out_len);
    if(!mgr.CalculateHash(alg_id, reinterpret_cast<const BYTE*>(content.data()), content.size(),
                          reinterpret_cast<BYTE*>(out->data()), &out_len)){
        error_ = "Не удалось вычислить хэш-значение, код ошибки: "+QString::number(GetLastError());
        return false;
    }
    return true;
}

void CryptoOperationsManager::MakeSignature()
{
    QString error = "";
    PCCRYPT_OID_INFO hoid = CryptFindOIDInfo(CRYPT_OID_INFO_ALGID_KEY, &alg_id_, 0);
    if(!hoid){
        error = "Не удалось получить OID алгоритма хэширования";
        emit signalReportError(error);
        emit signalFinish();
        return;
    }
    for(const auto& file_name : files_){
        QFile file(file_name);
        if(!file.open(QFile::ReadOnly)){
            error = file_name + ": не удалось открыть файл";
            emit signalReportError(error);
            continue;
        }
        QByteArray content = file.readAll();
        file.close();
        FILETIME cur_time;
        GetSystemTimeAsFileTime(&cur_time);
        DWORD etime_len = 0;
        if(!CryptEncodeObjectEx(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, szOID_RSA_signingTime, &cur_time, 0,
                                nullptr, nullptr, &etime_len)){
            error = file_name + ": операция завершилась неудачей, код ошибки " + QString::number(GetLastError());
            emit signalReportError(error);
            continue;
        }
        std::vector<BYTE> etime(etime_len);
        if(!CryptEncodeObjectEx(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, szOID_RSA_signingTime, &cur_time, 0,
                                nullptr, etime.data(), &etime_len)){
            error = file_name + ": операция завершилась неудачей, код ошибки " + QString::number(GetLastError());
            emit signalReportError(error);
            continue;
        }
        CRYPT_DATA_BLOB time_blob = { etime_len, etime.data() };
        std::vector<CRYPT_ATTRIBUTE> att = {
          { (LPSTR)szOID_RSA_signingTime, 1, &time_blob }
        };
        CRYPT_SIGN_MESSAGE_PARA sparam = { 0 };
        sparam.cbSize = sizeof(CRYPT_SIGN_MESSAGE_PARA);
        sparam.dwMsgEncodingType = X509_ASN_ENCODING | PKCS_7_ASN_ENCODING;
        sparam.pSigningCert = cert_;
        sparam.HashAlgorithm.pszObjId = const_cast<LPSTR>(hoid->pszOID);
        sparam.HashAlgorithm.Parameters.cbData = 0;
        sparam.rgpMsgCert = &cert_;
        sparam.cMsgCert = 1;
        sparam.cAuthAttr = static_cast<DWORD>(att.size());
        sparam.rgAuthAttr = att.data();
        const BYTE* mess[] = {
            reinterpret_cast<BYTE*>(content.data())
        };
        DWORD mess_len[] = {
            static_cast<DWORD>(content.size())
        };
        DWORD sig_len = 0;
        if(!CryptSignMessage(&sparam, is_detached_, 1, mess, mess_len, nullptr, &sig_len)){
            error = file_name + ": операция завершилась неудачей, код ошибки " + QString::number(GetLastError());
            emit signalReportError(error);
            continue;
        }
        std::vector<BYTE> sig(sig_len);
        if(!CryptSignMessage(&sparam, is_detached_, 1, mess, mess_len, sig.data(), &sig_len)){
            error = file_name + ": операция завершилась неудачей, код ошибки " + QString::number(GetLastError());
            emit signalReportError(error);
            continue;
        }
        QFileInfo fi(file_name);
        QFile ofile(out_file_path_ + QString("\\") + fi.fileName() + QString(".sig"));
        if(!ofile.open(QFile::WriteOnly)){
            error = file_name + ": не удалось сохранить файл подписи";
            emit signalReportError(error);
            continue;
        }
        QByteArray ba = QByteArray::fromRawData(reinterpret_cast<char*>(sig.data()), sig.size());
        if(use_base64_){
            ba = ba.toBase64();
        }
        ofile.write(ba);
        ofile.close();
        emit signalFileProcessed();
    }
    emit signalFinish();
    return;
}

void CryptoOperationsManager::VerifySignature()
{
    HCRYPTPROV hProv = NULL;
    HCERTSTORE hStore = NULL;
    QString error = "";
    QString sig_time = "";
    QString signer = "";
    HCRYPTMSG hMsg = NULL;
    DWORD plen = 0;
    std::vector<BYTE> param;
    std::vector<BYTE> cert_id;
    DATA_BLOB dbstore = { 0 };
    QByteArray ba_sig = "";
    CRYPT_ATTRIBUTES* atts = nullptr;
    PCCERT_CONTEXT pCert = nullptr;
    CRYPT_VERIFY_MESSAGE_PARA vparam = { 0 };
    bool is_valid = false;
    QFile file(sig_file_name_);
    std::unique_ptr<ICertificateManager> cmgr;
    QString cert_status;
    if(!file.open(QFile::ReadOnly)){
        error = sig_file_name_ + ": не удалось открыть файл";
        emit signalReportError(error);
        goto Exit;
    }
    ba_sig = file.readAll();
    if(ba_sig.isEmpty()){
        file.close();;
        error = sig_file_name_ + ": не удалось прочитать данные";
        emit signalReportError(error);
        goto Exit;
    }
    if(IsBase64(QString(ba_sig))){
        ba_sig = ba_sig.fromBase64(ba_sig);
    }
    file.close();

    hMsg = CryptMsgOpenToDecode(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, 0, 0, 0, nullptr, nullptr);
    if(!hMsg){
        error = "Операция завершилсь неудачей, код ошибки: "+QString::number(GetLastError());
        emit signalReportError(error);
        goto Exit;
    }

    if(!CryptMsgUpdate(hMsg, reinterpret_cast<BYTE*>(ba_sig.data()), ba_sig.size(), TRUE)){
        error = "Операция завершилсь неудачей, код ошибки: "+QString::number(GetLastError());
        emit signalReportError(error);
        goto Exit;
    }

    if(!CryptMsgGetParam(hMsg, CMSG_SIGNER_AUTH_ATTR_PARAM, 0, nullptr, &plen)){
        error = "Операция завершилсь неудачей, код ошибки: "+QString::number(GetLastError());
        emit signalReportError(error);
        goto Exit;
    }
    param.resize(plen);
    if(!CryptMsgGetParam(hMsg, CMSG_SIGNER_AUTH_ATTR_PARAM, 0, param.data(), &plen)){
        error = "Операция завершилсь неудачей, код ошибки: "+QString::number(GetLastError());
        emit signalReportError(error);
        goto Exit;
    }
    atts = reinterpret_cast<CRYPT_ATTRIBUTES*>(param.data());
    for(DWORD i = 0; i < atts->cAttr; ++i){
        QString oid(atts->rgAttr[i].pszObjId);
        if(oid == QString(szOID_RSA_signingTime)){
            DWORD err = DecodeSigningTime(atts->rgAttr[i].rgValue->pbData,
                                          atts->rgAttr[i].rgValue->cbData, &sig_time);
            if(err != ERROR_SUCCESS){
                error = "Операция завершилсь неудачей, код ошибки: "+QString::number(err);
                emit signalReportError(error);
                goto Exit;
            }
            break;
        }
    }

    if(!CryptMsgGetParam(hMsg, CMSG_SIGNER_CERT_ID_PARAM, 0, nullptr, &plen)){
        error = "Операция завершилсь неудачей, код ошибки: "+QString::number(GetLastError());
        emit signalReportError(error);
        goto Exit;
    }
    cert_id.resize(plen);
    if(!CryptMsgGetParam(hMsg, CMSG_SIGNER_CERT_ID_PARAM, 0, cert_id.data(), &plen)){
        error = "Операция завершилсь неудачей, код ошибки: "+QString::number(GetLastError());
        emit signalReportError(error);
        goto Exit;
    }

    dbstore.pbData = reinterpret_cast<BYTE*>(ba_sig.data());
    dbstore.cbData = static_cast<DWORD>(ba_sig.size());
    hStore = CertOpenStore(CERT_STORE_PROV_PKCS7, X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, NULL, 0,
                           reinterpret_cast<BYTE*>(&dbstore));
    if(!hStore){
        error = "Не удалось получить информацию о подписанте, код ошибки: "+QString::number(GetLastError());
        emit signalReportError(error);
        goto Exit;
    }

    pCert = CertFindCertificateInStore(hStore, X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, 0,
                                       CERT_FIND_CERT_ID, cert_id.data(), nullptr);
    if(!pCert){
        error = "Не удалось получить информацию о подписанте, код ошибки: "+QString::number(GetLastError());
        emit signalReportError(error);
        goto Exit;
    }
    DWORD err = DecodeSignerInfo(pCert, &signer);
    if(err != ERROR_SUCCESS){
        error = "Не удалось получить информацию о подписанте, код ошибки: "+QString::number(GetLastError());
        emit signalReportError(error);
        goto Exit;
    }

    cmgr = ICertificateManager::Create(CERTIFICATE_TYPE::SystemCertificate, pCert, false);
    if(!cmgr){
        error = "Не удалось построить цепочку сертификатов";
        emit signalReportError(error);
        goto Exit;
    }
    PCCERT_CHAIN_CONTEXT pChain = cmgr->GetCertificateChain();
    if(!pChain){
        error = "Не удалось построить цепочку сертификатов, код ошибки: "+QString::number(GetLastError());
        emit signalReportError(error);
        goto Exit;
    }
    cert_status = cmgr->CertTrustStatusToString(pChain->rgpChain[0]->TrustStatus);
    if(!CryptAcquireContext(&hProv, nullptr, nullptr, Constants::BIGN_CSP_TYPE, CRYPT_VERIFYCONTEXT)){
        error = "Операция завершилсь неудачей, код ошибки: "+QString::number(GetLastError());
        emit signalReportError(error);
        goto Exit;
    }
    vparam.cbSize = sizeof(CRYPT_VERIFY_MESSAGE_PARA);
    vparam.dwMsgAndCertEncodingType = X509_ASN_ENCODING | PKCS_7_ASN_ENCODING;
    vparam.hCryptProv = hProv;
    vparam.pfnGetSignerCertificate = nullptr;
    vparam.pvGetArg = nullptr;
    if(is_detached_){
        QFile dfile(data_file_name_);
        if(!dfile.open(QFile::ReadOnly)){
            error = data_file_name_ + ": не удалось открыть файл";
            emit signalReportError(error);
            goto Exit;
        }
        QByteArray ba = dfile.readAll();
        if(ba.isEmpty()){
            dfile.close();
            error = data_file_name_ + ": не удалось прочитать файл";
            emit signalReportError(error);
            goto Exit;
        }
        dfile.close();
        const BYTE* mess[] = {
            reinterpret_cast<BYTE*>(ba.data())
        };
        DWORD mess_len[] = {
            static_cast<DWORD>(ba.size())
        };
        if(!CryptVerifyDetachedMessageSignature(&vparam, 0, reinterpret_cast<BYTE*>(ba_sig.data()),
                                                static_cast<DWORD>(ba_sig.size()), 1,
                                                mess, mess_len, nullptr)){
            DWORD err = GetLastError();
            if(err == CRYPT_E_HASH_VALUE){
                error = "Документ не соответствует проверяемой подписи";
                emit signalReportError(error);
                goto Exit;
            }
            is_valid = false;
        } else {
            is_valid = true;
        }
    } else {
        if(!CryptVerifyMessageSignature(&vparam, 0, reinterpret_cast<BYTE*>(ba_sig.data()),
                                        static_cast<DWORD>(ba_sig.size()), nullptr, nullptr, nullptr)){
            is_valid = false;
        } else {
            is_valid = true;
        }
    }

    emit signalVerifyComplete(is_valid, signer, sig_time, cert_status, CertDuplicateCertificateContext(pCert));

Exit:
    if(pCert){
        CertFreeCertificateContext(pCert);
    }
    if(hStore){
        CertCloseStore(hStore, 0);
    }
    if(hProv){
        CryptReleaseContext(hProv, 0);
    }
    if(hMsg){
        CryptMsgClose(hMsg);
    }
    emit signalFinish();
    return;
}

void CryptoOperationsManager::Encrypt()
{
    QString error = "";
    PCCRYPT_OID_INFO pInfo = CryptFindOIDInfo(CRYPT_OID_INFO_ALGID_KEY, &alg_id_, CRYPT_ENCRYPT_ALG_OID_GROUP_ID);
    if(!pInfo){
        error = "Не удалось получить OID алгоритма шифрования";
        emit signalReportError(error);
        emit signalFinish();
        return;
    }
    CRYPT_ENCRYPT_MESSAGE_PARA eparam = { 0 };
    eparam.cbSize = sizeof(CRYPT_ENCRYPT_MESSAGE_PARA);
    eparam.ContentEncryptionAlgorithm.pszObjId = const_cast<LPSTR>(pInfo->pszOID);
    eparam.dwMsgEncodingType = X509_ASN_ENCODING | PKCS_7_ASN_ENCODING;
    for(const auto& file_name : files_){
        QFile file(file_name);
        if(!file.open(QFile::ReadOnly)){
            error = file_name + ": не удалось открыть файл";
            emit signalReportError(error);
            emit signalFileProcessed();
            continue;
        }
        QByteArray content = file.readAll();
        if(content.isEmpty()){
            error = file_name + ": не удалось прочитать файл";
            emit signalReportError(error);
            emit signalFileProcessed();
            continue;
        }
        file.close();
        DWORD elen = 0;
        if(!CryptEncryptMessage(&eparam, static_cast<DWORD>(certs_.size()), certs_.data(),
                                reinterpret_cast<BYTE*>(content.data()),
                                static_cast<DWORD>(content.size()), nullptr, &elen)){
            error = file_name + ": не удалось зашифровать файл, код ошибки " + QString::number(GetLastError());
            emit signalReportError(error);
            emit signalFileProcessed();
            continue;
        }
        std::vector<BYTE> enc(elen);
        if(!CryptEncryptMessage(&eparam, static_cast<DWORD>(certs_.size()), certs_.data(),
                                reinterpret_cast<BYTE*>(content.data()),
                                static_cast<DWORD>(content.size()), enc.data(), &elen)){
            error = file_name + ": не удалось зашифровать файл, код ошибки " + QString::number(GetLastError());
            emit signalReportError(error);
            emit signalFileProcessed();
            continue;
        }
        enc.resize(elen);

        QFileInfo fi(file_name);
        QFile ofile(out_file_path_ + QString("\\") + fi.fileName() + QString(".enc"));
        if(!ofile.open(QFile::ReadWrite)){
            error = file_name + ": не удалось сохранить зашифрованный файл";
            emit signalReportError(error);
            emit signalFileProcessed();
            continue;
        }
        QByteArray ba = QByteArray::fromRawData(reinterpret_cast<char*>(enc.data()), enc.size());
        if(use_base64_){
            ba = ba.toBase64();
        }
        if(ofile.write(ba) == -1){
            error = file_name + ": не удалось сохранить зашифрованный файл";
            emit signalReportError(error);
            emit signalFileProcessed();
            continue;
        }
        ofile.close();
        emit signalFileProcessed();
    }
    emit signalFinish();
    return;
}

void CryptoOperationsManager::Decrypt()
{
    QString error = "";
    hStore_ = CertOpenStore(CERT_STORE_PROV_SYSTEM,
                            0,
                            0,
                            CERT_SYSTEM_STORE_CURRENT_USER,
                            L"MY");
    if(!hStore_){
        error = "Не удалось получить доступ к хранилищу сертификатов";
        emit signalReportError(error);
        emit signalFinish();
        return;
    }
    CRYPT_DECRYPT_MESSAGE_PARA dparam = { 0 };
    dparam.cbSize = sizeof(CRYPT_DECRYPT_MESSAGE_PARA);
    dparam.dwMsgAndCertEncodingType = X509_ASN_ENCODING | PKCS_7_ASN_ENCODING;
    dparam.cCertStore = 1;
    dparam.rghCertStore = &hStore_;
    for(const auto& file_name : files_){
        QFile file(file_name);
        if(!file.open(QFile::ReadOnly)){
            error = file_name + ": не удалось открыть файл";
            emit signalReportError(error);
            emit signalFileProcessed();
            continue;
        }
        QByteArray content = file.readAll();
        if(content.isEmpty()){
            error = file_name + ": не удалось прочитать файл";
            emit signalReportError(error);
            emit signalFileProcessed();
            continue;
        }
        file.close();
        if(IsBase64(QString(content))){
           content = content.fromBase64(content);
        }
        DWORD dlen = 0;
        if(!CryptDecryptMessage(&dparam, reinterpret_cast<BYTE*>(content.data()),
                                static_cast<DWORD>(content.size()), nullptr, &dlen, nullptr)){
            error = file_name + ": не удалось расшифровать файл, код ошибки " + QString::number(GetLastError());
            emit signalReportError(error);
            emit signalFileProcessed();
            continue;
        }
        std::vector<BYTE> dec(dlen);
        if(!CryptDecryptMessage(&dparam, reinterpret_cast<BYTE*>(content.data()),
                                static_cast<DWORD>(content.size()), dec.data(), &dlen, nullptr)){
            error = file_name + ": не удалось расшифровать файл, код ошибки " + QString::number(GetLastError());
            emit signalReportError(error);
            emit signalFileProcessed();
            continue;
        }
        dec.resize(dlen);

        QFileInfo fi(file_name);
        QString new_name = fi.fileName();
        if(new_name.indexOf(".enc") != -1){
            new_name.chop(4);
        }
        QFile ofile(out_file_path_ + QString("\\") + new_name);
        if(!ofile.open(QFile::ReadWrite)){
            error = file_name + ": не удалось сохранить расшифрованный файл";
            emit signalReportError(error);
            emit signalFileProcessed();
            continue;
        }
        QByteArray ba = QByteArray::fromRawData(reinterpret_cast<char*>(dec.data()), dec.size());
        if(ofile.write(ba) == -1){
            error = file_name + ": не удалось сохранить расшифрованный файл";
            emit signalReportError(error);
            emit signalFileProcessed();
            continue;
        }
        ofile.close();
        emit signalFileProcessed();
    }
    emit signalFinish();
    return;
}


DWORD CryptoOperationsManager::DecodeSigningTime(const BYTE* aval, const DWORD alen, QString* sig_time)
{
    std::vector<BYTE> ft;
    DWORD size = 0;
    if(!CryptDecodeObjectEx(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, szOID_RSA_signingTime,
                            aval, alen, 0, nullptr, nullptr, &size)){
        return GetLastError();
    }
    ft .resize(size);
    if(!CryptDecodeObjectEx(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, szOID_RSA_signingTime,
                            aval, alen, 0, nullptr, ft.data(), &size)){
        return GetLastError();
    }
    FILETIME* pft = reinterpret_cast<FILETIME*>(ft.data());
    SYSTEMTIME st;
    FileTimeToSystemTime(pft, &st);
    *sig_time = QString("%1.%2.%3 %4:%5:%6").arg(st.wDay, 2, 10, QChar('0')).
                                                arg(st.wMonth, 2, 10, QChar('0')).
                                                arg(st.wYear, 2, 10, QChar('0')).
                                                arg(st.wHour, 2, 10, QChar('0')).
                                                arg(st.wMinute, 2, 10, QChar('0')).
                                                arg(st.wSecond, 2, 10, QChar('0'));
    return ERROR_SUCCESS;
}

DWORD CryptoOperationsManager::DecodeSignerInfo(PCCERT_CONTEXT pCert, QString* signer)
{
    QVector<wchar_t> name;
    DWORD name_size = CertGetNameString(pCert, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, nullptr, 0);
    if(name_size == 1){
        return GetLastError();
    }
    name.resize(name_size);
    name_size = CertGetNameString(pCert, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, name.data(), name_size);
    *signer = QString::fromWCharArray(name.data());
    return ERROR_SUCCESS;
}

bool CryptoOperationsManager::IsBase64(const QString& string)
{
    QRegExp rx("[^a-zA-Z0-9+/=]");
    if(rx.indexIn(string) == -1 && (string.length() % 4) == 0 && string.length() >= 4){
        return true;
    }
    return false;
}

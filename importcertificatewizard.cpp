#include "importcertificatewizard.h"
#include <QFile>
#include <QMessageBox>
#include <QCheckBox>
#include <QHeaderView>
#include <QEventLoop>
#include "Constants.h"
#include "keylinkmanager.h"
#include "icertificatemanager.h"
#include "certificatestoremanager.h"
#include "certificateui.h"
#include "crlui.h"
#include "asn1bel/CMSParser.h"

ImportCertificateWizard::ImportCertificateWizard(QWidget* parent, const QStringList& names)
    : parent_(parent), names_(names), vl_(this), btn_box_(QDialogButtonBox::Ok | QDialogButtonBox::Cancel),
      hPkcs7Store_(NULL)
{
    this->setWindowTitle(tr("Импорт сертификата"));
    this->setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowIcon(QIcon(":/images/images/Import_color_32px.png"));
    vl_.addWidget(&lb_descr_);
    vl_.addWidget(&tw_certs_);
    vl_.addWidget(&btn_box_);

    lb_descr_.setText(tr("Выберите объекты, которые необходимо импортировать"));
    btn_box_.button(QDialogButtonBox::Cancel)->setText(tr("Закрыть"));
    btn_box_.button(QDialogButtonBox::Cancel)->setIcon(QIcon(":/images/images/Delete_32px.png"));
    btn_box_.button(QDialogButtonBox::Ok)->setText(tr("Импортировать"));
    btn_box_.button(QDialogButtonBox::Ok)->setIcon(QIcon(":/images/images/Import_32px.png"));
    tw_certs_.setColumnCount(5);
    tw_certs_.setHorizontalHeaderItem(0, new QTableWidgetItem(tr("")));
    tw_certs_.setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Тип")));
    tw_certs_.setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Субъект")));
    tw_certs_.setHorizontalHeaderItem(3, new QTableWidgetItem(tr("Действителен с")));
    tw_certs_.setHorizontalHeaderItem(4, new QTableWidgetItem(tr("Действителен по")));
    QHeaderView* h = tw_certs_.horizontalHeader();
    h->setSectionResizeMode(0, QHeaderView::Fixed);
    h->resizeSection(0, 15);
    h->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    h->setSectionResizeMode(2, QHeaderView::Fixed);
    h->resizeSection(2, 200);
    h->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    h->setSectionResizeMode(4, QHeaderView::ResizeToContents);
    tw_certs_.setSelectionBehavior(QAbstractItemView::SelectRows);
    tw_certs_.setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    tw_certs_.setEditTriggers(QAbstractItemView::NoEditTriggers);

    connect(&btn_box_, SIGNAL(rejected()), this, SLOT(btn_cancel_clicked()));
    connect(&btn_box_, SIGNAL(accepted()), this, SLOT(btn_import_clicked()));
    connect(&tw_certs_, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(on_cert_double_clicked(QModelIndex)));
}

ImportCertificateWizard::~ImportCertificateWizard()
{
    for(const auto& obj : obj_list_){
        delete obj->mgr;
    }
    if(hPkcs7Store_){
        CertCloseStore(hPkcs7Store_, 0);
    }
}

void ImportCertificateWizard::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    QRect parentRect(parent_->mapToGlobal(QPoint(0, 0)), parent_->size());
    this->move(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), parentRect).topLeft());

    ParseCertFile();
    FillObjTable();
    tw_certs_.setFixedSize(tw_certs_.horizontalHeader()->length()+tw_certs_.verticalHeader()->width() + 12,
                           tw_certs_.verticalHeader()->length()+tw_certs_.horizontalHeader()->height());
    this->adjustSize();
    this->layout()->setSizeConstraint(QLayout::SetFixedSize);
}

void ImportCertificateWizard::ParseCertFile()
{
    for(const auto& name : names_){
        QFile file(name);
        if(!file.open(QFile::ReadOnly)){
            ShowErrorMessage("Не удалось открыть файл", GetLastError());
        }
        QByteArray content = file.readAll();
        if(name.indexOf(".pem") != -1){
            if(content.contains("-----BEGIN CERTIFICATE-----"))
            {
                content = content.mid(28, content.size() - 53);
                content = QByteArray::fromBase64(content);
            }
            ParseSingleCert(content);
        } else if(name.indexOf(".p7b") != -1){
            ParsePkcs7Cert(content);
        } else {
            ParseSingleCert(content);
        }
    }
}

void ImportCertificateWizard::ParseSingleCert(const QByteArray& content)
{
    PCCERT_CONTEXT pCert = CertCreateCertificateContext(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
                                                        reinterpret_cast<const BYTE*>(content.data()),
                                                        content.size());
    if(!pCert){
        return;
    }
    auto obj = std::unique_ptr<OBJECT_TO_BE_IMPORTED>(new OBJECT_TO_BE_IMPORTED());
    auto mgr = new SystemCertificateManager(pCert, true);
    obj->mgr = mgr;
    obj->type = OBJ_TYPE::Certificate;
    obj->subject = dynamic_cast<SystemCertificateManager*>(mgr)->GetCertSubjectSimpleName();
    obj->not_after = mgr->GetNotAfter();
    obj->not_before = mgr->GetNotBefore();
    obj->is_root = dynamic_cast<SystemCertificateManager*>(mgr)->IsRootCertificate();
    obj_list_.push_back(std::move(obj));
}

void ImportCertificateWizard::ParsePkcs7Cert(const QByteArray& content)
{
    PCCERT_CONTEXT pCert = nullptr;
    PCCRL_CONTEXT pCrl = nullptr;
    DATA_BLOB db ={
        static_cast<DWORD>(content.size()), const_cast<BYTE*>(reinterpret_cast<const BYTE*>(content.data()))
    };
    hPkcs7Store_ = CertOpenStore(CERT_STORE_PROV_PKCS7, X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, NULL, 0,
            reinterpret_cast<BYTE*>(&db));
    if(!hPkcs7Store_){
        ShowErrorMessage("Не удалось разобрать файл сертификата", GetLastError());
    }

    while(pCert = CertEnumCertificatesInStore(hPkcs7Store_, pCert)){
        auto obj = std::unique_ptr<OBJECT_TO_BE_IMPORTED>(new OBJECT_TO_BE_IMPORTED());
        auto mgr = new SystemCertificateManager(CertDuplicateCertificateContext(pCert), true);
        obj->mgr = mgr;
        obj->type = OBJ_TYPE::Certificate;
        obj->subject = dynamic_cast<SystemCertificateManager*>(mgr)->GetCertSubjectSimpleName();
        obj->not_after = mgr->GetNotAfter();
        obj->not_before = mgr->GetNotBefore();
        obj->is_root = dynamic_cast<SystemCertificateManager*>(mgr)->IsRootCertificate();
        obj_list_.push_back(std::move(obj));
    }

    while (pCrl = CertEnumCRLsInStore(hPkcs7Store_, pCrl)) {
        auto obj = std::unique_ptr<OBJECT_TO_BE_IMPORTED>(new OBJECT_TO_BE_IMPORTED());
        auto mgr = new CrlManager(CertDuplicateCRLContext(pCrl), true);
        obj->mgr = mgr;
        obj->type = OBJ_TYPE::CRL;
        obj->subject = mgr->GetCertIssuerSimpleName();
        obj->not_after = mgr->GetNotAfter();
        obj->not_before = mgr->GetNotBefore();
        obj->is_root = false;
        int res = CertVerifyCRLTimeValidity(NULL, pCrl->pCrlInfo);
        if(res == -1){
            obj->check.setEnabled(false);
            obj->check.setToolTip(tr("Срок действия СОС ещё не наступил"));
        } else if (res == 1) {
            obj->check.setEnabled(false);
            obj->check.setToolTip(tr("Срок действия СОС истёк"));
        }
        obj_list_.push_back(std::move(obj));
    }

    CmsParser cmsp(reinterpret_cast<const BYTE*>(content.data()), content.size());
    if(cmsp.Parse() != ASN1_ERROR::success){
        return;
    }

    auto attcerts = cmsp.GetAttributeCertificates();
    if(attcerts.empty()){
        return;
    }
    for(auto& cert : attcerts){
        auto obj = std::unique_ptr<OBJECT_TO_BE_IMPORTED>(new OBJECT_TO_BE_IMPORTED());
        auto mgr = new AttributeCertificateManager(cert);
        if(!mgr->Init()){
            continue;
        }
        obj->mgr = mgr;
        obj->type = OBJ_TYPE::AttributeCertificate;
        obj->subject = mgr->GetCertHolderSimpleName();
        obj->not_after = mgr->GetNotAfter();
        obj->not_before = mgr->GetNotBefore();
        obj->is_root = false;
        obj_list_.push_back(std::move(obj));
    }

    return;
}

void ImportCertificateWizard::FillObjTable()
{
    int i = 0;
    for(const auto& obj : obj_list_){
        tw_certs_.setRowCount(i + 1);
        obj->check.setContentsMargins(0, 0, 0, 0);
        tw_certs_.setCellWidget(i, 0, &obj->check);
        if(obj->type == OBJ_TYPE::Certificate){
            tw_certs_.setItem(i, 1, new QTableWidgetItem(tr("Сертификат")));
        } else if (obj->type == OBJ_TYPE::CRL){
            tw_certs_.setItem(i, 1, new QTableWidgetItem(tr("СОС")));
        } else if (obj->type == OBJ_TYPE::AttributeCertificate){
            tw_certs_.setItem(i, 1, new QTableWidgetItem(tr("Атрибутный\nсертификат")));
        } else {
            ShowErrorMessage("Не удалось создать контекст сертификата", GetLastError());
        }
        tw_certs_.setItem(i, 2, new QTableWidgetItem(obj->subject));
        tw_certs_.setItem(i, 3, new QTableWidgetItem(obj->not_before));
        tw_certs_.setItem(i, 4, new QTableWidgetItem(obj->not_after));
        ++i;
    }
}

void ImportCertificateWizard::btn_cancel_clicked()
{
    emit this->rejected();
    this->close();
}

void ImportCertificateWizard::btn_import_clicked()
{
    if(!IsAtLeastOneChecked()){
        QMessageBox::warning(this, tr("Внимание"), tr("Не выбран ни один из объектов для импорта"), QMessageBox::Ok);
        return;
    }
    for(const auto& obj : obj_list_){
        if(obj->check.isChecked()){
            if(obj->type == OBJ_TYPE::Certificate){
                CERTIFICATE_STORE_TYPE cstype = obj->is_root ? CERTIFICATE_STORE_TYPE::ROOT :
                                                               CERTIFICATE_STORE_TYPE::MY;
                SystemCertificateStoreManager scmgr(cstype);
                if(!scmgr.OpenStore()){
                    //ShowErrorMessage("Не удалось импортировать сертификаты", GetLastError());
                    AddToReport(obj, "Не удалось открыть хранилище сертификатов", GetLastError());
                    continue;
                }
                if(!obj->is_root){
                    while(true){
                        KeyLinkManager klmgr(static_cast<PCCERT_CONTEXT>(obj->mgr->GetRawContextPtr()), false);
                        DWORD err = klmgr.TryToLink();
                        if(err == 0xffffffff){
                            QMessageBox msg(this);
                            msg.setText(tr("Не найден ключ, соответствующий импортируемому личному сертификату.\n"
                                           "Пожалуйста, подключите носитель, содержащий соответствующий личный ключ "
                                           "и нажмите кнопку \"Повторить\".\n"
                                           "Чтобы импортировать сертификат без привязки к личному ключу нажмите кнопку \"Продолжить\"."));
                            msg.setIcon(QMessageBox::Warning);
                            QPushButton btn_retry(tr("Повторить"), &msg);
                            btn_retry.setIcon(QIcon(":/images/images/Refresh_32px.png"));
                            QPushButton btn_continue(tr("Продолжить"), &msg);
                            btn_continue.setIcon(QIcon(":/images/images/Right Arrow_32px.png"));
                            QPushButton btn_cancel(tr("Отмена"), &msg);
                            btn_cancel.setIcon(QIcon(":/images/images/Delete_32px.png"));
                            msg.addButton(&btn_retry, QMessageBox::AcceptRole);
                            msg.addButton(&btn_continue, QMessageBox::ActionRole);
                            msg.addButton(&btn_cancel, QMessageBox::RejectRole);
                            msg.exec();
                            if(msg.clickedButton() == &btn_cancel) {
                                emit this->rejected();
                                this->close();
                                return;
                            } else if (msg.clickedButton() == &btn_continue){
                                break;
                            }
                        } else  if (err == ERROR_SUCCESS){
                            break;
                        } else {
                            ShowErrorMessage("При выполнении операции произошла ошибка", err);
                            return;
                        }
                    }
                }
                DWORD err = scmgr.AddToStore(obj->mgr, CERT_STORE_ADD_NEW);
                if(err == CRYPT_E_EXISTS){
                    AddToReport(obj, "Такой сертификат уже существует",err);
                    continue;
                } else if (err == ERROR_ACCESS_DENIED){
                    AddToReport(obj, "Недостаточно прав для установки сертификата", err);
                    continue;
                } else if (err == ERROR_SUCCESS){
                    AddToReport(obj, "", ERROR_SUCCESS);
                } else {
                    AddToReport(obj, "", err);
                    continue;
                }
            } else if (obj->type == OBJ_TYPE::CRL){
                CrlStoreManager crlm;
                if(!crlm.OpenStore()){
                    AddToReport(obj, "Не удалось открыть хранилище сертификатов", GetLastError());
                    continue;
                }
                DWORD err = crlm.AddToStore(obj->mgr, CERT_STORE_ADD_NEW);
                if(err != ERROR_SUCCESS){
                    AddToReport(obj, "", err);
                    continue;
                } else {
                    AddToReport(obj, "", ERROR_SUCCESS);
                }
            } else if(obj->type == OBJ_TYPE::AttributeCertificate){
                AttributeCertificateStoreManager acsm;
                if(!acsm.OpenStore()){
                    AddToReport(obj, "Не удалось открыть хранилище сертификатов", GetLastError());
                    continue;
                }
                DWORD err = acsm.AddToStore(obj->mgr, CERT_STORE_ADD_NEW);
                if(err != ERROR_SUCCESS){
                    AddToReport(obj, "", err);
                    continue;
                } else {
                    AddToReport(obj, "", ERROR_SUCCESS);
                }
            } else {
                 AddToReport(obj, "", GetLastError());
                continue;
            }
        }
    }
    emit this->accept();
    this->close();
}

bool ImportCertificateWizard::IsAtLeastOneChecked()
{
    int count = 0;
    for (const auto& obj : obj_list_){
        if (obj->check.isChecked()){
            ++count;
            break;
        }
    }
    return (count != 0);
}

void ImportCertificateWizard::ShowErrorMessage(const QString& err, const DWORD err_num)
{
    QMessageBox::critical(this,
                          tr("Ошибка"),
                          err + tr(", код ошибки: ")+
                          QString::number(err_num),
                          QMessageBox::Ok);
    emit this->rejected();
    this->close();
}

void ImportCertificateWizard::AddToReport(const std::unique_ptr<OBJECT_TO_BE_IMPORTED>& obj, const QString& reason, const DWORD error)
{
    if(obj->type == OBJ_TYPE::Certificate){
        report_.append(tr("\nСертификат \""));
    } else if (obj->type == OBJ_TYPE::CRL){
        report_.append(tr("\nСОС \""));
    } else if (obj->type == OBJ_TYPE::AttributeCertificate){
        report_.append(tr("\nАтрибутный сертификат \""));
    }
    report_.append(obj->subject+"\"");
    if(error == ERROR_SUCCESS){
        report_.append(tr(" импортирован успешно\n\r"));
    } else {
        report_.append(tr(" не удалось импортировать, причина: ")+ reason +
                       "(код ошибки: "+QString::number(error) + ")\n\r");
    }
}

void ImportCertificateWizard::ShowReport()
{
    QMessageBox::information(this, tr("Информация"), report_, QMessageBox::Ok);
    emit this->accept();
    this->close();
    return;
}

void ImportCertificateWizard::on_cert_double_clicked(QModelIndex qmi)
{
    int idx = qmi.row();
    const auto& obj = obj_list_.at(idx);
    if(obj->type == OBJ_TYPE::Certificate){
        auto mgr = ICertificateManager::Create(CERTIFICATE_TYPE::SystemCertificate, obj->mgr->GetRawContextPtr(), false);
        CertificateUI* cui = new CertificateUI(this, CERT_UI_TYPE::X509Certificate, std::move(mgr));
        cui->show();
        QEventLoop loop;
        connect(cui, SIGNAL(destroyed()), &loop, SLOT(quit()));
        loop.exec();
    } else if(obj->type == OBJ_TYPE::CRL){
        CrlUI* crlui = new CrlUI(this, dynamic_cast<CrlManager*>(obj->mgr));
        crlui->show();
        QEventLoop loop;
        connect(crlui, SIGNAL(destroyed()), &loop, SLOT(quit()));
        loop.exec();
    } else if (obj->type == OBJ_TYPE::AttributeCertificate){
        const void* ptr = obj->mgr->GetRawContextPtr();
        auto mgr = ICertificateManager::Create(CERTIFICATE_TYPE::AttributeCertificate, ptr, false);
        CertificateUI* cui = new CertificateUI(this, CERT_UI_TYPE::AttributeCertificate, std::move(mgr));
        cui->show();
        QEventLoop loop;
        connect(cui, SIGNAL(destroyed()), &loop, SLOT(quit()));
        loop.exec();
    }
}

#ifndef __CNG__MANAGER__H__
#define __CNG__MANAGER__H__

#ifndef _WIN_XP_

#include "cngmanager.h"
#include <QMap>
#pragma comment(lib, "ncrypt.lib")

namespace CNG_CONSTANTS
{
    extern const std::wstring KSP_NAME = L"NTCrypto Key Storage Provider";
    extern const std::wstring PRIM_PROV_NAME = L"NTCrypto CNG Primitive Provider";

    const QMap<QPair<DWORD, DWORD>, std::wstring> algorithms_id =
    {
        {QPair<DWORD, DWORD>(322, 256), L"bign-curve256"},
        {QPair<DWORD, DWORD>(322, 384), L"bign-curve384"},
        {QPair<DWORD, DWORD>(322, 512), L"bign-curve512"},
    };
}

CngManager::CngManager()
    : hProv_(0), hKey_(0)
{

}

CngManager::~CngManager()
{
    if(hKey_)
    {
        NCryptFreeObject(hKey_);
    }

    if(hProv_)
    {
        NCryptFreeObject(hProv_);
    }
}

SECURITY_STATUS CngManager::Init()
{
    if(hProv_)
    {
        return NTE_INVALID_HANDLE;
    }

    return NCryptOpenStorageProvider(&hProv_, CNG_CONSTANTS::KSP_NAME.data(), 0);
}

SECURITY_STATUS CngManager::OpenKey(const QString& key_name, const DWORD flags)
{
    if(!hProv_ || hKey_)
    {
        return NTE_INVALID_HANDLE;
    }
    std::wstring w_key_name = key_name.toStdWString().data();
    return NCryptOpenKey(hProv_, &hKey_, w_key_name.data(), 0, flags);
}

SECURITY_STATUS CngManager::GenerateKeyPair(const std::wstring& alg_id, const QString& key_name,
                                            const DWORD flags)
{
    if(!hProv_ || hKey_)
    {
        return NTE_INVALID_HANDLE;
    }

    std::wstring w_key_name = key_name.toStdWString().data();
    SECURITY_STATUS status = NCryptCreatePersistedKey(hProv_, &hKey_, alg_id.data(),
                                                      w_key_name.data(), 0, flags);
    if(status != ERROR_SUCCESS)
    {
        return status;
    }

    return NCryptFinalizeKey(hKey_, 0);
}

#endif // _WIN_XP_

#endif // __CNG__MANAGER__H__

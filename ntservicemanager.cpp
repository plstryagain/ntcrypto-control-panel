#include "ntservicemanager.h"


NTServiceManager::NTServiceManager()
    : hSCManager_(0), hService_(0)
{

}

NTServiceManager::~NTServiceManager()
{
    if(hService_)
    {
        CloseServiceHandle(hService_);
    }

    if(hSCManager_)
    {
        CloseServiceHandle(hSCManager_);
    }
}

DWORD NTServiceManager::OpenWinSCManager(const DWORD desired_access)
{
    hSCManager_ = OpenSCManager(nullptr, nullptr, desired_access);
    return hSCManager_ ? ERROR_SUCCESS : GetLastError();
}

DWORD NTServiceManager::OpenNTService(const QString &srvc_name, const DWORD desired_access)
{
    hService_ = OpenService(hSCManager_, srvc_name.toStdWString().data(), desired_access);
    return hService_ ? ERROR_SUCCESS : GetLastError();
}

DWORD NTServiceManager::GetServiceState()
{
    SERVICE_STATUS_PROCESS srv_stat_proc = {0};
    DWORD bytes_needed = 0;

    if(!QueryServiceStatusEx(hService_,
                             SC_STATUS_PROCESS_INFO,
                             reinterpret_cast<BYTE*>(&srv_stat_proc),
                             sizeof(srv_stat_proc),
                             &bytes_needed))
    {
        return NTE_FAIL;
    }
    else
    {
        return srv_stat_proc.dwCurrentState;
    }
}

DWORD NTServiceManager::ControlNTService(const DWORD ctrl)
{
    SERVICE_STATUS srv_stat = {0};
    if(!ControlService(hService_, ctrl, &srv_stat))
    {
        return NTE_FAIL;
    }
    else
    {
        return srv_stat.dwCurrentState;
    }
}

DWORD NTServiceManager::StartNTService()
{
    return StartService(hService_, 0, nullptr) ? ERROR_SUCCESS : NTE_FAIL;
}


#include "certificaterequestwizard.h"
#include "ui_certificaterequestwizard.h"
#include "registry.h"
#include "genkeywizard.h"
#include "dialogpleasewait.h"
#include "certificaterequestmanager.h"
#include "Constants.h"
#include <QMessageBox>
#include <QFile>
#include <QFileDialog>
#include <QToolTip>
#include <QDebug>
#include <QThread>
#include <vector>
#include <QVector>
#include <memory>
#include "wordautomation.h"



//const std::vector<BYTE> fiz_policy = {
//    0x2A, 0x70, 0x01, 0x02, 0x01, 0x01, 0x01,  0x03, 0x01
//};

LPSTR fiz_policy = (LPSTR)"1.2.112.1.2.1.1.1.3.1";



CertificateRequestWizard::CertificateRequestWizard(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CertificateRequestWizard)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setWindowTitle(tr("Формирование заявки на сертификат"));

    ui->rb_exist_key->setChecked(true);

    ui->cb_cert_req_type->addItem("Сертификат физического лица");
    ui->cb_cert_req_type->addItem("Сертификат юридического лица");
    ui->cb_cert_req_type->addItem("Атрибутный сертификат");

    ui->ln_i_country->setText("BY");
    ui->ln_i_country->setReadOnly(true);
    ui->ln_e_country->setText("BY");
    ui->ln_e_country->setReadOnly(true);
    QRegExp lat_rx("[A-z\\s]+");
    QRegExpValidator* lat_v = new QRegExpValidator(lat_rx);
    ui->ln_i_common_name->setValidator(lat_v);
    QRegExp num_rx("[0-9]+");
    QRegExpValidator* num_v = new QRegExpValidator(num_rx);
    ui->ln_e_unp->setValidator(num_v);

    ui->chk_test_cert->setVisible(false);
    connect(ui->rb_exist_key, SIGNAL(toggled(bool)), ui->cb_keys, SLOT(setEnabled(bool)));
}

CertificateRequestWizard::~CertificateRequestWizard()
{
    delete ui;
}

void CertificateRequestWizard::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    EnumKeys();
}

void CertificateRequestWizard::EnumKeys()
{
    ui->cb_keys->clear();
    RegistryManager regmgr;
    NTSTATUS status = regmgr.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);
    if(status != ERROR_SUCCESS)
    {
        return;
    }

    status = regmgr.OpenKey(RegConstants::PROVIDER_KEYS_REG_PATH,
                            KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);
    if(status != ERROR_SUCCESS)
    {
        return;
    }

    DWORD index = 0;

    while(status == ERROR_SUCCESS)
    {
        QVector<wchar_t> name(MAX_PATH);
        DWORD name_len = MAX_PATH;
        status = regmgr.EnumKeyEx(index, &name, &name_len);
        if(status == ERROR_SUCCESS)
        {
            ui->cb_keys->addItem(QString::fromWCharArray(name.data()));
            ++index;
        }
    }
}


void CertificateRequestWizard::on_btn_cancel_clicked()
{
    this->close();
}

void CertificateRequestWizard::on_btn_ok_clicked()
{
    if (ui->cb_cert_req_type->currentIndex() == static_cast<int>(CERT_REQ_TYPE::AC))
    {
        this->setDisabled(true);
        FillAttributeCertTemplate();
        this->setEnabled(true);
        return;
    }
    if(ui->chk_test_cert->isChecked())
    {
        return GenTestCert();
    }
    QString subject = ConstructSubjectName();
    if(subject.isEmpty()){
        return;
    }

    QString key_name;
    if(ui->rb_new_key->isChecked())
    {
        GenKeyWizard gkw(false);
        gkw.Init();
        if(gkw.exec() == QDialog::Rejected)
        {
            return;
        }
        key_name = gkw.GetKeyName();
    }
    else
    {
        key_name = ui->cb_keys->currentText();
    }
    qRegisterMetaType<QVector<unsigned char>>("QVector<unsigned char>");
    DialogPleaseWait dpw(this);
    QThread* thread = new QThread();
    auto reqmgr = std::unique_ptr<CertificateRequestManager>(new CertificateRequestManager());
    reqmgr->Init(key_name, subject, ui->cb_cert_req_type->currentIndex());
    reqmgr->moveToThread(thread);
    connect(reqmgr.get(), SIGNAL(signalFinished()), thread, SLOT(quit()));
    connect(reqmgr.get(), SIGNAL(reportError(ulong)), this, SLOT(ShowError(ulong)));
    connect(reqmgr.get(), SIGNAL(operationComplete(QVector<unsigned char>)), this,
            SLOT(SaveRequest(QVector<unsigned char>)));
    connect(thread, SIGNAL(started()), reqmgr.get(), SLOT(SignRequest()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), &dpw, SLOT(accept()));
    thread->start();
    dpw.exec();

    return;
}



void CertificateRequestWizard::ShowError(unsigned long error)
{
    QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось сформировать заявку на сертификат")
                          + tr(", код ошибки: ") + QString::number(error),
                          QMessageBox::Ok);
}

void CertificateRequestWizard::SaveRequest(QVector<unsigned char> request)
{
    QString path = QFileDialog::getSaveFileName(this, tr("Сохранить файл запроса"), "",
                                                tr("Запрос на сертификат (*.req)"));
    QFile req_file(path);
    if(!req_file.open(QIODevice::ReadWrite))
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось сохранить файл заявки"),
                              QMessageBox::Ok);
        return;
    }

    if(req_file.write(reinterpret_cast<char*>(request.data()), request.size()) == -1)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось сохранить файл заявки"),
                              QMessageBox::Ok);
        req_file.close();
        return;
    }

    QMessageBox::information(this, tr("Информация"), tr("Заявка успешно сформирована"), QMessageBox::Ok);

    req_file.close();
    this->close();
    return;
}

QString CertificateRequestWizard::ConstructSubjectName()
{
    QString subject;
    auto subject_append = [&](const char* oid, QLineEdit* ln, const bool is_required) -> bool {
        if(is_required && ln->text().isEmpty()){
            ShowEmptyLineNotification(ln, "Заполните это поле");
            return false;
        }
      subject.append(QString(oid) + "=" + ln->text() + "\r");
      return true;
    };
    if(ui->cb_cert_req_type->currentIndex() == static_cast<int>(CERT_REQ_TYPE::INDIVIDUAL)){
        if(!subject_append(szOID_COUNTRY_NAME, ui->ln_i_country, true)){
            return "";
        }
        if(!subject_append(szOID_COMMON_NAME, ui->ln_i_common_name, true)){
            return "";
        }
        if(ui->ln_i_serial_num->text().size() < 14){
            ShowEmptyLineNotification(ui->ln_i_serial_num, "Поле должно содержать не менее 14 символов");
            return "";
        }
        if(!subject_append(szOID_DEVICE_SERIAL_NUMBER, ui->ln_i_serial_num, true)){
            return "";
        }
        if(!subject_append(szOID_SUR_NAME, ui->ln_i_surname, true)){
            return "";
        }
        if(!subject_append("2.5.4.41", ui->ln_i_name, true)){
            return "";
        }
        if(!subject_append(szOID_GIVEN_NAME, ui->ln_i_given_name, true)){
            return "";
        }
        subject_append(szOID_RSA_emailAddr, ui->ln_i_email, false);
        return subject;
    } else if (ui->cb_cert_req_type->currentIndex() == static_cast<int>(CERT_REQ_TYPE::ENTITY)){
        if(!subject_append(szOID_COUNTRY_NAME, ui->ln_i_country, true)){
            return "";
        }
        if(!subject_append(szOID_COMMON_NAME, ui->ln_e_common_name, true)){
            return "";
        }
        subject.append(QString(szOID_ORGANIZATION_NAME) + "=" + ui->ln_e_common_name->text() + "\r");
        if(!subject_append(szOID_STATE_OR_PROVINCE_NAME, ui->ln_e_locality, true)){
            return "";
        }
        if(!subject_append(szOID_LOCALITY_NAME, ui->ln_e_city, true)){
            return "";
        }
        if(!subject_append(szOID_STREET_ADDRESS, ui->ln_e_address, true)){
            return "";
        }
        if(!subject_append(szOID_SUR_NAME, ui->ln_e_surname, true)){
            return "";
        }
        if(!subject_append("2.5.4.41", ui->ln_e_name, true)){
            return "";
        }
        if(ui->ln_e_serial_num->text().size() < 14){
            ShowEmptyLineNotification(ui->ln_e_serial_num, "Поле должно содержать не менее 14 символов");
            return "";
        }
        if(!subject_append(szOID_DEVICE_SERIAL_NUMBER, ui->ln_e_serial_num, true)){
            return "";
        }
        if(ui->ln_e_unp->text().size() < 9){
            ShowEmptyLineNotification(ui->ln_e_unp, "Поле должно содержать не менее 9 цифр");
            return "";
        }
        if(!subject_append("1.2.112.1.2.1.1.1.1.2", ui->ln_e_unp, true)){
            return "";
        }
        if(!subject_append(szOID_TITLE, ui->ln_e_post, true)){
            return "";
        }
        if(!subject_append("1.2.112.1.2.1.1.5.2", ui->ln_e_ou, true)){
            return "";
        }
        subject_append(szOID_RSA_emailAddr, ui->ln_e_email, false);
        return subject;
    } else {
        return subject;
    }
    return subject;
}

QStringList CertificateRequestWizard::ConstructACInfo()
{
    QStringList info;
    auto info_append = [&](QLineEdit* ln) -> bool {
        if(ln->text().isEmpty()){
            ShowEmptyLineNotification(ln, "Заполните это поле");
            return false;
        }
        info.append(ln->text());
        return true;
    };
    if(!info_append(ui->ln_ac_org)){
       return QStringList();
    }
    if(!info_append(ui->ln_ac_ou)){
        return QStringList();
    }
    if(!info_append(ui->ln_ac_unp)){
        return QStringList();
    }
    if(!info_append(ui->ln_ac_post)){
        return QStringList();
    }
    if(!info_append(ui->ln_ac_obl)){
        return QStringList();
    }
    if(!info_append(ui->ln_ac_city)){
        return QStringList();
    }
    if(!info_append(ui->ln_ac_yur_adr)){
        return QStringList();
    }
    if(!info_append(ui->ln_ac_unpf)){
        return QStringList();
    }
    if(!info_append(ui->ln_ac_pkc_id)){
        return QStringList();
    }
    if(!info_append(ui->ln_ac_gis_id)){
        return QStringList();
    }
    if(!info_append(ui->ln_ac_tel)){
        return QStringList();
    }
    return info;
}

void CertificateRequestWizard::GenTestCert()
{
    QString key_name;
    if(ui->rb_new_key->isChecked())
    {
        GenKeyWizard gkw(false);
        gkw.Init();
        if(gkw.exec() == QDialog::Rejected)
        {
            return;
        }
        key_name = gkw.GetKeyName();
    }
    else
    {
        key_name = ui->cb_keys->currentText();
    }
    QFile file;
    QString path;
    DWORD error = NTE_FAIL;
    HCRYPTPROV hProv = NULL;
    CRYPT_ALGORITHM_IDENTIFIER sig_alg = { 0 };
    QString sname = ConstructSubjectName();
    if(sname.isEmpty()){
        QMessageBox::warning(this, "Внимание", "Заполните поля", QMessageBox::Ok);
        return;
    }
    std::wstring issuer = sname.toStdWString();
    QVector<BYTE> encoded_name;
    DWORD name_enc_len = 0;
    DWORD pub_key_info_len = 0;
    std::vector<BYTE> pub_key_info_buff;
    CERT_PUBLIC_KEY_INFO* pub_key_info = nullptr;
    SYSTEMTIME st = { 0 };
    FILETIME not_after = { 0 };
    FILETIME not_before = { 0 };
    CERT_INFO cert_info = { 0 };
    DWORD enc_cert_len = 0;
    std::vector<BYTE> enc_cert;
    std::vector<BYTE> cert_sn = { 0x01 };
    std::vector<BYTE> key_usage = { 0x03, 0x02, 0x01, 0x86 };
   // std::vector<BYTE> basic_con = { 0x30, 0x03, 0x01, 0x01, 0xFF };
     std::vector<BYTE> basic_con = { 0x30, 0x00 };

    std::vector<CERT_EXTENSION> cert_ext = {
       // { szOID_KEY_USAGE, TRUE, { static_cast<DWORD>(key_usage.size()), key_usage.data() } },
        { (LPSTR)szOID_BASIC_CONSTRAINTS2, FALSE, { static_cast<DWORD>(basic_con.size()), basic_con.data() } }
    };


    if (!CryptAcquireContext(&hProv, key_name.toStdWString().data(), NULL, Constants::BIGN_CSP_TYPE, Constants::SOFT_IMPL_FLAG))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptExportPublicKeyInfo(hProv,
        AT_SIGNATURE,
        X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
        nullptr,
        &pub_key_info_len))
    {
        error = GetLastError();
        goto Exit;
    }

    pub_key_info_buff.resize(pub_key_info_len);
    pub_key_info = reinterpret_cast<CERT_PUBLIC_KEY_INFO*>(pub_key_info_buff.data());
    if (!CryptExportPublicKeyInfo(hProv,
        AT_SIGNATURE,
        X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
        pub_key_info,
        &pub_key_info_len))
    {
        error = GetLastError();
        goto Exit;
    }

    if(!CertStrToName(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
                      issuer.data(),
                      CERT_OID_NAME_STR | CERT_NAME_STR_CRLF_FLAG | CERT_NAME_STR_FORCE_UTF8_DIR_STR_FLAG,
                      NULL,
                      NULL,
                      &name_enc_len,
                      NULL))
    {
        goto Exit;
    }

    encoded_name.resize(name_enc_len);
    if(!CertStrToName(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
                      issuer.data(),
                      CERT_OID_NAME_STR | CERT_NAME_STR_CRLF_FLAG | CERT_NAME_STR_FORCE_UTF8_DIR_STR_FLAG,
                      NULL,
                      encoded_name.data(),
                      &name_enc_len,
                      NULL))
    {
        goto Exit;
    }


    GetSystemTime(&st);
    SystemTimeToFileTime(&st, &not_before);
    st.wYear += 1;
    SystemTimeToFileTime(&st, &not_after);

    if (pub_key_info->Algorithm.pszObjId == Constants::STR_OID_STB1176_2_PUB_KEY)
    {
        sig_alg.pszObjId = const_cast<char*>(Constants::STR_OID_STB1176_2_SIGN.data());
    }
    else if (pub_key_info->Algorithm.pszObjId == Constants::STR_OID_STB1176_2_PRE_PUB_KEY)
    {
        sig_alg.pszObjId = const_cast<char*>(Constants::STR_OID_STB1176_2_PRE_SIGN.data());
    }
    else if (pub_key_info->Algorithm.pszObjId == Constants::STR_OID_BIGN_PUB_KEY)
    {
        std::vector<BYTE> param;
        std::copy(pub_key_info->Algorithm.Parameters.pbData,
            pub_key_info->Algorithm.Parameters.pbData + pub_key_info->Algorithm.Parameters.cbData,
            std::back_inserter(param));
        if (param == Constants::ASN1_OBJ_PARAMS256)
        {
            sig_alg.pszObjId = const_cast<char*>(Constants::STR_OID_BIGN_WITH_HBELT.data());
        }
        else if (param == Constants::ASN1_OBJ_PARAMS384)
        {
            sig_alg.pszObjId = const_cast<char*>(Constants::STR_OID_BIGN_WITH_HBELT.data());
        }
        else if (param == Constants::ASN1_OBJ_PARAMS512)
        {
            sig_alg.pszObjId = const_cast<char*>(Constants::STR_OID_BIGN_WITH_HBELT.data());
        }
        else
        {
            error = NTE_NOT_SUPPORTED;
            goto Exit;
        }
    }
    else
    {
        error = NTE_NOT_SUPPORTED;
        goto Exit;
    }

    cert_info.dwVersion = CERT_V3;
    cert_info.Issuer.cbData = static_cast<DWORD>(encoded_name.size());
    cert_info.Issuer.pbData = encoded_name.data();
    cert_info.NotAfter = not_after;
    cert_info.NotBefore = not_before;
    cert_info.cExtension = static_cast<DWORD>(cert_ext.size());
    cert_info.rgExtension = cert_ext.data();
    cert_info.SerialNumber.cbData = static_cast<DWORD>(cert_sn.size());
    cert_info.SerialNumber.pbData = cert_sn.data();
    cert_info.SignatureAlgorithm = sig_alg;
    cert_info.Subject.cbData = static_cast<DWORD>(encoded_name.size());
    cert_info.Subject.pbData = encoded_name.data();
    cert_info.SubjectPublicKeyInfo = *pub_key_info;

    if (!CryptSignAndEncodeCertificate(hProv,
        AT_SIGNATURE, PKCS_7_ASN_ENCODING | X509_ASN_ENCODING,
        X509_CERT_TO_BE_SIGNED,
        &cert_info,
        &sig_alg,
        nullptr,
        nullptr,
        &enc_cert_len))
    {
        error = GetLastError();
        goto Exit;
    }

    enc_cert.resize(enc_cert_len);
    if (!CryptSignAndEncodeCertificate(hProv,
        AT_SIGNATURE, PKCS_7_ASN_ENCODING | X509_ASN_ENCODING,
        X509_CERT_TO_BE_SIGNED,
        &cert_info,
        &sig_alg,
        nullptr,
        enc_cert.data(),
        &enc_cert_len))
    {
        error = GetLastError();
        goto Exit;
    }


    path = QFileDialog::getSaveFileName(this, tr("Save File"), "", "(*.cer)");
    if(path.isEmpty()){
        goto Exit;
    }
    file.setFileName(path);
    if(!file.open(QIODevice::ReadWrite))
    {
        goto Exit;
    }
    file.write(reinterpret_cast<char*>(enc_cert.data()), enc_cert.size());
    file.close();
    error = ERROR_SUCCESS;
    QMessageBox::information(this, "Информация", "Операция выполнена спешно", QMessageBox::Ok);

Exit:

    if (hProv)
    {
        CryptReleaseContext(hProv, 0);
    }

    return;
}

void CertificateRequestWizard::on_cb_cert_req_type_currentIndexChanged(int index)
{
    ui->sw_type->setCurrentIndex(index);
    if(ui->cb_cert_req_type->currentIndex() == static_cast<int>(CERT_REQ_TYPE::ENTITY) ||
       ui->cb_cert_req_type->currentIndex() == static_cast<int>(CERT_REQ_TYPE::INDIVIDUAL)){
        ui->cb_keys->setEnabled(true);
        ui->rb_exist_key->setEnabled(true);
        ui->rb_new_key->setEnabled(true);
        ui->lb_choose_key->setEnabled(true);
    } else if (ui->cb_cert_req_type->currentIndex() == static_cast<int>(CERT_REQ_TYPE::AC)){
        ui->cb_keys->setEnabled(false);
        ui->rb_exist_key->setEnabled(false);
        ui->rb_new_key->setEnabled(false);
        ui->lb_choose_key->setEnabled(false);
    }
}

void CertificateRequestWizard::ShowEmptyLineNotification(QWidget* w, const QString& text) const
{
    w->setFocus();
    QToolTip::showText(w->mapToGlobal(QPoint()), text);
    return;
}

void CertificateRequestWizard::FillAttributeCertTemplate()
{
    QStringList info = ConstructACInfo();
    if(info.isEmpty()){
        return;
    }

    WordAutomation wa;
    if(!wa.Init()){
        QMessageBox::critical(this, "Ошибка", "Не удалось сформировать заявку", QMessageBox::Ok);
        return;
    }

    HRESULT hr = wa.FillField(info.at(0), "o");
    if(hr != ERROR_SUCCESS){
        QMessageBox::critical(this, "Ошибка", "Не удалось сформировать заявку", QMessageBox::Ok);
        return;
    }
    hr = wa.FillField(info.at(1), "u");
    if(hr != ERROR_SUCCESS){
        QMessageBox::critical(this, "Ошибка", "Не удалось сформировать заявку", QMessageBox::Ok);
        return;
    }
    hr = wa.FillField(info.at(2), "p");
    if(hr != ERROR_SUCCESS){
        QMessageBox::critical(this, "Ошибка", "Не удалось сформировать заявку", QMessageBox::Ok);
        return;
    }
    hr = wa.FillField(info.at(3), "post");
    if(hr != ERROR_SUCCESS){
        return;
    }
    hr = wa.FillField(info.at(4), "obl");
    if(hr != ERROR_SUCCESS){
        QMessageBox::critical(this, "Ошибка", "Не удалось сформировать заявку", QMessageBox::Ok);
        return;
    }
    hr = wa.FillField(info.at(5), "city");
    if(hr != ERROR_SUCCESS){
        QMessageBox::critical(this, "Ошибка", "Не удалось сформировать заявку", QMessageBox::Ok);
        return;
    }
    hr = wa.FillField(info.at(6), "ya");
    if(hr != ERROR_SUCCESS){
        QMessageBox::critical(this, "Ошибка", "Не удалось сформировать заявку", QMessageBox::Ok);
        return;
    }
    hr = wa.FillField(info.at(7), "pf");
    if(hr != ERROR_SUCCESS){
        QMessageBox::critical(this, "Ошибка", "Не удалось сформировать заявку", QMessageBox::Ok);
        return;
    }
    hr = wa.FillField(info.at(8), "idc");
    if(hr != ERROR_SUCCESS){
        QMessageBox::critical(this, "Ошибка", "Не удалось сформировать заявку", QMessageBox::Ok);
        return;
    }
    hr = wa.FillField(info.at(9), "idg");
    if(hr != ERROR_SUCCESS){
        QMessageBox::critical(this, "Ошибка", "Не удалось сформировать заявку", QMessageBox::Ok);
        return;
    }
    hr = wa.FillField(info.at(10), "tel");
    if(hr != ERROR_SUCCESS){
        QMessageBox::critical(this, "Ошибка", "Не удалось сформировать заявку", QMessageBox::Ok);
        return;
    }
    QString path = QFileDialog::getSaveFileName(this, tr("Сохранить файл заявки на атрибутный сертификат"), "",
                                                tr("(*.docx)"));
    hr = wa.SaveAs(path);
    if(hr != ERROR_SUCCESS){
        QMessageBox::critical(this, "Ошибка", "Не удалось сохранить заявку", QMessageBox::Ok);
        return;
    }
    wa.Close();
    QMessageBox::information(this, "Информация", "Заявка успешно сформирована", QMessageBox::Ok);
    return;
}

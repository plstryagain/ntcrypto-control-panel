#ifndef CRYPTOOPERATIONSMANAGER_H
#define CRYPTOOPERATIONSMANAGER_H

#include <Windows.h>
#include <QObject>
#include <vector>

class SystemCertificateManager;

class CryptoOperationsManager : public QObject
{
    Q_OBJECT
public:
    explicit CryptoOperationsManager(QObject *parent = nullptr);
    ~CryptoOperationsManager();

signals:
    void signalFinish();
    void signalCalculated(QString, QByteArray);
    void signalReportError(QString);
    void signalCompared(QString);
    void signalFileProcessed();
    void signalVerifyComplete(bool, QString, QString, QString, const _CERT_CONTEXT*);

public slots:
    void CalculateHash();
    void CompareFiles();
    void MakeSignature();
    void VerifySignature();
    void Encrypt();
    void Decrypt();

public:
    void InitCalculateHashOperation(const ALG_ID alg_id, const DWORD impl, const QString& path, bool is_dir);
    void InitCompareOperation(const ALG_ID alg_id, const DWORD impl, const QString& path1, const QString& path2);
    void InitSigOperation(const ALG_ID alg_id, const QStringList& files_to_sig, PCCERT_CONTEXT cert,
                          const QString& out_file_path, const bool is_detached, const bool use_base64);
    void InitSigVerifyOperation(const QString& sig_file_name, const QString& data_file_name,
                                const bool is_detached);
    void InitEncryptOperation(const QStringList& files, const std::vector<PCCERT_CONTEXT>& certs,
                              const ALG_ID alg_id, const DWORD impl, const QString& out_path,
                              const bool use_base64);
    void InitDecryptOperation(const QStringList& files, const QString& out_path);


private:
    bool CalculateHash(const ALG_ID alg_id, const DWORD impl, const QByteArray& content, QByteArray* out);
    DWORD DecodeSigningTime(const BYTE *aval, const DWORD alen, QString* sig_time);
    DWORD DecodeSignerInfo(PCCERT_CONTEXT pCert, QString* signer);
    bool IsBase64(const QString& string);

private:
    bool is_calc_hash_op_init_ = false;
    bool is_compare_op_init_ = false;
    ALG_ID alg_id_ = 0;
    DWORD impl_ = 0;
    QString path_;
    QString path2_;
    bool is_dir_;
    QByteArray out_;
    QString error_;
    PCCERT_CONTEXT cert_;
    std::vector<PCCERT_CONTEXT> certs_;
    QStringList files_;
    QString out_file_path_;
    QString sig_file_name_;
    QString data_file_name_;
    bool is_detached_;
    bool use_base64_;
    HCERTSTORE hStore_ = 0;
};

#endif // CRYPTOOPERATIONSMANAGER_H

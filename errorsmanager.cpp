#include "errorsmanager.h"
#include <Windows.h>

ErrorsManager::ErrorsManager(QObject *parent) : QObject(parent)
{

}

QString ErrorsManager::CodeToStrError(const unsigned long code)
{
    switch(code){
    case NTE_FAIL:
        return "Внутренняя ошибка.";
    case NTE_PROV_DLL_NOT_FOUND:
        return "Библиотека криптопровайдера не может быть найдена.";
    case ERROR_NOT_FOUND:
    case NTE_NOT_FOUND:
        return "Объект не найден.";
    case NTE_DEVICE_NOT_FOUND:
    case NTE_DEVICE_NOT_READY:
        return "Устройство не найдено.";
    case NTE_BAD_ALGID:
        return "Указан неправильный алгоритм.";
    case NTE_BAD_KEY:
        return "Плохой ключ.";
    case NTE_BAD_KEYSET:
        return "Набор ключей не существует.";
    case NTE_BAD_PUBLIC_KEY:
        return "Неправильный открытый ключ.";
    case NTE_BAD_PROV_TYPE:
        return "Указан неправильный тип поставщика.";
    case NTE_BAD_KEY_STATE:
        return "Ключ не может быть использован в указанном состоянии.";
    case NTE_BAD_HASH:
        return "Плохой хэш.";
    case NTE_BAD_SIGNATURE:
        return "Неправильная подпись.";
    case NTE_INVALID_HANDLE:
        return "Предоставлен неправильный дескриптор.";
    case NTE_INVALID_PARAMETER:
        return "Неправильный параметр.";
    case NTE_INCORRECT_PASSWORD:
        return "Неверный пароль.";
    case ERROR_FILE_NOT_FOUND:
        return "Не удается найти указанный файл.";
    case ERROR_ACCESS_DENIED:
        return "Отказано в доступе.";
    case NTE_BAD_FLAGS:
        return "Указаны неправильные флаги.";
    case RPC_S_SERVER_UNAVAILABLE:
        return "Служба криптопровайдера недоступна.";
    case NTE_NOT_SUPPORTED:
        return "Запрошенная операция не поддерживается.";
    }
}

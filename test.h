#ifndef TEST_H
#define TEST_H

#include <QObject>
#include <Windows.h>
#include <QPair>
#include <QVector>

class Test : public QObject
{
    Q_OBJECT
public:
    explicit Test(QObject *parent = 0, DWORD dev_type = 0);

public slots:
    void LaunchAllTests();

private:
    void LaunchStb31Tests();
    void LaunchStb45Tests();
    void LaunchStb77Tests();
    DWORD LaunchHashTest(const ALG_ID alg_id, const std::vector<BYTE>& input_val,
                         const std::vector<BYTE>& key, const std::vector<BYTE>& exp_val);
    DWORD LaunchDecryptTest(const ALG_ID alg_id, const std::vector<BYTE>& input_val,
                           const std::vector<BYTE>& key, const std::vector<BYTE>&iv,
                           const std::vector<BYTE>& exp_val);
    DWORD LaunchEncryptTest(const ALG_ID alg_id, const std::vector<BYTE>& input_val,
                           const std::vector<BYTE>& key, const std::vector<BYTE>&iv,
                           const std::vector<BYTE>& exp_val);
    DWORD Stb45_bs11();
    DWORD Stb45_BS12Test();
    DWORD Stb45_BS13Test();
    DWORD Stb45_BS14Test();
    DWORD Stb45_BS18Test();
    void CreatePrivBlob(std::vector<BYTE>* blob_val, DWORD* blob_val_len);
    bool CheckResult(const BYTE* result, const DWORD result_len,
                     const BYTE* expect, const DWORD expect_len);

signals:
    void signalStb31Progress(int);
    void signalStb45Progress(int);
    void signalStb77Progress(int);
    void signalStb31TestFinish(QString);
    void signalStb45TestFinish(QString);
    void signalStb77TestFinish(QString);
    void signalFinish(void);

public slots:

private:
    DWORD dev_type_;
};

#endif // TEST_H

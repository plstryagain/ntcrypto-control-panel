#include "selectcertwizard.h"
#include "ui_selectcertwizard.h"
#include "certificatestoremanager.h"
#include "certificateui.h"

SelectCertWizard::SelectCertWizard(QWidget *parent, const QString &cert_store_name,
                                   const bool show_only_with_key) :
    QWidget(parent),
    ui(new Ui::SelectCertWizard),
    cert_store_name_(cert_store_name),
    selected_row_(-1),
    show_only_with_key_(show_only_with_key)
{
    ui->setupUi(this);
    this->setWindowTitle("Выбор сертификата");
    //this->setAttribute(Qt::WA_DeleteOnClose);
    //this->setWindowIcon(QIcon(":/images/images/Accreditation_32px.png"));
    this->setWindowFlags(Qt::Window);
    ui->tb_desc->setVisible(false);
}

SelectCertWizard::~SelectCertWizard()
{
    qDebug() << "SelectCertWizard HAS BEEN DESTRUCTED!!1";
    delete ui;
}


void SelectCertWizard::showEvent(QShowEvent *event)
{
    Q_UNUSED(event)
    CERTIFICATE_STORE_TYPE cst;
    if(cert_store_name_ == "MY"){
        cst = CERTIFICATE_STORE_TYPE::MY;
    } else if (cert_store_name_ == "ATTRIBUTE"){
        cst = CERTIFICATE_STORE_TYPE::ATTRIBUTE;
    } else {
        ui->tb_desc->setText("Не удалось открыть хранилище сертификатов");
        ui->btn_show_inf->setEnabled(false);
        return;
    }
    csmgr_ = CertificateStoreManager::Create(cst);
    if(!csmgr_->OpenStore()){
        ui->tb_desc->setText("Не удалось открыть хранилище сертификатов");
        ui->btn_show_inf->setEnabled(false);
        return;
    }
    csmgr_->EnumCertificates();
    int idx = 0;
    for(const auto& certmgr : csmgr_->GetCertificateManagerList()){
        auto syscert = dynamic_cast<SystemCertificateManager*>(certmgr.get());
        if(!syscert){
            continue;
        }
        if(show_only_with_key_){
            if(syscert->IsPrivateKeyForCertExist()){
                QListWidgetItem* item = new QListWidgetItem(syscert->GetCertSubjectSimpleName());
                item->setData(Qt::UserRole, QVariant(idx));
                ui->lw_certs->addItem(item);
            }
        } else {
            QListWidgetItem* item = new QListWidgetItem(syscert->GetCertSubjectSimpleName());
            item->setData(Qt::UserRole, QVariant(idx));
            ui->lw_certs->addItem(item);
        }
        ++idx;
    }
    if(ui->lw_certs->count() == 0){
        ui->tb_desc->setText("Не найдено ни одного сертификата, соответствующего требованиям");
        ui->btn_show_inf->setEnabled(false);
        return;
    }
    ui->lw_certs->setCurrentRow(0);
}

void SelectCertWizard::on_lw_certs_currentRowChanged(int currentRow)
{
    QString desc = "";
    if(cert_store_name_ == "MY"){
        int idx = ui->lw_certs->item(currentRow)->data(Qt::UserRole).value<int>();
        auto certmgr = dynamic_cast<SystemCertificateManager*>(csmgr_->GetCertificateManagerByIndex(idx).get());
        desc.append(certmgr->GetCertSubjectSimpleName()+"\n\n");
        desc.append("Издатель: "+certmgr->GetCertIssuerSimpleName()+"\n\n");
        desc.append("Действителен с "+certmgr->GetNotBefore()+" по "+certmgr->GetNotAfter());
        ui->btn_show_inf->setEnabled(true);
    } else if(cert_store_name_ == "ATTRIBUTE"){
        /* TODO */
    } else {
        desc = "Неизвестный тип сертификата";
        ui->btn_show_inf->setEnabled(false);
    }
    ui->lb_desc->setText(desc);
    ui->lb_desc->setWordWrap(true);
}

void SelectCertWizard::on_btn_show_inf_clicked()
{
    int row = ui->lw_certs->currentRow();
    if(row < 0){
        return;
    }
    int idx = ui->lw_certs->item(row)->data(Qt::UserRole).value<int>();
    const auto& certmgr = csmgr_->GetCertificateManagerByIndex(idx);
    CERT_UI_TYPE ui_type;
    if(cert_store_name_ == "MY"){
        ui_type = CERT_UI_TYPE::X509Certificate;
    } else if (cert_store_name_ == "ATTRIBUTE"){
        ui_type = CERT_UI_TYPE::AttributeCertificate;
    } else {
        return;
    }
    CertificateUI* cui = new CertificateUI(this, ui_type, certmgr);
    cui->show();
}

void SelectCertWizard::on_btn_cancel_clicked()
{
    emit selected();
    this->close();
}

const ICertificateManager* SelectCertWizard::GetSelectedCertificate() const
{
    if(selected_row_ < 0){
        return nullptr;
    } else {
        int idx = ui->lw_certs->item(selected_row_)->data(Qt::UserRole).value<int>();
        return csmgr_->GetCertificateManagerByIndex(idx).get();
    }
}

void SelectCertWizard::on_btn_ok_clicked()
{
    selected_row_ = ui->lw_certs->currentRow();
    emit selected();
    this->close();
}

#include "sigoperationwizard.h"
#include "ui_sigoperationwizard.h"
#include "icertificatemanager.h"
#include "selectcertwizard.h"
#include "certificatestoremanager.h"
#include "Constants.h"
#include "dialogpleasewait.h"
#include "cryptooperationsmanager.h"
#include <Windows.h>
#include <cryptuiapi.h>
#include <QFileDialog>
#include <QMessageBox>
#include <QToolTip>
#include <QThread>
#include <QMessageBox>
#include <QDebug>

SigOperationWizard::SigOperationWizard(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SigOperationWizard),
    cert_(nullptr)
{
    ui->setupUi(this);
    ui->cb_hash_alg->setEnabled(false);
    ui->cb_hash_alg->setToolTip("Выберите сертификат");
    ui->lb_selected_cert->setWordWrap(true);
}

SigOperationWizard::~SigOperationWizard()
{
    if(cert_){
        CertFreeCertificateContext(cert_);
    }
    delete ui;
}

void SigOperationWizard::on_btn_select_sig_clicked()
{
    QString path_to_save = QFileDialog::getExistingDirectory(this,
                                                            tr("Выберите куда вы хотите сохранить подписанные файлы"),
                                                            "");
    if(!path_to_save.isEmpty()){
        ui->ln_sig_file->setText(path_to_save);
    }
}

void SigOperationWizard::on_btn_select_cert_clicked()
{
    SelectCertWizard scw(this, "MY", true);
    scw.show();
    QEventLoop loop;
    connect(&scw, SIGNAL(selected()), &loop, SLOT(quit()));
    loop.exec();
    const SystemCertificateManager* cert = dynamic_cast<const SystemCertificateManager*>(scw.GetSelectedCertificate());
    if(!cert){
        return;
    } else {
        ui->lb_selected_cert->setText(cert->GetCertSubjectSimpleName());
        if(cert->GetPublicKeyAlgName() == "bign-pubkey"){
            ui->cb_hash_alg->setEnabled(true);
            ui->cb_hash_alg->clear();
            DWORD key_len = static_cast<PCCERT_CONTEXT>(
                        cert->GetRawContextPtr())->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData;
            if(key_len == Constants::BIGN_CURVE_256_PUBKEY_SIZE_IN_BYTES){
                ui->cb_hash_alg->addItem("belt-hash256", QVariant(CALG_BELT_HASH_256));
                ui->cb_hash_alg->addItem("bash256", QVariant(CALG_BASH_256));
            } else if(key_len == Constants::BIGN_CURVE_384_PUBKEY_SIZE_IN_BYTES){
                ui->cb_hash_alg->addItem("bash384", QVariant(CALG_BASH_384));
            } else if(key_len == Constants::BIGN_CURVE_512_PUBKEY_SIZE_IN_BYTES){
                ui->cb_hash_alg->addItem("bash512", QVariant(CALG_BASH_512));
            } else {
                ui->cb_hash_alg->setEnabled(false);
            }
        } else {
            ui->cb_hash_alg->setEnabled(false);
            ui->cb_hash_alg->setToolTip("Алгоритм ключа не поддерживается");
        }
        if(cert_){
            CertFreeCertificateContext(cert_);
            cert_ = nullptr;
        }
        cert_ = CertDuplicateCertificateContext(static_cast<PCCERT_CONTEXT>(cert->GetRawContextPtr()));
    }
}

void SigOperationWizard::on_btn_sign_clicked()
{
    is_op_success_ = true;
    ui->tb_result->clear();
    if(ui->lw_cont_file->count() == 0){
        ShowEmptyLineNotification(ui->lw_cont_file, "Выберите файлы для подписи");
        return;
    }
    if(!cert_){
        ShowEmptyLineNotification(ui->lb_selected_cert, "Выберите сертификат");
        return;
    }
    if(ui->ln_sig_file->text().isEmpty()){
        ShowEmptyLineNotification(ui->ln_sig_file, "Выберите куда сохранить результат");
        return;
    }
    int count = ui->lw_cont_file->count();
    ALG_ID halg = ui->cb_hash_alg->currentData().value<ALG_ID>();
    bool use_base64 = ui->chk_base64->isChecked();
    QStringList files_to_sig;
    for(int i = 0; i < count; ++i){
        files_to_sig.append(ui->lw_cont_file->item(i)->text());
    }
    DialogPleaseWait dpw(this);
    dpw.SetAmountOfFiles(count);
    QThread* thread = new QThread();
    std::unique_ptr<CryptoOperationsManager> comgr =
            std::unique_ptr<CryptoOperationsManager>(new CryptoOperationsManager());
    bool is_detached = !ui->chk_detached->isChecked();
    comgr->InitSigOperation(halg, files_to_sig, cert_, ui->ln_sig_file->text(), is_detached, use_base64);
    comgr->moveToThread(thread);
    connect(comgr.get(), SIGNAL(signalFinish()), thread, SLOT(quit()));
    connect(comgr.get(), SIGNAL(signalReportError(QString)), this, SLOT(Error(QString)));
    connect(thread, SIGNAL(started()), comgr.get(), SLOT(MakeSignature()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(comgr.get(), SIGNAL(signalFileProcessed()), &dpw, SLOT(ProgressUpdate()));
    connect(thread, SIGNAL(finished()), &dpw, SLOT(accept()));
    thread->start();
    dpw.exec();
    if(is_op_success_){
        QMessageBox::information(this, "Информация", "Операция выполнена успешно", QMessageBox::Ok);
    } else {
        QMessageBox::critical(this, "Ошибка", "Операция не выполнена", QMessageBox::Ok);
    }
}

void SigOperationWizard::on_btn_add_file_clicked()
{
    QStringList cont_files = QFileDialog::getOpenFileNames(this,
                                                                tr("Выберите файлы для подписания"),
                                                                "",
                                                                tr("(*.*)"));
    if(cont_files.isEmpty()){
        return;
    } else {
        for(const auto& file : cont_files){
            ui->lw_cont_file->addItem(file);
        }
    }
}

void SigOperationWizard::on_btn_rem_file_clicked()
{
    int row = ui->lw_cont_file->currentRow();
    if(row < 0){
        return;
    }
    delete ui->lw_cont_file->takeItem(row);
}

void SigOperationWizard::ShowEmptyLineNotification(QWidget* w, const QString& text)
{
    w->setFocus();
    QToolTip::showText(w->mapToGlobal(QPoint()), text);
    return;
}

void SigOperationWizard::Error(QString error)
{
    is_op_success_ = false;
    ui->tb_result->append(error);
}

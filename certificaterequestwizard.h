#ifndef CERTIFICATEREQUESTWIZARD_H
#define CERTIFICATEREQUESTWIZARD_H

#include <QWidget>

namespace Ui {
class CertificateRequestWizard;
}

class CertificateRequestWizard : public QWidget
{
    Q_OBJECT

public:
    explicit CertificateRequestWizard(QWidget *parent = 0);
    ~CertificateRequestWizard();

protected:
    void showEvent(QShowEvent *event) override;

private slots:
    void on_btn_cancel_clicked();
    void on_btn_ok_clicked();

    void on_cb_cert_req_type_currentIndexChanged(int index);

    void ShowError(unsigned long error);
    void SaveRequest(QVector<unsigned char> request);
    void FillAttributeCertTemplate();


private:
    void EnumKeys();
    QString ConstructSubjectName();
    QStringList ConstructACInfo();
    void GenTestCert();
    void ShowEmptyLineNotification(QWidget *w, const QString& text) const;


private:
    Ui::CertificateRequestWizard *ui;
    QString key_name_;
    QString subject_;
};

#endif // CERTIFICATEREQUESTWIZARD_H

#ifndef ENCRYPTOPERATIONWIZARD_H
#define ENCRYPTOPERATIONWIZARD_H

#include <QWidget>
#include <vector>

namespace Ui {
class EncryptOperationWizard;
}

struct _CERT_CONTEXT;

class EncryptOperationWizard : public QWidget
{
    Q_OBJECT

public:
    explicit EncryptOperationWizard(QWidget *parent = 0);
    ~EncryptOperationWizard();

private slots:
    void on_rb_dec_toggled(bool checked);

    void on_btn_add_file_clicked();

    void on_btn_del_file_clicked();

    void on_btn_enc_dec_clicked();

    void on_btn_out_file_clicked();

    void on_btn_add_cert_clicked();

    void on_btn_del_cert_clicked();

    void Error(QString error);

private:
    void ShowEmptyLineNotification(QWidget* w, const QString& text);

private:
    Ui::EncryptOperationWizard *ui;
    std::vector<const _CERT_CONTEXT*> certs_;
    QString report_;
};

#endif // ENCRYPTOPERATIONWIZARD_H

#include "sigverifyoperationwizard.h"
#include "ui_sigverifyoperationwizard.h"
#include "dialogpleasewait.h"
#include "cryptooperationsmanager.h"
#include "certificateui.h"
#include "icertificatemanager.h"
#include <Windows.h>
#include <QFileDialog>
#include <QFile>
#include <QThread>
#include <QToolTip>
#include <QMessageBox>
#include <QEventLoop>
#include <QDebug>
#include <memory>

SigVerifyOperationWizard::SigVerifyOperationWizard(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SigVerifyOperationWizard)
{
    ui->setupUi(this);
    ui->btn_show_cert->setEnabled(false);
    ui->lb_signer_txt->setEnabled(false);
    ui->lb_sig_date_txt->setEnabled(false);
    ui->lb_sig_status_txt->setEnabled(false);
    ui->lb_cert_status_txt->setEnabled(false);
    ui->lb_data_file->setVisible(false);
    ui->ln_data_file->setVisible(false);
    ui->btn_data_file->setVisible(false);
}

SigVerifyOperationWizard::~SigVerifyOperationWizard()
{
    delete ui;
}

void SigVerifyOperationWizard::on_btn_sig_file_clicked()
{
    QString sig_file = QFileDialog::getOpenFileName(this,
                                                      tr("Выберите файл подписи"),
                                                      "",
                                                      tr("Файлы подписи(*.sig);;Все файлы (*.*)"));
    if(sig_file.isEmpty()){
        return;
    } else {
        ui->ln_sig_file->setText(sig_file);
        QFileInfo fi(sig_file);
        if(!fi.exists()){
            return;
        }
    }
    bool is_detached = false;
    IsDetached(ui->ln_sig_file->text(), &is_detached);
    if(is_detached){
        ui->lb_data_file->setVisible(true);
        ui->ln_data_file->setVisible(true);
        ui->btn_data_file->setVisible(true);
    } else {
        ui->lb_data_file->setVisible(false);
        ui->ln_data_file->setVisible(false);
        ui->btn_data_file->setVisible(false);
    }
}

bool SigVerifyOperationWizard::IsDetached(const QString& sig_file, bool* is_detached)
{
    HCRYPTMSG hMsg = NULL;
    bool res = false;
    DWORD dlen = 0;
    QFile file(sig_file);
    QByteArray ba;
    if(!file.open(QFile::ReadOnly)){
        goto Exit;
    }
    ba = file.readAll();
    if(ba.isEmpty()){
        goto Exit;
    }
    if(IsBase64(QString(ba))){
        ba = QByteArray::fromBase64(ba);
    }
    file.close();
    hMsg = CryptMsgOpenToDecode(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, 0, 0, 0, nullptr, nullptr);
    if(!hMsg){
        goto Exit;
    }
    if(!CryptMsgUpdate(hMsg, reinterpret_cast<BYTE*>(ba.data()), ba.size(), TRUE)){
        goto Exit;
    }

    if(!CryptMsgGetParam(hMsg, CMSG_CONTENT_PARAM, 0, nullptr, &dlen)){
        goto Exit;
    }

    *is_detached = (dlen == 0);
    res = true;

Exit:
    if(hMsg){
        CryptMsgClose(hMsg);
    }
    return res;
}

void SigVerifyOperationWizard::on_btn_verify_sig_clicked()
{
    is_op_success_ = true;
    ui->tb_result->clear();
    if(ui->ln_sig_file->text().isEmpty()){
        ShowEmptyLineNotification(ui->ln_sig_file, "Выберите файл подписи");
        return;
    }
    if(ui->ln_data_file->isVisible() && ui->ln_data_file->text().isEmpty()){
        ShowEmptyLineNotification(ui->ln_data_file, "Выберите документ для которго необходимо проверить подпись");
        return;
    }
    QString sig_file_name = ui->ln_sig_file->text();
    bool is_detached = false;
    QString data_file_name = "";
    if(ui->ln_data_file->isVisible()){
        is_detached = true;
        data_file_name = ui->ln_data_file->text();
    }
    DialogPleaseWait dpw(this);
    QThread* thread = new QThread();
    std::unique_ptr<CryptoOperationsManager> comgr =
            std::unique_ptr<CryptoOperationsManager>(new CryptoOperationsManager());
    comgr->InitSigVerifyOperation(sig_file_name, data_file_name, is_detached);
    comgr->moveToThread(thread);
    connect(comgr.get(), SIGNAL(signalFinish()), thread, SLOT(quit()));
    connect(comgr.get(), SIGNAL(signalReportError(QString)), this, SLOT(Error(QString)));
    connect(comgr.get(), SIGNAL(signalVerifyComplete(bool, QString, QString, QString, const _CERT_CONTEXT*)),
            this, SLOT(VerifyComplete(bool,QString,QString, QString, const _CERT_CONTEXT*)));
    connect(thread, SIGNAL(started()), comgr.get(), SLOT(VerifySignature()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), &dpw, SLOT(accept()));
    thread->start();
    dpw.exec();
    if(is_op_success_){
        QMessageBox::information(this, "Информация", "Операция выполнена успешно", QMessageBox::Ok);
    } else {
        QMessageBox::critical(this, "Ошибка", "Операция не выполнена", QMessageBox::Ok);
    }
}

void SigVerifyOperationWizard::VerifyComplete(bool is_valid, QString signer, QString sig_time, QString cert_status,
                                              PCCERT_CONTEXT pCert)
{
    ui->lb_signer_txt->setEnabled(true);
    ui->lb_cert_status_txt->setEnabled(true);
    ui->lb_sig_date_txt->setEnabled(true);
    ui->lb_sig_status_txt->setEnabled(true);
    ui->btn_show_cert->setEnabled(true);
    if(cert_status.indexOf("сертификат действителен") != -1){
        ui->lb_cert_status->setStyleSheet("QLabel#lb_cert_status{ color : green }");
    } else {
        ui->lb_cert_status->setStyleSheet("QLabel#lb_cert_status{ color : red }");
    }
    ui->lb_cert_status->setText(cert_status);

    if(is_valid){
        ui->lb_sig_status->setStyleSheet("QLabel#lb_sig_status{ color : green }");
        ui->lb_sig_status_ico->setPixmap(QPixmap(":/images/images/Checkmark_16px.png"));
        ui->lb_sig_status->setText("Верна");
    } else {
        ui->lb_sig_status->setStyleSheet("QLabel#lb_sig_status{ color : red }");
        ui->lb_sig_status_ico->setPixmap(QPixmap(":/images/images/Delete_16px.png"));
        ui->lb_sig_status->setText("Не верна");
    }
    ui->lb_signer->setText(signer);
    ui->lb_sig_date->setText(sig_time);
    if(pCert_){
        CertFreeCertificateContext(pCert_);
    }
    pCert_ = pCert;
}

void SigVerifyOperationWizard::ShowEmptyLineNotification(QWidget* w, const QString& text)
{
    w->setFocus();
    QToolTip::showText(w->mapToGlobal(QPoint()), text);
    return;
}

void SigVerifyOperationWizard::Error(const QString& error)
{
    is_op_success_ = false;
    ui->tb_result->append(error);
}

void SigVerifyOperationWizard::on_btn_data_file_clicked()
{
    QString data_file = QFileDialog::getOpenFileName(this,
                                                      tr("Выберите документ, для которого необходимо проверить подпись"),
                                                      "",
                                                      tr("(*.*)"));
    if(data_file.isEmpty()){
        return;
    } else {
        ui->ln_data_file->setText(data_file);
    }
}

void SigVerifyOperationWizard::on_ln_sig_file_textChanged(const QString &arg1)
{
    Q_UNUSED(arg1);
    ui->btn_show_cert->setEnabled(false);
    ui->lb_signer_txt->setEnabled(false);
    ui->lb_signer->setText("");
    ui->lb_sig_date_txt->setEnabled(false);
    ui->lb_sig_date->setText("");
    ui->lb_sig_status_txt->setEnabled(false);
    ui->lb_sig_status->setText("");
    ui->lb_sig_status_ico->clear();
    ui->lb_cert_status_txt->setEnabled(false);
    ui->lb_cert_status->setEnabled(false);
    ui->lb_cert_status->setText("");
}

void SigVerifyOperationWizard::on_btn_show_cert_clicked()
{
    if(!pCert_){
        return;
    }
    auto cmgr = ICertificateManager::Create(CERTIFICATE_TYPE::SystemCertificate, pCert_, false);
    CertificateUI* cui = new CertificateUI(this, CERT_UI_TYPE::X509Certificate, cmgr);
    cui->show();
    QEventLoop loop;
    connect(cui, SIGNAL(destroyed()), &loop, SLOT(quit()));
    loop.exec();
}

bool SigVerifyOperationWizard::IsBase64(const QString& string)
{
    QRegExp rx("[^a-zA-Z0-9+/=]");
    if(rx.indexIn(string) == -1 && (string.length() % 4) == 0 && string.length() >= 4){
        return true;
    }
    return false;
}

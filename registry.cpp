#include "registry.h"
#include <QDebug>
namespace RegConstants
{
    const QString DEFAULT_PROVIDER_OPTIONS_REG_PATH = "Software\\NII TZI\\NTCrypto\\Options";
    const QString PROVIDER_KEYS_REG_PATH = "Software\\NII TZI\\NTCrypto\\Keys\\";
    const QString DEFAULT_STORAGE_OPTION = "DefaultStorage";
    const QString LOG_FILE_PATH_OPTION = "LogFilePath";
    const QString CNG_LOG_FILE_PATH_OPTION = "CNGLogFilePath";
    const QString KEY_STORAGE_OPTION = "KeyStorage";
    const QString KEY_STORAGE_SLOT_ID_OPTION = "SlotId";
    const QString INSTALLED_PATH_OPTION = "InstalledPath";
    const QString CRLDP_URLS_OPTION = "CRLUrls";
}

RegistryManager::RegistryManager() :
    hKey_(NULL), hCurrentUser_(NULL)
{

}

RegistryManager::~RegistryManager()
{
    if(hKey_)
    {
        RegCloseKey(hKey_);
    }
}

NTSTATUS RegistryManager::OpenCurrentUser(const DWORD access)
{
    if(hCurrentUser_)
    {
        return NTE_FAIL;
    }

    return RegOpenCurrentUser(access, &hCurrentUser_);
}

NTSTATUS RegistryManager::OpenDefaultUser()
{
    if(hCurrentUser_)
    {
        return NTE_FAIL;
    }

    hCurrentUser_ = HKEY_USERS;
    sub_path_ = ".DEFAULT\\";
    return ERROR_SUCCESS;
}

NTSTATUS RegistryManager::OpenKey(const QString& sub_path, const DWORD access)
{
    if(!hCurrentUser_)
    {
        return ERROR_NOT_READY;
    }

    if(hKey_)
    {
        return ERROR_ALREADY_INITIALIZED;
    }

    sub_path_.append(sub_path);
    return RegOpenKeyEx(hCurrentUser_,
                        sub_path_.toStdWString().data(),
                        0,
                        access,
                        &hKey_);
}

NTSTATUS RegistryManager::CreateKey(const QString& sub_path)
{
    if(!hCurrentUser_)
    {
        return ERROR_NOT_READY;
    }

    if(hKey_)
    {
        return ERROR_ALREADY_INITIALIZED;
    }

    sub_path_.append(sub_path);
    return RegCreateKeyEx(
                hCurrentUser_,
                sub_path_.toStdWString().data(),
                0,
                NULL,
                REG_OPTION_NON_VOLATILE,
                KEY_WRITE,
                NULL,
                &hKey_,
                NULL);
}

NTSTATUS RegistryManager::QueryValue(const QString& value_name, BYTE *data, DWORD *data_len)
{
    if(!hKey_)
    {
        return ERROR_NOT_READY;
    }

    return RegQueryValueEx(
                hKey_,
                value_name.toStdWString().data(),
                0,
                0,
                data,
                data_len);
}

NTSTATUS RegistryManager::SetKeyValue(const QString& sub_path, const QString& val_name,
                                      const DWORD type, const BYTE* data, const DWORD data_len)
{
    if(!hKey_)
    {
        return ERROR_NOT_READY;
    }

#ifdef _WIN_XP_

    Q_UNUSED(sub_path);

    return RegSetValueEx(hKey_, val_name.toStdWString().data(), 0, type, data, data_len);

#else

    return RegSetKeyValue(hKey_,
                          sub_path.toStdWString().data(),
                          val_name.toStdWString().data(),
                          type,
                          data,
                          data_len);

#endif
}

NTSTATUS RegistryManager::EnumKeyEx(const DWORD index, QVector<wchar_t>* name, DWORD* name_len)
{
    if(!hKey_)
    {
        return ERROR_NOT_READY;
    }

    return RegEnumKeyEx(hKey_,
                        index,
                        name->data(),
                        name_len,
                        nullptr,
                        nullptr,
                        nullptr,
                        nullptr);
}

NTSTATUS RegistryManager::DeleteKey(const QString &sub_path)
{
    if(!hCurrentUser_)
    {
        return ERROR_NOT_READY;
    }

    sub_path_.append(sub_path);
    return RegDeleteKey(hCurrentUser_, sub_path_.toStdWString().data());
}

#include <QMessageBox>
#include <QToolTip>
#include <QDebug>
#include "genkeywizard.h"
#include "ntcrypto.h"
#include "cngmanager.h"
#include "Constants.h"

GenKeyWizard::GenKeyWizard(const bool is_local_machine)
 : layout_(this), btn_box_(QDialogButtonBox::Ok | QDialogButtonBox::Cancel),
   is_local_machine_(is_local_machine)
{
    this->setWindowTitle(tr("Укажите параметры"));
    this->setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowIcon(QIcon(QString::fromUtf8(":/images/images/Key_gen_32px.png")));
}

void GenKeyWizard::Init()
{
    cb_prov_type_.addItem(tr("CAPI"));
#ifndef _WIN_XP_
    cb_prov_type_.addItem(tr("CNG"));
#endif

    cb_algs_.addItem(tr("bign-genkeypair"));
    cb_algs_.addItem(tr("stb11762-sign"));
    cb_algs_.addItem(tr("stb11762pre-sign"));

    cb_key_strength_.addItem(tr("128"));
    cb_key_strength_.addItem(tr("256"));
    cb_key_strength_.addItem(tr("384"));

    layout_.addRow(tr("Имя ключа: "), &ln_key_name_);
    layout_.addRow(tr("Тип провайдера"), &cb_prov_type_);
    layout_.addRow(tr("Алгоритм: "), &cb_algs_);
    layout_.addRow(tr("Уровень стойкости: "), &cb_key_strength_);

    btn_box_.button(QDialogButtonBox::Cancel)->setText(tr("Отмена"));

    layout_.addRow(&btn_box_);

    connect(&cb_algs_, SIGNAL(currentIndexChanged(int)), this, SLOT(FillKeyStrengthCBox(int)));
    connect(&ln_key_name_, SIGNAL(textChanged(QString)), this, SLOT(ChangeLineBorderColor(QString)));
    connect(&btn_box_, SIGNAL(rejected()), this, SLOT(btn_cancel_clicked()));
    connect(&btn_box_, SIGNAL(accepted()), this, SLOT(btn_ok_clicked()));
}

void GenKeyWizard::FillKeyStrengthCBox(int index)
{
    cb_key_strength_.clear();
    switch(index)
    {
    case 0:
        cb_key_strength_.addItem(tr("128"));
        cb_key_strength_.addItem(tr("256"));
        cb_key_strength_.addItem(tr("512"));
        break;

    case 1:
    case 2:
        cb_key_strength_.addItem(tr("3"));
        cb_key_strength_.addItem(tr("6"));
        cb_key_strength_.addItem(tr("10"));
        break;

    default:
        return;
    }
}

void GenKeyWizard::btn_cancel_clicked()
{
    emit this->rejected();
    this->close();
}

void GenKeyWizard::btn_ok_clicked()
{
    DWORD key_len = 0;
    bool ok = false;
    DWORD prov_type = 0;

    switch(cb_algs_.currentIndex())
    {
    case 0:
        prov_type = Constants::BIGN_CSP_TYPE;
        break;

    case 1:
        prov_type = Constants::STB11762_CSP_TYPE;
        break;

    case 2:
        prov_type = Constants::STB11762_PRE_CSP_TYPE;
        break;

    default:
        this->close();
        return;
    }

    switch(cb_key_strength_.currentText().toInt(&ok))
    {
    case 128:
        key_len = 256;
        break;

    case 256:
        key_len = 384;
        break;

    case 384:
        key_len = 512;
        break;

    case 3:
        key_len = 168;
        break;

    case 6:
        key_len = 200;
        break;

    case 10:
        key_len = 256;
        break;

    default:
        this->close();
        return;
    }

    QString key_name = ln_key_name_.text();
    if(key_name.isEmpty())
    {
        ln_key_name_.setFocus();
        ln_key_name_.setStyleSheet("QLineEdit {border-style: solid;"
                                   "border-width: 1px;"
                                   "border-color: red;}");
        QToolTip::showText(ln_key_name_.mapToGlobal(QPoint()), tr("Пожалуйста, введите имя ключа"));
        return;
    }

    if(cb_prov_type_.currentText() == "CNG")
    {
        return GenerateCNGKey(prov_type, key_len, key_name);
    }
    else
    {
        return GenerateCAPIKey(prov_type, key_len, key_name);
    }
}

void GenKeyWizard::GenerateCAPIKey(const unsigned long prov_type, const unsigned long key_len,
                                   const QString &key_name)
{
    DWORD flags = is_local_machine_ ? CRYPT_MACHINE_KEYSET | CRYPT_NEWKEYSET
                                    : CRYPT_NEWKEYSET;
    NTCryptoManager ntcmgr(key_name, prov_type, flags);
    if(!ntcmgr.Init())
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось сгенерировать ключевую пару, код ошибки: ")
                              + QString::number(GetLastError()), QMessageBox::Ok);
        return;
    }

    if(!ntcmgr.GenerateKeyPair(AT_SIGNATURE, (key_len << 16)))
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось сгенерировать ключевую пару, код ошибки: ")
                              + QString::number(GetLastError()), QMessageBox::Ok);
        this->close();
    }
    else
    {
        QMessageBox::information(this, tr("Информация"), tr("Ключевая пара успешно сгенерирована"),
                                 QMessageBox::Ok);
       emit this->accept();
       this->close();
    }
}

void GenKeyWizard::GenerateCNGKey(const unsigned long prov_type, const unsigned long key_len,
                                  const QString &key_name)
{
#ifndef _WIN_XP_
    DWORD flags = is_local_machine_ ? NCRYPT_MACHINE_KEY_FLAG : 0;

    CngManager cngmgr;
    SECURITY_STATUS status = cngmgr.Init();
    if(status != ERROR_SUCCESS)
    {
        ShowStatusMessage(status);
        this->close();
    }

    std::wstring alg_id = CNG_CONSTANTS::algorithms_id.value(QPair<DWORD, DWORD>(prov_type, key_len));

    status = cngmgr.GenerateKeyPair(alg_id, key_name, flags);
    if(status != ERROR_SUCCESS)
    {
        ShowStatusMessage(status);
        this->close();
    }

    ShowStatusMessage(status);
    emit this->accept();
    this->close();
#else
    Q_UNUSED(prov_type);
    Q_UNUSED(key_len);
    Q_UNUSED(key_name);

#endif // _WIN_XP_
}

QString GenKeyWizard::GetKeyName()
{
    return ln_key_name_.text();
}

void GenKeyWizard::ChangeLineBorderColor(QString)
{

    static_cast<QLineEdit*>(QObject::sender())->setStyleSheet(
                            "QLineEdit {border-style: solid;"
                               "border-width: 1px;"
                               "border-color: green;}");
}

void GenKeyWizard::ShowStatusMessage(const unsigned long status)
{
    if(status != ERROR_SUCCESS)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось сгенерировать ключевую пару, код ошибки: ")
                              + QString::number(status), QMessageBox::Ok);
    }
    else
    {
        QMessageBox::information(this, tr("Информация"), tr("Ключевая пара успешно сгенерирована"),
                                 QMessageBox::Ok);
    }
}

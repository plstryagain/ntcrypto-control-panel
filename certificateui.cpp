#include "certificateui.h"
#include "ui_certificateui.h"
#include "icertificatemanager.h"
#include <QToolTip>
#include <QMouseEvent>
#include <QDebug>

CertificateUI::CertificateUI(QWidget *parent, const CERT_UI_TYPE ct,
                             const std::unique_ptr<ICertificateManager>& cmgr) :
    QWidget(parent),
    ui(new Ui::CertificateUI),
    ct_(ct),
    cmgr_(cmgr)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setWindowIcon(QIcon(":/images/images/Accreditation_32px.png"));
    this->setWindowFlags(Qt::Window);
    ui->tw_composition->setColumnCount(2);
    ui->tw_composition->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Поле")));
    ui->tw_composition->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Значение")));
    QHeaderView* h = ui->tw_composition->horizontalHeader();
    h->setSectionResizeMode(0, QHeaderView::Fixed);
    h->resizeSection(0, 150);
    h->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->tw_composition->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tw_composition->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tw_composition->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->tw_composition->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tw_composition->horizontalHeader()->setStretchLastSection(true);
    ui->tw_composition->verticalHeader()->setVisible(false);
    //ui->tw_composition->resizeRowsToContents();
    ui->lb_issuer_name->setWordWrap(true);
    ui->lb_subject_name->setWordWrap(true);

    if(ct == CERT_UI_TYPE::X509Certificate){
        this->setWindowTitle(tr("Сертификат"));
        ui->tw_attributes->setVisible(false);
        ui->gb_attributes->setVisible(false);
    } else if(ct == CERT_UI_TYPE::AttributeCertificate){
        this->setWindowTitle(tr("Атрибутный сертификат"));
        ui->lb_has_priv_key->setVisible(false);
        ui->lb_has_priv_key_icon->setVisible(false);
        ui->lb_issuer->setVisible(false);
        ui->lb_issuer_name->setVisible(false);
        ui->lb_subject->setVisible(false);
        ui->lb_subject_name->setVisible(false);
        ui->tw_attributes->setColumnCount(2);
        ui->tw_attributes->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Поле")));
        ui->tw_attributes->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Значение")));
        h = ui->tw_attributes->horizontalHeader();
        h->setSectionResizeMode(0, QHeaderView::Fixed);
        h->resizeSection(0, 150);
        h->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        ui->tw_attributes->setSelectionBehavior(QAbstractItemView::SelectRows);
        ui->tw_attributes->setSelectionMode(QAbstractItemView::SingleSelection);
        ui->tw_attributes->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui->tw_attributes->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->tw_attributes->horizontalHeader()->setStretchLastSection(true);
        ui->tw_attributes->verticalHeader()->setVisible(false);
        ui->tw_attributes->resizeRowsToContents();
    }
}

CertificateUI::~CertificateUI()
{
    delete ui;
}

void CertificateUI::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    if(ct_ == CERT_UI_TYPE::X509Certificate){
        SystemCertificateManager* scmgr = dynamic_cast<SystemCertificateManager*>(cmgr_.get());
        if(!scmgr->IsPrivateKeyForCertExist()){
            ui->lb_has_priv_key->setVisible(false);
            ui->lb_has_priv_key_icon->setVisible(false);
        }
        pChain_ = scmgr->GetCertificateChain();
        if(!pChain_){
            ui->lb_cert_icon->setPixmap(QPixmap(":/images/images/Certificate_unknown2_50px.png"));
            ui->lb_cert_status->setText("Не удалось построить цепочку сертификатов.");
        } else {
            PCCERT_CHAIN_CONTEXT pChain = static_cast<PCCERT_CHAIN_CONTEXT>(pChain_);
            if(pChain->rgpChain[0]->TrustStatus.dwErrorStatus == CERT_TRUST_NO_ERROR){
                ui->lb_cert_icon->setPixmap(QPixmap(":/images/images/Certificate_ok_50px.png"));
            } else if((pChain->rgpChain[0]->TrustStatus.dwErrorStatus & CERT_TRUST_IS_PARTIAL_CHAIN)){
                ui->lb_cert_icon->setPixmap(QPixmap(":/images/images/Certificate_unknown2_50px.png"));
            } else {
                ui->lb_cert_icon->setPixmap(QPixmap(":/images/images/Certificate_invalid_50px.png"));
            }
            ui->lb_cert_status->setText(scmgr->CertTrustStatusToString(pChain->rgpChain[0]->TrustStatus));
        }
        ui->lb_subject_name->setText(scmgr->GetCertSubjectSimpleName());
        ui->lb_issuer_name->setText(scmgr->GetCertIssuerSimpleName());
    } else if(ct_ == CERT_UI_TYPE::AttributeCertificate){
        AttributeCertificateManager* acm = dynamic_cast<AttributeCertificateManager*>(cmgr_.get());
        if(!acm){
            return;
        }
        pChain_ = acm->GetCertificateChain();
        QString status = "";
        if(!acm->CheckSignature(&status)){
            if(!pChain_){
                ui->lb_cert_icon->setPixmap(QPixmap(":/images/images/Certificate_unknown2_50px.png"));
            } else {
                ui->lb_cert_icon->setPixmap(QPixmap(":/images/images/Certificate_invalid_50px.png"));
            }
        } else {
            ui->lb_cert_icon->setPixmap(QPixmap(":/images/images/Certificate_ok_50px.png"));
        }
        ui->lb_cert_status->setText(status);
        int row_count = 0;
        auto oid_list = acm->GetAttributeOidList();
        for(const auto& oid : oid_list){
            ui->tw_attributes->setRowCount(row_count + 1);
            QTableWidgetItem* item = new QTableWidgetItem(acm->GetAttributeNameString(oid));
            item->setIcon(QIcon(":/images/images/cert_prop_blue32px.png"));
            ui->tw_attributes->setItem(row_count, 0, item);
            ui->tw_attributes->item(row_count, 0)->setToolTip(acm->GetAttributeNameString(oid));
            ui->tw_attributes->setItem(row_count, 1, new QTableWidgetItem(acm->GetAttributeValueString(oid)));
            ui->tw_attributes->setRowHeight(row_count, 20);
            ++row_count;
        }

    } else {
        return;
    }
    ui->lb_not_before_val->setText(cmgr_->GetNotBefore());
    ui->lb_not_after_val->setText(cmgr_->GetNotAfter());
}


void CertificateUI::on_btn_ok_clicked()
{
    this->close();
}



void CertificateUI::on_tabw_about_cert_currentChanged(int index)
{
    switch(index){
    case 0:
        break;
    case 1:
        return FillCompositeTable();
    case 2:
        return FillCertificateChainTree();
    default:
        return;
    }
}

void CertificateUI::FillCompositeTable()
{
    switch(ct_){
    case CERT_UI_TYPE::AttributeCertificate:
        return FillAttributeCertCompositeTable();
    case CERT_UI_TYPE::X509Certificate:
        return FillX509CertCompositeTable();
    default:
        return;
    }
}

void CertificateUI::FillCertificateChainTree()
{
    if(is_chain_already_build_){
        return;
    }
    PCCERT_CHAIN_CONTEXT pChain = static_cast<PCCERT_CHAIN_CONTEXT>(pChain_);
    if(!pChain){
        QTreeWidgetItem* item = new QTreeWidgetItem(ui->tree_cert_chain);
        QString name = dynamic_cast<AttributeCertificateManager*>(cmgr_.get())->GetHolderCertificateSerialNumber();
        item->setText(0, name);
        item->setIcon(0, QIcon(QString::fromUtf8(":/images/images/Diploma_unknown_24px.png")));
        return;
    }
    ui->tree_cert_chain->header()->setVisible(false);
    QTreeWidgetItem* parent = nullptr;
    for(int i = pChain->rgpChain[0]->cElement - 1; i >= 0; --i){
        QTreeWidgetItem* item = nullptr;
        if(!parent){
            item = new QTreeWidgetItem(ui->tree_cert_chain);
        } else {
             item = new QTreeWidgetItem(parent);
        }
        QString name = GetSimpleSubjectName(pChain->rgpChain[0]->rgpElement[i]->pCertContext);
        item->setText(0, name);
        item->setToolTip(0, name);
        item->setData(0, Qt::ItemDataRole::UserRole, QVariant(i));
        if(pChain->rgpChain[0]->rgpElement[i]->TrustStatus.dwErrorStatus == CERT_TRUST_NO_ERROR){
            item->setIcon(0, QIcon(QString::fromUtf8(":/images/images/Diploma_ok_24px.png")));
        } else {
            item->setIcon(0, QIcon(QString::fromUtf8(":/images/images/Diploma_24px.png")));
        }
        parent = item;
    }
    if(ct_ == CERT_UI_TYPE::AttributeCertificate){
        QTreeWidgetItem* item = new QTreeWidgetItem(parent);
        QString name = dynamic_cast<AttributeCertificateManager*>(cmgr_.get())->GetHolderCertificateSerialNumber();
        item->setText(0, name);
    }
    pChain_ = pChain;
    is_chain_already_build_ = true;
    ui->tree_cert_chain->expandAll();
}

void CertificateUI::FillAttributeCertCompositeTable()
{
    if(is_compos_table_already_fill_){
        return;
    }
    int row_count = 9;
    ui->tw_composition->setRowCount(row_count);
    ui->tw_composition->setItem(0, 0, new QTableWidgetItem(tr("Версия")));
    ui->tw_composition->setItem(1, 0, new QTableWidgetItem(tr("Серийный номер")));
    ui->tw_composition->setItem(2, 0, new QTableWidgetItem(tr("Алгоритм подписи")));
    ui->tw_composition->setItem(3, 0, new QTableWidgetItem(tr("Хэш-алгоритм подписи")));
    ui->tw_composition->setItem(4, 0, new QTableWidgetItem(tr("Поставщик")));
    ui->tw_composition->setItem(5, 0, new QTableWidgetItem(tr("Действителен с")));
    ui->tw_composition->setItem(6, 0, new QTableWidgetItem(tr("Действителен по")));
    ui->tw_composition->setItem(7, 0, new QTableWidgetItem(tr("Поставщик исходного сертификата")));
    ui->tw_composition->setItem(8, 0, new QTableWidgetItem(tr("Серийный номер исходного сертификата")));

    for(int i = 0; i < row_count; ++i){
        QTableWidgetItem* item = ui->tw_composition->item(i, 0);
        item->setIcon(QIcon(QString::fromUtf8(":/images/images/cert_prop_orange_32px.png")));
        item->setToolTip(item->text());
    }

    AttributeCertificateManager* acm = dynamic_cast<AttributeCertificateManager*>(cmgr_.get());
    if(!acm){
        return;
    }
    ui->tw_composition->setItem(0, 1, new QTableWidgetItem(acm->GetVersion()));
    ui->tw_composition->setItem(1, 1, new QTableWidgetItem(acm->GetSerialNumber()));
    ui->tw_composition->setItem(2, 1, new QTableWidgetItem(acm->GetSignatureAlgName()));
    ui->tw_composition->setItem(3, 1, new QTableWidgetItem(acm->GetHashAlgName()));
    QString is = acm->GetCertIssuerName();
    is.replace("\r", " \r");
    ui->tw_composition->setItem(4, 1, new QTableWidgetItem(is));
    ui->tw_composition->setItem(5, 1, new QTableWidgetItem(acm->GetNotBefore()));
    ui->tw_composition->setItem(6, 1, new QTableWidgetItem(acm->GetNotAfter()));
    QString holder = acm->GetCertHolderName();
    holder.replace("\r", " \r");
    ui->tw_composition->setItem(7, 1, new QTableWidgetItem(holder));
    ui->tw_composition->setItem(8, 1, new QTableWidgetItem(acm->GetHolderCertificateSerialNumber()));

    auto att_oid_list = acm->GetAttributeOidList();
    for(const auto& oid : att_oid_list){
        ui->tw_composition->setRowCount(row_count + 1);
        QTableWidgetItem* item = new QTableWidgetItem(acm->GetAttributeNameString(oid));
        item->setIcon(QIcon(QString::fromUtf8(":/images/images/cert_prop_blue32px.png")));
        ui->tw_composition->setItem(row_count, 0, item);
        ui->tw_composition->item(row_count, 0)->setToolTip(acm->GetAttributeNameString(oid));
        ui->tw_composition->setItem(row_count, 1, new QTableWidgetItem(acm->GetAttributeValueString(oid)));
        ++row_count;
    }

    auto oid_list = acm->GetExtensionOidList();
    for(const auto& oid : oid_list){
        ui->tw_composition->setRowCount(row_count + 1);
        QTableWidgetItem* item = new QTableWidgetItem(acm->GetExtansionNameString(oid));
        item->setIcon(QIcon(QString::fromUtf8(":/images/images/cert_ext_32px.png")));
        ui->tw_composition->setItem(row_count, 0, item);
        ui->tw_composition->item(row_count, 0)->setToolTip(acm->GetExtansionNameString(oid));
        ui->tw_composition->setItem(row_count, 1, new QTableWidgetItem(acm->GetExtensionValueString(oid)));
        ++row_count;
    }
    for(auto i = 0; i < row_count; ++i){
        ui->tw_composition->setRowHeight(i, 20);
    }
    is_compos_table_already_fill_ = true;
}

void CertificateUI::FillX509CertCompositeTable()
{
    if(is_compos_table_already_fill_){
        return;
    }
    int row_count = 10;
    ui->tw_composition->setRowCount(row_count);
    ui->tw_composition->setItem(0, 0, new QTableWidgetItem(tr("Версия")));
    ui->tw_composition->setItem(1, 0, new QTableWidgetItem(tr("Серийный номер")));
    ui->tw_composition->setItem(2, 0, new QTableWidgetItem(tr("Алгоритм подписи")));
    ui->tw_composition->setItem(3, 0, new QTableWidgetItem(tr("Хэш-алгоритм подписи")));
    ui->tw_composition->setItem(4, 0, new QTableWidgetItem(tr("Издатель")));
    ui->tw_composition->setItem(5, 0, new QTableWidgetItem(tr("Действителен с")));
    ui->tw_composition->setItem(6, 0, new QTableWidgetItem(tr("Действителен по")));
    ui->tw_composition->setItem(7, 0, new QTableWidgetItem(tr("Субъект")));
    ui->tw_composition->setItem(8, 0, new QTableWidgetItem(tr("Открытый ключ")));
    ui->tw_composition->setItem(9, 0, new QTableWidgetItem(tr("Параметры открытого ключа")));

    for(int i = 0; i < row_count; ++i){
        QTableWidgetItem* item = ui->tw_composition->item(i, 0);
        item->setIcon(QIcon(QString::fromUtf8(":/images/images/cert_prop_orange_32px.png")));
        item->setToolTip(item->text());
    }

    SystemCertificateManager* scm = dynamic_cast<SystemCertificateManager*>(cmgr_.get());
    if(!scm){
        return;
    }

    ui->tw_composition->setItem(0, 1, new QTableWidgetItem(scm->GetVersion()));
    ui->tw_composition->setItem(1, 1, new QTableWidgetItem(scm->GetSerialNumber()));
    ui->tw_composition->setItem(2, 1, new QTableWidgetItem(scm->GetSignatureAlgName()));
    ui->tw_composition->setItem(3, 1, new QTableWidgetItem(scm->GetHashAlgName()));
    QString is = scm->GetCertIssuerName();
    is.replace("\r", " \r");
    ui->tw_composition->setItem(4, 1, new QTableWidgetItem(is));
    ui->tw_composition->setItem(5, 1, new QTableWidgetItem(scm->GetNotBefore()));
    ui->tw_composition->setItem(6, 1, new QTableWidgetItem(scm->GetNotAfter()));
    QString subj = scm->GetCertSubjectName();
    subj.replace("\r"," \r");
    ui->tw_composition->setItem(7, 1, new QTableWidgetItem(subj));
    ui->tw_composition->setItem(8, 1, new QTableWidgetItem(scm->GetPublicKeyAlgName()));
    ui->tw_composition->setItem(9, 1, new QTableWidgetItem(scm->GetPublicKeyParamString()));

    auto oid_list = scm->GetExtensionOidList();
    for(const auto& oid : oid_list){
        ui->tw_composition->setRowCount(row_count + 1);
        QString ext_name = scm->GetExtansionNameString(oid);
        QTableWidgetItem* item = new QTableWidgetItem(ext_name);
        item->setIcon(QIcon(QString::fromUtf8(":/images/images/cert_ext_32px.png")));
        ui->tw_composition->setItem(row_count, 0, item);
        ui->tw_composition->item(row_count, 0)->setToolTip(ext_name);
        ui->tw_composition->setItem(row_count, 1, new QTableWidgetItem(scm->GetExtensionValueString(oid)));
        ++row_count;
    }

    for(auto i = 0; i < row_count; ++i){
        ui->tw_composition->setRowHeight(i, 20);
    }
    is_compos_table_already_fill_ = true;
}

void CertificateUI::on_tw_composition_itemSelectionChanged()
{
    int row = ui->tw_composition->currentRow();
    const QTableWidgetItem* item = ui->tw_composition->item(row, 1);
    if(!item){
        return;
    }
    if(ct_ == CERT_UI_TYPE::X509Certificate){
        if(row == 8){
            SystemCertificateManager* scm = dynamic_cast<SystemCertificateManager*>(cmgr_.get());
            ui->tb_value->setText(scm->GetPublicKeyValueString());
        } else {
             ui->tb_value->setText(item->text());
        }
    } else {
        ui->tb_value->setText(item->text());
    }
}

QString CertificateUI::GetSimpleSubjectName(const void *pCert) const
{
    QVector<wchar_t> name;
    DWORD name_size = CertGetNameString(static_cast<PCCERT_CONTEXT>(pCert), CERT_NAME_SIMPLE_DISPLAY_TYPE,
                                        0, NULL, nullptr, 0);
    name.resize(name_size);
    CertGetNameString(static_cast<PCCERT_CONTEXT>(pCert), CERT_NAME_SIMPLE_DISPLAY_TYPE,
                      0, NULL, name.data(), name_size);
    return QString::fromWCharArray(name.data());
}

void CertificateUI::on_tree_cert_chain_itemSelectionChanged()
{
    QTreeWidgetItem* item = ui->tree_cert_chain->currentItem();
    int i = item->data(0, Qt::UserRole).value<int>();
    if(i == 0){
        ui->btn_view_cert->setEnabled(false);
    } else {
        ui->btn_view_cert->setEnabled(true);
    }
    PCCERT_CHAIN_CONTEXT pChain = static_cast<PCCERT_CHAIN_CONTEXT>(pChain_);
    QString status = cmgr_->CertTrustStatusToString(pChain->rgpChain[0]->rgpElement[i]->TrustStatus);
    ui->tb_cert_status->setText(status);
}

void CertificateUI::on_btn_view_cert_clicked()
{
    QTreeWidgetItem* item = ui->tree_cert_chain->currentItem();
    int i = item->data(0, Qt::UserRole).value<int>();
    PCCERT_CHAIN_CONTEXT pChain = static_cast<PCCERT_CHAIN_CONTEXT>(pChain_);
    PCCERT_CONTEXT pCert = pChain->rgpChain[0]->rgpElement[i]->pCertContext;
    auto crtmgr = ICertificateManager::Create(CERTIFICATE_TYPE::SystemCertificate, pCert, false);
    CertificateUI* cui = new CertificateUI(this, CERT_UI_TYPE::X509Certificate, std::move(crtmgr));
    cui->show();
    QEventLoop loop;
    connect(cui, SIGNAL(destroyed()), &loop, SLOT(quit()));
    loop.exec();
}

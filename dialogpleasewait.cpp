#include "dialogpleasewait.h"
#include "ui_dialogpleasewait.h"
#include <QStyleFactory>
#include <QKeyEvent>
#include <QDebug>

DialogPleaseWait::DialogPleaseWait(QWidget *parent) :
    parent_(parent),
    ui(new Ui::DialogPleaseWait)
{
    ui->setupUi(this);
    this->setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    this->setWindowTitle("Выполнение операции");
    ui->pb_status->setStyle(QStyleFactory::create("Fusion"));
    ui->pb_status->setMaximum(0);
    ui->pb_status->setMinimum(0);
    ui->lb_num_files->setVisible(false);
}

DialogPleaseWait::~DialogPleaseWait()
{
    delete ui;
}

void DialogPleaseWait::showEvent(QShowEvent *event)
{
    Q_UNUSED(event)
    QRect parentRect(parent_->mapToGlobal(QPoint(0, 0)), parent_->size());
    this->move(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), parentRect).topLeft());
}

void DialogPleaseWait::keyPressEvent(QKeyEvent *e)
{
    if(e->key() != Qt::Key_Escape){
        QDialog::keyPressEvent(e);
    }
}

void DialogPleaseWait::SetAmountOfFiles(const int amout_of_files)
{
    amount_of_files_ = amout_of_files;
    ui->lb_num_files->setVisible(true);
    ui->lb_num_files->setText("("+QString::number(0)+
                              " из "+QString::number(amount_of_files_)+")");
}

void DialogPleaseWait::ProgressUpdate()
{
    ++num_of_proc_files_;
    ui->lb_num_files->setText("("+QString::number(num_of_proc_files_)+
                              " из "+QString::number(amount_of_files_)+")");
}

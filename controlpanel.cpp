#include "controlpanel.h"
#include "ui_controlpanel.h"


#include <QDebug>
#include <QMessageBox>
#include <QStandardPaths>
#include <QFileInfo>
#include <QDialog>
#include <QComboBox>
#include <QFormLayout>
#include <QFile>
#include <string>
#include <QTextBrowser>
#include <QDesktopServices>
#include <QTextStream>
#include <QFileDialog>
#include <QDir>
#include <QTextCodec>
#include <QMenu>
#include "keyregisterwizard.h"
#include "ntservicemanager.h"
#include "genkeywizard.h"
#include "selftestwizard.h"
#include "Constants.h"
#include "downloadcrlwizard.h"
#include "crldpwizard.h"
#include "importcertificatewizard.h"
#include "keylinkmanager.h"
#include "certificateui.h"
#include "certificatestoremanager.h"
#include "icertificatemanager.h"
#include "crlui.h"
#include "hashoperationwizard.h"
#include "sigoperationwizard.h"
#include "certificaterequestwizard.h"
#include "sigverifyoperationwizard.h"
#include "encryptoperationwizard.h"
#include "integritycontrol.h"
#include "dialogpleasewait.h"
#include <QStyleFactory>
#include <map>

const std::map<STORAGE_TYPE, int> storage_to_index_table = {
    {STORAGE_TYPE::FileSystem, 0},
    {STORAGE_TYPE::Sigma, 1},
    {STORAGE_TYPE::NTStore, 2}
};

ControlPanel::ControlPanel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ControlPanel),
    MENU_STATE_FLAGS(0)
{
    ui->setupUi(this);

    qApp->setStyle(QStyleFactory::create("Fusion"));

    ui->btn_pref_apply->setText(tr("Применить"));
    ui->btn_pref_cancel->setText(tr("Отмена"));
    ui->btn_pref_ok->setText(tr("Ок"));

    ui->stacked_widget_main->setCurrentIndex(0);
    emit ui->stacked_widget_main->currentChanged(0);
    ui->list_widget_menu->setCurrentRow(0);

    ui->lb_page_caption->setStyleSheet("font-size: 10pt;");
    ui->lb_page_caption->setText(tr("Общие сведения"));

    QStringList lw_labels = QStringList() << "Общие сведения" << "Настройки" << "Ключевые контейнеры"
                                          << "Сертификаты" << "Состав" << "Службы"
                                          << "Криптографические\nоперации";
    int cur_row = 0;
    for(const auto& label : lw_labels){
        QListWidgetItem *item = new QListWidgetItem(label);
//        if(cur_row % 2 != 0)
//            item->setBackground(Qt::lightGray);
        ui->list_widget_menu->addItem(item);
        ++cur_row;
    }

    //ui->lb_version->setStyleSheet("font-size: 10pt");
    //GetCurrentVersion();
    this->setWindowTitle(tr("Панель управления криптопровайдером NTCrypto"));
    ui->lb_version_app->setText(QString("%1.%2.%3").arg(VERSION_MAJOR).arg(VERSION_MINOR).arg(VERSION_BUILD));
    this->setWindowIcon(QIcon(QString::fromUtf8(":/images/images/Administrative Tools_50px.png")));
    ui->btn_import_key->setVisible(false);
}

ControlPanel::~ControlPanel()
{
    delete ui;
}

void ControlPanel::GetCurrentVersion()
{
//    NTCryptoManager ntprovider("", Constants::BIGN_CSP_TYPE, CRYPT_VERIFYCONTEXT);
//    if(!ntprovider.Init())
//    {
//        ui->lb_version->setText(tr("Версия: не известна"));
//    }
//    else
//    {
//        PROV_VERSION_EX prov_version = {0};
//        DWORD size = sizeof(prov_version);
//        if(!ntprovider.GetProvParam(Constants::PP_VERSION_EX, (BYTE*)&prov_version, &size, 0))
//        {
//            ui->lb_version->setText(tr("Версия: не известна"));
//        }
//        else
//        {
//            QString str = "Версия: ";
//            str.append(QString::number(prov_version.version_major) + ".");
//            str.append(QString::number(prov_version.version_minor) + ".");
//            str.append(QString::number(prov_version.version_revision) + ".");
//            str.append(QString::number(prov_version.version_build));
//            ui->lb_version->setText(str);
//        }
//    }
}

void ControlPanel::on_list_widget_menu_itemClicked(QListWidgetItem *item)
{
    Q_UNUSED(item);
   ui->stacked_widget_main->setCurrentIndex(ui->list_widget_menu->currentRow());
}

NTSTATUS ControlPanel::LoadLogFilePath(const bool is_cng_log)
{
    QVector<BYTE> buff;
    QString option = is_cng_log ? RegConstants::CNG_LOG_FILE_PATH_OPTION :
                                  RegConstants::LOG_FILE_PATH_OPTION;
    NTSTATUS status = LoadOption(option, &buff);
    if(status != ERROR_SUCCESS)
    {
        return status;
    }

    curr_log_file_path_ = QString::fromWCharArray(reinterpret_cast<wchar_t*>(buff.data()));
    if(is_cng_log)
    {
        ui->ln_log_cng_path->setText(curr_log_file_path_);
    }
    else
    {
        ui->ln_log_path->setText(curr_log_file_path_);
    }
    return ERROR_SUCCESS;
}



NTSTATUS ControlPanel::LoadOption(const QString& option, QVector<BYTE>* data)
{
    RegistryManager regmgr;
    NTSTATUS status = NTE_FAIL;


    status = regmgr.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);
    if (status != ERROR_SUCCESS)
    {
        return status;
    }

    status = regmgr.OpenKey(RegConstants::DEFAULT_PROVIDER_OPTIONS_REG_PATH,
                            KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);
    if (status != ERROR_SUCCESS)
    {
        return status;
    }

    DWORD size = 0;
    status = regmgr.QueryValue(option, nullptr, &size);
    if (status != ERROR_SUCCESS)
    {
        return status;
    }

    data->resize(size);
    status = regmgr.QueryValue(option, data->data(), &size);
    if (status != ERROR_SUCCESS)
    {
        return status;
    }

    return ERROR_SUCCESS;
}

NTSTATUS ControlPanel::LoadDefaultStorageValue()
{
    QVector<BYTE> buff;
    NTSTATUS status = LoadOption(RegConstants::DEFAULT_STORAGE_OPTION, &buff);
    if(status != ERROR_SUCCESS)
    {
        return status;
    }

    DWORD default_storage = *reinterpret_cast<BYTE*>(buff.data());
    auto it = storage_to_index_table.find(static_cast<STORAGE_TYPE>(default_storage));
    if(it == storage_to_index_table.end()){
        curr_default_storage_ = 1;
        ui->cb_default_prov->setCurrentIndex(0);
    } else {
        curr_default_storage_ = static_cast<int>(default_storage);
        ui->cb_default_prov->setCurrentIndex(it->second);
    }
    return ERROR_SUCCESS;
}

bool ControlPanel::IsRegOptionsExist()
{
    RegistryManager regmgr;
    NTSTATUS status = regmgr.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);
    if(status != ERROR_SUCCESS)
    {
        return status;
    }

    status = regmgr.OpenKey(RegConstants::DEFAULT_PROVIDER_OPTIONS_REG_PATH,
                            KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);
    return (status == ERROR_SUCCESS) ? true : false;
}

NTSTATUS ControlPanel::CreateDefaultRegOptions()
{
   RegistryManager regmgr;
   NTSTATUS status = regmgr.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);
   if(status != ERROR_SUCCESS)
   {
       return status;
   }

   status = regmgr.CreateKey(RegConstants::DEFAULT_PROVIDER_OPTIONS_REG_PATH);
   if(status != ERROR_SUCCESS)
   {
       return status;
   }

   DWORD default_storage = static_cast<DWORD>(STORAGE_TYPE::FileSystem);
   status = regmgr.SetKeyValue(nullptr, RegConstants::DEFAULT_STORAGE_OPTION, REG_DWORD,
                               reinterpret_cast<BYTE*>(&default_storage), sizeof(default_storage));
   if(status != ERROR_SUCCESS)
   {
       return status;
   }

    QString log_path = QStandardPaths::standardLocations(QStandardPaths::HomeLocation).join("") +
                        "/AppData/Roaming/NII TZI/NTCrypto/ntcrypto_log.txt";
    QVector<wchar_t> buff((log_path.size() + 1)*sizeof(wchar_t));
    log_path.toWCharArray(buff.data());

    status = regmgr.SetKeyValue(nullptr, RegConstants::LOG_FILE_PATH_OPTION, REG_SZ,
                                reinterpret_cast<BYTE*>(buff.data()), static_cast<DWORD>(buff.size()));
    if(status != ERROR_SUCCESS)
    {
        return status;
    }

    return ERROR_SUCCESS;
}

void ControlPanel::CheckIntegrity()
{
    DWORD error = ERROR_SUCCESS;
    COMPARE_RESULT cr;
    CNG_COMPARE_RESULT ccr;
    DialogPleaseWait dpw(this);
    QThread* thread = new QThread();
    auto itc = std::unique_ptr<IntegrityControl>(new IntegrityControl());
    itc->moveToThread(thread);
    connect(thread, SIGNAL(started()), itc.get(), SLOT(CheckIntegrity()));
    connect(itc.get(), SIGNAL(signalFinished()), thread, SLOT(quit()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), &dpw, SLOT(accept()));
    thread->start();
    dpw.exec();

    error = itc->GetStatus();
    if(error != ERROR_SUCCESS){
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось проверит целостность файлов, код ошибки: ")+
                              QString::number(error), QMessageBox::Ok);
        return;
    }
    cr = itc->GetResult();
    ccr = itc->GetCngResult();
    std::wstring installed_path = L"";
    error = FindInstalledPath(&installed_path);
    if(error != ERROR_SUCCESS){
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось прочитать значение из реестра, код ошибки: ")+
                              QString::number(error), QMessageBox::Ok);
        return;
    }
    QString qpath = QString::fromStdWString(installed_path);
    int curr_row = 0;
    bool is_win64 = IsWin64bit();
    if(is_win64)
    {
#ifdef _WIN_XP_
        ui->tw_modules->setRowCount(16);
#else
        ui->tw_modules->setRowCount(24);
#endif
    }
    else
    {
#ifdef _WIN_XP_
        ui->tw_modules->setRowCount(8);
#else
        ui->tw_modules->setRowCount(12);
#endif
    }

    FillModulesTable(&cr, is_win64, &curr_row, qpath);
    FillCngModulesTable(&ccr, is_win64, curr_row, qpath);
    ui->tw_modules->horizontalHeader()->setStretchLastSection(true);
    ui->tw_modules->resizeColumnsToContents();
    ui->tw_modules->resizeRowsToContents();
}

void ControlPanel::FillModulesTable(void *cr, const bool is_win64, int *curr_row_,
                                     const QString &qpath)
{
    int* res = static_cast<int*>(cr);
    int curr_row = *curr_row_;
    for(const auto& module : sys_modules_list)
    {
        QString path = is_win64 ? "C:\\Windows\\SysWOW64" : "C:\\Windows\\System32";
        ui->tw_modules->setItem(curr_row, 0, new QTableWidgetItem(module));
        ui->tw_modules->setItem(curr_row, 1, new QTableWidgetItem("x86"));
        QString version = GetModuleVersion(path + "\\" + module);
        ui->tw_modules->setItem(curr_row, 2, new QTableWidgetItem(version));
        if(*res == 1)
        {
            ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла не нарушена"));
            ui->tw_modules->item(curr_row, 3)->setBackgroundColor("light green");
        }
        else
        {
            ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла нарушена"));
            ui->tw_modules->item(curr_row, 3)->setBackgroundColor(QColor(254, 44, 70));

        }
        ui->tw_modules->setItem(curr_row, 4, new QTableWidgetItem(path));
        ++curr_row;
        res += 1;
    }

    for(const auto& module : modules_list)
    {
        ui->tw_modules->setItem(curr_row, 0, new QTableWidgetItem(module));
        ui->tw_modules->setItem(curr_row, 1, new QTableWidgetItem("x86"));
        QString version = GetModuleVersion(qpath + "\\x86\\" + module);
        ui->tw_modules->setItem(curr_row, 2, new QTableWidgetItem(version));
        if(*res == 1)
        {
            ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла не нарушена"));
            ui->tw_modules->item(curr_row, 3)->setBackgroundColor("light green");
        }
        else
        {
            ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла нарушена"));
            ui->tw_modules->item(curr_row, 3)->setBackgroundColor(QColor(254, 44, 70));
        }
        ui->tw_modules->setItem(curr_row, 4, new QTableWidgetItem(qpath));
        ++curr_row;
        res += 1;
    }

    if(is_win64)
    {
        for(const auto& module : sys_modules_list)
        {
            QString path = "C:\\Windows\\System32";
            ui->tw_modules->setItem(curr_row, 0, new QTableWidgetItem(module));
            ui->tw_modules->setItem(curr_row, 1, new QTableWidgetItem("x64"));
            QString version = GetModuleVersion(path + "\\" + module);
            ui->tw_modules->setItem(curr_row, 2, new QTableWidgetItem(version));
            if(*res == 1)
            {
                ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла не нарушена"));
                ui->tw_modules->item(curr_row, 3)->setBackgroundColor("light green");
            }
            else
            {
                ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла нарушена"));
                ui->tw_modules->item(curr_row, 3)->setBackgroundColor(QColor(254, 44, 70));
            }
            ui->tw_modules->setItem(curr_row, 4, new QTableWidgetItem(path));
            ++curr_row;
            res += 1;
        }

        for(const auto& module : modules_list)
        {
            ui->tw_modules->setItem(curr_row, 0, new QTableWidgetItem(module));
            ui->tw_modules->setItem(curr_row, 1, new QTableWidgetItem("x64"));
            QString version = GetModuleVersion(qpath + "\\x64\\" + module);
            ui->tw_modules->setItem(curr_row, 2, new QTableWidgetItem(version));
            if(*res == 1)
            {
                ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла не нарушена"));
                ui->tw_modules->item(curr_row, 3)->setBackgroundColor("light green");
            }
            else
            {
                ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла нарушена"));
                ui->tw_modules->item(curr_row, 3)->setBackgroundColor(QColor(254, 44, 70));
            }
            ui->tw_modules->setItem(curr_row, 4, new QTableWidgetItem(qpath));
            ++curr_row;
            res += 1;
        }
    }
    *curr_row_ = curr_row;
}

void ControlPanel::FillCngModulesTable(void* ccr, const bool is_win64, int curr_row, const QString &qpath)
{
#ifndef _WIN_XP_
    CNG_COMPARE_RESULT* ccr_ = static_cast<CNG_COMPARE_RESULT*>(ccr);
    int rv = 0;
    for(const auto& module : cng_sys_modules_list)
    {
        QString path = is_win64 ? "C:\\Windows\\SysWOW64" : "C:\\Windows\\System32";
        ui->tw_modules->setItem(curr_row, 0, new QTableWidgetItem(module));
        ui->tw_modules->setItem(curr_row, 1, new QTableWidgetItem("x86"));
        QString version = GetModuleVersion(path + "\\" + module);
        ui->tw_modules->setItem(curr_row, 2, new QTableWidgetItem(version));
        if(module == "ntcryptocng.dll")
        {
            rv = ccr_->ntcryptocng;
        }
        else
        {
            rv = ccr_->ntcryptoksp;
        }
        if(rv == 1)
        {
            ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла не нарушена"));
            ui->tw_modules->item(curr_row, 3)->setBackgroundColor("light green");
        }
        else
        {
            ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла нарушена"));
            ui->tw_modules->item(curr_row, 3)->setBackgroundColor(QColor(254, 44, 70));

        }
        ui->tw_modules->setItem(curr_row, 4, new QTableWidgetItem(path));
        ++curr_row;
    }

    for(const auto& module : cng_modules_list)
    {
        ui->tw_modules->setItem(curr_row, 0, new QTableWidgetItem(module));
        ui->tw_modules->setItem(curr_row, 1, new QTableWidgetItem("x86"));
        QString version = GetModuleVersion(qpath + "\\x86\\" + module);
        ui->tw_modules->setItem(curr_row, 2, new QTableWidgetItem(version));
        if(module == "ntcryptocngext.dll")
        {
            rv = ccr_->ntcryptocngext;
        }
        else
        {
            rv = ccr_->ntcngkeyisosrvc;
        }

        if(rv == 1)
        {
            ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла не нарушена"));
            ui->tw_modules->item(curr_row, 3)->setBackgroundColor("light green");
        }
        else
        {
            ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла нарушена"));
            ui->tw_modules->item(curr_row, 3)->setBackgroundColor(QColor(254, 44, 70));
        }
        ui->tw_modules->setItem(curr_row, 4, new QTableWidgetItem(qpath));
        ++curr_row;
    }

    if(is_win64)
    {
        for(const auto& module : cng_sys_modules_list)
        {
            QString path = "C:\\Windows\\System32";
            ui->tw_modules->setItem(curr_row, 0, new QTableWidgetItem(module));
            ui->tw_modules->setItem(curr_row, 1, new QTableWidgetItem("x64"));
            QString version = GetModuleVersion(path + "\\" + module);
            ui->tw_modules->setItem(curr_row, 2, new QTableWidgetItem(version));
            if(module == "ntcryptocng.dll")
            {
                rv = ccr_->ntcryptocng;
            }
            else
            {
                rv = ccr_->ntcryptoksp;
            }
            if(rv == 1)
            {
                ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла не нарушена"));
                ui->tw_modules->item(curr_row, 3)->setBackgroundColor("light green");
            }
            else
            {
                ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла нарушена"));
                ui->tw_modules->item(curr_row, 3)->setBackgroundColor(QColor(254, 44, 70));
            }
            ui->tw_modules->setItem(curr_row, 4, new QTableWidgetItem(path));
            ++curr_row;
        }

        for(const auto& module : cng_modules_list)
        {
            ui->tw_modules->setItem(curr_row, 0, new QTableWidgetItem(module));
            ui->tw_modules->setItem(curr_row, 1, new QTableWidgetItem("x64"));
            QString version = GetModuleVersion(qpath + "\\x64\\" + module);
            ui->tw_modules->setItem(curr_row, 2, new QTableWidgetItem(version));
            if(module == "ntcryptocngext.dll")
            {
                rv = ccr_->ntcryptocngext;
            }
            else
            {
                rv = ccr_->ntcngkeyisosrvc;
            }

            if(rv == 1)
            {
                ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла не нарушена"));
                ui->tw_modules->item(curr_row, 3)->setBackgroundColor("light green");
            }
            else
            {
                ui->tw_modules->setItem(curr_row, 3, new QTableWidgetItem("Целостность файла нарушена"));
                ui->tw_modules->item(curr_row, 3)->setBackgroundColor(QColor(254, 44, 70));
            }
            ui->tw_modules->setItem(curr_row, 4, new QTableWidgetItem(qpath));
            ++curr_row;
        }
    }
#endif
}

void ControlPanel::FillContainersTable()
{
    QTableWidget* tw = ui->tw_containers;

    tw->clear();
    tw->setColumnCount(2);
    tw->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Имя контейнера")));
    tw->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Носитель")));
    QHeaderView* h = tw->horizontalHeader();
    h->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    h->setSectionResizeMode(1, QHeaderView::Stretch);

    RegistryManager regmgr;
    NTSTATUS status = NTE_FAIL;

    if(ui->rb_user->isChecked())
    {
        status = regmgr.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);
        if(status != ERROR_SUCCESS)
        {
            return;
        }
    }
    else
    {
        status = regmgr.OpenDefaultUser();
    }

    status = regmgr.OpenKey(RegConstants::PROVIDER_KEYS_REG_PATH, KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);
    if(status != ERROR_SUCCESS)
    {
        return;
    }

    DWORD index = 0;

    while(status == ERROR_SUCCESS)
    {
        QVector<wchar_t> name(MAX_PATH);
        DWORD name_len = MAX_PATH;
        status = regmgr.EnumKeyEx(index, &name, &name_len);
        if(status == ERROR_SUCCESS)
        {
            tw->setRowCount(index + 1);
            tw->setItem(index, 0, new QTableWidgetItem(QString::fromWCharArray(name.data())));

            RegistryManager regmgr2;
            QString reg_key_path =  RegConstants::PROVIDER_KEYS_REG_PATH;
            reg_key_path.append(QString::fromWCharArray(name.data()));
            if(ui->rb_local_machine->isChecked())
            {
                regmgr2.OpenDefaultUser();
            }
            else
            {
                regmgr2.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);
            }

            regmgr2.OpenKey(reg_key_path, KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);

            DWORD storage_type = 0;
            DWORD size = sizeof(storage_type);
            regmgr2.QueryValue(RegConstants::KEY_STORAGE_OPTION,
                               reinterpret_cast<BYTE*>(&storage_type),
                               &size);

            switch(storage_type)
            {
            case 1:
                tw->setItem(index, 1, new QTableWidgetItem(tr("Жёсткий диск")));
                break;

            case 2:
                tw->setItem(index, 1, new QTableWidgetItem(tr("СКЗИ \"Сигма\"")));
                break;

            case 3:
                tw->setItem(index, 1, new QTableWidgetItem(tr("NTStore")));
                break;

            default:
                tw->setItem(index, 1, new QTableWidgetItem(tr("Неизвестный тип носителя")));
                break;
            }

        }
        ++index;
    }

}

QString ControlPanel::GetModuleVersion(const QString& path)
{
    DWORD dwHandle = 0;
    DWORD buff_len = GetFileVersionInfoSize(path.toStdWString().data(), &dwHandle);

    QVector<BYTE> buff(buff_len);
    if(!GetFileVersionInfo(path.toStdWString().data(), dwHandle, buff_len, buff.data()))
    {
        return "";
    }

    VS_FIXEDFILEINFO* info = nullptr;
    UINT info_len = 0;
    if(!VerQueryValue(buff.data(),
                      QString("\\").toStdWString().data(),
                      reinterpret_cast<LPVOID*>(&info),
                      &info_len))
    {
        return "";
    }
    else
    {
        return QString::number( (info->dwFileVersionMS >> 16 ) & 0xffff ) + "." +
               QString::number( ( info->dwFileVersionMS) & 0xffff ) + "." +
               QString::number( ( info->dwFileVersionLS >> 16 ) & 0xffff ) + "." +
               QString::number( ( info->dwFileVersionLS) & 0xffff );
    }
}

void ControlPanel::on_btn_cont_refresh_clicked()
{
    //ui->tw_containers->clear();

    FillContainersTable();

    ui->tw_containers->resizeRowsToContents();
}

void ControlPanel::on_btn_change_pass_clicked()
{
    ui->btn_change_pass->setEnabled(false);
    ChangePass();
    ui->btn_change_pass->setEnabled(true);
}

void ControlPanel::ChangePass()
{
    if(ui->tw_containers->currentRow() < 0)
    {
        QMessageBox::warning(this,
                             tr("Внимание"),
                             tr("Выберите ключ!"),
                             QMessageBox::Ok);
        return;
    }

    QString name = ui->tw_containers->item(ui->tw_containers->currentRow(), 0)->text();
    if(ui->tw_containers->item(ui->tw_containers->currentRow(), 1)->text().indexOf("Сигма") != -1 ||
       ui->tw_containers->item(ui->tw_containers->currentRow(), 1)->text().indexOf("NTStore") != -1)
    {
        QMessageBox::warning(this, tr("Внимание"), tr("Можно изменить пароль только для программного контейнера"), QMessageBox::Ok);
        return;
    }

    DWORD local_machine = ui->rb_local_machine->isChecked() ? CRYPT_MACHINE_KEYSET : 0;
    NTCryptoManager ntcryptomgr(name, Constants::BIGN_CSP_TYPE, local_machine);
    if(!ntcryptomgr.Init())
    {
        QMessageBox::critical(this,
                              tr("Ошибка"),
                              tr("Не удалось изменить пароль, ошибка: ")+
                              ErrorCodeToString(GetLastError()),
                              QMessageBox::Ok);
        return;
    }

    if(!ntcryptomgr.GetUserKey(AT_SIGNATURE))
    {
        QMessageBox::critical(this,
                              tr("Ошибка"),
                              tr("Не удалось изменить пароль, ошибка: ")+
                              ErrorCodeToString(GetLastError()),
                              QMessageBox::Ok);
        return;
    }

    if(!ntcryptomgr.SetKeyParam(KP_CHANGE_PASSWORD, nullptr, 0))
    {
        QMessageBox::critical(this,
                              tr("Ошибка"),
                              tr("Не удалось изменить пароль, ошибка: ")+
                              ErrorCodeToString(GetLastError()),
                              QMessageBox::Ok);
        return;
    }

    QMessageBox::information(this,
                             tr("Информация"),
                             tr("Пароль был успешно изменён"),
                             QMessageBox::Ok);
}

void ControlPanel::on_rb_user_toggled(bool checked)
{
    Q_UNUSED(checked);
    ui->tw_containers->clear();

    FillContainersTable();

    ui->tw_containers->resizeRowsToContents();
}

void ControlPanel::on_btn_del_container_clicked()
{
    ui->btn_del_container->setEnabled(false);
    if(QMessageBox::warning(this,
                            tr("Внимание"),
                            tr("Вы действительно хотите удалить выбранный ключ?"
                               "\nВосстановить его будет невозможно."),
                            QMessageBox::Ok | QMessageBox::Cancel) == QMessageBox::Ok)
    {
        DeleteContainer();
    }

    ui->btn_del_container->setEnabled(true);
}

void ControlPanel::DeleteContainer()
{
    if(ui->tw_containers->currentRow() < 0)
    {
        QMessageBox::warning(this,
                             tr("Внимание"),
                             tr("Выберите ключ!"),
                             QMessageBox::Ok);
        return;
    }

    QString name = ui->tw_containers->item(ui->tw_containers->currentRow(), 0)->text();
    DWORD local_machine = ui->rb_local_machine->isChecked() ? CRYPT_MACHINE_KEYSET : 0;
    NTCryptoManager ntcryptomgr(name, Constants::BIGN_CSP_TYPE, local_machine | CRYPT_DELETEKEYSET);
    if(!ntcryptomgr.Init())
    {
        QMessageBox::critical(this,
                              tr("Ошибка"),
                              tr("Не удалось удалить контейнер, ошибка: ")+
                              ErrorCodeToString(GetLastError()),
                              QMessageBox::Ok);
        return;
    }

    QMessageBox::information(this,
                             tr("Информация"),
                             tr("Контейнер был успешно удалён"),
                             QMessageBox::Ok);

    ui->tw_containers->clear();

    FillContainersTable();

    ui->tw_containers->resizeRowsToContents();
}

QString ControlPanel::ErrorCodeToString(const DWORD error_code)
{
    QString error_str = tr("Неизвестная ошибка");

    switch(error_code)
    {
    case NTE_FAIL:
        error_str = tr("Внутренняя ошибка");
        break;

    case ERROR_NOT_FOUND:
    case NTE_NOT_FOUND:
        error_str = tr("Файл не найден");
        break;

    case STATUS_INVALID_PARAMETER:
    case NTE_INVALID_PARAMETER:
        error_str = tr("Параметр задан неверно");
        break;

    case STATUS_INVALID_HANDLE:
    case NTE_INVALID_HANDLE:
        error_str = tr("Неправильный дескриптор");
        break;

    case NTE_EXISTS:
        error_str = tr("Файл уже существует");
        break;

    case NTE_DEVICE_NOT_FOUND:
        error_str = tr("Устройство не найдено");
        break;

    case RPC_S_SERVER_UNAVAILABLE:
        error_str = tr("Служба изоляции ключей не доступна");
        break;

    case ERROR_CANCELLED:
        error_str = tr("Отменено пользователем");
        break;

    default:
        break;
    }

    error_str.append(QString(" (0x%1)").arg(error_code, 1, 16));

    return error_str;
}

void ControlPanel::FillCertsList()
{
    ui->lw_certs->clear();
    if(csmgr_){
        csmgr_->CloseStore();
    }
    CERTIFICATE_STORE_TYPE store_type = static_cast<CERTIFICATE_STORE_TYPE>(ui->cb_cert_store->currentIndex());
    csmgr_ = CertificateStoreManager::Create(store_type);
    if(!csmgr_->OpenStore()){
        QMessageBox::critical(this,
                              tr("Ошибка"),
                              tr("Не удалось открыть хранилище сертификатов, код ошибки: ")+
                              QString::number(GetLastError()),
                              QMessageBox::Ok);
        return;
    }
    csmgr_->EnumCertificates();
    switch(store_type){
    case CERTIFICATE_STORE_TYPE::MY:
    case CERTIFICATE_STORE_TYPE::ROOT:
    case CERTIFICATE_STORE_TYPE::SIGMA:
    case CERTIFICATE_STORE_TYPE::NTSTORE:
        return FillSystemCertList();
    case CERTIFICATE_STORE_TYPE::CRL:
        return FillCrlList();
    case CERTIFICATE_STORE_TYPE::ATTRIBUTE:
        return FillAttributeCertList();
    default:
        return;
    }
}

void ControlPanel::FillSystemCertList()
{
    const auto& crtmgr_list = csmgr_->GetCertificateManagerList();
    int cur_row = 0;
    for(const auto& crtmgr : crtmgr_list){
        SystemCertificateManager* scmgr = dynamic_cast<SystemCertificateManager*>(crtmgr.get());
        QString status = "";
        DWORD err = CERT_TRUST_IS_PARTIAL_CHAIN;
        QString issuer = scmgr->GetCertIssuerSimpleName();
        QString subject = scmgr->GetCertSubjectSimpleName();
        QString str = tr("Издатель: ")+issuer+tr(" \nСубъект: ")+subject;
        if(scmgr->IsPrivateKeyForCertExist())
        {
            str.append(tr("\nЕсть личный ключ"));
        }
        PCCERT_CHAIN_CONTEXT pChain = scmgr->GetCertificateChain();
        if(!pChain){
            status = "Не удалось постоить цепочку сертификатов.";
        } else {
            err = pChain->TrustStatus.dwErrorStatus;
            status = scmgr->CertTrustStatusToString(pChain->TrustStatus);
        }
        str.append(tr("\nСостояние: ")+status);
        QListWidgetItem *item = new QListWidgetItem(str);
        if(cur_row % 2 != 0)
            item->setBackground(Qt::lightGray);
        if(err == CERT_TRUST_NO_ERROR){
            item->setForeground(Qt::darkGreen);
            //item->setIcon(QIcon(":/images/images/Certificate_ok_50px.png"));
        } else {
            item->setForeground(Qt::darkRed);
            //item->setIcon(QIcon(":/images/images/Certificate_invalid_50px.png"));
        }
        ui->lw_certs->addItem(item);
        ++cur_row;
    }
}

void ControlPanel::FillCrlList()
{
    const auto& crtmgr_list = csmgr_->GetCertificateManagerList();
    int cur_row = 0;
    for(const auto& crtmgr : crtmgr_list){
        CrlManager* crlmgr = dynamic_cast<CrlManager*>(crtmgr.get());
        QString info;
        bool is_expired = crlmgr->IsExpired();
        if(is_expired)
        {
            info = tr("\nСписок отозванных сертификатов устарел");
        }
        else
        {
            info = tr("\nСписок отозванных сертификатов актуален");
        }
        QString issuer = crlmgr->GetCertIssuerSimpleName();
        QString this_upd = crlmgr->GetNotBefore();
        QString next_upd = crlmgr->GetNotAfter();
        QString str = tr("Издатель: ") + issuer +
                tr("\nДействителен с: ")+this_upd +
                tr("\nСледующее обновление: ") + next_upd +
                info;

        QListWidgetItem *item = new QListWidgetItem(str);
        if(is_expired)
        {
            item->setForeground(Qt::darkRed);
        }
        else
        {
            item->setForeground(Qt::darkGreen);
        }
        if(cur_row % 2 != 0)
            item->setBackground(Qt::lightGray);
        ui->lw_certs->addItem(item);
        ++cur_row;
    }
}

void ControlPanel::FillAttributeCertList()
{
    const auto& crtmgr_list = csmgr_->GetCertificateManagerList();
    int cur_row = 0;
    for(const auto& crtmgr : crtmgr_list){
        AttributeCertificateManager* acm = dynamic_cast<AttributeCertificateManager*>(crtmgr.get());
        QString status = "";
        DWORD err = CERT_TRUST_IS_PARTIAL_CHAIN;
        QString holder = acm->GetCertHolderSimpleName();
        QString issuer = acm->GetCertIssuerSimpleName();
        QString str = tr("Поставщик: ")+issuer+tr(" \nПоставщик исходного сертификата: ")+holder;
        PCCERT_CHAIN_CONTEXT pChain = acm->GetCertificateChain();
        if(!pChain){
            status = "Не удалось постоить цепочку сертификатов.";
        } else {
            err = pChain->TrustStatus.dwErrorStatus;
            status = acm->CertTrustStatusToString(pChain->TrustStatus);
        }
        str.append(tr("\nСостояние: ")+status);
        QListWidgetItem *item = new QListWidgetItem(str);
        if(cur_row % 2 != 0)
            item->setBackground(Qt::lightGray);
        if(err == CERT_TRUST_NO_ERROR){
            item->setForeground(Qt::darkGreen);
        } else {
            item->setForeground(Qt::darkRed);
        }
        ui->lw_certs->addItem(item);
        ++cur_row;
    }
}

void ControlPanel::on_lw_certs_itemDoubleClicked(QListWidgetItem *item)
{
    Q_UNUSED(item);
    int row = ui->lw_certs->currentRow();
    if(row == -1){
        return;
    }
    const auto& crtmgr = csmgr_->GetCertificateManagerByIndex(static_cast<quint32>(row));
    CERT_UI_TYPE uitype = CERT_UI_TYPE::InvalidType;
    switch(static_cast<CERTIFICATE_STORE_TYPE>(ui->cb_cert_store->currentIndex())){
    case CERTIFICATE_STORE_TYPE::MY:
    case CERTIFICATE_STORE_TYPE::ROOT:
    case CERTIFICATE_STORE_TYPE::SIGMA:
    case CERTIFICATE_STORE_TYPE::NTSTORE:
        uitype = CERT_UI_TYPE::X509Certificate;
        break;
    case CERTIFICATE_STORE_TYPE::ATTRIBUTE:
        uitype = CERT_UI_TYPE::AttributeCertificate;
        break;
    case CERTIFICATE_STORE_TYPE::CRL:
    {
        CrlUI* crlui = new CrlUI(this, static_cast<CrlManager*>(crtmgr.get()));
        crlui->show();
        return;
    }
    default:
        return;
    }
    CertificateUI* cui = new CertificateUI(this, uitype, crtmgr);
    cui->show();
}


void ControlPanel::on_btn_show_log_clicked()
{
    return ShowLog(false);
}

void ControlPanel::on_btn_open_log_file_folder_clicked()
{
    return OpenLogFileFolder(false);
}

void ControlPanel::on_btn_clear_log_clicked()
{
    return ClearLog(false);
}

void ControlPanel::on_btn_import_cert_clicked()
{
//    QString filename = QFileDialog::getOpenFileName(this,
//                                                    tr("Выберите файл"),
//                                                    "",
//                                                    tr("Сертификат (*.crt *.pem *.der *.cer *.p7b);;Все файлы (*.*)"));
//    if(filename.isEmpty())
//    {
//        return;
//    }

    QStringList filenames = QFileDialog::getOpenFileNames(this,
                                                    tr("Выберите файлы"),
                                                    "",
                                                    tr("Сертификат (*.crt *.pem *.der *.cer *.p7b);;Все файлы (*.*)"));
    if(filenames.isEmpty())
    {
        return;
    }

    ImportCertificateWizard icw(this, filenames);
    if(icw.exec() == QDialog::Accepted){
        icw.ShowReport();
    }
    FillCertsList();
}

void ControlPanel::on_tw_containers_itemDoubleClicked(QTableWidgetItem *item)
{
    Q_UNUSED(item);
    QString name = ui->tw_containers->item(ui->tw_containers->currentRow(), 0)->text();
    DWORD local_machine = ui->rb_local_machine->isChecked() ? CRYPT_MACHINE_KEYSET : 0;
    NTCryptoManager ntcryptomgr(name, Constants::BIGN_CSP_TYPE, local_machine);
    if(!ntcryptomgr.Init())
    {
        QMessageBox::critical(this,
                              tr("Ошибка"),
                              tr("Не удалось получить информацию о ключе, ошибка: ")+
                              ErrorCodeToString(GetLastError()),
                              QMessageBox::Ok);
        return;
    }


    if(!ntcryptomgr.GetUserKey(AT_SIGNATURE))
    {
        QMessageBox::critical(this,
                              tr("Ошибка"),
                              tr("Не удалось получить информацию о ключе, ошибка: ")+
                              ErrorCodeToString(GetLastError()),
                              QMessageBox::Ok);
        return;
    }

    DWORD key_len = 0;
    DWORD temp = sizeof(key_len);
    if(!ntcryptomgr.GetKeyParam(KP_KEYLEN, reinterpret_cast<BYTE*>(&key_len), &temp, 0))
    {
        QMessageBox::critical(this,
                              tr("Ошибка"),
                              tr("Не удалось получить информацию о ключе, ошибка: ")+
                              ErrorCodeToString(GetLastError()),
                              QMessageBox::Ok);
        return;
    }

    ALG_ID alg_id = 0;
    temp = sizeof(alg_id);
    if(!ntcryptomgr.GetKeyParam(KP_ALGID, reinterpret_cast<BYTE*>(&alg_id), &temp, 0))
    {
        QMessageBox::critical(this,
                              tr("Ошибка"),
                              tr("Не удалось получить информацию о ключе, ошибка: ")+
                              ErrorCodeToString(GetLastError()),
                              QMessageBox::Ok);
        return;
    }

    DWORD blob_value_len = 0;
    if(!ntcryptomgr.ExportKey(PUBLICKEYBLOB, 0, nullptr, &blob_value_len))
    {
        QMessageBox::critical(this,
                              tr("Ошибка"),
                              tr("Не удалось получить информацию о ключе, ошибка: ")+
                              ErrorCodeToString(GetLastError()),
                              QMessageBox::Ok);
        return;
    }

    QVector<BYTE> blob_value(blob_value_len);
    if(!ntcryptomgr.ExportKey(PUBLICKEYBLOB, 0, blob_value.data(), &blob_value_len))
    {
        QMessageBox::critical(this,
                              tr("Ошибка"),
                              tr("Не удалось получить информацию о ключе, ошибка: ")+
                              ErrorCodeToString(GetLastError()),
                              QMessageBox::Ok);
        return;
    }

    QVector<BYTE> key_value = blob_value.mid(sizeof(BLOBHEADER)+sizeof(TZI_ASYMM_KEY_BLOB_HEADER));
    QDialog dlg_key_info(this);
    dlg_key_info.setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    QFormLayout layout(&dlg_key_info);
    QLabel lb_cont(name);
    QLabel lb_algid(GetStringAlgById(alg_id)+tr(" (")+QString::number(alg_id) + tr(")"));
    QLabel lb_key_len(QString::number(key_len)); /* Private key length in bits */
    QTextBrowser tb_key_value;
    QString hex_key = QByteArray::fromRawData(reinterpret_cast<char*>(key_value.data()), key_value.size()).toHex();
    for(int i = 2; i < hex_key.size(); i += 3)
    {
        hex_key.insert(i, " ");
    }
    tb_key_value.setText(hex_key);
    layout.addRow(tr("Имя контейнера: "), &lb_cont);
    layout.addRow(tr("ID алгоритма: "), &lb_algid);
    layout.addRow(tr("Длина ключа"), &lb_key_len);
    layout.addRow(tr("Значение открытого ключа: "), &tb_key_value);
    dlg_key_info.exec();

}

bool ControlPanel::IsWin64bit()
{
    BOOL isWow64 = FALSE;

    LPFN_ISWOW64PROCESS fnIsWow64Process  = (LPFN_ISWOW64PROCESS)
            GetProcAddress(GetModuleHandle(TEXT("kernel32")),"IsWow64Process");

    if(fnIsWow64Process)
    {
        if (!fnIsWow64Process(GetCurrentProcess(), &isWow64))
            return false;

        return isWow64 ? true : false;
    }
    else
        return false;
}

QByteArray ControlPanel::CalculateModuleHash(const QString& path)
{
    QFile module(path);
    if(!module.open(QFile::ReadOnly))
    {
        return "";
    }

    QByteArray module_bin = module.readAll();
    NTCryptoManager ntcmgr("", Constants::BIGN_CSP_TYPE, CRYPT_VERIFYCONTEXT);
    if(!ntcmgr.Init())
    {
        return "";
    }

    DWORD hash_val_len = 0;
    if(!ntcmgr.CalculateHash(CALG_BELT_HASH_256,
                             reinterpret_cast<BYTE*>(module_bin.data()),
                             module_bin.size(),
                             nullptr,
                             &hash_val_len))
    {
        return "";
    }

    QVector<BYTE> hash_val(hash_val_len);
    if(!ntcmgr.CalculateHash(CALG_BELT_HASH_256,
                             reinterpret_cast<BYTE*>(module_bin.data()),
                             module_bin.size(),
                             hash_val.data(),
                             &hash_val_len))
    {
        return "";
    }

    return QByteArray::fromRawData(reinterpret_cast<char*>(hash_val.data()), hash_val_len).toHex();
}

void ControlPanel::on_btn_change_log_path_clicked()
{
    QString new_path = QFileDialog::getSaveFileName(this,
                                                    tr("Выберите файл для сохранения лога"),
                                                    "",
                                                    tr("Текстовый файл (*.txt)"));
    if(!new_path.isEmpty())
    {
        ui->ln_log_path->setText(new_path);
    }
}

QString ControlPanel::GetStringAlgById(const ALG_ID alg_id)
{
    switch(alg_id)
    {
    case CALG_BELT_HASH_256:
        return tr("СТБ 34.101.31: belt-hash256");

    case CALG_BASH_256:
        return tr("СТБ 34.101.77: bash256");

    case CALG_BASH_384:
        return tr("СТБ 34.101.77: bash384");

    case CALG_BASH_512:
        return tr("СТБ 34.101.77: bash512");

    case CALG_BIGN:
        return tr("СТБ 34.101.45: bign-pubkey");

    case CALG_STB1176_2:
        return tr("СТБ 1176.2: stb11762-pubkey");

    case CALG_STB1176_2_PRE:
        return tr("СТБ 1176.2: stb11762pre-pubkey");

    default:
        return "";
    }
}

void ControlPanel::on_btn_reg_key_clicked()
{
    ui->btn_reg_key->setEnabled(false);
    KeyRegisterWizard klw(ui->rb_local_machine->isChecked());
    connect(&klw, SIGNAL(RegUnregKeyFinish()), this, SLOT(on_btn_cont_refresh_clicked()));
    klw.Init();
    klw.exec();
    ui->btn_reg_key->setEnabled(true);
}

void ControlPanel::on_btn_unreg_key_clicked()
{
    if(ui->tw_containers->currentRow() < 0)
    {
        QMessageBox::warning(this,
                             tr("Внимание"),
                             tr("Выберите ключ!"),
                             QMessageBox::Ok);
        return;
    }
    ui->btn_unreg_key->setEnabled(false);
    KeyRegisterWizard klw(ui->rb_local_machine->isChecked());
    connect(&klw, SIGNAL(RegUnregKeyFinish()), this, SLOT(on_btn_cont_refresh_clicked()));
    klw.UnregisterKey(ui->tw_containers->item(ui->tw_containers->currentRow(), 0)->text());
    ui->btn_unreg_key->setEnabled(true);
}

void ControlPanel::on_btn_pref_apply_clicked()
{
    if(ui->cb_default_prov->currentIndex() < 0)
    {
        return;
    }

    if(ui->cb_default_prov->currentData().value<int>() != curr_default_storage_)
    {
        DWORD new_value = static_cast<DWORD>(ui->cb_default_prov->currentData().value<int>());
        RegistryManager regmgr;
        NTSTATUS status = regmgr.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE | KEY_SET_VALUE);
        if(status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
            return;
        }

        status = regmgr.OpenKey(RegConstants::DEFAULT_PROVIDER_OPTIONS_REG_PATH,
                                         KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE | KEY_SET_VALUE);
        if(status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
            return;
        }

        status = regmgr.SetKeyValue("",
                                    RegConstants::DEFAULT_STORAGE_OPTION,
                                    REG_DWORD,
                                    reinterpret_cast<BYTE*>(&new_value),
                                    sizeof(new_value));
        if(status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
            qDebug() << status;
            return;
        }

        curr_default_storage_ = static_cast<int>(
                    ui->cb_default_prov->currentData().value<DWORD>());
    }

    if(ui->ln_log_path->text() != curr_log_file_path_)
    {
        if(ui->ln_log_path->text().isEmpty())
        {
            return;
        }

        QString new_value = ui->ln_log_path->text();
        RegistryManager regmgr;
        NTSTATUS status = regmgr.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE | KEY_SET_VALUE);
        if(status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
            return;
        }

        status = regmgr.OpenKey(RegConstants::DEFAULT_PROVIDER_OPTIONS_REG_PATH,
                                         KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE | KEY_SET_VALUE);
        if(status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
            return;
        }

        status = regmgr.SetKeyValue("",
                                    RegConstants::LOG_FILE_PATH_OPTION,
                                    REG_SZ,
                                    reinterpret_cast<const BYTE*>(new_value.toStdWString().data()),
                                    (new_value.size() + 1) * sizeof(wchar_t));
        if(status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
            qDebug() << status;
            return;
        }

        curr_log_file_path_ = ui->ln_log_path->text();
    }
}

void ControlPanel::on_btn_pref_cancel_clicked()
{
    QApplication::quit();
}

void ControlPanel::on_btn_pref_ok_clicked()
{
    if(ui->cb_default_prov->currentIndex() < 0)
    {
        QApplication::quit();
        return;
    }

    if(ui->cb_default_prov->currentData().value<int>() != curr_default_storage_)
    {
        DWORD new_value = static_cast<DWORD>(ui->cb_default_prov->currentData().value<int>());
        RegistryManager regmgr;
        NTSTATUS status = regmgr.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE | KEY_SET_VALUE);
        if(status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
            return;
        }

        status = regmgr.OpenKey(RegConstants::DEFAULT_PROVIDER_OPTIONS_REG_PATH,
                                         KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE | KEY_SET_VALUE);
        if(status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
            return;
        }

        status = regmgr.SetKeyValue("",
                                    RegConstants::DEFAULT_STORAGE_OPTION,
                                    REG_DWORD,
                                    reinterpret_cast<BYTE*>(&new_value),
                                    sizeof(new_value));
        if(status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
            qDebug() << status;
            return;
        }
    }

    if(ui->ln_log_path->text() != curr_log_file_path_)
    {
        if(ui->ln_log_path->text().isEmpty())
        {
            QApplication::quit();
            return;
        }

        QString new_value = ui->ln_log_path->text();
        RegistryManager regmgr;
        NTSTATUS status = regmgr.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE | KEY_SET_VALUE);
        if(status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
            return;
        }

        status = regmgr.OpenKey(RegConstants::DEFAULT_PROVIDER_OPTIONS_REG_PATH,
                                         KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE | KEY_SET_VALUE);
        if(status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
            return;
        }

        status = regmgr.SetKeyValue("",
                                    RegConstants::LOG_FILE_PATH_OPTION,
                                    REG_SZ,
                                    reinterpret_cast<const BYTE*>(new_value.toStdWString().data()),
                                    (new_value.size() + 1) * sizeof(wchar_t));
        if(status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
            qDebug() << status;
            return;
        }
    }

    QApplication::quit();
}


DWORD ControlPanel::GetServiceStatus(const QString& srvc_name)
{
    NTServiceManager scmgr;
    DWORD err = scmgr.OpenWinSCManager(SC_MANAGER_ENUMERATE_SERVICE);
    if(err != ERROR_SUCCESS)
    {
        return NTE_FAIL;
    }

    if(scmgr.OpenNTService(srvc_name, SERVICE_QUERY_STATUS) != ERROR_SUCCESS)
    {
        return NTE_FAIL;
    }

    return scmgr.GetServiceState();
}

void ControlPanel::ServiceStatusToString(const DWORD status)
{
    switch(status)
    {
    case SERVICE_RUNNING:
        ui->lb_svc_status->setStyleSheet("QLabel { color : green; }");
        ui->lb_svc_status->setText(tr("Служба работает"));
        ui->btn_svc_start->setEnabled(false);
        ui->btn_svc_stop->setEnabled(true);
        break;

    case SERVICE_STOPPED:
        ui->lb_svc_status->setStyleSheet("QLabel { color : red; }");
        ui->lb_svc_status->setText(tr("Служба остановлена"));
        ui->btn_svc_start->setEnabled(true);
        ui->btn_svc_stop->setEnabled(false);
        break;

    case NTE_FAIL:
        ui->lb_svc_status->setStyleSheet("QLabel { color : red; }");
        ui->lb_svc_status->setText(tr("Не удалось получить статус"));
        break;

    default:
        return;
    }
}

void ControlPanel::on_btn_svc_refresh_clicked()
{
    DWORD status = GetServiceStatus(key_iso_srvc_name);
    ServiceStatusToString(status);
}

void ControlPanel::on_btn_svc_cng_refresh_clicked()
{
    DWORD status = GetServiceStatus(cng_key_iso_srvc_name);
    CngServiceStatusToString(status);
}

void ControlPanel::on_btn_svc_stop_clicked()
{
    ServiceStop(key_iso_srvc_name);
    DWORD status = GetServiceStatus(key_iso_srvc_name);
    ServiceStatusToString(status);
}

void ControlPanel::on_btn_svc_cng_stop_clicked()
{
    ServiceStop(cng_key_iso_srvc_name);
    DWORD status = GetServiceStatus(cng_key_iso_srvc_name);
    CngServiceStatusToString(status);
}

void ControlPanel::on_btn_svc_start_clicked()
{
    ServiceStart(key_iso_srvc_name);
    DWORD status = GetServiceStatus(key_iso_srvc_name);
    ServiceStatusToString(status);
}

void ControlPanel::on_btn_svc_cng_start_clicked()
{
    ServiceStart(cng_key_iso_srvc_name);
    DWORD status = GetServiceStatus(cng_key_iso_srvc_name);
    CngServiceStatusToString(status);
}

void ControlPanel::ServiceStop(const QString& svc_name)
{
    DWORD start_time = GetTickCount();
    DWORD timeout = 30000;
    NTServiceManager scmgr;
    DWORD err = scmgr.OpenWinSCManager(SC_MANAGER_ALL_ACCESS);
    if(err != ERROR_SUCCESS)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось остановить службу, код ошибки: ")+
                              QString::number(err), QMessageBox::Ok);
        return;
    }

    err = scmgr.OpenNTService(svc_name, SERVICE_STOP | SERVICE_QUERY_STATUS);
    if(err != ERROR_SUCCESS)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось остановить службу, код ошибки: ")+
                              QString::number(err), QMessageBox::Ok);
        return;
    }

    DWORD status = scmgr.GetServiceState();
    if(status == NTE_FAIL)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось остановить службу, код ошибки: ")+
                              QString::number(GetLastError()), QMessageBox::Ok);
        return;
    }

    if(status == SERVICE_STOPPED)
    {
        QMessageBox::information(this, tr("Ошибка"), tr("Служба уже остановлена"), QMessageBox::Ok);
        return;
    }

    while(status == SERVICE_STOP_PENDING)
    {
        DWORD wait_time = 1000;

        Sleep(wait_time);

        status = scmgr.GetServiceState();
        if(status == NTE_FAIL)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось остановить службу, код ошибки: ")+
                                  QString::number(GetLastError()), QMessageBox::Ok);
            return;
        }

        if(status == SERVICE_STOPPED)
        {
            QMessageBox::information(this, tr("Информация"), tr("Служба успешно остановлена"),
                                     QMessageBox::Ok);
            return;
        }

        if(GetTickCount() - start_time > timeout)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось остановить службу, причина: timeuot"),
                                  QMessageBox::Ok);
            return;
        }
    }

    status = scmgr.ControlNTService(SERVICE_CONTROL_STOP);
    if(status == NTE_FAIL)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось остановить службу, код ошибки: ")+
                              QString::number(GetLastError()), QMessageBox::Ok);
        return;
    }

    while(status != SERVICE_STOPPED)
    {
        DWORD wait_time = 1000;
        Sleep(wait_time);
        status = scmgr.GetServiceState();
        if(status == NTE_FAIL)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось остановить службу, код ошибки: ")+
                                  QString::number(GetLastError()), QMessageBox::Ok);
            return;
        }

        if(status == SERVICE_STOPPED)
        {
            break;
        }

        if(GetTickCount() - start_time > timeout)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось остановить службу, причина: timeout"),
                                  QMessageBox::Ok);
            return;
        }

        QMessageBox::information(this, tr("Информация"), tr("Служба успешно остановлена"),
                                 QMessageBox::Ok);
        return;
    }
}

void ControlPanel::CngServiceStatusToString(const DWORD status)
{
    switch(status)
    {
    case SERVICE_RUNNING:
        ui->lb_svc_cng_status->setStyleSheet("QLabel { color : green; }");
        ui->lb_svc_cng_status->setText(tr("Служба работает"));
        ui->btn_svc_cng_start->setEnabled(false);
        ui->btn_svc_cng_stop->setEnabled(true);
        break;

    case SERVICE_STOPPED:
        ui->lb_svc_cng_status->setStyleSheet("QLabel { color : red; }");
        ui->lb_svc_cng_status->setText(tr("Служба остановлена"));
        ui->btn_svc_cng_start->setEnabled(true);
        ui->btn_svc_cng_stop->setEnabled(false);
        break;

    case NTE_FAIL:
        ui->lb_svc_cng_status->setStyleSheet("QLabel { color : red; }");
        ui->lb_svc_cng_status->setText(tr("Не удалось получить статус"));
        break;

    default:
        return;
    }
}

void ControlPanel::ServiceStart(const QString& svc_name)
{
    NTServiceManager scmgr;
    DWORD status = scmgr.OpenWinSCManager(SC_MANAGER_ALL_ACCESS);
    if(status != ERROR_SUCCESS)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось запустить службу, код ошибки: ")+
                              QString::number(status), QMessageBox::Ok);
        return;
    }

    status = scmgr.OpenNTService(svc_name, SERVICE_START | SERVICE_QUERY_STATUS);
    if(status != ERROR_SUCCESS)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось запустить службу, код ошибки: ")+
                              QString::number(status), QMessageBox::Ok);
        return;
    }

    status = scmgr.GetServiceState();
    if(status == NTE_FAIL)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось запустить службу, код ошибки: ")+
                              QString::number(GetLastError()), QMessageBox::Ok);
        return;
    }

    if(status != SERVICE_STOPPED && status != SERVICE_STOP_PENDING)
    {
        QMessageBox::information(this, tr("Ошибка"), tr("Служба уже запущена"),
                                 QMessageBox::Ok);
        return;
    }

    DWORD start_time = GetTickCount();
    DWORD timeout = 30000;
    while(status == SERVICE_STOP_PENDING)
    {
        DWORD wait_time = 1000;
        Sleep(wait_time);
        status = scmgr.GetServiceState();
        if(status == NTE_FAIL)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось запустить службу, код ошибки: ")+
                                  QString::number(GetLastError()), QMessageBox::Ok);
            return;
        }

        if(GetTickCount() - start_time > timeout)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось запусить службу, причина: timeout"),
                                  QMessageBox::Ok);
            return;
        }
    }

    status = scmgr.StartNTService();
    if(status == NTE_FAIL)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось запустить службу, код ошибки: ")+
                              QString::number(GetLastError()), QMessageBox::Ok);
        return;
    }

    status = scmgr.GetServiceState();
    if(status == NTE_FAIL)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось запустить службу, код ошибки: ")+
                              QString::number(GetLastError()), QMessageBox::Ok);
        return;
    }

    start_time = GetTickCount();
    while(status == SERVICE_START_PENDING)
    {
        DWORD wait_time = 1000;
        Sleep(wait_time);
        status = scmgr.GetServiceState();
        if(status == NTE_FAIL)
        {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось запустить службу, код ошибки: ")+
                                  QString::number(GetLastError()), QMessageBox::Ok);
            return;
        }

        if(GetTickCount() - start_time > timeout)
        {
            break;
        }
    }

    if(status == SERVICE_RUNNING)
    {
        QMessageBox::information(this, tr("Информация"), tr("Служба успешно запущена"),
                                 QMessageBox::Ok);
        return;
    }
    else
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось запустить службу, код ошибки: ")+
                              QString::number(GetLastError()), QMessageBox::Ok);
        return;
    }
}



void ControlPanel::on_btn_show_log_cng_clicked()
{
    return ShowLog(true);
}

void ControlPanel::ShowLog(const bool is_cng_log)
{
    QString path = is_cng_log ? ui->ln_log_cng_path->text() : ui->ln_log_path->text();
    if(path.isEmpty())
    {
        return;
    }


    QFile log_file(path);
    if(!log_file.open(QFile::ReadOnly))
    {
        QMessageBox::warning(this, tr("Ошибка"), tr("Не удалось открыть файл"), QMessageBox::Ok);
        return;
    }

    QDialog dlg_log(this);
    QFormLayout layout(&dlg_log);
    QTextBrowser tb_log;
    QTextStream in(&log_file);
    while(!in.atEnd())
    {
        QString line = in.readLine();
        if(line.indexOf("success") < 0)
        {
            tb_log.setTextColor("Red");
            tb_log.append(line);
            tb_log.setTextColor("Black");
        }
        else
        {
            tb_log.append(line);
        }
    }

    tb_log.setFixedWidth(500);
    layout.addRow(&tb_log);
    dlg_log.exec();
    log_file.close();
}

void ControlPanel::on_btn_open_log_file_cng_folder_clicked()
{
    return OpenLogFileFolder(true);
}

void ControlPanel::OpenLogFileFolder(const bool is_cng_log)
{
    QString path = is_cng_log ? ui->ln_log_cng_path->text() : ui->ln_log_path->text();
    if(path.isEmpty())
    {
        return;
    }

    int index = path.lastIndexOf("\\");
    if(index == -1)
    {
        index = path.lastIndexOf("/");
    }
    path.truncate(index);
    QDesktopServices::openUrl(QUrl::fromLocalFile(path));
}

void ControlPanel::on_btn_clear_log_cng_clicked()
{
    return ClearLog(true);
}

void ControlPanel::ClearLog(const bool is_cng_log)
{
    QString path = is_cng_log ? ui->ln_log_cng_path->text() : ui->ln_log_path->text();
    if(path.isEmpty())
    {
        return;
    }

    QFile file_log(path);
    if(!file_log.open(QFile::ReadWrite))
    {
        QMessageBox::warning(this, tr("Ошибка"), tr("Не удалось открыть файл"), QMessageBox::Ok);
        return;
    }

    if(!file_log.resize(0))
    {
        QMessageBox::warning(this, tr("Ошибка"), tr("Не удалось очистить файл"), QMessageBox::Ok);
        return;
    }

    file_log.close();
    QMessageBox::information(this, tr("Информация"), tr("Файл успешно очищен"), QMessageBox::Ok);
}

void ControlPanel::on_stacked_widget_main_currentChanged(int arg1)
{
    switch(arg1)
    {
    case MenuItems::Information:
        ui->lb_page_caption->setText(tr("Общие сведения"));
        ui->lb_page_icon->setPixmap(QPixmap(":/images/images/Information_32px.png"));
        if(!(MENU_STATE_FLAGS & static_cast<unsigned long>(MENU_STATE_FLAG::INFORMATION_STATE_FLAG)))
        {         
            QString version = GetModuleVersion("C:\\Windows\\System32\\ntcrypto.dll");
            ui->lb_version_csp->setText(version);

#ifdef _WIN_XP_
            ui->lb_version_cng->setVisible(false);
            ui->lb_version_cng_txt->setVisible(false);
            ui->lb_version_ksp_txt->setVisible(false);
            ui->lb_version_ksp->setVisible(false);
#else
            version = GetModuleVersion("C:\\Windows\\System32\\ntcryptocng.dll");
            ui->lb_version_cng->setText(version);
            version = GetModuleVersion("C:\\Windows\\System32\\ntcryptoksp.dll");
            ui->lb_version_ksp->setText(version);
#endif

            MENU_STATE_FLAGS |= static_cast<unsigned long>(MENU_STATE_FLAG::INFORMATION_STATE_FLAG);
        }
        break;

    case MenuItems::Preferences:
        ui->lb_page_caption->setText(tr("Настройки криптопровайдера"));
        ui->lb_page_icon->setPixmap(QPixmap(":/images/images/Maintenance_32px.png"));
        if(!(MENU_STATE_FLAGS & static_cast<unsigned long>(MENU_STATE_FLAG::PREFERENCES_STATE_FLAG)))
        {
            ui->lb_defult_prov->setText(tr("Выберите реализацию,"
                                           "которая будет использоваться по умолчанию для операций,\n"
                                           "не требующих использования личных ключей:"));
            ui->lb_log_path->setText(tr("Укажите путь для сохранения лог-файла:"));
            ui->btn_open_log_file_folder->setText(tr("Показать лог-файл в папке"));
            ui->btn_clear_log->setText(tr("Очистить лог-файл"));
            ui->lb_selftest->setText(tr("Выполнить тестирование поддерживаемых алгоритмов:"));

            ui->cb_default_prov->addItem("Программная реализация",
                                         QVariant(static_cast<int>(STORAGE_TYPE::FileSystem)));
            ui->cb_default_prov->addItem("СКЗИ \"Сигма\"",
                                         QVariant(static_cast<int>(STORAGE_TYPE::Sigma)));
            ui->cb_default_prov->addItem("СКЗИ NTStore",
                                         QVariant(static_cast<int>(STORAGE_TYPE::NTStore)));

            if(!IsRegOptionsExist())
            {
                NTSTATUS status = CreateDefaultRegOptions();
                if(status != ERROR_SUCCESS)
                {
                    QMessageBox::warning(this, tr("Ошибка"), tr("Не удалось создать раздел с настройками в реестре, код ошибки: ")+QString::number(status), QMessageBox::Ok);
                }
            }

            NTSTATUS status = LoadLogFilePath(false);
            if(status != ERROR_SUCCESS)
            {
                QMessageBox::warning(this, tr("Ошибка"), tr("Не удалось прочитать настройки из реестра, код ошибки: ")+QString::number(status), QMessageBox::Ok);
            }

            status = LoadDefaultStorageValue();
            if(status != ERROR_SUCCESS)
            {
                QMessageBox::warning(this, tr("Ошибка"), tr("Не удалось прочитать настройки из реестра, код ошибки: ")+QString::number(status), QMessageBox::Ok);
            }





#ifdef _WIN_XP_
            ui->gb_log_cng->setVisible(false);
            ui->lb_log_cng_path->setVisible(false);
            ui->ln_log_cng_path->setVisible(false);
            ui->btn_change_log_cng_path->setVisible(false);
            ui->btn_clear_log_cng->setVisible(false);
            ui->btn_show_log_cng->setVisible(false);
            ui->btn_open_log_file_cng_folder->setVisible(false);
#else
            ui->btn_clear_log_cng->setText(tr("Очистить CNG лог-файл"));
            ui->btn_show_log_cng->setText(tr("Показать CNG лог-файл"));
            ui->btn_open_log_file_cng_folder->setText(tr("Показать CNG лог-файл в папке"));
            ui->lb_log_cng_path->setText(tr("Укажите путь для сохранения CNG лог-файла:"));

            status = LoadLogFilePath(true);
            if(status != ERROR_SUCCESS)
            {
                QMessageBox::warning(this, tr("Ошибка"), tr("Не удалось прочитать настройки из реестра, код ошибки: ")+QString::number(status), QMessageBox::Ok);
            }
#endif

            MENU_STATE_FLAGS |= static_cast<unsigned long>(MENU_STATE_FLAG::PREFERENCES_STATE_FLAG);
        }
        break;

    case MenuItems::KeyContainers:
        ui->lb_page_caption->setText(tr("Управление ключевыми контейнерами"));
        ui->lb_page_icon->setPixmap(QPixmap(":/images/images/Key 2_32px.png"));
        if(!(MENU_STATE_FLAGS & static_cast<unsigned long>(MENU_STATE_FLAG::KEY_CONTAINERS_STATE_FLAG)))
        {
            ui->tw_containers->setSelectionBehavior(QAbstractItemView::SelectRows);
            ui->tw_containers->horizontalHeader()->setVisible(true);
            ui->tw_containers->verticalHeader()->setVisible(false);
            ui->tw_containers->horizontalHeader()->setStretchLastSection(true);

            ui->btn_reg_key->setText(tr("Зарегистрировать\nновый ключевой\nконтейнер"));
            ui->btn_change_pass->setText(tr("Изменить\nпароль"));
            ui->btn_unreg_key->setText(tr("Отменить\nрегистрацию\nконтейнера"));
            ui->btn_del_container->setText(tr("Удалить"));
            ui->btn_cont_refresh->setText(tr("Обновить"));
            ui->rb_local_machine->setText(tr("Компьютер"));
            ui->rb_user->setText(tr("Текущий пользователь"));
            ui->rb_user->setChecked(true);

            FillContainersTable();

            ui->tw_containers->resizeColumnsToContents();
            ui->tw_containers->resizeRowsToContents();

            MENU_STATE_FLAGS |= static_cast<unsigned long>(MENU_STATE_FLAG::KEY_CONTAINERS_STATE_FLAG);
        }
        break;

    case MenuItems::Certificates:
        ui->lb_page_caption->setText(tr("Сертификаты"));
        ui->lb_page_icon->setPixmap(QPixmap(":/images/images/Accreditation_32px.png"));
        if(!(MENU_STATE_FLAGS & static_cast<unsigned long>(MENU_STATE_FLAG::CERTIFICATES_STATE_FLAG)))
        {
            ui->gb_cert_store->setTitle(tr("Хранилище сертификатов"));
            ui->btn_cers_list_refresh->setText(tr("Обновить"));
            ui->btn_import_cert->setText(tr("Импорт сертификата\nиз файла"));
            ui->lw_certs->setContextMenuPolicy(Qt::CustomContextMenu);
            connect(ui->lw_certs, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(ShowCertContextMenu(QPoint)));
            FillCertsList();

            MENU_STATE_FLAGS |= static_cast<unsigned long>(MENU_STATE_FLAG::CERTIFICATES_STATE_FLAG);
        }
        break;

    case MenuItems::Composition:
        ui->lb_page_caption->setText(tr("Информация о составе криптопровайдера"));
        ui->lb_page_icon->setPixmap(QPixmap(":/images/images/Menu_32px.png"));
        if(!(MENU_STATE_FLAGS & static_cast<unsigned long>(MENU_STATE_FLAG::COMPOSITION_STATE_FLAG)))
        {
            ui->tw_modules->setSelectionBehavior(QAbstractItemView::SelectRows);
            ui->tw_modules->setSelectionMode(QAbstractItemView::SingleSelection);
            ui->tw_modules->horizontalHeader()->setVisible(true);
            ui->tw_modules->verticalHeader()->setVisible(false);
            ui->tw_modules->setContextMenuPolicy(Qt::CustomContextMenu);
            connect(ui->tw_modules, SIGNAL(customContextMenuRequested(QPoint)),
                    this, SLOT(ShowModulesContextMenu(QPoint)));

            CheckIntegrity();

            MENU_STATE_FLAGS |= static_cast<unsigned long>(MENU_STATE_FLAG::COMPOSITION_STATE_FLAG);
        }
        break;

    case MenuItems::Services:
        ui->lb_page_caption->setText(tr("Управление службами"));
        ui->lb_page_icon->setPixmap(QPixmap(":/images/images/Services_32px.png"));
        if(!(MENU_STATE_FLAGS & static_cast<unsigned long>(MENU_STATE_FLAG::SERVICES_STATE_FLAG)))
        {
            ui->gb_svc->setTitle(tr("ntkeyisosrvc.exe"));
            ui->lb_svc_descr->setText(tr("Служба изоляции ключей криптопровайдера NTCrypto"));
            ui->lb_svc_status_txt->setText(tr("Состояние службы: "));
            ui->btn_svc_refresh->setText(tr("Обновить"));
            ui->btn_svc_start->setText(tr("Запустить"));
            ui->btn_svc_stop->setText(tr("Остановить"));

            DWORD status = GetServiceStatus(key_iso_srvc_name);
            ServiceStatusToString(status);

#ifdef _WIN_XP_
            ui->gb_svc_cng->setVisible(false);
            ui->lb_svc_cng_descr->setVisible(false);
            ui->lb_svc_cng_status_txt->setVisible(false);
            ui->btn_svc_cng_refresh->setVisible(false);
            ui->btn_svc_cng_start->setVisible(false);
            ui->btn_svc_cng_stop->setVisible(false);
#else
            ui->gb_svc_cng->setTitle(tr("ntcngkeyisosrvc.exe"));
            ui->lb_svc_cng_descr->setText(tr("Служба изоляции CNG ключей криптопровайдера NTCrypto"));
            ui->lb_svc_cng_status_txt->setText(tr("Состояние службы: "));
            ui->btn_svc_cng_refresh->setText(tr("Обновить"));
            ui->btn_svc_cng_start->setText(tr("Запустить"));
            ui->btn_svc_cng_stop->setText(tr("Остановить"));

            status = GetServiceStatus(cng_key_iso_srvc_name);
            CngServiceStatusToString(status);
#endif



            MENU_STATE_FLAGS |= static_cast<unsigned long>(MENU_STATE_FLAG::SERVICES_STATE_FLAG);
        }
        break;

    case MenuItems::CryptographicOperations:
        ui->lb_page_caption->setText(tr("Криптографические операции"));
        //ui->lb_page_icon
        if(!(MENU_STATE_FLAGS & static_cast<unsigned long>(MENU_STATE_FLAG::CRYPTO_OP_STATE_FLAG))){
            ui->btn_go_to_co_select->setVisible(false);
            how_ = std::unique_ptr<HashOperationWizard>(new HashOperationWizard(
                        ui->sw_crypto_op->widget(static_cast<int>(CRYPTO_OPERATION::Hash))));
            sow_ = std::unique_ptr<SigOperationWizard>(new SigOperationWizard(
                        ui->sw_crypto_op->widget(static_cast<int>(CRYPTO_OPERATION::MakeSig))));
            svow_ = std::unique_ptr<SigVerifyOperationWizard>(new SigVerifyOperationWizard(
                        ui->sw_crypto_op->widget(static_cast<int>(CRYPTO_OPERATION::VerifySig))));
            eow_ = std::unique_ptr<EncryptOperationWizard>(new EncryptOperationWizard(
                        ui->sw_crypto_op->widget(static_cast<int>(CRYPTO_OPERATION::Encryption))));
            MENU_STATE_FLAGS |= static_cast<unsigned long>(MENU_STATE_FLAG::CRYPTO_OP_STATE_FLAG);
        }
        break;

    default:
        /* something wrong */
        return;
    }
}

void ControlPanel::on_btn_gen_key_pair_clicked()
{
    ui->btn_gen_key_pair->setEnabled(false);
    bool is_local_machine = ui->rb_local_machine->isChecked();
    GenKeyWizard gkw(is_local_machine);
    gkw.Init();
    gkw.exec();

    FillContainersTable();
    ui->tw_containers->resizeRowsToContents();
    ui->btn_gen_key_pair->setEnabled(true);
}

void ControlPanel::on_btn_req_cert_clicked()
{
    CertificateRequestWizard* crw = new CertificateRequestWizard(this);
    crw->show();
}


void ControlPanel::on_cb_cert_store_currentIndexChanged(int index)
{
    Q_UNUSED(index);
    ui->cb_cert_store->setEnabled(false);
    FillCertsList();
    ui->cb_cert_store->setEnabled(true);
}

void ControlPanel::on_btn_cers_list_refresh_clicked()
{
    ui->btn_cers_list_refresh->setEnabled(false);
    FillCertsList();
    ui->btn_cers_list_refresh->setEnabled(true);
}

void ControlPanel::ShowCertContextMenu(const QPoint& pos)
{
    QPoint globalPos = ui->lw_certs->mapToGlobal(pos);

    CERTIFICATE_STORE_TYPE curr_store = static_cast<CERTIFICATE_STORE_TYPE>(ui->cb_cert_store->currentIndex());
    QMenu myMenu;
    if(curr_store == CERTIFICATE_STORE_TYPE::MY ||
       curr_store == CERTIFICATE_STORE_TYPE::ROOT) {
        myMenu.addAction(QIcon(":/images/images/Information_blue_32px.png"),
                         tr("Показать информацию о личном ключе"), this, SLOT(ShowPrivKeyInfo()));
        myMenu.addAction(QIcon(":/images/images/Link_32px.png"),
                         tr("Связать сертификат с личным ключом"), this, SLOT(LinkKeyWithcert()));
        myMenu.addAction(QIcon(":/images/images/Delete_32px.png"),
                         tr("Удалить сертификат"), this, SLOT(RemoveCertFromStore()));
    }
    else if(curr_store == CERTIFICATE_STORE_TYPE::SIGMA ||
            curr_store == CERTIFICATE_STORE_TYPE::NTSTORE) {
        myMenu.addAction(QIcon(":/images/images/Import_32px.png"),
                         tr("Импортировать сертификат в системное хранилище"), this, SLOT(ImportCertToSysStore()));
    }
    else if(curr_store == CERTIFICATE_STORE_TYPE::CRL) {
        myMenu.addAction(QIcon(":/images/images/Delete_32px.png"),
                         trUtf8("Удалить"), this, SLOT(RemoveCrlFromStore()));
    } else if(curr_store == CERTIFICATE_STORE_TYPE::ATTRIBUTE){
        myMenu.addAction(QIcon(":/images/images/Delete_32px.png"),
                         trUtf8("Удалить"), this, SLOT(RemoveAttCertFromStore()));
    }

    myMenu.exec(globalPos);
}

void ControlPanel::ShowPrivKeyInfo()
{
    int row = ui->lw_certs->currentRow();
    if(row < 0){
        return;
    }
    SystemCertificateManager* certmgr = dynamic_cast<SystemCertificateManager*>(csmgr_->GetCertificateManagerByIndex(row).get());
    if(!certmgr->IsPrivateKeyForCertExist()){
        QMessageBox::warning(this, tr("Информация"), tr("Для этого сертификата отсутствует личный ключ"), QMessageBox::Ok);
        return;
    }
    QString csp_name = certmgr->GetPrivKeyCSPName();
    QString key_name = certmgr->GetPrivKeyName();
    QString str = tr("Криптопровайдер: ")+csp_name+tr("\nИмя контейнера ключа: ")+key_name;
    QMessageBox::information(this, tr("Информация о личном ключе"), str, QMessageBox::Ok);
}

void ControlPanel::LinkKeyWithcert()
{
    int row = ui->lw_certs->currentRow();
    if(row < 0){
        return;
    }

    SystemCertificateManager* certmgr = dynamic_cast<SystemCertificateManager*>(csmgr_->GetCertificateManagerByIndex(row).get());
    if(certmgr->IsPrivateKeyForCertExist())
    {
        QMessageBox msg;
        msg.setText(tr("Выбранный сертификат уже связан с личным ключом!\n"
                       "Вы действительно хотите продолжить?"));
        msg.setIcon(QMessageBox::Warning);
        QPushButton btn_ok(tr("Да"), &msg);
        btn_ok.setIcon(QIcon(":/images/images/Checkmark_32px.png"));
        QPushButton btn_cancel(tr("Отмена"), &msg);
        btn_cancel.setIcon(QIcon(":/images/images/Delete_32px.png"));
        msg.addButton(&btn_ok, QMessageBox::AcceptRole);
        msg.addButton(&btn_cancel, QMessageBox::RejectRole);
        msg.exec();
        if(msg.clickedButton() == &btn_cancel)
        {
            return;
        }
    }

    bool is_need_cng = false;
#ifndef _WIN_XP_
    QDialog dlg_prov_type(this);
    dlg_prov_type.setWindowTitle(tr("Выберите тип провайдера"));
    dlg_prov_type.setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    QFormLayout layout(&dlg_prov_type);
    QComboBox cb_types;
    cb_types.addItem(tr("CAPI"));
    cb_types.addItem(tr("CNG"));
    QDialogButtonBox btn_box(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    btn_box.button(QDialogButtonBox::Cancel)->setText(tr("Отмена"));
    btn_box.button(QDialogButtonBox::Ok)->setText(tr("Далее>>"));
    layout.addRow(tr("Тип провайдера: "), &cb_types);
    layout.addRow(&btn_box);
    connect(&btn_box, SIGNAL(accepted()), &dlg_prov_type, SLOT(accept()));
    connect(&btn_box, SIGNAL(rejected()), &dlg_prov_type, SLOT(reject()));
    if(dlg_prov_type.exec() == QDialog::Rejected)
    {
        return;
    }
    is_need_cng = cb_types.currentIndex() == 1 ? true : false;
#endif

    while(true){
        KeyLinkManager klmgr(static_cast<PCCERT_CONTEXT>(certmgr->GetRawContextPtr()), is_need_cng);
        DWORD err = klmgr.TryToLink();
        if(err == 0xffffffff){
            QMessageBox msg(this);
            msg.setText(tr("Не найден личный ключ, соответствующий сертификату.\n"
                           "Пожалуйста, подключите носитель, содержащий соответствующий личный ключ "
                           "и нажмите кнопку \"Повторить\".\n"));
            msg.setIcon(QMessageBox::Warning);
            QPushButton btn_retry(tr("Повторить"), &msg);
            btn_retry.setIcon(QIcon(":/images/images/Refresh_32px.png"));
            QPushButton btn_cancel(tr("Отмена"), &msg);
            btn_cancel.setIcon(QIcon(":/images/images/Delete_32px.png"));
            msg.addButton(&btn_retry, QMessageBox::AcceptRole);
            msg.addButton(&btn_cancel, QMessageBox::RejectRole);
            msg.exec();
            if(msg.clickedButton() == &btn_cancel) {
                return;
            }
        } else if (err == ERROR_SUCCESS){
            QMessageBox::information(this, tr("Информация"), tr("Операция выполнена успешно"), QMessageBox::Ok);
            break;
        } else {
            QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось связать сертификат с личным ключом"), QMessageBox::Ok);
            return;
        }
    }
    FillCertsList();
}

void ControlPanel::ImportCertToSysStore()
{
    int row = ui->lw_certs->currentRow();
    if(row < 0){
        return;
    }
    const auto& certmgr = csmgr_->GetCertificateManagerByIndex(row);
    bool is_root = static_cast<SystemCertificateManager*>(certmgr.get())->IsRootCertificate();
    CERTIFICATE_STORE_TYPE store = is_root ? CERTIFICATE_STORE_TYPE::ROOT : CERTIFICATE_STORE_TYPE::MY;
    auto storemgr = CertificateStoreManager::Create(store);
    if(!storemgr->OpenStore()){
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось добавить сертификат в хранилище, код ошибки: ")+
                              QString::number(GetLastError()), QMessageBox::Ok);
        return;
    }
    DWORD err = storemgr->AddToStore(certmgr, CERT_STORE_ADD_ALWAYS);
    if(err != ERROR_SUCCESS){
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось добавить сертифкат хранилище, код ошибки: ")+
                              QString::number(err), QMessageBox::Ok);
    } else {
        QMessageBox::information(this, tr("Информация"),
                                 tr("Сертификат успешно добавлен в хранилище"), QMessageBox::Ok);
    }
}

void ControlPanel::RemoveCertFromStore()
{
    int row = ui->lw_certs->currentRow();
    if(row == -1){
        return;
    }
    SECURITY_STATUS err = csmgr_->RemoveFromStoreByIndex(row);
    if(err != ERROR_SUCCESS){
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось удалить сертифкат из хранилища, код ошибки: ")+
                              QString::number(err), QMessageBox::Ok);
    } else {
        QMessageBox::information(this, tr("Информация"),
                                 tr("Сертификат был успешно удалён из хранилища"), QMessageBox::Ok);
        FillCertsList();
    }
}

void ControlPanel::RemoveCrlFromStore()
{
    int row = ui->lw_certs->currentRow();
    if(row == -1){
        return;
    }
    SECURITY_STATUS err = csmgr_->RemoveFromStoreByIndex(row);
    if(err != ERROR_SUCCESS){
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось удалить СОС из хранилища, код ошибки: ")+
                              QString::number(err), QMessageBox::Ok);
    } else {
        QMessageBox::information(this, tr("Информация"),
                                 tr("СОС был успешно удалён из хранилища"), QMessageBox::Ok);
        FillCertsList();
    }
}

void ControlPanel::RemoveAttCertFromStore()
{
    int row = ui->lw_certs->currentRow();
    if(row == -1){
        return;
    }
    SECURITY_STATUS err = csmgr_->RemoveFromStoreByIndex(row);
    if(err != ERROR_SUCCESS){
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось удалить атрибутный сертификат из хранилища, код ошибки: ")+
                              QString::number(err), QMessageBox::Ok);
    } else {
        QMessageBox::information(this, tr("Информация"),
                                 tr("Атрибутный сертификат был успешно удалён из хранилища"), QMessageBox::Ok);
        FillCertsList();
    }
}

void ControlPanel::on_btn_change_log_cng_path_clicked()
{
    QString new_path = QFileDialog::getSaveFileName(this,
                                                    tr("Выберите файл для сохранения лога"),
                                                    "",
                                                    tr("Текстовый файл (*.txt)"));
    if(!new_path.isEmpty())
    {
        ui->ln_log_cng_path->setText(new_path);
    }
}



DWORD ControlPanel::FindInstalledPath(std::wstring* installed_path)
{
    LSTATUS status = NTE_FAIL;
    HKEY hKey = NULL;
    LPCWSTR sub_key = L"Software\\NII TZI\\NTCrypto";
    LPCWSTR value_name = L"InstalledPath";
    DWORD data_len = 0;
    std::vector<BYTE> data;
    DWORD dac = IsWin64bit() ? KEY_QUERY_VALUE | KEY_READ | KEY_WOW64_64KEY
                             : KEY_QUERY_VALUE | KEY_READ;
    status = RegOpenKeyEx(HKEY_LOCAL_MACHINE, sub_key, 0, dac, &hKey);
    if (status != ERROR_SUCCESS)
    {
        goto Exit;
    }

    status = RegQueryValueEx(hKey, value_name, 0, 0, nullptr, &data_len);
    if (status != ERROR_SUCCESS)
    {
        goto Exit;
    }

    data.resize(data_len);
    status = RegQueryValueEx(hKey, value_name, 0, 0, data.data(), &data_len);
    if (status != ERROR_SUCCESS)
    {
        goto Exit;
    }

    *installed_path = std::wstring(reinterpret_cast<wchar_t*>(data.data()));

Exit:
    if (hKey)
    {
        RegCloseKey(hKey);
    }
    return status;
}

void ControlPanel::ShowModulesContextMenu(const QPoint& pos)
{
    QPoint globalPos = ui->tw_modules->mapToGlobal(pos);
    QMenu myMenu;
    myMenu.addAction(tr("Проверить целостность всех файлов"), this, SLOT(RecheckIntegrity()));
    myMenu.exec(globalPos);
}

void ControlPanel::RecheckIntegrity()
{  
    ui->tw_modules->setRowCount(0);
    CheckIntegrity();
}

void ControlPanel::on_btn_selftest_clicked()
{
    SelftestWizard stw(this);
    stw.exec();
}

void ControlPanel::on_btn_get_crl_clicked()
{
    QStringList crldp_urls;
    NTSTATUS status = LoadCrldpUrls(&crldp_urls);
    if(status != ERROR_SUCCESS)
    {
        QMessageBox::warning(this,
                             tr("Ошибка"),
                             tr("Не удалось прочитать настройки из реестра, код ошибки: ")+QString::number(status),
                             QMessageBox::Ok);
        return;
    }

    DownloadCrlWizard dcw(this, crldp_urls);
    dcw.exec();
    QList<QByteArray> crls = dcw.GetCrls();
    if(crls.isEmpty())
    {
        return;
    }

    auto csm = CertificateStoreManager::Create(CERTIFICATE_STORE_TYPE::CRL);
    if(!csm->OpenStore()){
        QMessageBox::critical(this, tr("Ошибка"),
                              tr("Не удалось добавить в хранилище полученные СОС"),
                              QMessageBox::Ok);
        return;
    }

    for(const auto& crl : crls) {
        PCCRL_CONTEXT pCrl = CertCreateCRLContext(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
                                                  reinterpret_cast<const BYTE*>(crl.data()),
                                                  crl.size());
        if(!pCrl) {
            continue;
        }
        auto crlmgr = ICertificateManager::Create(CERTIFICATE_TYPE::CRL, pCrl, true);
        DWORD err = csm->AddToStore(crlmgr, CERT_STORE_ADD_NEWER);
        if(err != ERROR_SUCCESS){
            QMessageBox::critical(this, tr("Ошибка"),
                                  tr("Не удалось добавить в хранилище полученные СОС"),
                                  QMessageBox::Ok);
            return;
        }
    }

    QMessageBox::information(this, tr("Информация"),
                             tr("Все полученные СОС успешно добавлены в хранилище "),
                             QMessageBox::Ok);
}



void ControlPanel::on_btn_crldl_view_clicked()
{
    QStringList crldp_urls;
    NTSTATUS status = LoadCrldpUrls(&crldp_urls);
    if(status != ERROR_SUCCESS)
    {
        QMessageBox::warning(this,
                             tr("Ошибка"),
                             tr("Не удалось прочитать настройки из реестра, код ошибки: ")+QString::number(status),
                             QMessageBox::Ok);
        return;
    }
    CrlDpWizard cdw(this, crldp_urls);
    connect(&cdw, SIGNAL(urlEditFinished(QStringList)), this, SLOT(OnUrlEditFinished(QStringList)));
    cdw.exec();
}

NTSTATUS ControlPanel::LoadCrldpUrls(QStringList* crldp_urls)
{
    QVector<BYTE> buff;
    QString option = RegConstants::CRLDP_URLS_OPTION;
    NTSTATUS status = LoadOption(option, &buff);
    if(status != ERROR_SUCCESS)
    {
        return status;
    }
    QString str = QString::fromWCharArray(reinterpret_cast<wchar_t*>(buff.data()),
                                          buff.size() / sizeof(wchar_t));
    str.replace(QChar('\0'), "");
    *crldp_urls = str.split(";", QString::SkipEmptyParts);
    return ERROR_SUCCESS;
}

void ControlPanel::OnUrlEditFinished(QStringList new_url_list)
{
    QString str;
    int size = 0;
    for(const auto& url : new_url_list)
    {
        str.append(url + ";" + QChar('\0'));
        size += (url.size() + 2)*sizeof(wchar_t);
    }

    RegistryManager regmgr;
    NTSTATUS status = regmgr.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE | KEY_SET_VALUE);
    if(status != ERROR_SUCCESS)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
        return;
    }

    status = regmgr.OpenKey(RegConstants::DEFAULT_PROVIDER_OPTIONS_REG_PATH,
                                     KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE | KEY_SET_VALUE);
    if(status != ERROR_SUCCESS)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
        return;
    }

    status = regmgr.SetKeyValue("",
                                RegConstants::CRLDP_URLS_OPTION,
                                REG_MULTI_SZ,
                                reinterpret_cast<const BYTE*>(str.toStdWString().data()),
                                size);
    if(status != ERROR_SUCCESS)
    {
        QMessageBox::critical(this, tr("Ошибка"), tr("Не удалось изменить настройки"), QMessageBox::Ok);
        qDebug() << status;
        return;
    }

}

void ControlPanel::on_btn_co_hash_clicked()
{
    ui->btn_go_to_co_select->setVisible(true);
    ui->sw_crypto_op->setCurrentIndex(static_cast<int>(CRYPTO_OPERATION::Hash));
}

void ControlPanel::on_btn_co_make_sig_clicked()
{
    ui->btn_go_to_co_select->setVisible(true);
    ui->sw_crypto_op->setCurrentIndex(static_cast<int>(CRYPTO_OPERATION::MakeSig));
}

void ControlPanel::on_btn_co_verify_sig_clicked()
{
    ui->btn_go_to_co_select->setVisible(true);
    ui->sw_crypto_op->setCurrentIndex(static_cast<int>(CRYPTO_OPERATION::VerifySig));
}

void ControlPanel::on_btn_co_enc_clicked()
{
    ui->btn_go_to_co_select->setVisible(true);
    ui->sw_crypto_op->setCurrentIndex(static_cast<int>(CRYPTO_OPERATION::Encryption));
}

void ControlPanel::on_btn_go_to_co_select_clicked()
{
    ui->sw_crypto_op->setCurrentIndex(0);
    ui->btn_go_to_co_select->setVisible(false);
}

void ControlPanel::on_btn_import_key_clicked()
{
    QString key_name = QFileDialog::getOpenFileName(this, "Выберите файл ключа");
    if(key_name.isEmpty()){
        return;
    }
    QFileInfo fi(key_name);

    QString keys_path = QStandardPaths::standardLocations(QStandardPaths::HomeLocation).join("") +
                        "/AppData/Roaming/NII TZI/NTCrypto/Keys/";
    QDir dir(keys_path);
    if(!dir.exists()){
        dir.mkpath(".");
    }
    if(!QFile::copy(key_name, keys_path + fi.fileName())){
        QMessageBox::critical(this, "Ошибка", "Не удалось импортировать ключ", QMessageBox::Ok);
        return;
    }

    RegistryManager regmgr;
    NTSTATUS status = regmgr.OpenCurrentUser(KEY_WRITE);
    if (status != ERROR_SUCCESS)
    {
        QMessageBox::critical(this, "Ошибка", "Не удалось импортировать ключ", QMessageBox::Ok);
        return;
    }

    status = regmgr.CreateKey(RegConstants::PROVIDER_KEYS_REG_PATH+fi.fileName());
    if (status != ERROR_SUCCESS)
    {
        QMessageBox::critical(this, "Ошибка", "Не удалось импортировать ключ", QMessageBox::Ok);
        return;
    }
    DWORD device_type = 1;
    DWORD slot_id = 0;
    status = regmgr.SetKeyValue("",
                                RegConstants::KEY_STORAGE_OPTION,
                                REG_DWORD,
                                reinterpret_cast<const BYTE*>(&device_type),
                                sizeof(device_type));
    if (status != ERROR_SUCCESS)
    {
        QMessageBox::critical(this, "Ошибка", "Не удалось импортировать ключ", QMessageBox::Ok);
        return;
    }

    status = regmgr.SetKeyValue("",
                                RegConstants::KEY_STORAGE_SLOT_ID_OPTION,
                                REG_DWORD,
                                reinterpret_cast<BYTE*>(&slot_id),
                                sizeof(slot_id));
    if (status != ERROR_SUCCESS)
    {
        QMessageBox::critical(this, "Ошибка", "Не удалось импортировать ключ", QMessageBox::Ok);
        return;
    } else {
        QMessageBox::information(this, "Информация", "Операция выполнена успешно");
    }
    FillContainersTable();
    ui->tw_containers->resizeRowsToContents();
}

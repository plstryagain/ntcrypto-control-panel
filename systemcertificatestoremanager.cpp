#include "certificatestoremanager.h"
#include "Constants.h"
#include <string>
#include <QDebug>

SystemCertificateStoreManager::SystemCertificateStoreManager(const CERTIFICATE_STORE_TYPE cst)
    : cst_(cst), hStore_(nullptr)
{

}

SystemCertificateStoreManager::~SystemCertificateStoreManager()
{
    if(hStore_){
        CertCloseStore(hStore_, 0);
    }
}

bool SystemCertificateStoreManager::OpenStore()
{
    std::wstring store_name;
    switch(cst_){
    case CERTIFICATE_STORE_TYPE::MY:
        store_name = L"MY";
        break;
    case CERTIFICATE_STORE_TYPE::ROOT:
        store_name = L"ROOT";
        break;
    default:
        return false;
    }
    hStore_ = CertOpenStore(CERT_STORE_PROV_SYSTEM,
                         0,
                         0,
                         CERT_SYSTEM_STORE_CURRENT_USER,
                         store_name.data());
    return hStore_ ? true : false;
}

void SystemCertificateStoreManager::CloseStore()
{
    if(!hStore_){
        return;
    }
    CertCloseStore(hStore_, 0);
    hStore_ = nullptr;
}

void SystemCertificateStoreManager::EnumCertificates()
{
    if(!hStore_){
        return;
    }

    PCCERT_CONTEXT pCert = nullptr;
    while(pCert = CertEnumCertificatesInStore(hStore_, pCert))
    {
        auto scmgr = ICertificateManager::Create(CERTIFICATE_TYPE::SystemCertificate,
                                                 CertDuplicateCertificateContext(pCert), true);
        crt_mgr_list_.push_back(std::move(scmgr));
    }
}

DWORD SystemCertificateStoreManager::AddToStore(const std::unique_ptr<ICertificateManager>& mgr, const DWORD flag)
{
    return AddToStore(mgr.get(), flag);
}

DWORD SystemCertificateStoreManager::AddToStore(const ICertificateManager* mgr, const DWORD flag)
{
    if(!hStore_){
        return NTE_INVALID_PARAMETER;
    }
    PCCERT_CONTEXT pCert = static_cast<PCCERT_CONTEXT>(mgr->GetRawContextPtr());
    if(!pCert){
        return NTE_FAIL;
    }
    QVector<BYTE> hval;
    if(!CalculateCertHash(pCert->pbCertEncoded, pCert->cbCertEncoded, &hval)){
        return GetLastError();
    }
    DATA_BLOB db = { static_cast<DWORD>(hval.size()), hval.data() };
    if(!CertSetCertificateContextProperty(pCert, CERT_SIGNATURE_HASH_PROP_ID, 0, &db)){
        return GetLastError();
    }
    if(!CertAddCertificateContextToStore(hStore_, pCert, flag, nullptr)){
        return GetLastError();
    } else {
        return ERROR_SUCCESS;
    }
}

bool SystemCertificateStoreManager::CalculateCertHash(const BYTE* pbCertEncoded, const DWORD cbCertEncoded,
                                                      QVector<BYTE>* hval)
{
    bool res = false;
    HCRYPTPROV hProv = NULL;
    DWORD hlen = 0;

    if(!CryptAcquireContext(&hProv, nullptr, nullptr, Constants::BIGN_CSP_TYPE, CRYPT_VERIFYCONTEXT)){
        goto Exit;
    }

    if(!CryptHashToBeSigned(hProv, X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, pbCertEncoded,
                            cbCertEncoded, nullptr, &hlen)){
        goto Exit;
    }

    hval->resize(hlen);
    if(!CryptHashToBeSigned(hProv, X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, pbCertEncoded,
                            cbCertEncoded, hval->data(), &hlen)){
        goto Exit;
    }

    res = true;

Exit:
    if(hProv){
        CryptReleaseContext(hProv, 0);
    }
    return res;
}

DWORD SystemCertificateStoreManager::RemoveFromStore(const std::unique_ptr<ICertificateManager> &mgr)
{
    return RemoveFromStore((mgr.get()));
}
DWORD SystemCertificateStoreManager::RemoveFromStore(const ICertificateManager* mgr)
{
    if(!hStore_){
        return NTE_INVALID_PARAMETER;
    }
    PCCERT_CONTEXT pCert = static_cast<PCCERT_CONTEXT>(mgr->GetRawContextPtr());
    if(!CertDeleteCertificateFromStore(pCert)){
        return GetLastError();
    } else {
        if(!crt_mgr_list_.empty()){
            for(size_t i = 0; i < crt_mgr_list_.size(); ++i){
                if(crt_mgr_list_.at(i).get() == mgr){
                    auto it = crt_mgr_list_.begin();
                    std::advance(it, i);
                    crt_mgr_list_.erase(it);
                    break;
                }
            }
        }
        return ERROR_SUCCESS;
    }
}
DWORD SystemCertificateStoreManager::RemoveFromStoreByIndex(const quint32 idx)
{
    if(idx >= crt_mgr_list_.size()){
        return NTE_INVALID_PARAMETER;
    }
    return RemoveFromStore(crt_mgr_list_.at(idx));
}

#ifndef ICERTIFICATEMANAGER_H
#define ICERTIFICATEMANAGER_H

#include <Windows.h>
#include <memory>
#include <vector>
#include <QString>
#include <QDebug>

enum class CERTIFICATE_TYPE
{
    InvalidCertificateType = -1,
    SystemCertificate = 0,
    X509Certificate = 1,
    CRL = 2,
    AttributeCertificate = 3
};

class ICertificateManager
{
protected:
    ICertificateManager();
    ICertificateManager(const ICertificateManager& other) = delete;

public:
    static std::unique_ptr<ICertificateManager> Create(const CERTIFICATE_TYPE& ct, const void* ctx,
                                                       const bool is_need_free_ctx);
    virtual ~ICertificateManager() {  }

public:
    virtual QString GetVersion() const = 0;
    virtual QString GetSerialNumber() const = 0;
    virtual QString GetSignatureAlgName() const = 0;
    virtual QString GetHashAlgName() const = 0;
    virtual QString GetCertIssuerName() const = 0;
    virtual QString GetCertIssuerSimpleName() const = 0;
    virtual QString GetNotBefore() const = 0;
    virtual QString GetNotAfter() const = 0;
    virtual std::vector<std::vector<unsigned char>> GetExtensionOidList() const = 0;
    virtual QString GetExtansionNameString(const std::vector<unsigned char>& oid) const = 0;
    virtual QString GetExtensionValueString(const std::vector<unsigned char>& oid) const = 0;
    virtual PCCERT_CHAIN_CONTEXT GetCertificateChain() = 0;
    virtual QString CertTrustStatusToString(const CERT_TRUST_STATUS& cts) const = 0;
    virtual const void* GetRawContextPtr() const = 0;
};

class SystemCertificateManager final : public ICertificateManager
{
public:
    SystemCertificateManager(PCCERT_CONTEXT ctx, const bool is_need_free_ctx);
    ~SystemCertificateManager();
    SystemCertificateManager(const SystemCertificateManager& other) = delete;

public:
    QString GetVersion() const override;
    QString GetSerialNumber() const override;
    QString GetSignatureAlgName() const override;
    QString GetHashAlgName() const override;
    QString GetCertIssuerName() const override;
    QString GetCertIssuerSimpleName() const override;
    QString GetNotBefore() const override;
    QString GetNotAfter() const override;
    std::vector<std::vector<unsigned char>> GetExtensionOidList() const override;
    QString GetExtansionNameString(const std::vector<unsigned char>& oid) const override;
    QString GetExtensionValueString(const std::vector<unsigned char>& oid) const override;
    PCCERT_CHAIN_CONTEXT GetCertificateChain() override;
    QString CertTrustStatusToString(const CERT_TRUST_STATUS& cts) const override;
    const void* GetRawContextPtr() const override;

public:
    QString GetCertSubjectName() const;
    QString GetCertSubjectSimpleName() const;
    QString GetPublicKeyAlgName() const;
    QString GetPublicKeyValueString() const;
    QString GetPublicKeyParamString() const;
    bool IsRootCertificate() const;
    bool IsPrivateKeyForCertExist() const ;
    QString GetPrivKeyCSPName() const;
    QString GetPrivKeyName() const;

private:
    PCCERT_CONTEXT pCert_;
    PCCERT_CHAIN_CONTEXT pChain_;
    bool is_need_free_ctx_;
};

class AttributeCertificateParser;

class AttributeCertificateManager final : public ICertificateManager
{
public:
    AttributeCertificateManager(const QByteArray* ba);
    AttributeCertificateManager(const std::vector<unsigned char>& ba);
    ~AttributeCertificateManager();
    AttributeCertificateManager(const AttributeCertificateManager& other) = delete;

public:
    QString GetVersion() const override;
    QString GetSerialNumber() const override;
    QString GetSignatureAlgName() const override;
    QString GetHashAlgName() const override;
    QString GetCertIssuerName() const override;
    QString GetCertIssuerSimpleName() const override;
    QString GetNotBefore() const override;
    QString GetNotAfter() const override;
    std::vector<std::vector<unsigned char>> GetExtensionOidList() const override;
    QString GetExtansionNameString(const std::vector<unsigned char>& oid) const override;
    QString GetExtensionValueString(const std::vector<unsigned char>& oid) const override;
    PCCERT_CHAIN_CONTEXT GetCertificateChain() override;
    QString CertTrustStatusToString(const CERT_TRUST_STATUS& cts) const override;
    const void* GetRawContextPtr() const override;

public:
    bool Init();
    QString GetCertHolderName() const;
    QString GetCertHolderSimpleName() const;
    QString GetHolderCertificateSerialNumber() const;
    std::vector<std::vector<unsigned char>> GetAttributeOidList() const;
    QString GetAttributeNameString(const std::vector<unsigned char>& oid) const;
    QString GetAttributeValueString(const std::vector<unsigned char>& oid) const;
    bool CheckSignature(QString *status);

private:
    PCCERT_CONTEXT FindHolderCertificate(const std::vector<unsigned char>& key_id) const;

private:
    QByteArray pCert_;
    std::unique_ptr<AttributeCertificateParser> acp_;
    PCCERT_CHAIN_CONTEXT pChain_;
};

class CrlManager final : public ICertificateManager
{
public:
    CrlManager(PCCRL_CONTEXT pCrl, const bool is_need_free_ctx);
    ~CrlManager();
    CrlManager(const CrlManager& other) = delete;

public:
    QString GetVersion() const override;
    QString GetSerialNumber() const override;
    QString GetSignatureAlgName() const override;
    QString GetHashAlgName() const override;
    QString GetCertIssuerName() const override;
    QString GetCertIssuerSimpleName() const override;
    QString GetNotBefore() const override;
    QString GetNotAfter() const override;
    std::vector<std::vector<unsigned char>> GetExtensionOidList() const override;
    QString GetExtansionNameString(const std::vector<unsigned char>& oid) const override;
    QString GetExtensionValueString(const std::vector<unsigned char>& oid) const override;
    PCCERT_CHAIN_CONTEXT GetCertificateChain() override;
    QString CertTrustStatusToString(const CERT_TRUST_STATUS& cts) const override;
    const void* GetRawContextPtr() const override;

public:
    bool IsExpired() const;
    DWORD GetCrlEntryCount() const;
    QString GetCrlEntrySerialNumber(const DWORD idx) const;
    QString GetCrlEntryRevokeDate(const DWORD idx) const;
    QString GetCrlEntryRevokeReason(const DWORD idx) const;

private:
    PCCRL_CONTEXT pCrl_;
    bool is_need_free_ctx_;
};

#endif // ICERTIFICATEMANAGER_H

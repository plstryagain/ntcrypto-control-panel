#include "downloadcrlwizard.h"
#include "downloadcrlmanager.h"

DownloadCrlWizard::DownloadCrlWizard(QWidget* parent, const QStringList &url_list)
    : parent_(parent), url_list_(url_list)
{
    this->setWindowTitle("Пожалуйста подождите");
    this->setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setLayout(&layout_);


    layout_.addRow(&lb_plswait_);
    layout_.addRow(&lb_url_);
    lb_plswait_.setText("Идёт получение списка отозванных сертификатов...");
}

void DownloadCrlWizard::showEvent(QShowEvent* event)
{
    Q_UNUSED(event);
    QRect parentRect(parent_->mapToGlobal(QPoint(0, 0)), parent_->size());
    this->move(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), parentRect).topLeft());
    QThread* thread = new QThread();
    DownloadCrlManager* dmc = new DownloadCrlManager(nullptr, url_list_);
    dmc->moveToThread(thread);
    connect(thread, SIGNAL(started()), dmc, SLOT(DownloadCrls()));
    connect(dmc, SIGNAL(queryProxyCredentials(QNetworkProxy,QAuthenticator*)),
            this, SLOT(OnQueryProxyCredentials(QNetworkProxy,QAuthenticator*)));
    connect(dmc, SIGNAL(replyFinished(QString)), this, SLOT(OnReplyFinished(QString)));
    connect(dmc, SIGNAL(sendCrls(QList<QByteArray>)), this, SLOT(ReceiveCrls(QList<QByteArray>)));
    connect(dmc, SIGNAL(finished()), thread, SLOT(quit()));
    connect(dmc, SIGNAL(finished()), dmc, SLOT(deleteLater()));
    connect(dmc, SIGNAL(finished()), this, SLOT(close()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();
}

void DownloadCrlWizard::OnQueryProxyCredentials(QNetworkProxy proxy, QAuthenticator* authentificator)
{
    Q_UNUSED(proxy);
    QDialog proxy_dlg;
    QLineEdit ln_login;
    QLineEdit ln_pass;
    QLabel lb_message;
    ln_pass.setEchoMode(QLineEdit::Password);
    proxy_dlg.setWindowTitle(tr("Требуется аутентификация"));
    proxy_dlg.setWindowFlags(proxy_dlg.windowFlags() & ~Qt::WindowContextHelpButtonHint);
    QFormLayout layout(&proxy_dlg);
    lb_message.setText(tr("Прокси-сервер запрашивает\nимя пользователя и пароль."));
    layout.addRow(&lb_message);
    layout.addRow(tr("Имя пользователя:"), &ln_login);
    layout.addRow(tr("Пароль:"),&ln_pass);
    QDialogButtonBox btnBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(&btnBox, SIGNAL(accepted()), &proxy_dlg, SLOT(accept()));
    connect(&btnBox, SIGNAL(rejected()), &proxy_dlg, SLOT(reject()));
    layout.addRow(&btnBox);
    proxy_dlg.exec();
    if(proxy_dlg.result() == QDialog::Rejected)
    {
        return;
    }
    else
    {
        authentificator->setUser(ln_login.text());
        authentificator->setPassword(ln_pass.text());
        return;
    }
}

void DownloadCrlWizard::OnReplyFinished(QString url)
{
    lb_url_.setText("\n" + url + " получен");
    qDebug() << url << "\n";
}

void DownloadCrlWizard::ReceiveCrls(QList<QByteArray> crls)
{
    crls_ = crls;
}

QList<QByteArray> DownloadCrlWizard::GetCrls()
{
    return crls_;
}

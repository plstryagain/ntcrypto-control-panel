#ifndef CRLDPWIZARD_H
#define CRLDPWIZARD_H

#include <QObject>
#include <QDialog>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QListWidget>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QSpacerItem>
#include <QLineEdit>
#include <QFormLayout>
#include <QMessageBox>

class CrlDpWizard : public QDialog
{

    Q_OBJECT

public:
    CrlDpWizard(QWidget* parent, const QStringList& crldp_urls);
    ~CrlDpWizard();

signals:
    void urlEditFinished(QStringList);

protected:
    void showEvent(QShowEvent *event) override;

private slots:
    void OnBtnCancelClicked();
    void OnBtnOkClicked();
    void OnBtnAddClicked();
    void OnBtnEditClicked();
    void OnBtnDelClicked();
    bool ShowUrlEditDlg(QString* text);

private:
    QWidget* parent_;
    QGridLayout gl_;
    QVBoxLayout vl_;
    QDialogButtonBox btn_box_;
    QPushButton btn_add_;
    QPushButton btn_edit_;
    QPushButton btn_del_;
    QStringList crldp_urls_;
    QListWidget list_urls_;
    QSpacerItem* spacer_;
    QLineEdit ln_edit_;
    QDialogButtonBox edit_btn_box_;
};

#endif // CRLDPWIZARD_H

#ifndef CERTIFICATESTOREMANAGER_H
#define CERTIFICATESTOREMANAGER_H

#include <Windows.h>
#include <memory>
#include <vector>
#include "icertificatemanager.h"

enum class CERTIFICATE_STORE_TYPE
{
    INVALID = -1,
    MY,
    ROOT,
    CRL,
    SIGMA,
    NTSTORE,
    ATTRIBUTE
};

class CertificateStoreManager
{
public:
    static std::unique_ptr<CertificateStoreManager> Create(const CERTIFICATE_STORE_TYPE cst);
    virtual ~CertificateStoreManager(){}

protected:
    CertificateStoreManager();
    CertificateStoreManager(const CertificateStoreManager& other) = delete;

public:
    virtual bool OpenStore() = 0;
    virtual void CloseStore() = 0;
    virtual void EnumCertificates() = 0;
    virtual DWORD AddToStore(const std::unique_ptr<ICertificateManager> &mgr, const DWORD flag) = 0;
    virtual DWORD AddToStore(const ICertificateManager* mgr, const DWORD flag) = 0;
    virtual DWORD RemoveFromStore(const std::unique_ptr<ICertificateManager> &mgr) = 0;
    virtual DWORD RemoveFromStore(const ICertificateManager* mgr) = 0;
    virtual DWORD RemoveFromStoreByIndex(const quint32 idx) = 0;

public:
    const std::vector<std::unique_ptr<ICertificateManager>>& GetCertificateManagerList() const;
    const std::unique_ptr<ICertificateManager>& GetCertificateManagerByIndex(const quint32 idx) const;

protected:
    std::vector<std::unique_ptr<ICertificateManager>> crt_mgr_list_;
};

class SystemCertificateStoreManager : public CertificateStoreManager
{
public:
    SystemCertificateStoreManager(const CERTIFICATE_STORE_TYPE cst);
    ~SystemCertificateStoreManager();
    SystemCertificateStoreManager() = delete;
    SystemCertificateStoreManager(const  SystemCertificateStoreManager& other) = delete;

public:
    bool OpenStore() override;
    void CloseStore() override;
    void EnumCertificates() override;
    DWORD AddToStore(const std::unique_ptr<ICertificateManager> &mgr, const DWORD flag) override;
    DWORD AddToStore(const ICertificateManager* mgr, const DWORD flag) override;
    DWORD RemoveFromStore(const std::unique_ptr<ICertificateManager> &mgr)override;
    DWORD RemoveFromStore(const ICertificateManager* mgr) override;
    DWORD RemoveFromStoreByIndex(const quint32 idx) override;

private:
    bool CalculateCertHash(const BYTE* pbCertEncoded, const DWORD cbCertEncoded, QVector<BYTE>* hval);

private:
    HCERTSTORE hStore_;
    CERTIFICATE_STORE_TYPE cst_;
};

class SigmaCertificateStoreManager final : public CertificateStoreManager
{
public:
    SigmaCertificateStoreManager();
    ~SigmaCertificateStoreManager();
    SigmaCertificateStoreManager(const SigmaCertificateStoreManager& other) = delete;

public:
    bool OpenStore() override;
    void CloseStore() override;
    void EnumCertificates() override;
    DWORD AddToStore(const std::unique_ptr<ICertificateManager> &mgr, const DWORD flag) override;
    DWORD AddToStore(const ICertificateManager* mgr, const DWORD flag) override;
    DWORD RemoveFromStore(const std::unique_ptr<ICertificateManager> &mgr)override;
    DWORD RemoveFromStore(const ICertificateManager* mgr) override;
    DWORD RemoveFromStoreByIndex(const quint32 idx) override;

private:
    HCERTSTORE hStore_;
    HCRYPTPROV hProv_;
};

class NtstoreCertificateStoreManager final : public CertificateStoreManager
{
public:
    NtstoreCertificateStoreManager();
    ~NtstoreCertificateStoreManager();
    NtstoreCertificateStoreManager(const NtstoreCertificateStoreManager& other) = delete;

public:
    bool OpenStore() override;
    void CloseStore() override;
    void EnumCertificates() override;
    DWORD AddToStore(const std::unique_ptr<ICertificateManager> &mgr, const DWORD flag) override;
    DWORD AddToStore(const ICertificateManager* mgr, const DWORD flag) override;
    DWORD RemoveFromStore(const std::unique_ptr<ICertificateManager> &mgr)override;
    DWORD RemoveFromStore(const ICertificateManager* mgr) override;
    DWORD RemoveFromStoreByIndex(const quint32 idx) override;

private:
    HCERTSTORE hStore_;
    HCRYPTPROV hProv_;
};

class AttributeCertificateStoreManager final : public CertificateStoreManager
{
public:
    AttributeCertificateStoreManager();
    ~AttributeCertificateStoreManager();
    AttributeCertificateStoreManager(const AttributeCertificateStoreManager& other) = delete;

public:
    bool OpenStore() override;
    void CloseStore() override;
    void EnumCertificates() override;
    DWORD AddToStore(const std::unique_ptr<ICertificateManager> &mgr, const DWORD flag) override;
    DWORD AddToStore(const ICertificateManager* mgr, const DWORD flag) override;
    DWORD RemoveFromStore(const std::unique_ptr<ICertificateManager> &mgr)override;
    DWORD RemoveFromStore(const ICertificateManager* mgr) override;
    DWORD RemoveFromStoreByIndex(const quint32 idx) override;

private:
    bool is_store_open_ = false;
    QString att_store_path_;
};

class CrlStoreManager final : public CertificateStoreManager
{
public:
    CrlStoreManager();
    ~CrlStoreManager();
    CrlStoreManager(const CrlStoreManager& other) = delete;

public:
    bool OpenStore() override;
    void CloseStore() override;
    void EnumCertificates() override;
    DWORD AddToStore(const std::unique_ptr<ICertificateManager> &mgr, const DWORD flag) override;
    DWORD AddToStore(const ICertificateManager* mgr, const DWORD flag) override;
    DWORD RemoveFromStore(const std::unique_ptr<ICertificateManager> &mgr)override;
    DWORD RemoveFromStore(const ICertificateManager* mgr) override;
    DWORD RemoveFromStoreByIndex(const quint32 idx) override;

private:
    HCERTSTORE hStore_;
};

#endif // CERTIFICATESTOREMANAGER_H

#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H

#include <QWidget>
#include <QListWidget>
#include <QList>
#include <QTableWidgetItem>
#include "ntcrypto.h"
#include "registry.h"
#include "certificatemanager.h"
#include <memory>

#define SERVICE_CONTROL_INTEGRITY_CHECK	129
#define SERVICE_CONTROL_CLOSE_SHARED_HANDLE 130

typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);

namespace Ui {
class ControlPanel;
}

enum class MenuItems : int
{
    Information,
    Preferences,
    KeyContainers,
    Certificates,
    Composition,
    Services,
    CryptographicOperations
};

enum class MENU_STATE_FLAG : unsigned long
{
    INFORMATION_STATE_FLAG = 0x00000001,
    PREFERENCES_STATE_FLAG = 0x00000002,
    KEY_CONTAINERS_STATE_FLAG = 0x00000004,
    CERTIFICATES_STATE_FLAG = 0x00000008,
    COMPOSITION_STATE_FLAG = 0x000000010,
    SERVICES_STATE_FLAG = 0x00000020,
    CRYPTO_OP_STATE_FLAG = 0x00000040
};

enum class CRYPTO_OPERATION
{
    Hash = 1,
    MakeSig = 2,
    VerifySig = 3,
    Encryption = 4
};


const QStringList sys_modules_list = { "ntcrypto.dll", "ntcrypt32.dll" };

const QStringList modules_list = {"ntcryptocore.dll", "ntcryptoext.dll",
                            "ntkeyisosrvc.exe", "libnki3.0.dll", "ntstore.dll", "ntcryptoui.dll" };

#ifndef _WIN_XP_
    const QStringList cng_sys_modules_list = { "ntcryptocng.dll", "ntcryptoksp.dll" };
    const QStringList cng_modules_list = { "ntcryptocngext.dll", "ntcngkeyisosrvc.exe" };
#endif



class CertificateStoreManager;
class HashOperationWizard;
class SigOperationWizard;
class SigVerifyOperationWizard;
class EncryptOperationWizard;

class ControlPanel : public QWidget
{
    Q_OBJECT

public:
    explicit ControlPanel(QWidget *parent = 0);
    ~ControlPanel();

private slots:

    void on_list_widget_menu_itemClicked(QListWidgetItem *item);
    void GetCurrentVersion();
    NTSTATUS LoadLogFilePath(const bool is_cng_log);
    bool IsRegOptionsExist();
    NTSTATUS CreateDefaultRegOptions();
    NTSTATUS LoadDefaultStorageValue();
    void CheckIntegrity();
    void FillModulesTable(void* cr, const bool is_win64, int* curr_row_, const QString& qpath);
    void FillCngModulesTable(void* ccr, const bool is_win64, int curr_row, const QString &qpath);
    void FillContainersTable();
    QString GetModuleVersion(const QString& path);
    QString ErrorCodeToString(const DWORD error_code);
    void FillCertsList();
    void FillCrlList();
    void FillSystemCertList();
    void FillAttributeCertList();

    void on_btn_cont_refresh_clicked();

    void on_btn_change_pass_clicked();

    void on_rb_user_toggled(bool checked);

    void on_btn_del_container_clicked();

    void on_lw_certs_itemDoubleClicked(QListWidgetItem *item);

    void on_btn_cers_list_refresh_clicked();

    void on_btn_show_log_clicked();

    void on_btn_open_log_file_folder_clicked();

    void on_btn_clear_log_clicked();

    void on_btn_import_cert_clicked();

    void on_tw_containers_itemDoubleClicked(QTableWidgetItem *item);

    bool IsWin64bit();
    QByteArray CalculateModuleHash(const QString& path);

    void on_btn_change_log_path_clicked();

    QString GetStringAlgById(const ALG_ID alg_id);

    void on_btn_reg_key_clicked();

    void on_btn_unreg_key_clicked();

    NTSTATUS LoadOption(const QString& option, QVector<BYTE>* data);

    void on_btn_pref_apply_clicked();

    void on_btn_pref_cancel_clicked();

    void on_btn_pref_ok_clicked();

    DWORD GetServiceStatus(const QString& srvc_name);

    void ServiceStatusToString(const DWORD status);

    void on_btn_svc_refresh_clicked();

    void CngServiceStatusToString(const DWORD status);

    void on_btn_svc_cng_refresh_clicked();

    void on_btn_svc_stop_clicked();

    void ServiceStop(const QString& svc_name);

    void on_btn_svc_cng_stop_clicked();

    void ServiceStart(const QString& svc_name);

    void on_btn_svc_start_clicked();

    void on_btn_svc_cng_start_clicked();

    void on_btn_show_log_cng_clicked();

    void ShowLog(const bool is_cng_log);

    void on_btn_open_log_file_cng_folder_clicked();

    void OpenLogFileFolder(const bool is_cng_log);

    void on_btn_clear_log_cng_clicked();

    void ClearLog(const bool is_cng_log);

    void on_stacked_widget_main_currentChanged(int arg1);

    void on_btn_gen_key_pair_clicked();

    void on_btn_req_cert_clicked();

    void on_cb_cert_store_currentIndexChanged(int index);

    void ShowCertContextMenu(const QPoint& pos);

    void ShowPrivKeyInfo();

    void LinkKeyWithcert();

    void ImportCertToSysStore();

    void RemoveCertFromStore();
    void RemoveCrlFromStore();
    void RemoveAttCertFromStore();
    void DeleteContainer();

    void ChangePass();

    void on_btn_change_log_cng_path_clicked();

    DWORD FindInstalledPath(std::wstring* installed_path);

    void ShowModulesContextMenu(const QPoint &pos);

    void RecheckIntegrity();

    void on_btn_selftest_clicked();

    void on_btn_get_crl_clicked();

    NTSTATUS LoadCrldpUrls(QStringList *crldp_urls);

    void on_btn_crldl_view_clicked();

    void OnUrlEditFinished(QStringList new_url_list);

    void on_btn_co_hash_clicked();

    void on_btn_co_make_sig_clicked();

    void on_btn_co_verify_sig_clicked();

    void on_btn_co_enc_clicked();

    void on_btn_go_to_co_select_clicked();

    void on_btn_import_key_clicked();

private:
    Ui::ControlPanel *ui;
    unsigned long MENU_STATE_FLAGS;
    QString curr_log_file_path_;
    QString curr_installed_path_;
    int curr_default_storage_;
    std::unique_ptr<CertificateStoreManager> csmgr_;
    std::unique_ptr<HashOperationWizard> how_;
    std::unique_ptr<SigOperationWizard> sow_;
    std::unique_ptr<SigVerifyOperationWizard> svow_;
    std::unique_ptr<EncryptOperationWizard> eow_;
};

#endif // CONTROLPANEL_H

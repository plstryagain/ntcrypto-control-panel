#include "selftestwizard.h"
#include "test.h"
#include "Constants.h"
#include <Qdebug>
#include <QFormLayout>
#include <QTextBrowser>

SelftestWizard::SelftestWizard(QWidget* parent)
    : parent_(parent), btn_box_(QDialogButtonBox::Ok | QDialogButtonBox::Cancel
                                           | QDialogButtonBox::Open)
{
    this->setWindowTitle(tr("Самотестирование"));
    this->setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowIcon(QIcon(QString::fromUtf8(":/images/images/Administrative Tools_50px.png")));
    this->setLayout(&gl_);

    hl_.addWidget(&rb_sigma_);
    hl_.addWidget(&rb_ntstore_);
    gl_.addWidget(&rb_soft_, 0, 0);
    gl_.addLayout(&hl_, 0, 1);
    gl_.addWidget(&lb_stb31, 1, 0);
    gl_.addWidget(&bar_stb31, 1, 1);
    gl_.addWidget(&lb_stb31_status, 1, 2);
    gl_.addWidget(&lb_stb45, 2, 0);
    gl_.addWidget(&bar_stb45, 2, 1);
    gl_.addWidget(&lb_stb45_status, 2, 2);
    gl_.addWidget(&lb_stb77, 3, 0);
    gl_.addWidget(&bar_stb77, 3, 1);
    gl_.addWidget(&lb_stb77_status, 3, 2);
    gl_.addWidget(&btn_box_, 4, 1);

    rb_soft_.setChecked(true);
    rb_soft_.setText(tr("Программная\nреализация"));
    rb_sigma_.setText(tr("СКЗИ \"Сигма\""));
    rb_ntstore_.setText(tr("СКЗИ NTStore"));
    lb_stb31.setText(tr("СТБ 34.101.31 - 2011"));
    lb_stb31_status.setText(tr("Ожидание..."));
    lb_stb45.setText(tr("СТБ 34.101.45 - 2013"));
    lb_stb45_status.setText(tr("Ожидание..."));
    lb_stb77.setText(tr("СТБ 34.101.77 - 2016"));
    lb_stb77_status.setText(tr("Ожидание..."));
    btn_box_.button(QDialogButtonBox::Ok)->setText(tr("Начать тест"));
    btn_box_.button(QDialogButtonBox::Cancel)->setText(tr("Закрыть"));
    btn_box_.button(QDialogButtonBox::Open)->setText(tr("Показать отчёт"));
    btn_box_.button(QDialogButtonBox::Cancel)->setIcon(QIcon(":/images/images/Delete_32px.png"));
    btn_box_.button(QDialogButtonBox::Ok)->setIcon(QIcon(":/images/images/Play_32px.png"));
    btn_box_.button(QDialogButtonBox::Open)->setIcon(QIcon(":/images/images/View_32px.png"));
    gl_.setSizeConstraint(QLayout::SetFixedSize);

    bar_stb77.setMinimum(0);
    bar_stb77.setMaximum(10);


    connect(&btn_box_, SIGNAL(rejected()), this, SLOT(btn_cancel_clicked()));
    connect(btn_box_.button(QDialogButtonBox::Ok), SIGNAL(clicked(bool)), this, SLOT(btn_ok_clicked()));
    connect(btn_box_.button(QDialogButtonBox::Open), SIGNAL(clicked(bool)), this, SLOT(ShowReport()));
    connect(&rb_sigma_, SIGNAL(toggled(bool)), this, SLOT(InitUiByDevType()));
    connect(&rb_soft_, SIGNAL(toggled(bool)), this, SLOT(InitUiByDevType()));
    connect(&rb_ntstore_, SIGNAL(toggled(bool)), this, SLOT(InitUiByDevType()));
}

void SelftestWizard::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    QRect parentRect(parent_->mapToGlobal(QPoint(0, 0)), parent_->size());
    this->move(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), parentRect).topLeft());
}

void SelftestWizard::InitUiByDevType()
{
    if(rb_soft_.isChecked())
    {
        bar_stb77.setEnabled(true);
        lb_stb77.setEnabled(true);
        lb_stb77_status.setEnabled(true);
        lb_stb77_status.setText(tr("Ожидание..."));
    }
    else if (rb_sigma_.isChecked())
    {
        bar_stb77.setEnabled(false);
        lb_stb77.setEnabled(false);
        lb_stb77_status.setEnabled(false);
        lb_stb77_status.setText(tr("Не доступен"));
    }
    else if (rb_ntstore_.isChecked())
    {
        bar_stb77.setEnabled(true);
        lb_stb77.setEnabled(true);
        lb_stb77_status.setEnabled(true);
        lb_stb77_status.setText(tr("Ожидание..."));
    }
    else
    {
        return;
    }
}

void SelftestWizard::Reset()
{
    if(rb_ntstore_.isChecked())
    {
        bar_stb31.setMaximum(2);
        bar_stb45.setMaximum(2);
    }
    else
    {
        bar_stb31.setMaximum(17);
        bar_stb45.setMaximum(5);
    }
    bar_stb31.setMinimum(0);
    bar_stb45.setMinimum(0);
    report_.clear();
    bar_stb31.reset();
    bar_stb45.reset();
    bar_stb77.reset();
    lb_stb31.setText(tr("СТБ 34.101.31 - 2011"));
    lb_stb31_status.setText(tr("Ожидание..."));
    lb_stb45.setText(tr("СТБ 34.101.45 - 2013"));
    lb_stb45_status.setText(tr("Ожидание..."));
    lb_stb77.setText(tr("СТБ 34.101.77 - 2016"));
    lb_stb77_status.setText(tr("Ожидание..."));
    btn_box_.button(QDialogButtonBox::Open)->setEnabled(false);
}

void SelftestWizard::btn_ok_clicked()
{
    Reset();
    rb_ntstore_.setEnabled(false);
    rb_sigma_.setEnabled(false);
    rb_soft_.setEnabled(false);
    btn_box_.button(QDialogButtonBox::Ok)->setEnabled(false);
    btn_box_.button(QDialogButtonBox::Cancel)->setEnabled(false);
    lb_stb31_status.setText(tr("Идёт тестирование..."));
    DWORD dev_type = 0;
    if(rb_soft_.isChecked())
    {
        dev_type = Constants::SOFT_IMPL_FLAG;
    }
    else if (rb_sigma_.isChecked())
    {
        dev_type = Constants::SIGMA_IMPL_FLAG;
        bar_stb77.setEnabled(false);
        lb_stb77.setEnabled(false);
        lb_stb77_status.setEnabled(false);
    }
    else if (rb_ntstore_.isChecked())
    {
        dev_type = Constants::NTSTORE_IMPL_FLAG;
    }
    else
    {
        return;
    }

    QThread* thread = new QThread();
    Test* test = new Test(0, dev_type);
    test->moveToThread(thread);
    connect(thread, SIGNAL(started()), test, SLOT(LaunchAllTests()));
    connect(test, SIGNAL(signalStb31Progress(int)), &bar_stb31, SLOT(setValue(int)));
    connect(test, SIGNAL(signalStb45Progress(int)), &bar_stb45, SLOT(setValue(int)));
    connect(test, SIGNAL(signalStb77Progress(int)), &bar_stb77, SLOT(setValue(int)));
    connect(test, SIGNAL(signalStb31TestFinish(QString)), this, SLOT(SetStb31Status(QString)));
    connect(test, SIGNAL(signalStb45TestFinish(QString)), this, SLOT(SetStb45Status(QString)));
    connect(test, SIGNAL(signalStb77TestFinish(QString)), this, SLOT(SetStb77Status(QString)));
    connect(test, SIGNAL(signalFinish()), this, SLOT(AnalyzeResult()));
    connect(test, SIGNAL(signalFinish()), thread, SLOT(quit()));
    connect(test, SIGNAL(signalFinish()), test, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();
}

void SelftestWizard::btn_cancel_clicked()
{
    this->close();
}

void SelftestWizard::SetStb31Status(QString report)
{
    report_.append("----СТБ 34.101.31----\n");
    report_.append(report);
    if(report.contains("код ошибки"))
    {
        lb_stb31_status.setText(tr("Тестирование завершено с ошибкой"));
    }
    else
    {
        lb_stb31_status.setText(tr("Тестирование успешно завершено"));
    }
    lb_stb45_status.setText(tr("Идёт тестирование..."));
}

void SelftestWizard::SetStb45Status(QString report)
{
    report_.append("----СТБ 34.101.45----\n");
    report_.append(report);
    if(report.contains("код ошибки"))
    {
        lb_stb45_status.setText(tr("Тестирование завершено с ошибкой"));
    }
    else
    {
        lb_stb45_status.setText(tr("Тестирование успешно завершено"));
    }
    lb_stb77_status.setText(tr("Идёт тестирование..."));
}

void SelftestWizard::SetStb77Status(QString report)
{
    report_.append("----СТБ 34.101.77----\n");
    report_.append(report);
    if(report.contains("код ошибки"))
    {
        lb_stb77_status.setText(tr("Тестирование завершено с ошибкой"));
    }
    else
    {
        lb_stb77_status.setText(tr("Тестирование успешно завершено"));
    }
}

void SelftestWizard::AnalyzeResult()
{
    btn_box_.button(QDialogButtonBox::Cancel)->setEnabled(true);
    btn_box_.button(QDialogButtonBox::Ok)->setEnabled(true);
    btn_box_.button(QDialogButtonBox::Open)->setEnabled(true);
    rb_ntstore_.setEnabled(true);
    rb_sigma_.setEnabled(true);
    rb_soft_.setEnabled(true);
}

void SelftestWizard::ShowReport()
{
    QDialog dlg;
    dlg.setWindowTitle(trUtf8("Отчёт"));
    dlg.setWindowFlags(dlg.windowFlags() & ~Qt::WindowContextHelpButtonHint);
    QFormLayout lt(&dlg);
    QTextBrowser tb;
    tb.append(report_);
    if(report_.contains("код ошибки"))
    {
        tb.setFixedWidth(325);
    }
    lt.addRow(&tb);
    dlg.exec();
}

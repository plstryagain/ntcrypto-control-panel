#include "icertificatemanager.h"
#include "asn1bel/Asn1Decoder.h"
#include "asn1bel/AttributeCertificateParser.h"
#include "Constants.h"
#include <Windows.h>
#include <algorithm>

AttributeCertificateManager::AttributeCertificateManager(const QByteArray *ba)
    : pCert_(*ba), pChain_(nullptr)
{
    acp_ = std::unique_ptr<AttributeCertificateParser>(
                new AttributeCertificateParser(reinterpret_cast<const unsigned char*>(pCert_.data()),
                                               pCert_.size()));
}

AttributeCertificateManager::AttributeCertificateManager(const std::vector<unsigned char>& ba)
    : pCert_(QByteArray::fromRawData(reinterpret_cast<const char*>(ba.data()), ba.size())),
      pChain_(nullptr)
{
    acp_ = std::unique_ptr<AttributeCertificateParser>(
                new AttributeCertificateParser(reinterpret_cast<const unsigned char*>(pCert_.data()),
                                               pCert_.size()));
}

AttributeCertificateManager::~AttributeCertificateManager()
{
    if(pChain_){
        CertFreeCertificateChain(pChain_);
    }
}

const void* AttributeCertificateManager::GetRawContextPtr() const
{
    return static_cast<const void*>(&pCert_);
}

bool AttributeCertificateManager::Init()
{
    return (acp_->Parse() == ASN1_ERROR::success);
}

QString AttributeCertificateManager::GetVersion() const
{
    return QString::fromStdString(acp_->GetCertificateVersionStr());
}

QString AttributeCertificateManager::GetSerialNumber() const
{
    return QString::fromStdWString(acp_->GetCertificateSerialNumberStr());
}

QString AttributeCertificateManager::GetSignatureAlgName() const
{
    std::string oid = acp_->GetSignatureAlgorithmIdentifierStr();
    PCCRYPT_OID_INFO pInfo =  CryptFindOIDInfo(CRYPT_OID_INFO_OID_KEY,
                                               const_cast<char*>(oid.data()),
                                               0);
    if(!pInfo){
        return QString::fromStdString(oid);
    }
    return QString::fromWCharArray(pInfo->pwszName);
}

QString AttributeCertificateManager::GetHashAlgName() const
{
    std::string oid = acp_->GetSignatureAlgorithmIdentifierStr();
    PCCRYPT_OID_INFO pInfo =  CryptFindOIDInfo(CRYPT_OID_INFO_OID_KEY,
                                               const_cast<char*>(oid.data()),
                                               0);
    if(!pInfo){
        return QString::fromStdString(oid);
    }
    PCCRYPT_OID_INFO pHashInfo = CryptFindOIDInfo(CRYPT_OID_INFO_ALGID_KEY,
                                                  const_cast<ALG_ID*>(&pInfo->Algid),
                                                  0);
    if(!pHashInfo){
        return "";
    }
    return QString::fromWCharArray(pHashInfo->pwszName);
}

QString AttributeCertificateManager::GetCertHolderName() const
{
    return QString::fromStdWString(acp_->GetHolderNameStr());
}

QString AttributeCertificateManager::GetCertIssuerName() const
{
    return QString::fromStdWString(acp_->GetIssuerNameStr());
}

QString AttributeCertificateManager::GetCertHolderSimpleName() const
{  
    QString full_name = QString::fromStdWString(acp_->GetHolderNameStr());
    int idx = full_name.indexOf("CN");
    if(idx == -1){
        return full_name;
    }
    int idx2 = full_name.indexOf("\n", idx);
    full_name = full_name.mid(idx + 3, idx2 - idx - 3);
    return full_name;
}

QString AttributeCertificateManager::GetCertIssuerSimpleName() const
{
    QString full_name = QString::fromStdWString(acp_->GetIssuerNameStr());
    int idx = full_name.indexOf("CN");
    if(idx == -1){
        return full_name;
    }
    int idx2 = full_name.indexOf("\n", idx);
    full_name = full_name.mid(idx + 3, idx2 - idx - 3);
    return full_name;
}

QString AttributeCertificateManager::GetNotBefore() const
{
    return QString::fromStdString(acp_->GetNotBeforeStr());
}

QString AttributeCertificateManager::GetNotAfter() const
{
    return QString::fromStdString(acp_->GetNotAfterStr());
}

QString AttributeCertificateManager::GetHolderCertificateSerialNumber() const
{
    return QString::fromStdWString(acp_->GetHolderCertificateSerialNumberStr());
}

std::vector<std::vector<unsigned char>> AttributeCertificateManager::GetExtensionOidList() const
{
    return acp_->GetExtensionsOidList();
}

QString AttributeCertificateManager::GetExtansionNameString(const std::vector<unsigned char>& oid) const
{
    return QString::fromStdWString(acp_->GetExtensionNameStr(oid));
}

QString AttributeCertificateManager::GetExtensionValueString(const std::vector<unsigned char>& oid) const
{
    return QString::fromStdWString(acp_->GetExtensionValueStr(oid));
}

std::vector<std::vector<unsigned char>> AttributeCertificateManager::GetAttributeOidList() const
{
    return acp_->GetAttributeOidList();
}

QString AttributeCertificateManager::GetAttributeNameString(const std::vector<unsigned char>& oid) const
{
    return QString::fromStdWString(acp_->GetAttributeNameStr(oid));
}

QString AttributeCertificateManager::GetAttributeValueString(const std::vector<unsigned char>& oid) const
{
    return QString::fromStdWString(acp_->GetAttributeValueStr(oid));
}

bool AttributeCertificateManager::CheckSignature(QString* status)
{
    bool res = false;
    if(!pChain_){
        status->append("Не удалось проверить подпись.\n"
                      "Не удалось построить цепочку сертификатов.");
        return res;
    }

    HCRYPTPROV hProv = NULL;
    HCRYPTKEY hKey = NULL;
    HCRYPTHASH hHash = NULL;
    ALG_ID hash_alg_id = 0;
    PCCRYPT_OID_INFO pOid = nullptr;
    std::vector<unsigned char> tbs;
    std::vector<unsigned char> sig;

    if(!CryptAcquireContext(&hProv, nullptr, nullptr, Constants::BIGN_CSP_TYPE, Constants::SOFT_IMPL_FLAG)){
        status->append("Не удалось проверить подпись.");
        goto Exit;
    }

    if(!CryptImportPublicKeyInfoEx(hProv, X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
                                   &pChain_->rgpChain[0]->rgpElement[0]->pCertContext->pCertInfo->SubjectPublicKeyInfo,
                                   CALG_BIGN, 0, nullptr, &hKey)) {
        status->append("Не удалось проверить подпись.");
        goto Exit;
    }

    pOid = CryptFindOIDInfo(CRYPT_OID_INFO_OID_KEY,
                            pChain_->rgpChain[0]->rgpElement[0]->pCertContext->pCertInfo->SignatureAlgorithm.pszObjId,
            0);
    if(!pOid){
        status->append("Не удалось проверить подпись.");
        goto Exit;
    }

    hash_alg_id = pOid->Algid;

    if(!CryptCreateHash(hProv, hash_alg_id, 0, 0, &hHash)){
        status->append("Не удалось проверить подпись.");
        goto Exit;
    }

    tbs = acp_->GetToBeSignedValue();
    if(!CryptHashData(hHash, tbs.data(), static_cast<DWORD>(tbs.size()), 0)){
        status->append("Не удалось проверить подпись.");
        goto Exit;
    }

    sig = acp_->GetSignatureValue();

    std::reverse(sig.begin(), sig.end());
    if(!CryptVerifySignature(hHash, sig.data(), static_cast<DWORD>(sig.size()), hKey, nullptr, 0)){
        status->append("Сертификат содержит недействительную подпись.");
        goto Exit;
    }

    status->append("Этот сертификат действителен.");
    res = true;

Exit:
    if(hHash){
        CryptDestroyHash(hHash);
    }
    if(hKey){
        CryptDestroyKey(hKey);
    }
    if(hProv){
        CryptReleaseContext(hProv, 0);
    }
    return res;
}

PCCERT_CHAIN_CONTEXT AttributeCertificateManager::GetCertificateChain()
{
    std::vector<byte> key_id = acp_->GetAuthorityKeyIdValue();
    PCCERT_CONTEXT pHolder = FindHolderCertificate(key_id);
    if(!pHolder){
        return nullptr;
    }
    CERT_CHAIN_PARA chainPara;
    chainPara.cbSize = sizeof(CERT_CHAIN_PARA);
    chainPara.RequestedUsage.dwType = USAGE_MATCH_TYPE_AND;
    chainPara.RequestedUsage.Usage.cUsageIdentifier = 0;
    if(!CertGetCertificateChain(NULL, pHolder, NULL, NULL, &chainPara, CERT_CHAIN_CACHE_END_CERT |
                                CERT_CHAIN_CACHE_ONLY_URL_RETRIEVAL |
                                CERT_CHAIN_DISABLE_MY_PEER_TRUST |
                                CERT_CHAIN_ENABLE_PEER_TRUST |
                                CERT_CHAIN_REVOCATION_ACCUMULATIVE_TIMEOUT |
                                CERT_CHAIN_REVOCATION_CHECK_CHAIN_EXCLUDE_ROOT, NULL, &pChain_)){
        return nullptr;
    } else {
        return pChain_;
    }
}

PCCERT_CONTEXT AttributeCertificateManager::FindHolderCertificate(const std::vector<unsigned char>& key_id) const
{
    QStringList store_names = QStringList() << "MY" << "ROOT";
    PCCERT_CONTEXT pHolder = nullptr;
    for(const auto& name : store_names){
        HCERTSTORE hStore = CertOpenStore(CERT_STORE_PROV_SYSTEM, 0, 0,
                                          CERT_SYSTEM_STORE_CURRENT_USER, name.toStdWString().data());
        if (!hStore) {
            continue;
        }
        while (pHolder = CertEnumCertificatesInStore(hStore, pHolder)) {
            for (DWORD i = 0; i < pHolder->pCertInfo->cExtension; ++i) {
                CERT_EXTENSION pExt = pHolder->pCertInfo->rgExtension[i];
                if (strcmp(pExt.pszObjId, szOID_SUBJECT_KEY_IDENTIFIER) == 0) {
                    if (memcmp(pExt.Value.pbData + 2, key_id.data(), key_id.size()) == 0) {
                        CertCloseStore(hStore, 0);
                        return CertDuplicateCertificateContext(pHolder);
                    }
                }
            }
        }
        CertCloseStore(hStore, 0);
    }
    return pHolder;
}

 QString AttributeCertificateManager::CertTrustStatusToString(const CERT_TRUST_STATUS& cts) const
 {
     QString status = "";
     if(cts.dwErrorStatus == CERT_TRUST_NO_ERROR){
         status.append("Этот сертификат действителен.\n");
         return status;
     }
     if(cts.dwErrorStatus & CERT_TRUST_IS_NOT_TIME_VALID){
         status.append("Срок действия этого сертификата уже истёк или ещё не наступил.\n");
     }
     if(cts.dwErrorStatus & CERT_TRUST_IS_REVOKED){
         status.append("Этот сертификат был отозван.\n");
     }
     if(cts.dwErrorStatus & CERT_TRUST_IS_NOT_SIGNATURE_VALID){
         status.append("Этот сертификат содержит недействительную цифровую подпись.\n");
     }
     //     if(cts.dwErrorStatus & CERT_TRUST_IS_NOT_VALID_FOR_USAGE){
     //         status.append("Этот сертификат "))
     if(cts.dwErrorStatus & CERT_TRUST_IS_UNTRUSTED_ROOT){
         status.append("Нет доверия к корневому сертификату центра сертификации.\n");
     }
     if(cts.dwErrorStatus & CERT_TRUST_REVOCATION_STATUS_UNKNOWN){
         status.append("Статус отзыва сертификата неизвестен.\n");
     }
     if(cts.dwErrorStatus & CERT_TRUST_IS_PARTIAL_CHAIN){
         status.append("Недостаточно информации для проверки этого сертификата.\n");
     }
     if(status.isEmpty()){
         status.append("При проверке сертификата возникла неизвестная ошибка.\n");
     }
     return status;
 }

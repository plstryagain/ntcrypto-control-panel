#ifndef HASHOPERATIONWIZARD_H
#define HASHOPERATIONWIZARD_H

#include <QWidget>
#include <QLineEdit>

namespace Ui {
class HashOperationWizard;
}

class HashOperationWizard : public QWidget
{
    Q_OBJECT

    enum class HASH_ALG
    {
        belt_hash256,
        bash256,
        bash384,
        bash512
    };

    enum class ALG_IMPL
    {
        Soft,
        Sigma,
        NTStore
    };

public:
    explicit HashOperationWizard(QWidget *parent = 0);
    ~HashOperationWizard();

private slots:
    void on_rb_calc_hash_toggled(bool checked);

    void on_rb_compare_files_toggled(bool checked);

    void on_cb_alg_currentIndexChanged(int index);

    void on_btn_start_operation_clicked();
    void Calculated(QString file_name, QByteArray hash_val);
    void Compared(QString result);
    void Error(QString error);

    void on_btn_select_file_one_clicked();

    void on_cb_fil_or_dir_currentIndexChanged(int index);
    void SaveToFile();
    void ShowEmptyLineNotification(QLineEdit *line, const QString& text);
    void ChangeLineBorderColor(QString);

    void on_btn_select_file_two_clicked();

private:
    void CalcHashOperation();
    void CompareOperation();

private:
    Ui::HashOperationWizard *ui;
    bool is_op_success_ = true;
};

#endif // HASHOPERATIONWIZARD_H

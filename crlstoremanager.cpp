#include "certificatestoremanager.h"

CrlStoreManager::CrlStoreManager()
    : hStore_(nullptr)
{

}

CrlStoreManager::~CrlStoreManager()
{
    if(hStore_){
        CertCloseStore(hStore_, 0);
    }
}


bool CrlStoreManager::OpenStore()
{
    hStore_ = CertOpenStore(CERT_STORE_PROV_SYSTEM,
                                0,
                                0,
                                CERT_SYSTEM_STORE_CURRENT_USER,
                                L"CA");
    return hStore_ ? true : false;
}

void CrlStoreManager::CloseStore()
{
    if(!hStore_){
        return;
    }
    CertCloseStore(hStore_, 0);
    hStore_ = nullptr;
}

void CrlStoreManager::EnumCertificates()
{
    if(!hStore_){
        return;
    }

    PCCRL_CONTEXT pCrl = nullptr;
    while(pCrl = CertEnumCRLsInStore(hStore_, pCrl)) {
        auto crlmgr = ICertificateManager::Create(CERTIFICATE_TYPE::CRL,
                                                 CertDuplicateCRLContext(pCrl), false);
        crt_mgr_list_.push_back(std::move(crlmgr));
    }
}

DWORD CrlStoreManager::AddToStore(const std::unique_ptr<ICertificateManager> &mgr, const DWORD flag)
{
    return AddToStore(mgr.get(), flag);
}

DWORD CrlStoreManager::AddToStore(const ICertificateManager* mgr, const DWORD flag)
{
    PCCRL_CONTEXT pCrl = static_cast<PCCRL_CONTEXT>(mgr->GetRawContextPtr());
    if(!CertAddCRLContextToStore(hStore_, pCrl, flag, nullptr)){
        return GetLastError();
    } else {
        return ERROR_SUCCESS;
    }
}

DWORD CrlStoreManager::RemoveFromStore(const std::unique_ptr<ICertificateManager> &mgr)
{
    return RemoveFromStore((mgr.get()));
}

DWORD CrlStoreManager::RemoveFromStore(const ICertificateManager* mgr)
{
    PCCRL_CONTEXT pCrl = static_cast<PCCRL_CONTEXT>(mgr->GetRawContextPtr());
    if(!CertDeleteCRLFromStore(pCrl)){
        return GetLastError();
    } else {
        if(!crt_mgr_list_.empty()){
            for(size_t i = 0; i < crt_mgr_list_.size(); ++i){
                if(crt_mgr_list_.at(i).get() == mgr){
                    auto it = crt_mgr_list_.begin();
                    std::advance(it, i);
                    crt_mgr_list_.erase(it);
                    break;
                }
            }
        }
        return ERROR_SUCCESS;
    }
}

DWORD CrlStoreManager::RemoveFromStoreByIndex(const quint32 idx)
{
    if(idx >= crt_mgr_list_.size()){
        return NTE_INVALID_PARAMETER;
    }
    return RemoveFromStore(crt_mgr_list_.at(idx));
}

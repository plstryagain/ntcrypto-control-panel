#ifndef ERRORSMANAGER_H
#define ERRORSMANAGER_H

#include <QObject>

class ErrorsManager : public QObject
{
    Q_OBJECT
public:
    explicit ErrorsManager(QObject *parent = 0);
    QString CodeToStrError(const unsigned long code);

signals:

public slots:
};

#endif // ERRORSMANAGER_H

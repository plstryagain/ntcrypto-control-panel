#include "crlui.h"
#include "ui_crlui.h"
#include "icertificatemanager.h"

CrlUI::CrlUI(QWidget *parent, const CrlManager* crlmgr) :
    QWidget(parent),
    ui(new Ui::CrlUI),
    crlmgr_(crlmgr)
{
    ui->setupUi(this);
    this->setWindowTitle("Список отозванных сертификатов");
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setWindowIcon(QIcon(":/images/images/Delete Document_32px.png"));
    this->setWindowFlags(Qt::Window);
    ui->tw_composition->setColumnCount(2);
    ui->tw_composition->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Поле")));
    ui->tw_composition->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Значение")));
    QHeaderView* h = ui->tw_composition->horizontalHeader();
    h->setSectionResizeMode(0, QHeaderView::Fixed);
    h->resizeSection(0, 150);
    h->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->tw_composition->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tw_composition->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tw_composition->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->tw_composition->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tw_composition->horizontalHeader()->setStretchLastSection(true);
    ui->tw_composition->verticalHeader()->setVisible(false);

}

CrlUI::~CrlUI()
{
    delete ui;
}

void CrlUI::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    ui->lb_crl_statusicon->setPixmap(QPixmap(":/images/images/Delete Document_blue_32px.png"));
    int row_count = 6;
    ui->tw_composition->setRowCount(row_count);
    ui->tw_composition->setItem(0, 0, new QTableWidgetItem(tr("Версия")));
    ui->tw_composition->setItem(1, 0, new QTableWidgetItem(tr("Издатель")));
    ui->tw_composition->setItem(2, 0, new QTableWidgetItem(tr("Действителен с")));
    ui->tw_composition->setItem(3, 0, new QTableWidgetItem(tr("Следующее обновление")));
    ui->tw_composition->setItem(4, 0, new QTableWidgetItem(tr("Алгоритм подписи")));
    ui->tw_composition->setItem(5, 0, new QTableWidgetItem(tr("Хэш-алгоритм подписи")));


    for(int i = 0; i < row_count; ++i){
        QTableWidgetItem* item = ui->tw_composition->item(i, 0);
        item->setIcon(QIcon(QString::fromUtf8(":/images/images/cert_prop_orange_32px.png")));
        item->setToolTip(item->text());
    }

    ui->tw_composition->setItem(0, 1, new QTableWidgetItem(crlmgr_->GetVersion()));
    ui->tw_composition->setItem(1, 1, new QTableWidgetItem(crlmgr_->GetCertIssuerName()));
    ui->tw_composition->setItem(2, 1, new QTableWidgetItem(crlmgr_->GetNotBefore()));
    ui->tw_composition->setItem(3, 1, new QTableWidgetItem(crlmgr_->GetNotAfter()));
    ui->tw_composition->setItem(4, 1, new QTableWidgetItem(crlmgr_->GetSignatureAlgName()));
    ui->tw_composition->setItem(5, 1, new QTableWidgetItem(crlmgr_->GetHashAlgName()));

    auto oid_list = crlmgr_->GetExtensionOidList();
    for(const auto& oid : oid_list){
        ui->tw_composition->setRowCount(row_count + 1);
        QString ext_name = crlmgr_->GetExtansionNameString(oid);
        QTableWidgetItem* item = new QTableWidgetItem(ext_name);
        item->setIcon(QIcon(QString::fromUtf8(":/images/images/cert_ext_32px.png")));
        ui->tw_composition->setItem(row_count, 0, item);
        ui->tw_composition->item(row_count, 0)->setToolTip(ext_name);
        ui->tw_composition->setItem(row_count, 1, new QTableWidgetItem(crlmgr_->GetExtensionValueString(oid)));
        ++row_count;
    }

    for(auto i = 0; i < row_count; ++i){
        ui->tw_composition->setRowHeight(i, 20);
    }
}

void CrlUI::on_tabw_about_crl_currentChanged(int index)
{
    switch(index){
    case 1:
        return FillCrlTable();
    default:
        return;
    }
}

void CrlUI::FillCrlTable()
{
    if(is_crl_table_already_fill_){
        return;
    }
    ui->tw_crl_list->setColumnCount(2);
    ui->tw_crl_list->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Серийный номер")));
    ui->tw_crl_list->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Дата отзыва")));
    QHeaderView* h = ui->tw_crl_list->horizontalHeader();
    h->setSectionResizeMode(0, QHeaderView::Fixed);
    h->resizeSection(0, 200);
    h->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->tw_crl_list->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tw_crl_list->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tw_crl_list->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->tw_crl_list->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tw_crl_list->horizontalHeader()->setStretchLastSection(true);
    ui->tw_crl_list->verticalHeader()->setVisible(false);


    ui->tw_crl_element->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tw_crl_element->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tw_crl_element->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->tw_crl_element->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tw_crl_element->horizontalHeader()->setStretchLastSection(true);
    ui->tw_crl_element->verticalHeader()->setVisible(false);

    DWORD ecount = crlmgr_->GetCrlEntryCount();
    ui->lb_crl_num->setText(QString::number(ecount));
    int rcount = 0;
    for(DWORD i = 0; i < ecount; ++i){
        ui->tw_crl_list->setRowCount(rcount + 1);
        QString snumber = crlmgr_->GetCrlEntrySerialNumber(i);
        QTableWidgetItem* item = new QTableWidgetItem(snumber);
        item->setToolTip(snumber);
        ui->tw_crl_list->setItem(rcount, 0, item);
        QString rdate = crlmgr_->GetCrlEntryRevokeDate(i);
        item = new QTableWidgetItem(rdate);
        item->setToolTip(rdate);
        ui->tw_crl_list->setItem(rcount, 1, item);
        ui->tw_crl_list->setRowHeight(rcount, 20);
        ++rcount;
    }
    is_crl_table_already_fill_ = true;
}

void CrlUI::on_tw_crl_list_itemSelectionChanged()
{
    ui->tw_crl_element->clear();
    int i = ui->tw_crl_list->currentRow();
    if(i == -1){
        return;
    }
    ui->tw_crl_element->setColumnCount(2);
    ui->tw_crl_element->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Поле")));
    ui->tw_crl_element->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Значение")));
    QHeaderView* h = ui->tw_crl_element->horizontalHeader();
    h->setSectionResizeMode(0, QHeaderView::Fixed);
    h->resizeSection(0, 150);
    h->setSectionResizeMode(1, QHeaderView::ResizeToContents);

    ui->tw_crl_element->setRowCount(3);
    ui->tw_crl_element->setItem(0, 0, new QTableWidgetItem("Серийный номер"));
    ui->tw_crl_element->setItem(0, 1, new QTableWidgetItem(ui->tw_crl_list->item(i, 0)->text()));
    ui->tw_crl_element->setRowHeight(0, 20);
    ui->tw_crl_element->setItem(1, 0, new QTableWidgetItem("Дата отзыва"));
    ui->tw_crl_element->setItem(1, 1, new QTableWidgetItem(ui->tw_crl_list->item(i, 1)->text()));
    ui->tw_crl_element->setRowHeight(1, 20);

    QString reason = crlmgr_->GetCrlEntryRevokeReason(i);
    if(!reason.isEmpty()){
        ui->tw_crl_element->setItem(2, 0, new QTableWidgetItem("Код причины отзыва"));
        QTableWidgetItem* item = new QTableWidgetItem(reason);
        item->setToolTip(reason);
        ui->tw_crl_element->setItem(2, 1, item);
        ui->tw_crl_element->setRowHeight(2, 20);
    }
}

void CrlUI::on_btn_ok_clicked()
{
    this->close();
}

void CrlUI::on_tw_crl_element_itemSelectionChanged()
{
    int i = ui->tw_crl_element->currentRow();
    if(i == -1){
        return;
    }
    ui->tb_crl_element_value->setText(ui->tw_crl_element->item(i, 1)->text());
}

void CrlUI::on_tw_composition_itemSelectionChanged()
{
    int i = ui->tw_composition->currentRow();
    if(i == -1){
        return;
    }
    ui->tb_value->setText(ui->tw_composition->item(i, 1)->text());
}

#include "keyregisterwizard.h"
#include "ntcrypto.h"
#include "registry.h"
#include "Constants.h"
#include <QList>
#include <QDebug>
#include <QVector>
#include <QTextCodec>
#include <QMessageBox>

KeyRegisterWizard::KeyRegisterWizard(const bool is_machine_keyset) :
    layout_(this), btn_box_(QDialogButtonBox::Ok | QDialogButtonBox::Cancel),
    is_machine_keyset_(is_machine_keyset)
{
    this->setWindowTitle(tr("Регистрация контейнера"));
    this->setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowIcon(QIcon(QString::fromUtf8(":/images/images/Add_32px.png")));
}

void KeyRegisterWizard::Init()
{
    btn_box_.button(QDialogButtonBox::Cancel)->setText(tr("Отмена"));
    cb_devices_.addItem(tr("Программная реализация"));
    cb_devices_.addItem(tr("СКЗИ \"Сигма\""));
    cb_devices_.addItem(tr("NTStore"));
    layout_.addRow(tr("Устройство: "), &cb_devices_);
    layout_.addRow(tr("Контейнер: "), &cb_containers_);
    layout_.addRow(&btn_box_);

    connect(&btn_box_, SIGNAL(rejected()), this, SLOT(btn_cancel_clicked()));
    connect(&btn_box_, SIGNAL(accepted()), this, SLOT(btn_ok_clicked()));
    connect(&cb_devices_, SIGNAL(currentIndexChanged(int)), this, SLOT(EnumKeys()));

    EnumKeys();
}

void KeyRegisterWizard::EnumKeys()
{
    cb_containers_.clear();
    DWORD curr_dev = Constants::SOFT_IMPL_FLAG;
    if(cb_devices_.currentIndex() == 0)
    {
        curr_dev = Constants::SOFT_IMPL_FLAG;
    }
    else if (cb_devices_.currentIndex() == 1)
    {
        curr_dev = Constants::SIGMA_IMPL_FLAG;
    }
    else if (cb_devices_.currentIndex() == 2)
    {
        curr_dev = Constants::NTSTORE_IMPL_FLAG;
    }

    DWORD flags = is_machine_keyset_ ? CRYPT_VERIFYCONTEXT | curr_dev | CRYPT_MACHINE_KEYSET
                                     : CRYPT_VERIFYCONTEXT | curr_dev;
    NTCryptoManager ntcmgr("", Constants::BIGN_CSP_TYPE, flags);
    if(!ntcmgr.Init())
    {
        return;
    }

    DWORD temp_size = sizeof(device_type_);
    if(!ntcmgr.GetProvParam(PP_DEVICE_TYPE, reinterpret_cast<BYTE*>(&device_type_), &temp_size, 0))
    {
        return;
    }

    temp_size = sizeof(slot_id_);
    if(!ntcmgr.GetProvParam(PP_SLOT_ID, reinterpret_cast<BYTE*>(&slot_id_), &temp_size, 0))
    {
        return;
    }

    qDebug() << "Device type: " << device_type_ << ", slotId: " << slot_id_ << "\r\n";

    DWORD name_len = 0;
    DWORD flag = CRYPT_FIRST;
    while (ntcmgr.GetProvParam(PP_ENUMCONTAINERS, nullptr, &name_len, flag))
    {
        if (name_len == 0)
            break;

        QVector<BYTE> name(name_len);

        if (!ntcmgr.GetProvParam(PP_ENUMCONTAINERS, name.data(), &name_len, flag))
        {
            if (GetLastError() != ERROR_NO_MORE_ITEMS)
            {
                return;
            }
            break;
        }


        //cb_containers_.addItem(QString::fromUtf8(reinterpret_cast<char*>(name.data()), name_len));
        QTextCodec *codec = QTextCodec::codecForName( "CP1251" );
        QByteArray text = QByteArray::fromRawData(reinterpret_cast<char*>(name.data()), name_len);
        cb_containers_.addItem(QString(codec->toUnicode(text)));
        flag = 0;
    }
}

void KeyRegisterWizard::btn_ok_clicked()
{
    QString container_name = cb_containers_.currentText();
    if(IsRegistered(container_name))
    {
        QMessageBox::warning(this,
                             tr("Внимание"),
                             tr("Контейнер ключей с таким именем уже зарегистрирован!"),
                             QMessageBox::Ok);
        this->close();
        return;
    }

    if(RegisterKey(container_name) != ERROR_SUCCESS)
    {
        QMessageBox::critical(this,
                             tr("Ошибка"),
                             tr("Не удалось зарегистрировать контейнер!"),
                             QMessageBox::Ok);
    }
    else
    {
        QMessageBox::information(this,
                                    tr("Информация"),
                                    tr("Ключевой контейнер успешно зарегистрирован"),
                                    QMessageBox::Ok);
        emit RegUnregKeyFinish();
    }

    this->close();
}

bool KeyRegisterWizard::IsRegistered(const QString& container_name)
{
    RegistryManager regmgr;
    NTSTATUS status = NTE_FAIL;

    if (is_machine_keyset_)
    {
        status = regmgr.OpenDefaultUser();
        if (status != ERROR_SUCCESS)
        {
            return false;
        }
    }
    else
    {
        status = regmgr.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);
        if (status != ERROR_SUCCESS)
        {
            return false;
        }
    }

    status = regmgr.OpenKey(RegConstants::PROVIDER_KEYS_REG_PATH + container_name,
                            KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);

    return (status == ERROR_SUCCESS) ? true : false;
}

unsigned long KeyRegisterWizard::RegisterKey(const QString& container_name)
{
    RegistryManager regmgr;
    NTSTATUS status = NTE_FAIL;


    if (is_machine_keyset_)
    {
        status = regmgr.OpenDefaultUser();
        if (status != ERROR_SUCCESS)
        {
            return status;
        }
    }
    else
    {
        status = regmgr.OpenCurrentUser(KEY_WRITE);
        if (status != ERROR_SUCCESS)
        {
            return status;
        }
    }

    status = regmgr.CreateKey(RegConstants::PROVIDER_KEYS_REG_PATH+container_name);
    if (status != ERROR_SUCCESS)
    {
        return status;
    }

    status = regmgr.SetKeyValue("",
                                RegConstants::KEY_STORAGE_OPTION,
                                REG_DWORD,
                                reinterpret_cast<const BYTE*>(&device_type_),
                                sizeof(device_type_));
    if (status != ERROR_SUCCESS)
    {
        return status;
    }

    status = regmgr.SetKeyValue("",
                                RegConstants::KEY_STORAGE_SLOT_ID_OPTION,
                                REG_DWORD,
                                reinterpret_cast<BYTE*>(&slot_id_),
                                sizeof(slot_id_));
    return status;
}

void KeyRegisterWizard::btn_cancel_clicked()
{
    this->close();
}

void KeyRegisterWizard::UnregisterKey(const QString& container_name)
{
    if(!IsRegistered(container_name))
    {
        QMessageBox::warning(this,
                             tr("Внимание"),
                             tr("Контейнер ключей с таким именем не зарегистрирован!"),
                             QMessageBox::Ok);
        this->close();
        return;
    }

    RegistryManager regmgr;
    NTSTATUS status = NTE_FAIL;

    if (is_machine_keyset_)
    {
        status = regmgr.OpenDefaultUser();
        if (status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this,
                                 tr("Ошибка"),
                                 tr("Не удалось отменить регистрацию контейнера, код ошибки: ")
                                 +QString::number(status),
                                 QMessageBox::Ok);
            this->close();
            return;
        }
    }
    else
    {
        status = regmgr.OpenCurrentUser(KEY_WRITE);
        if (status != ERROR_SUCCESS)
        {
            QMessageBox::critical(this,
                                 tr("Ошибка"),
                                 tr("Не удалось отменить регистрацию контейнера, код ошибки: ")
                                 +QString::number(status),
                                 QMessageBox::Ok);
            this->close();
            return;
        }
    }


    status = regmgr.DeleteKey(RegConstants::PROVIDER_KEYS_REG_PATH+container_name);
    if(status != ERROR_SUCCESS)
    {
        QMessageBox::critical(this,
                             tr("Ошибка"),
                             tr("Не удалось отменить регистрацию контейнера, код ошибки: ")
                             +QString::number(status),
                             QMessageBox::Ok);
    }
    else
    {
        QMessageBox::information(this,
                                    tr("Информация"),
                                    tr("Регистрация ключевого контейнера успешно отменена"),
                                    QMessageBox::Ok);
        emit RegUnregKeyFinish();
    }
    this->close();
}

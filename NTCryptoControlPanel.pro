#-------------------------------------------------
#
# Project created by QtCreator 2017-04-15T18:29:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets network axcontainer

_WIN_XP_{

TARGET = NTCryptoControlPanel_xp

DEFINES += _WIN_XP_

win32 {
    QMAKE_LFLAGS += /SUBSYSTEM:CONSOLE,5.01
    QMAKE_CXX += /D_USING_V110_SDK71_
    LIBS *= -L"%ProgramFiles(x86)%/Microsoft SDKs/Windows/7.1A/Lib"
    INCLUDEPATH += "%ProgramFiles(x86)%/Microsoft SDKs/Windows/7.1A/Include"
}
} else {

TARGET = NTCryptoControlPanel

}

TEMPLATE = app

VERSION_MAJOR = 0
VERSION_MINOR = 3
VERSION_BUILD = 0

DEFINES += "VERSION_MAJOR=$$VERSION_MAJOR"\
       "VERSION_MINOR=$$VERSION_MINOR"\
       "VERSION_BUILD=$$VERSION_BUILD"

#Target version
VERSION = $${VERSION_MAJOR}.$${VERSION_MINOR}.$${VERSION_BUILD}

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS Q_COMPILER_INITIALIZER_LISTS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG(debug, debug|release) {
    LIBS += -Lasn1bel/debug -lAsn1Bel
}else {
    LIBS += -Lasn1bel/release -lAsn1Bel
}


SOURCES += main.cpp\
        controlpanel.cpp \
    ntcrypto.cpp \
    registry.cpp \
    certificatemanager.cpp \
    keyregisterwizard.cpp \
    ntservicemanager.cpp \
    genkeywizard.cpp \
    cngmanager.cpp \
    selftestwizard.cpp \
    Constants.cpp \
    test.cpp \
    crlmanager.cpp \
    downloadcrlmanager.cpp \
    downloadcrlwizard.cpp \
    crldpwizard.cpp \
    importcertificatewizard.cpp \
    keylinkmanager.cpp \
    certificateui.cpp \
    certificatestoremanager.cpp \
    systemcertificatestoremanager.cpp \
    icertificatemanager.cpp \
    systemcertificatemanager.cpp \
    attributecertificatestoremanager.cpp \
    attributecertificatemanager.cpp \
    ntstorecertificatemanager.cpp \
    sigmacertificatestoremanager.cpp \
    crlstoremanager.cpp \
    crlui.cpp \
    hashoperationwizard.cpp \
    dialogpleasewait.cpp \
    cryptooperationsmanager.cpp \
    sigoperationwizard.cpp \
    selectcertwizard.cpp \
    certificaterequestwizard.cpp \
    sigverifyoperationwizard.cpp \
    encryptoperationwizard.cpp \
    certificaterequestmanager.cpp \
    integritycontrol.cpp \
    errorsmanager.cpp \
    wordautomation.cpp

HEADERS  += controlpanel.h \
    ntcrypto.h \
    registry.h \
    certificatemanager.h \
    keyregisterwizard.h \
    ntservicemanager.h \
    genkeywizard.h \
    cngmanager.h \
    selftestwizard.h \
    testdata.h \
    Constants.h \
    test.h \
    downloadcrlmanager.h \
    downloadcrlwizard.h \
    crldpwizard.h \
    importcertificatewizard.h \
    keylinkmanager.h \
    certificateui.h \
    certificatestoremanager.h \
    icertificatemanager.h \
    crlui.h \
    hashoperationwizard.h \
    dialogpleasewait.h \
    cryptooperationsmanager.h \
    sigoperationwizard.h \
    selectcertwizard.h \
    certificaterequestwizard.h \
    sigverifyoperationwizard.h \
    encryptoperationwizard.h \
    certificaterequestmanager.h \
    integritycontrol.h \
    errorsmanager.h \
    wordautomation.h

FORMS    += controlpanel.ui \
    certificateui.ui \
    crlui.ui \
    hashoperationwizard.ui \
    dialogpleasewait.ui \
    sigoperationwizard.ui \
    selectcertwizard.ui \
    certificaterequestwizard.ui \
    sigverifyoperationwizard.ui \
    encryptoperationwizard.ui

LIBS += -lVersion

RESOURCES += \
    icons.qrc



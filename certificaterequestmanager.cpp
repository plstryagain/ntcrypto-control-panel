#include <Windows.h>
#include <vector>
#include "certificaterequestmanager.h"
#include "ntcrypto.h"
#include "Constants.h"
#include "wordautomation.h"
#include <QVector>
#include <QMessageBox>


CertificateRequestManager::CertificateRequestManager(QObject *parent) : QObject(parent)
{

}

void CertificateRequestManager::Init(const QString& key_name, const QString& subject, int req_type)
{
    key_name_ = key_name;
    subject_ = subject;
    req_type_ = static_cast<CERT_REQ_TYPE>(req_type);
}

void CertificateRequestManager::Init(const QStringList &info)
{
    info_ = info;
}

void CertificateRequestManager::SignRequest()
{
    DWORD name_enc_len = 0;
    if(!CertStrToName(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
                      subject_.toStdWString().data(),
                      CERT_OID_NAME_STR | CERT_NAME_STR_CRLF_FLAG | CERT_NAME_STR_FORCE_UTF8_DIR_STR_FLAG,
                      NULL,
                      NULL,
                      &name_enc_len,
                      NULL))
    {
        emit reportError(GetLastError());
        return;
    }

    std::vector<BYTE> name_enc(name_enc_len);
    if(!CertStrToName(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
                      subject_.toStdWString().data(),
                      CERT_OID_NAME_STR | CERT_NAME_STR_CRLF_FLAG | CERT_NAME_STR_FORCE_UTF8_DIR_STR_FLAG,
                      NULL,
                      name_enc.data(),
                      &name_enc_len,
                      NULL))
    {
        emit reportError(GetLastError());
        emit signalFinished();
        return;
    }

    NTCryptoManager ntcmgr(key_name_, Constants::BIGN_CSP_TYPE, 0);
    if(!ntcmgr.Init())
    {
        emit reportError(GetLastError());
        emit signalFinished();
        return;
    }

    DWORD pub_key_info_len = 0;
    if(!ntcmgr.ExportPublicKeyInfo(AT_SIGNATURE, nullptr, &pub_key_info_len))
    {
        emit reportError(GetLastError());
        emit signalFinished();
        return;
    }

    std::vector<BYTE> pub_key_info_buff(pub_key_info_len);
    CERT_PUBLIC_KEY_INFO* pub_key_info = reinterpret_cast<CERT_PUBLIC_KEY_INFO*>(pub_key_info_buff.data());
    if(!ntcmgr.ExportPublicKeyInfo(AT_SIGNATURE, pub_key_info, &pub_key_info_len))
    {
        emit reportError(GetLastError());
        emit signalFinished();
        return;
    }

    CERT_POLICY_INFO cpi = { nullptr, 0, nullptr };
    if(req_type_ == CERT_REQ_TYPE::INDIVIDUAL ||
       req_type_ == CERT_REQ_TYPE::ENTITY){
        cpi.pszPolicyIdentifier = (LPSTR)"1.2.112.1.2.1.1.1.3.2.1";
    }
    CERT_POLICIES_INFO cpsi = { 1, &cpi };
    void* encoded = 0;
    DWORD elen = 0;
    if(!CryptEncodeObjectEx(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, szOID_CERT_POLICIES, &cpsi, CRYPT_ENCODE_ALLOC_FLAG,
                        nullptr, &encoded, &elen))
    {
        emit reportError(GetLastError());
        emit signalFinished();
        return;
    }

    if(!encoded){
        emit reportError(GetLastError());
        emit signalFinished();
        return;
    }

    CRYPT_DATA_BLOB cert_pol_bd = { elen, static_cast<BYTE*>(encoded) };
    CRYPT_ATTRIBUTE attr[] = {
        {(LPSTR)szOID_CERT_POLICIES, 1, &cert_pol_bd }
    };

    CERT_REQUEST_INFO cert_req_info;
    cert_req_info.cAttribute = 1;
    cert_req_info.rgAttribute = attr;
    cert_req_info.dwVersion = CERT_REQUEST_V1;
    cert_req_info.Subject.cbData = name_enc_len;
    cert_req_info.Subject.pbData = const_cast<BYTE*>(name_enc.data());
    cert_req_info.SubjectPublicKeyInfo = *pub_key_info;

    CRYPT_ALGORITHM_IDENTIFIER sig_alg = { 0 };
    std::vector<BYTE> params;
    std::copy(pub_key_info->Algorithm.Parameters.pbData,
              pub_key_info->Algorithm.Parameters.pbData + pub_key_info->Algorithm.Parameters.cbData,
              std::back_inserter(params));
    std::string sign_oid = GetSignatureOid(pub_key_info->Algorithm.pszObjId, params);
    sig_alg.pszObjId = const_cast<char*>(sign_oid.data());

    DWORD enc_cert_len = 0;
    if(!ntcmgr.SignAndEncodeCertificate(AT_SIGNATURE, X509_CERT_REQUEST_TO_BE_SIGNED,
                                        &cert_req_info, &sig_alg, nullptr, &enc_cert_len))
    {
        emit reportError(GetLastError());
        emit signalFinished();
        return;
    }

    QVector<BYTE> enc_cert(enc_cert_len);
    if(!ntcmgr.SignAndEncodeCertificate(AT_SIGNATURE, X509_CERT_REQUEST_TO_BE_SIGNED,
                                        &cert_req_info, &sig_alg, enc_cert.data(), &enc_cert_len))
    {
        emit reportError(GetLastError());
        emit signalFinished();
        return;
    }
    emit signalFinished();
    emit operationComplete(enc_cert);
    return;
}

std::string CertificateRequestManager::GetSignatureOid(const std::string& pub_key_oid,
                                                        const std::vector<unsigned char> &params)
{
    if(params == Constants::ASN1_OBJ_PARAMS256)
    {
        return Constants::STR_OID_BIGN_WITH_HBELT;
    }
    else if(params == Constants::ASN1_OBJ_PARAMS384)
    {
        return Constants::STR_OID_BIGN_WITH_BASH384;
    }
    else if(params == Constants::ASN1_OBJ_PARAMS512)
    {
        return Constants::STR_OID_BIGN_WITH_BASH512;
    }
    else if(pub_key_oid == Constants::STR_OID_STB1176_2_PUB_KEY)
    {
        return Constants::STR_OID_STB1176_2_SIGN;
    }
    else if (pub_key_oid == Constants::STR_OID_STB1176_2_PRE_PUB_KEY)
    {
        return Constants::STR_OID_STB1176_2_PRE_SIGN;
    }
    else
    {
        return "";
    }
}




#ifndef DOWNLOADCRLWIZARD_H
#define DOWNLOADCRLWIZARD_H

#include <QObject>
#include <QDialog>
#include <QLabel>
#include <QFormLayout>
#include <QThread>
#include <QAuthenticator>
#include <QNetworkProxy>
#include <QNetworkProxyQuery>
#include <QStyle>
#include <QLineEdit>
#include <QLabel>
#include <QDialogButtonBox>
#include <QFormLayout>

class DownloadCrlWizard : public QDialog
{

    Q_OBJECT

public:
    explicit DownloadCrlWizard(QWidget* parent, const QStringList& url_list);
    QList<QByteArray> GetCrls();

private slots:
    void OnQueryProxyCredentials(QNetworkProxy proxy, QAuthenticator* authentificator);
    void OnReplyFinished(QString url);
    void ReceiveCrls(QList<QByteArray> crls);

protected:
    void showEvent(QShowEvent *event) override;

private:
    QWidget* parent_;
    QFormLayout layout_;
    QLabel lb_plswait_;
    QLabel lb_url_;
    QStringList url_list_;
    QList<QByteArray> crls_;
};

#endif // DOWNLOADCRLWIZARD_H

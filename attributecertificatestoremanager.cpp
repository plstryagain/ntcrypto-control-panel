#include "certificatestoremanager.h"
#include <QStandardPaths>
#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QDebug>

AttributeCertificateStoreManager::AttributeCertificateStoreManager()
{

}

AttributeCertificateStoreManager::~AttributeCertificateStoreManager()
{

}

bool AttributeCertificateStoreManager::OpenStore()
{
    if(is_store_open_){
        return true;
    }
    att_store_path_ = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    int i = att_store_path_.indexOf("NTCryptoControlPanel");
    att_store_path_.truncate(i);
    att_store_path_.append("NII TZI/NTCrypto/AttCertStore");
    if(!QDir(att_store_path_).exists()){
        if(!QDir().mkdir(att_store_path_)){
            return false;
        }
    }
    is_store_open_ = true;
    return true;
}

void AttributeCertificateStoreManager::CloseStore()
{
    is_store_open_ = false;
}

void AttributeCertificateStoreManager::EnumCertificates()
{
    if(!is_store_open_){
        return;
    }
    QDirIterator it(att_store_path_, QStringList() << "*.acr", QDir::Files);
    while(it.hasNext()){
        QFile file(it.next());
        if(!file.open(QFile::ReadOnly)){
            continue;
        }
        QByteArray ba = file.readAll();
        auto acmgr = ICertificateManager::Create(CERTIFICATE_TYPE::AttributeCertificate, &ba, false);
        AttributeCertificateManager* acm = dynamic_cast<AttributeCertificateManager*>(acmgr.get());
        if(!acm->Init()){
            file.close();
            continue;
        }
        crt_mgr_list_.push_back(std::move(acmgr));
        file.close();
    }
}

DWORD AttributeCertificateStoreManager::AddToStore(const std::unique_ptr<ICertificateManager> &mgr, const DWORD flag)
{
    return AddToStore(mgr.get(), flag);
}

DWORD AttributeCertificateStoreManager::AddToStore(const ICertificateManager* mgr, const DWORD flag)
{
    Q_UNUSED(flag);
    auto acert = dynamic_cast<const AttributeCertificateManager*>(mgr);
    QString file_name = acert->GetHolderCertificateSerialNumber();
    QFile file(att_store_path_ + "/" + file_name + ".acr");
    if(!file.open(QFile::ReadWrite)){
        return NTE_FAIL;
    }
    const QByteArray* ctx = static_cast<const QByteArray*>(acert->GetRawContextPtr());
    /* If attribute cert come from p7b, then replace Context Specific(2) to Sequence */
    if(ctx->at(0) == '\xa2'){
        char seq = '\x30';
        memcpy(const_cast<char*>(ctx->data()), &seq, 1);
    }
    file.write(*ctx);
    file.close();
    return ERROR_SUCCESS;
}

DWORD AttributeCertificateStoreManager::RemoveFromStore(const std::unique_ptr<ICertificateManager> &mgr)
{
    return RemoveFromStore((mgr.get()));
}
DWORD AttributeCertificateStoreManager::RemoveFromStore(const ICertificateManager* mgr)
{
    auto acert = dynamic_cast<const AttributeCertificateManager*>(mgr);
    QString file_name = acert->GetHolderCertificateSerialNumber();
    QFile file(att_store_path_ + "/" + file_name + ".acr");
    if(!file.remove()){
        return NTE_FAIL;
    } else {
        return ERROR_SUCCESS;
    }
}

DWORD AttributeCertificateStoreManager::RemoveFromStoreByIndex(const quint32 idx)
{
    if(idx >= crt_mgr_list_.size()){
        return NTE_INVALID_PARAMETER;
    }
    return RemoveFromStore(crt_mgr_list_.at(idx));
}

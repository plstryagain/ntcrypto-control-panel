#ifndef NTPROVIDER_H
#define NTPROVIDER_H

#include <QObject>
#include <windows.h>
#include <wincrypt.h>
#include <string>
#include <vector>

struct PROV_VERSION_EX
{
    unsigned long version_major;
    unsigned long version_minor;
    unsigned long version_revision;
    unsigned long version_build;
};



class NTCryptoManager : public QObject
{
    Q_OBJECT
public:
    explicit NTCryptoManager(const QString& container, const DWORD prov_type, const DWORD flags);
    NTCryptoManager(const NTCryptoManager& other) = delete;
    ~NTCryptoManager();

signals:

public slots:
    int Init();
    int GetProvParam(const DWORD param, BYTE* data, DWORD* data_len, const DWORD flag);
    int GetUserKey(const DWORD spec);
    int SetKeyParam(const DWORD param, const BYTE* data, const DWORD data_len);
    int GetKeyParam(const DWORD param, BYTE* data, DWORD* data_len, const DWORD flags);
    int ExportKey(const DWORD blob_type, const DWORD flags, BYTE* key_value, DWORD* key_value_len);
    bool CalculateHash(const ALG_ID alg_id,
                       const BYTE* data,
                       const DWORD data_len,
                       BYTE* hash_val,
                       DWORD* hash_val_len);
    int GenerateKeyPair(const ALG_ID alg_id, const DWORD flags);
    int ExportPublicKeyInfo(const DWORD key_spec, CERT_PUBLIC_KEY_INFO* pub_key_info,
                                               DWORD* pub_key_info_len);
    int SignAndEncodeCertificate(const DWORD key_spec, const char* struct_type,
                                 const void* struct_info, PCRYPT_ALGORITHM_IDENTIFIER sign_alg,
                                 BYTE* enc_cert, DWORD* enc_cert_len);

private slots:

private:
    HCRYPTPROV hProv_;
    HCRYPTKEY hKey_;
    HCRYPTHASH hHash_;
    QString container_;
    unsigned long prov_type_;
    unsigned long flags_;
};

#endif // NTPROVIDER_H

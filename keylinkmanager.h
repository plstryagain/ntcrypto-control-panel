#ifndef KEYLINKMANAGER_H
#define KEYLINKMANAGER_H

#include <QObject>
#include <QVector>
#include <QString>
#include <Windows.h>

class KeyLinkManager
{
public:
    KeyLinkManager(PCCERT_CONTEXT pCert, bool is_need_use_cng);
    SECURITY_STATUS TryToLink();

private:
    SECURITY_STATUS ComparePublicKeys(const DWORD csp_type, const DWORD dev_type, const QString &name, const QVector<BYTE> &ckval);
    QString Cp1251ToQstring(const QVector<BYTE>& cp1251str);
    SECURITY_STATUS SetKeyProvInfo(const QString& prov_name, const QString& key_name, const DWORD csp_type);
    bool IsRegistered(const QString& container_name);
    SECURITY_STATUS RegisterKey(const QString& container_name, const DWORD dev_type,
                                                const DWORD slot_id);

private:
    PCCERT_CONTEXT pCert_;
    bool is_need_use_cng_;
};

#endif // KEYLINKMANAGER_H

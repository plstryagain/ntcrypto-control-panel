#include "downloadcrlmanager.h"
#include <QDebug>


DownloadCrlManager::DownloadCrlManager(QObject *parent, const QStringList &url_list)
    : QObject(parent), reply_count_(0), url_list_(url_list)
{

}

DownloadCrlManager::~DownloadCrlManager()
{
    for(const auto& nac : nac_list_)
    {
        nac->deleteLater();
    }
}


void DownloadCrlManager::DownloadCrls()
{
    for(const auto& str_url : url_list_)
    {
        QNetworkAccessManager* nac = new QNetworkAccessManager();
        QUrl url(str_url);
        QNetworkRequest request(url);
        nac->get(request);
        connect(nac, SIGNAL(finished(QNetworkReply*)), this, SLOT(OnReplyFinished(QNetworkReply*)));
        connect(nac, SIGNAL(proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)),
                this, SLOT(OnProxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)));
        ++reply_count_;
        nac_list_.append(nac);
    }
}




QList<QByteArray> DownloadCrlManager::GetCrlsList()
{
    return crls_;
}

void DownloadCrlManager::OnProxyAuthenticationRequired(QNetworkProxy proxy, QAuthenticator* authentificator)
{
    emit queryProxyCredentials(proxy, authentificator);
}

void DownloadCrlManager::OnReplyFinished(QNetworkReply* reply)
{
    --reply_count_;
    if(reply->error() == QNetworkReply::NoError)
    {
        QByteArray tmpData;
        tmpData = reply->readAll();
        qDebug() << "REPLY SIZE: " << tmpData.size();
        qDebug() << tmpData;
        crls_.append(tmpData);
        reply->deleteLater();
        emit replyFinished(reply->url().toString());
    }
    else
    {
        emit replyError(reply->errorString());
        qDebug() << reply->errorString();
    }
    if(reply_count_ == 0)
    {
        emit sendCrls(crls_);
        emit finished();
    }
}

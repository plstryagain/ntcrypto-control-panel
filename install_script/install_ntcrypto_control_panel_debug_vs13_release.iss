[Setup]
AppName=������ ���������� ����������������� NTCrypto
AppVersion=0.3.0
AppCopyright=��� ���
PrivilegesRequired=none
AppId={{C7B23036-EBB5-4CA2-81F8-70A03ECB4F26}
DefaultDirName={pf}\NIITZI\NTCrypto Control Panel
OutputBaseFilename=install_NTCrypto_control_panel_v{#SetupSetting("AppVersion")}_release

[Files]
Source: "..\release\NTCryptoControlPanel.exe"; DestDir: "{app}"; Flags: 32bit ignoreversion; MinVersion: 0,6.1
Source: "..\release\NTCryptoControlPanel_xp.exe"; DestDir: "{app}"; DestName: "NTCryptoControlPanel.exe"; Flags: ignoreversion; MinVersion: 0,5.01sp3; OnlyBelowVersion: 0,6.1
Source: "C:\Qt\Qt5.5.0\5.5\msvc2013\bin\Qt5Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\Qt5.5.0\5.5\msvc2013\bin\Qt5Gui.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\Qt5.5.0\5.5\msvc2013\bin\Qt5Widgets.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\Qt5.5.0\5.5\msvc2013\bin\Qt5Network.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Qt\Qt5.5.0\5.5\msvc2013\plugins\platforms\qwindows.dll"; DestDir: "{app}\platforms"; Flags: ignoreversion
Source: "..\images\icons8_Administrative_Tools.ico"; DestDir: "{app}"; DestName: "icon.png"; Flags: ignoreversion



[Dirs]
Name: "{app}\platforms"; Flags: deleteafterinstall

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"

[Icons]
Name: "{commondesktop}\������ ���������� NTCrypto"; Filename: "{app}\NTCryptoControlPanel.exe"; IconFilename: "{app}\icon.png"

[Messages]
russian.WelcomeLabel1=��� ������������ ������ ��������� ��������� "������ ���������� ����������������� NTCrypto"
russian.WelcomeLabel2=������ ��������� ��������� "[name/ver]" �� ��� ���������.%n%n������������� ������� ��� ������ ���������� ����� ���, ��� ����������.
russian.FinishedHeadingLabel=���������� ������� ��������� ��������� "������ ���������� ����������������� NTCrypto"
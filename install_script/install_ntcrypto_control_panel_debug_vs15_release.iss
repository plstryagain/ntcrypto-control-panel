[Setup]
AppName=������ ���������� ����������������� NTCrypto
AppVersion=0.3.0.0
AppCopyright=��� ���
PrivilegesRequired=none
AppId={{C7B23036-EBB5-4CA2-81F8-70A03ECB4F26}
DefaultDirName={pf}\NIITZI\NTCrypto Control Panel
OutputBaseFilename=install_NTCrypto_control_panel_v{#SetupSetting("AppVersion")}_with_vcredist

[Files]
Source: "..\release\NTCryptoControlPanel.exe"; DestDir: "{app}"; Flags: 32bit ignoreversion; MinVersion: 0,6.1
;Source: "..\release\NTCryptoControlPanel_xp.exe"; DestDir: "{app}"; DestName: "NTCryptoControlPanel.exe"; Flags: ignoreversion; MinVersion: 0,5.01sp3; OnlyBelowVersion: 0,6.1
Source: "..\..\..\..\Programs\Qt5.9.2\5.9.2\msvc2015\bin\Qt5Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\..\..\Programs\Qt5.9.2\5.9.2\msvc2015\bin\Qt5Gui.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\..\..\Programs\Qt5.9.2\5.9.2\msvc2015\bin\Qt5Widgets.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\..\..\Programs\Qt5.9.2\5.9.2\msvc2015\bin\Qt5Network.dll"; DestDir: "{app}"; Flags: ignoreversion
;Source: "..\..\Bin\ControlPanel_debug\libeay32.dll"; DestDir: "{app}"; Flags: ignoreversion
;Source: "..\..\Bin\ControlPanel_debug\ssleay32.dll"; DestDir: "{app}"; Flags: ignoreversion
;Source: "..\..\Bin\ControlPanel_debug\platforms\qminimald.dll"; DestDir: "{app}\platforms"; Flags: ignoreversion
;Source: "..\..\Bin\ControlPanel_debug\platforms\qoffscreend.dll"; DestDir: "{app}\platforms"; Flags: ignoreversion
Source: "..\..\..\..\Programs\Qt5.9.2\5.9.2\msvc2015\plugins\platforms\qwindows.dll"; DestDir: "{app}\platforms"; Flags: ignoreversion
Source: "..\images\icons8_Administrative_Tools.ico"; DestDir: "{app}"; DestName: "icon.png"; Flags: ignoreversion
;Source: "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\redist\debug_nonredist\x86\Microsoft.VC140.DebugCRT\msvcp140d.dll"; DestDir: "{app}"; Flags: ignoreversion
;Source: "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\redist\debug_nonredist\x86\Microsoft.VC140.DebugCRT\vcruntime140d.dll"; DestDir: "{app}"; Flags: ignoreversion
;Source: "C:\Program Files (x86)\Windows Kits\10\bin\x86\ucrt\ucrtbased.dll"; DestDir: "{app}"; Flags: ignoreversion

Source: "E:\install\vcredist\2015\vc_redist_x86.exe"; DestDir: "{tmp}\x86"; DestName: "vc_redist_x86.exe"; Flags: deleteafterinstall; MinVersion: 0,5.01sp3


[Run]
Filename: "{tmp}\x86\vc_redist_x86.exe"; Parameters: "/install /quiet"; Check: not VCinstalled; StatusMsg: "Installing VC++ 2015 Redistributables..." 

[Dirs]
Name: "{app}\platforms"; Flags: deleteafterinstall

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"

[Icons]
Name: "{commondesktop}\������ ���������� NTCrypto"; Filename: "{app}\NTCryptoControlPanel.exe"; IconFilename: "{app}\icon.png"

[Messages]
russian.WelcomeLabel1=��� ������������ ������ ��������� ��������� "������ ���������� ����������������� NTCrypto"
russian.WelcomeLabel2=������ ��������� ��������� "[name/ver]" �� ��� ���������.%n%n������������� ������� ��� ������ ���������� ����� ���, ��� ����������.
russian.FinishedHeadingLabel=���������� ������� ��������� ��������� "������ ���������� ����������������� NTCrypto"

[Code]
function VCinstalled: Boolean;
 // Function for Inno Setup Compiler
 // Returns True if same or later Microsoft Visual C++ 2015 Redistributable is installed, otherwise False.
 var
  major: Cardinal;
  minor: Cardinal;
  bld: Cardinal;
  rbld: Cardinal;
  key: String;
 begin
  Result := False;
  if IsWin64 then    key := 'SOFTWARE\Wow6432Node\Microsoft\VisualStudio\14.0\VC\Runtimes\x86'
  else
    key := 'SOFTWARE\Microsoft\VisualStudio\14.0\VC\Runtimes\x86';
  if RegQueryDWordValue(HKEY_LOCAL_MACHINE, key, 'Major', major) then begin
    if RegQueryDWordValue(HKEY_LOCAL_MACHINE, key, 'Minor', minor) then begin
      if RegQueryDWordValue(HKEY_LOCAL_MACHINE, key, 'Bld', bld) then begin
        if RegQueryDWordValue(HKEY_LOCAL_MACHINE, key, 'RBld', rbld) then begin
            Log('VC 2015 Redist Major is: ' + IntToStr(major) + ' Minor is: ' + IntToStr(minor) + ' Bld is: ' + IntToStr(bld) + ' Rbld is: ' + IntToStr(rbld));
            // Version info was found. Return true if later or equal to our 14.0.24212.00 redistributable
            // Note brackets required because of weird operator precendence
            Result := (major >= 14) and (minor >= 0) and (bld >= 24123) and (rbld >= 0)
        end;
      end;
    end;
  end;
 end;
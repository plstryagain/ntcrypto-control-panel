#ifndef DOWNLOADCRLMANAGER_H
#define DOWNLOADCRLMANAGER_H

#include <QObject>
#include <QUrl>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkProxy>
#include <QNetworkProxyQuery>
#include <QMessageBox>
#include <QNetworkReply>
#include <QAuthenticator>
#include <QVector>
#include <vector>


class DownloadCrlManager : public QObject
{
    Q_OBJECT
public:
    explicit DownloadCrlManager(QObject *parent, const QStringList& url_list);
    ~DownloadCrlManager();


    QList<QByteArray> GetCrlsList();

signals:
    void finished();
    void sendCrls(QList<QByteArray>);
    void urlChanged(QString);
    void replyFinished(QString);
    void replyError(QString);
    void queryProxyCredentials(QNetworkProxy proxy, QAuthenticator* authentificator);

public slots:
    void DownloadCrls();

private slots:
    void OnProxyAuthenticationRequired(QNetworkProxy proxy, QAuthenticator*authentificator);
    void OnReplyFinished(QNetworkReply* reply);


private:
    bool QueryProxyCredentials(QString *user, QString *pass);
    QString DownloadCrl(const QUrl& url);

private:
    QString usr_;
    QString pswd_;
    QList<QByteArray> crls_;
    int reply_count_;
    QList<QNetworkAccessManager*> nac_list_;
    QStringList url_list_;
};

#endif // DOWNLOADCRLMANAGER_H

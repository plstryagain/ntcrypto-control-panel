#ifndef CNGMANAGER_H
#define CNGMANAGER_H

#include <QObject>

#ifndef _WIN_XP_

#include <Windows.h>
#include <wincrypt.h>
#include <ncrypt.h>
#include <string>

namespace CNG_CONSTANTS
{
    extern const std::wstring KSP_NAME;
    extern const std::wstring PRIM_PROV_NAME;

    extern const QMap<QPair<DWORD, DWORD>, std::wstring> algorithms_id;
}

class CngManager : public QObject
{
    Q_OBJECT
public:
    explicit CngManager();
    ~CngManager();

public:
    SECURITY_STATUS Init();
    SECURITY_STATUS OpenKey(const QString& key_name, const DWORD flags);
    SECURITY_STATUS GenerateKeyPair(const std::wstring& alg_id, const QString& key_name,
                                    const DWORD flags);

signals:

public slots:


private:
    NCRYPT_PROV_HANDLE hProv_;
    NCRYPT_KEY_HANDLE hKey_;
};

#endif // _WIN_XP_

#endif // CNGMANAGER_H

#ifndef WORDAUTOMATION_H
#define WORDAUTOMATION_H

#include <QObject>
#include <Windows.h>
#include <atlbase.h>
#include <memory>

class QAxObject;

class WordAutomation : public QObject
{
    Q_OBJECT
public:
    explicit WordAutomation(QObject *parent = 0);
    ~WordAutomation();
    bool Init();

signals:

public slots:
    HRESULT FillField(const QString& data, const QString& tag);
    HRESULT SaveAs(const QString& path);
    void Close();

private:

private:
    QString temp_;
    QAxObject* pWord_ = nullptr;
    QAxObject* pDocs_ = nullptr;
    QAxObject* pDoc_ = nullptr;
    QAxObject* pBms_ = nullptr;
};

#endif // WORDAUTOMATION_H

#include "certificatestoremanager.h"
#include "Constants.h"

NtstoreCertificateStoreManager::NtstoreCertificateStoreManager()
    : hStore_(NULL), hProv_(NULL)
{

}

NtstoreCertificateStoreManager::~NtstoreCertificateStoreManager()
{
    if(hStore_){
        CertCloseStore(hStore_, 0);
    }
    if(hProv_){
        CryptReleaseContext(hProv_, 0);
    }
}


bool NtstoreCertificateStoreManager::OpenStore()
{
    if(!CryptAcquireContext(&hProv_, nullptr, nullptr, Constants::BIGN_CSP_TYPE, Constants::NTSTORE_IMPL_FLAG)){
        return false;
    }

    DWORD size = sizeof(hStore_);
    if (!CryptGetProvParam(hProv_,
                           PP_USER_CERTSTORE,
                           reinterpret_cast<BYTE*>(&hStore_),
                           &size,
                           0)) {
        return false;
    }
    return hStore_ ? true : false;
}

void NtstoreCertificateStoreManager::CloseStore()
{
    if(hStore_){
        CertCloseStore(hStore_, 0);
    }
    if(hProv_){
        CryptReleaseContext(hProv_, 0);
    }
}

void NtstoreCertificateStoreManager::EnumCertificates()
{
    if(!hStore_){
        return;
    }

    PCCERT_CONTEXT pCert = nullptr;
    while(pCert = CertEnumCertificatesInStore(hStore_, pCert))
    {
        auto scmgr = ICertificateManager::Create(CERTIFICATE_TYPE::SystemCertificate,
                                                 CertDuplicateCertificateContext(pCert), false);
        crt_mgr_list_.push_back(std::move(scmgr));
    }
}

DWORD NtstoreCertificateStoreManager::AddToStore(const std::unique_ptr<ICertificateManager> &mgr, const DWORD flag)
{
    return AddToStore(mgr.get(), flag);
}

DWORD NtstoreCertificateStoreManager::AddToStore(const ICertificateManager* mgr, const DWORD flag)
{
    Q_UNUSED(mgr)
    Q_UNUSED(flag)
    return NTE_NOT_SUPPORTED;
}

DWORD NtstoreCertificateStoreManager::RemoveFromStore(const std::unique_ptr<ICertificateManager> &mgr)
{
    return RemoveFromStore((mgr.get()));
}

DWORD NtstoreCertificateStoreManager::RemoveFromStore(const ICertificateManager* mgr)
{
    Q_UNUSED(mgr)
    return NTE_NOT_SUPPORTED;
}

DWORD NtstoreCertificateStoreManager::RemoveFromStoreByIndex(const quint32 idx)
{
    if(idx >= crt_mgr_list_.size()){
        return NTE_INVALID_PARAMETER;
    }
    return RemoveFromStore(crt_mgr_list_.at(idx));
}

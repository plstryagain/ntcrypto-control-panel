#include "certificatestoremanager.h"
#include "Constants.h"

SigmaCertificateStoreManager::SigmaCertificateStoreManager()
    : hStore_(NULL), hProv_(NULL)
{

}

SigmaCertificateStoreManager::~SigmaCertificateStoreManager()
{
    if(hStore_){
        CertCloseStore(hStore_, 0);
    }
    if(hProv_){
        CryptReleaseContext(hProv_, 0);
    }
}


bool SigmaCertificateStoreManager::OpenStore()
{
    if(!CryptAcquireContext(&hProv_, nullptr, nullptr, Constants::BIGN_CSP_TYPE, Constants::SIGMA_IMPL_FLAG)){
        return false;
    }

    DWORD size = sizeof(hStore_);
    if (!CryptGetProvParam(hProv_,
                           PP_USER_CERTSTORE,
                           reinterpret_cast<BYTE*>(&hStore_),
                           &size,
                           0)) {
        return false;
    }
    return hStore_ ? true : false;
}

void SigmaCertificateStoreManager::CloseStore()
{
    if(hStore_){
        CertCloseStore(hStore_, 0);
    }
    if(hProv_){
        CryptReleaseContext(hProv_, 0);
    }
}

void SigmaCertificateStoreManager::EnumCertificates()
{
    if(!hStore_){
        return;
    }

    PCCERT_CONTEXT pCert = nullptr;
    while(pCert = CertEnumCertificatesInStore(hStore_, pCert))
    {
        auto scmgr = ICertificateManager::Create(CERTIFICATE_TYPE::SystemCertificate,
                                                 CertDuplicateCertificateContext(pCert), false);
        crt_mgr_list_.push_back(std::move(scmgr));
    }
}

DWORD SigmaCertificateStoreManager::AddToStore(const std::unique_ptr<ICertificateManager> &mgr, const DWORD flag)
{
    return AddToStore(mgr.get(), flag);
}

DWORD SigmaCertificateStoreManager::AddToStore(const ICertificateManager* mgr, const DWORD flag)
{
    Q_UNUSED(mgr)
    Q_UNUSED(flag)
    return NTE_NOT_SUPPORTED;
}

DWORD SigmaCertificateStoreManager::RemoveFromStore(const std::unique_ptr<ICertificateManager> &mgr)
{
    return RemoveFromStore((mgr.get()));
}

DWORD SigmaCertificateStoreManager::RemoveFromStore(const ICertificateManager* mgr)
{
    Q_UNUSED(mgr)
    return NTE_NOT_SUPPORTED;
}

DWORD SigmaCertificateStoreManager::RemoveFromStoreByIndex(const quint32 idx)
{
    if(idx >= crt_mgr_list_.size()){
        return NTE_INVALID_PARAMETER;
    }
    return RemoveFromStore(crt_mgr_list_.at(idx));
}

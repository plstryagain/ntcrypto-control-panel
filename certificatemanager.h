#ifndef CERTIFICATEMANAGER_H
#define CERTIFICATEMANAGER_H

#include <QObject>
#include <QVector>
#include <Windows.h>
#include <wincrypt.h>
#include "cryptuiapi.h"

#pragma comment(lib, "Cryptui.lib")

class CertificateManager
{
public:
    CertificateManager(const PCCERT_CONTEXT& cert_ctx, bool need_free_ctx);
    ~CertificateManager();
    CertificateManager(const CertificateManager& other) = delete;

    QString GetCertSubjectName();
    QString GetCertIssuerName();
    bool IsPrivKeyExist();
    void ShowUICertificate();
    QString GetKeyContainerName();
    QString GetKeyCSPName();
    int SetCertCtxProperty(const DWORD prop_id, const DWORD flags, const void* data);
    PCCERT_CONTEXT GetCertCtx();
    QString GetNotBefore();
    QString GetNotAfter();
    bool IsRootCertificate();

private:
    PCCERT_CONTEXT cert_ctx_;
    bool need_free_ctx_;
};

#endif // CERTIFICATEMANAGER_H

#ifndef GENKEYWIZARD_H
#define GENKEYWIZARD_H

#include <QObject>
#include <QDialog>
#include <QFormLayout>
#include <QComboBox>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>

class GenKeyWizard : public QDialog
{
    Q_OBJECT

public:
    GenKeyWizard(const bool is_local_machine);

public slots:
    void Init();
    void FillKeyStrengthCBox(int index);
    QString GetKeyName();

private slots:
    void btn_cancel_clicked();
    void btn_ok_clicked();
    void GenerateCNGKey(const unsigned long prov_type, const unsigned long key_len, const QString& key_name);
    void GenerateCAPIKey(const unsigned long prov_type, const unsigned long key_len,
                         const QString& key_name);
    void ChangeLineBorderColor(QString);
    void ShowStatusMessage(const unsigned long status);

private:
    bool is_local_machine_;
    QFormLayout layout_;
    QComboBox cb_algs_;
    QComboBox cb_key_strength_;
    QComboBox cb_prov_type_;
    QLineEdit ln_key_name_;
    QDialogButtonBox btn_box_;
};

#endif // GENKEYWIZARD_H

#include <QTextCodec>
#include "keylinkmanager.h"
#include "Constants.h"
#include "ntcrypto.h"
#include "registry.h"

KeyLinkManager::KeyLinkManager(PCCERT_CONTEXT pCert, bool is_need_use_cng)
    : pCert_(pCert), is_need_use_cng_(is_need_use_cng)
{

}

SECURITY_STATUS KeyLinkManager::TryToLink()
{
    DWORD err = NTE_FAIL;
    QList<DWORD> devices = {
        Constants::SOFT_IMPL_FLAG,
        Constants::SIGMA_IMPL_FLAG,
        Constants::NTSTORE_IMPL_FLAG
    };

    for (const auto& dev : devices){
        DWORD alg_id = CertOIDToAlgId(pCert_->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId);
        DWORD csp_type = 0;
        switch(alg_id){
        case CALG_BIGN:
            csp_type = Constants::BIGN_CSP_TYPE;
            break;
        case CALG_STB1176_2:
            csp_type = Constants::STB11762_CSP_TYPE;
            break;
        case CALG_STB1176_2_PRE:
            csp_type = Constants::STB11762_PRE_CSP_TYPE;
        default:
            return NTE_NOT_SUPPORTED;
        }
        NTCryptoManager ntcmgr("", csp_type, CRYPT_VERIFYCONTEXT | dev);
        if(!ntcmgr.Init()){
            continue;
        }
        DWORD nlen = 0;
        DWORD flag = CRYPT_FIRST;
        while(ntcmgr.GetProvParam(PP_ENUMCONTAINERS, nullptr, &nlen, flag)){
            QVector<BYTE> name(nlen);
            if (!ntcmgr.GetProvParam(PP_ENUMCONTAINERS, name.data(), &nlen, flag)){
                break;
            }
            flag = CRYPT_NEXT;
            QString qname = Cp1251ToQstring(name);
            QVector<BYTE> ckval;
            for(DWORD i = 0; i < pCert_->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData; ++i)
            {
                ckval.push_back(pCert_->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData[i]);
            }
            err = ComparePublicKeys(csp_type, dev, qname, ckval);
            if(err == ERROR_SUCCESS){
                QString qprov_name;
                DWORD slot_id = 0;
                DWORD slot_id_len = sizeof(slot_id);
                if(!is_need_use_cng_){
                    QVector<BYTE> prov_name;
                    DWORD prov_name_len = 0;
                    if(!ntcmgr.GetProvParam(PP_NAME, nullptr, &prov_name_len, 0)){
                        return NTE_FAIL;
                    }
                    prov_name.resize(prov_name_len);
                    if(!ntcmgr.GetProvParam(PP_NAME, prov_name.data(), &prov_name_len, 0)) {
                        return NTE_FAIL;
                    }
                    qprov_name = Cp1251ToQstring(prov_name);
                } else {
                    qprov_name = "NTCrypto Key Storage Provider";
                    csp_type = 0;
                }
                if(!ntcmgr.GetProvParam(PP_SLOT_ID, reinterpret_cast<BYTE*>(&slot_id), &slot_id_len, 0)){
                    return NTE_FAIL;
                }
                err = SetKeyProvInfo(qprov_name, qname, csp_type);
                if(err != ERROR_SUCCESS){
                    return err;
                }
                if(!IsRegistered(qname)){
                    return RegisterKey(qname, dev, slot_id);
                } else {
                    return ERROR_SUCCESS;
                }
            } else if (err == 0xffffffff){
                continue;
            } else {
                return err;
            }
        }
    }
    return err;
}

SECURITY_STATUS KeyLinkManager::ComparePublicKeys(const DWORD csp_type, const DWORD dev_type,
                                                  const QString& name, const QVector<BYTE>& ckval)
{
    DWORD err = NTE_FAIL;
    HCRYPTPROV hProv = NULL;
    HCRYPTKEY hKey = NULL;
    QVector<BYTE> ukval;
    DWORD uklen = 0;
    if(!CryptAcquireContext(&hProv, name.toStdWString().data(), nullptr, csp_type, dev_type)){
        err = GetLastError();
        goto Exit;
    }

    if(!CryptGetUserKey(hProv, AT_SIGNATURE, &hKey)){
        err = GetLastError();
        goto Exit;
    }

    if(!CryptExportKey(hKey, 0, PUBLICKEYBLOB, 0, nullptr, &uklen)){
        err = GetLastError();
        goto Exit;
    }

    ukval.resize(uklen);
    if(!CryptExportKey(hKey, 0, PUBLICKEYBLOB, 0, ukval.data(), &uklen)){
        err = GetLastError();
        goto Exit;
    }

    ukval = ukval.mid(sizeof(BLOBHEADER) + sizeof(TZI_ASYMM_KEY_BLOB_HEADER));
    err = ckval == ukval ? ERROR_SUCCESS : 0xffffffff;

Exit:
    if(hProv){
        CryptReleaseContext(hProv, 0);
    }
    return err;
}

QString KeyLinkManager::Cp1251ToQstring(const QVector<BYTE>& cp1251str)
{
    QTextCodec *codec = QTextCodec::codecForName( "CP1251" );
    QByteArray text = QByteArray::fromRawData(reinterpret_cast<const char*>(cp1251str.data()),
                                              cp1251str.size());
    return codec->toUnicode(text);
}

SECURITY_STATUS KeyLinkManager::SetKeyProvInfo(const QString& prov_name, const QString& key_name,
                                               const DWORD csp_type)
{
    CRYPT_KEY_PROV_INFO kpi = { 0 };
    std::wstring wprov_name = prov_name.toStdWString();
    std::wstring wkey_name = key_name.toStdWString();
    kpi.pwszProvName = const_cast<wchar_t*>(wprov_name.data());
    kpi.pwszContainerName = const_cast<wchar_t*>(wkey_name.data());
    kpi.dwKeySpec = AT_SIGNATURE;
    kpi.dwProvType = csp_type;
    kpi.dwFlags = CERT_SET_KEY_PROV_HANDLE_PROP_ID;
    if(!CertSetCertificateContextProperty(pCert_, CERT_KEY_PROV_INFO_PROP_ID, 0, &kpi)){
        return GetLastError();
    }
    return ERROR_SUCCESS;
}

SECURITY_STATUS KeyLinkManager::RegisterKey(const QString& container_name, const DWORD dev_type,
                                            const DWORD slot_id)
{
    RegistryManager regmgr;
    NTSTATUS status = NTE_FAIL;
    DWORD dev = 0;

    switch(dev_type){
    case Constants::SOFT_IMPL_FLAG:
        dev = 1;
        break;
    case Constants::SIGMA_IMPL_FLAG:
        dev = 2;
        break;
    case Constants::NTSTORE_IMPL_FLAG:
        dev = 3;
        break;
    default:
        return status;
    }

    status = regmgr.OpenCurrentUser(KEY_WRITE);
    if (status != ERROR_SUCCESS)
    {
        return status;
    }

    status = regmgr.CreateKey(RegConstants::PROVIDER_KEYS_REG_PATH+container_name);
    if (status != ERROR_SUCCESS)
    {
        return status;
    }

    status = regmgr.SetKeyValue("",
                                RegConstants::KEY_STORAGE_OPTION,
                                REG_DWORD,
                                reinterpret_cast<const BYTE*>(&dev),
                                sizeof(dev));
    if (status != ERROR_SUCCESS)
    {
        return status;
    }

    status = regmgr.SetKeyValue("",
                                RegConstants::KEY_STORAGE_SLOT_ID_OPTION,
                                REG_DWORD,
                                reinterpret_cast<const BYTE*>(&slot_id),
                                sizeof(slot_id));
    return status;
}

bool KeyLinkManager::IsRegistered(const QString& container_name)
{
    RegistryManager regmgr;
    NTSTATUS status = NTE_FAIL;

    status = regmgr.OpenCurrentUser(KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);
    if (status != ERROR_SUCCESS)
    {
        return false;
    }

    status = regmgr.OpenKey(RegConstants::PROVIDER_KEYS_REG_PATH + container_name,
                            KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE);

    return (status == ERROR_SUCCESS) ? true : false;
}

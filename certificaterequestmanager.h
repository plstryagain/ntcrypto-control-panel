#ifndef CERTIFICATEREQUESTMANAGER_H
#define CERTIFICATEREQUESTMANAGER_H

#include <QObject>

enum class CERT_REQ_TYPE : int
{
    INVALID = -1,
    INDIVIDUAL = 0,
    ENTITY = 1,
    AC = 2
};

class CertificateRequestManager : public QObject
{
    Q_OBJECT



public:
    explicit CertificateRequestManager(QObject *parent = 0);
    void Init(const QString& key_name, const QString& subject, int req_type);
    void Init(const QStringList& info);

signals:
    void reportError(unsigned long);
    void operationComplete(QVector<unsigned char>);
    void signalFinished();

public slots:
    void SignRequest();

private:
    std::string GetSignatureOid(const std::string& pub_key_oid,
                                const std::vector<unsigned char> &params);

private:
    QString key_name_;
    QString subject_;
    CERT_REQ_TYPE req_type_;
    QStringList info_;
};

#endif // CERTIFICATEREQUESTMANAGER_H

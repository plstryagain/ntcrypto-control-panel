#ifndef SELFTESTWIZARD_H
#define SELFTESTWIZARD_H

#include <QObject>
#include <QDialog>
#include <QGridLayout>
#include <QLabel>
#include <QProgressBar>
#include <QShowEvent>
#include <QStyle>
#include <QDialogButtonBox>
#include <QPushButton>
#include <Windows.h>
#include <QVector>
#include <QThread>
#include <QPair>
#include <QRadioButton>
#include <QHBoxLayout>

class SelftestWizard : public QDialog
{
    Q_OBJECT

public:
    SelftestWizard(QWidget* parent = 0);

protected:
    void showEvent(QShowEvent *) override;

private slots:
    void Reset();
    void btn_ok_clicked();
    void btn_cancel_clicked();
    void SetStb31Status(QString report);
    void SetStb45Status(QString report);
    void SetStb77Status(QString report);
    void AnalyzeResult();
    void ShowReport();
    void InitUiByDevType();

private:
    QGridLayout gl_;
    QHBoxLayout hl_;
    QLabel lb_stb31;
    QProgressBar bar_stb31;
    QLabel lb_stb31_status;
    QLabel lb_stb45;
    QProgressBar bar_stb45;
    QLabel lb_stb45_status;
    QLabel lb_stb77;
    QProgressBar bar_stb77;
    QLabel lb_stb77_status;
    QDialogButtonBox btn_box_;
    QRadioButton rb_soft_;
    QRadioButton rb_sigma_;
    QRadioButton rb_ntstore_;
    QWidget* parent_;
    QString report_;
};

#endif // SELFTESTWIZARD_H

#include "crldpwizard.h"
#include <QDebug>

CrlDpWizard::CrlDpWizard(QWidget *parent, const QStringList &crldp_urls)
    : parent_(parent), crldp_urls_(crldp_urls),
      gl_(this), vl_(),
      btn_box_(QDialogButtonBox::Ok | QDialogButtonBox::Cancel),
      spacer_(new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Expanding))
{
    this->setWindowTitle(tr("Точки распространения СОС"));
    this->setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowIcon(QIcon(QString::fromUtf8(":/images/images/Administrative Tools_50px.png")));

    vl_.addWidget(&btn_add_);
    vl_.addWidget(&btn_edit_);
    vl_.addWidget(&btn_del_);
    vl_.addSpacerItem(spacer_);
    gl_.addWidget(&list_urls_, 0, 0);
    gl_.addLayout(&vl_, 0, 1);
    gl_.addWidget(&btn_box_, 1, 0);
    gl_.setSizeConstraint(QLayout::SetFixedSize);

    btn_box_.button(QDialogButtonBox::Cancel)->setText(tr("Закрыть"));

    btn_add_.setText(trUtf8("Добавить"));
    btn_add_.setIcon(QIcon(":/images/images/Plus Math_32px.png"));
    btn_edit_.setText(trUtf8("Изменить"));
    btn_edit_.setIcon(QIcon(":/images/images/Edit_32px.png"));
    btn_del_.setText(trUtf8("Удалить"));
    btn_del_.setIcon(QIcon(":/images/images/Subtract_32px.png"));
    btn_box_.button(QDialogButtonBox::Cancel)->setIcon(QIcon(":/images/images/Delete_32px.png"));
    btn_box_.button(QDialogButtonBox::Ok)->setIcon(QIcon(":/images/images/Checkmark_32px.png"));

    list_urls_.addItems(crldp_urls_);
//    for(auto i = 0; i < list_urls_.count(); ++i)
//    {
//        list_urls_.item(i)->setFlags(list_urls_.item(i)->flags()  | Qt::ItemIsEditable);
//    }
//    list_urls_.setEditTriggers(QAbstractItemView::DoubleClicked);


    connect(&btn_add_, SIGNAL(clicked(bool)), this, SLOT(OnBtnAddClicked()));
    connect(&btn_edit_, SIGNAL(clicked(bool)), this, SLOT(OnBtnEditClicked()));
    connect(&btn_del_, SIGNAL(clicked(bool)), this, SLOT(OnBtnDelClicked()));
    connect(btn_box_.button(QDialogButtonBox::Cancel), SIGNAL(clicked()),
            this, SLOT(OnBtnCancelClicked()));
    connect(btn_box_.button(QDialogButtonBox::Ok), SIGNAL(clicked(bool)),
            this, SLOT(OnBtnOkClicked()));
}

CrlDpWizard::~CrlDpWizard()
{

}

void CrlDpWizard::showEvent(QShowEvent* event)
{
    Q_UNUSED(event);
    QRect parentRect(parent_->mapToGlobal(QPoint(0, 0)), parent_->size());
    this->move(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), parentRect).topLeft());
}

void CrlDpWizard::OnBtnCancelClicked()
{
    this->close();
}

void CrlDpWizard::OnBtnOkClicked()
{
    QStringList new_url_list;
    for(auto i = 0; i < list_urls_.count(); ++i)
    {
        new_url_list.append(list_urls_.item(i)->text());
    }
    if(new_url_list != crldp_urls_)
    {
        emit urlEditFinished(new_url_list);
    }
    this->close();
}

void CrlDpWizard::OnBtnAddClicked()
{
    if(list_urls_.currentRow() < 0)
    {
        return;
    }
    QString text = "";
    if(ShowUrlEditDlg(&text))
    {
        list_urls_.addItem(text);
    }
}

void CrlDpWizard::OnBtnEditClicked()
{
    if(list_urls_.currentRow() < 0)
    {
        return;
    }
    QString text = list_urls_.currentItem()->text();
    if(ShowUrlEditDlg(&text))
    {
        list_urls_.currentItem()->setText(text);
    }
}

void CrlDpWizard::OnBtnDelClicked()
{
    QMessageBox msg_box(QMessageBox::Warning,
                        trUtf8("Внимание"),
                        trUtf8("Вы действительно хотите удалить ссылку\n")+
                        list_urls_.currentItem()->text() + trUtf8("?"),
                        QMessageBox::Yes | QMessageBox::No);
    msg_box.setButtonText(QMessageBox::Yes, trUtf8("Да"));
    msg_box.setButtonText(QMessageBox::No, trUtf8("Нет"));
    if(msg_box.exec() == QMessageBox::No)
    {
        return;
    }

    delete list_urls_.takeItem(list_urls_.currentRow());
}

bool CrlDpWizard::ShowUrlEditDlg(QString* text)
{
    QDialog dlg(this);
    dlg.setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    QFormLayout fl(&dlg);
    QLineEdit ln_edit(*text);
    ln_edit.setFixedSize(450, 22);
    QDialogButtonBox btn_box(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    btn_box.button(QDialogButtonBox::Cancel)->setText(tr("Отмена"));
    btn_box.button(QDialogButtonBox::Cancel)->setIcon(QIcon(":/images/images/Delete_32px.png"));
    btn_box.button(QDialogButtonBox::Ok)->setIcon(QIcon(":/images/images/Checkmark_32px.png"));
    fl.addRow(trUtf8("URL:"), &ln_edit);
    fl.addRow(&btn_box);
    connect(btn_box.button(QDialogButtonBox::Ok), SIGNAL(clicked(bool)), &dlg, SLOT(accept()));
    connect(btn_box.button(QDialogButtonBox::Cancel), SIGNAL(clicked(bool)), &dlg, SLOT(reject()));
    if(dlg.exec() == QDialog::Accepted)
    {
        *text = ln_edit.text();
        return true;
    }
    else
    {
        return false;
    }
}

#ifndef IMPORTCERTIFICATEWIZARD_H
#define IMPORTCERTIFICATEWIZARD_H

#include <QObject>
#include <QDialog>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QTableWidget>
#include <QList>
#include <QLabel>
#include <QTableWidget>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QCheckBox>
#include <Windows.h>
#include <memory>
#include <vector>
#include <QDebug>


class ICertificateManager;
class SystemCertificateManager;

class ImportCertificateWizard : public QDialog
{

    Q_OBJECT

    enum class OBJ_TYPE
    {
        InvalidType = -1,
        Certificate = 0,        
        CRL = 1,
        AttributeCertificate = 2
    };

    struct OBJECT_TO_BE_IMPORTED
    {
        OBJECT_TO_BE_IMPORTED() = default;

        ~OBJECT_TO_BE_IMPORTED() = default;

        QCheckBox check;
        OBJ_TYPE type = OBJ_TYPE::InvalidType;
        QString subject;
        QString not_before;
        QString not_after;
        ICertificateManager* mgr = nullptr;
        bool is_root = false;
    };


public:
    ImportCertificateWizard(QWidget* parent, const QStringList& names);
    ~ImportCertificateWizard();
     void ShowReport();

protected:
    void showEvent(QShowEvent *event) override;

private slots:
    void btn_cancel_clicked();
    void btn_import_clicked();
    void on_cert_double_clicked(QModelIndex qmi);

private:
    void ParseCertFile();
    void ParseSingleCert(const QByteArray& content);
    void ParsePkcs7Cert(const QByteArray& content);
    void FillObjTable();
    void ShowErrorMessage(const QString& err, const DWORD err_num);
    bool IsAtLeastOneChecked();
    void AddToReport(const std::unique_ptr<OBJECT_TO_BE_IMPORTED> &obj, const QString &reason, const DWORD error);

private:
    QWidget* parent_;
    QStringList names_;
    QVBoxLayout vl_;
    QLabel lb_descr_;
    QTableWidget tw_certs_;
    QDialogButtonBox btn_box_;
    HCERTSTORE hPkcs7Store_;
    QString report_;
    std::vector<std::unique_ptr<OBJECT_TO_BE_IMPORTED>> obj_list_;
};

#endif // IMPORTCERTIFICATEWIZARD_H

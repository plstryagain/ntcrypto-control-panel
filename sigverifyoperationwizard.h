#ifndef SIGVERIFYOPERATIONWIZARD_H
#define SIGVERIFYOPERATIONWIZARD_H

#include <QWidget>

namespace Ui {
class SigVerifyOperationWizard;
}

struct _CERT_CONTEXT;

class SigVerifyOperationWizard : public QWidget
{
    Q_OBJECT

public:
    explicit SigVerifyOperationWizard(QWidget *parent = 0);
    ~SigVerifyOperationWizard();

private slots:
    void on_btn_sig_file_clicked();
    void on_btn_verify_sig_clicked();
    void Error(const QString& error);
    void VerifyComplete(bool is_valid, QString signer, QString sig_time, QString cert_status,
                        const _CERT_CONTEXT* pCert);
    void on_btn_data_file_clicked();


    void on_ln_sig_file_textChanged(const QString &arg1);

    void on_btn_show_cert_clicked();

private:
    bool IsDetached(const QString& sig_file, bool *is_detached);
    void ShowEmptyLineNotification(QWidget* w, const QString& text);
    bool IsBase64(const QString& string);

private:
    Ui::SigVerifyOperationWizard *ui;
    bool is_op_success_ = true;
    const _CERT_CONTEXT* pCert_ = nullptr;
};

#endif // SIGVERIFYOPERATIONWIZARD_H

#include "test.h"
#include "testdata.h"

Test::Test(QObject *parent, DWORD dev_type) : QObject(parent), dev_type_(dev_type)
{

}

void Test::LaunchAllTests()
{
    LaunchStb31Tests();
    LaunchStb45Tests();
    if(dev_type_ != Constants::SIGMA_IMPL_FLAG)
    {
        LaunchStb77Tests();
    }
    emit signalFinish();
}

void Test::LaunchStb31Tests()
{
    DWORD error = NTE_FAIL;
    QString report;
    int i = 0;
    for(const auto& test : stb31_test_data)
    {
        if(dev_type_ == Constants::NTSTORE_IMPL_FLAG)
        {
            if(test.type == TEST_TYPE::HASH && test.alg_id == CALG_BELT_HASH_256)
            {
                error = LaunchHashTest(test.alg_id, test.input_data, test.key, test.exp);
            }
            else
            {
                continue;
            }
        }
        else
        {
            switch(test.type)
            {
            case TEST_TYPE::HASH:
                error = LaunchHashTest(test.alg_id, test.input_data, test.key, test.exp);
                break;

            case TEST_TYPE::ENCRYPT:
                error = LaunchEncryptTest(test.alg_id, test.input_data, test.key, test.iv, test.exp);
                break;

            case TEST_TYPE::DECRYPT:
                error = LaunchDecryptTest(test.alg_id, test.input_data, test.key, test.iv, test.exp);
                break;

            default:
                return;
            }
        }

        if(error != ERROR_SUCCESS)
        {
            report.append("Тест "+QString::number(i)+": завершился неудачей (код ошибки: "
                          + QString::number(error) + ")\n");
        }
        else
        {
            report.append("Тест "+QString::number(i)+": завершился успешно\n");
        }
        emit signalStb31Progress(i);
        ++i;
    }
    emit signalStb31TestFinish(report);
}

void Test::LaunchStb45Tests()
{
    int i = 1;
    QString report;
    DWORD error = Stb45_bs11();
    if(error != ERROR_SUCCESS)
    {
        report.append("Тест "+QString::number(i)+": завершился неудачей (код ошибки: "
                      + QString::number(error) + ")\n");
    }
    else
    {
        report.append("Тест "+QString::number(i)+": завершился успешно\n");
    }
    emit signalStb45Progress(i);
    ++i;

    error = Stb45_BS12Test();
    if(error != ERROR_SUCCESS)
    {
        report.append("Тест "+QString::number(i)+": завершился неудачей (код ошибки: "
                      + QString::number(error) + ")\n");
    }
    else
    {
        report.append("Тест "+QString::number(i)+": завершился успешно\n");
    }
    emit signalStb45Progress(i);
    ++i;

    if(dev_type_ != Constants::NTSTORE_IMPL_FLAG)
    {
        error = Stb45_BS13Test();
        if(error != ERROR_SUCCESS)
        {
            report.append("Тест "+QString::number(i)+": завершился неудачей (код ошибки: "
                          + QString::number(error) + ")\n");
        }
        else
        {
            report.append("Тест "+QString::number(i)+": завершился успешно\n");
        }
        emit signalStb45Progress(i);
        ++i;

        error = Stb45_BS14Test();
        if(error != ERROR_SUCCESS)
        {
            report.append("Тест "+QString::number(i)+": завершился неудачей (код ошибки: "
                          + QString::number(error) + ")\n");
        }
        else
        {
            report.append("Тест "+QString::number(i)+": завершился успешно\n");
        }
        emit signalStb45Progress(i);
        ++i;

        error = Stb45_BS18Test();
        if(error != ERROR_SUCCESS)
        {
            report.append("Тест "+QString::number(i)+": завершился неудачей (код ошибки: "
                          + QString::number(error) + ")\n");
        }
        else
        {
            report.append("Тест "+QString::number(i)+": завершился успешно\n");
        }
        emit signalStb45Progress(i);
        ++i;
    }

    emit signalStb45TestFinish(report);

}

void Test::LaunchStb77Tests()
{
    DWORD error = NTE_FAIL;
    QString report;
    int i = 1;
    std::vector<BYTE> key;
    for(const auto& test : stb77_test_data)
    {
        error = LaunchHashTest(test.alg_id, test.input_data, key, test.exp);
        if(error != ERROR_SUCCESS)
        {
            report.append("Тест "+QString::number(i)+": завершился неудачей (код ошибки: "
                          + QString::number(error) + ")\n");
        }
        else
        {
            report.append("Тест "+QString::number(i)+": завершился успешно\n");
        }
        emit signalStb77Progress(i);
        ++i;
    }
    emit signalStb77TestFinish(report);
}

DWORD Test::LaunchHashTest(const ALG_ID alg_id, const std::vector<BYTE>& input_val, const std::vector<BYTE>& key,
    const std::vector<BYTE>& exp_val)
{
    DWORD error = NTE_FAIL;
    HCRYPTPROV hProv = NULL;
    HCRYPTHASH hHash = NULL;
    HCRYPTKEY hKey = NULL;
    std::vector<BYTE> hash_result;
    DWORD hash_result_len = 0;
    bool is_test_passed = false;
    DWORD res = 0;

    BLOBHEADER blob_header;
    TZI_SYMM_KEY_BLOB_HEADER tzi_header;
    std::vector<BYTE> blob;
    BYTE* pCurr = nullptr;

    if (!CryptAcquireContext(&hProv, NULL, NULL, 322, CRYPT_VERIFYCONTEXT | dev_type_))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!key.empty())
    {
        blob_header.aiKeyAlg = CALG_BELT_CBC_256;
        blob_header.bType = SIMPLEBLOB;
        blob_header.bVersion = CUR_BLOB_VERSION;
        blob_header.reserved = 0;

        tzi_header.magic = Constants::BELT_KEY_MAGIC;
        tzi_header.length = static_cast<DWORD>(key.size());

        blob.resize(sizeof(BLOBHEADER)+sizeof(TZI_SYMM_KEY_BLOB_HEADER)+key.size());
        pCurr = blob.data();
        CopyMemory(pCurr, &blob_header, sizeof(BLOBHEADER));
        pCurr += sizeof(BLOBHEADER);
        CopyMemory(pCurr, &tzi_header, sizeof(TZI_SYMM_KEY_BLOB_HEADER));
        pCurr += sizeof(TZI_SYMM_KEY_BLOB_HEADER);
        CopyMemory(pCurr, key.data(), key.size());

        if (!CryptImportKey(hProv, blob.data(), blob.size(), hKey, 0, &hKey))
        {
            error = GetLastError();
            goto Exit;
        }
    }


    if (!CryptCreateHash(hProv, alg_id, hKey, 0, &hHash))
    {
        error = GetLastError();
        goto Exit;
    }


    if (!CryptHashData(hHash, input_val.data(), static_cast<DWORD>(input_val.size()), 0))
    {
        error = GetLastError();
        goto Exit;
    }

    res = sizeof(hash_result_len);
    if (!CryptGetHashParam(hHash, HP_HASHSIZE, reinterpret_cast<BYTE*>(&hash_result_len), &res, 0))
    {
        error = GetLastError();
        goto Exit;
    }

    hash_result.resize(hash_result_len);
    if (!CryptGetHashParam(hHash, HP_HASHVAL, hash_result.data(), &hash_result_len, 0))
    {
        error = GetLastError();
        goto Exit;
    }

    is_test_passed = CheckResult(hash_result.data(), hash_result_len, exp_val.data(),
        static_cast<DWORD>(exp_val.size()));

    error = is_test_passed ? ERROR_SUCCESS : NTE_FAIL;

Exit:

    if (hHash)
    {
        CryptDestroyHash(hHash);
    }

    if (hProv)
    {
        CryptReleaseContext(hProv, NULL);
    }

    return error;
}

DWORD Test::LaunchEncryptTest(const ALG_ID alg_id, const std::vector<BYTE>& input_val,
    const std::vector<BYTE>& key, const std::vector<BYTE>&iv, const std::vector<BYTE>& exp_val)
{
    DWORD error = NTE_FAIL;
    HCRYPTPROV hProv = NULL;
    HCRYPTKEY hSecKey = NULL;
    DWORD buffer_len = 0;
    DWORD in_len = 0;
    bool is_test_passed = false;
    std::vector<BYTE> data_for_enc = input_val;

    BLOBHEADER blob_header;
    TZI_SYMM_KEY_BLOB_HEADER tzi_header;
    std::vector<BYTE> blob;
    BYTE* pCurr = nullptr;

    blob_header.aiKeyAlg = alg_id;
    blob_header.bType = SIMPLEBLOB;
    blob_header.bVersion = CUR_BLOB_VERSION;
    blob_header.reserved = 0;

    tzi_header.magic = Constants::BELT_KEY_MAGIC;
    tzi_header.length = static_cast<DWORD>(key.size());

    blob.resize(sizeof(BLOBHEADER)+sizeof(TZI_SYMM_KEY_BLOB_HEADER)+key.size());
    pCurr = blob.data();
    CopyMemory(pCurr, &blob_header, sizeof(BLOBHEADER));
    pCurr += sizeof(BLOBHEADER);
    CopyMemory(pCurr, &tzi_header, sizeof(TZI_SYMM_KEY_BLOB_HEADER));
    pCurr += sizeof(TZI_SYMM_KEY_BLOB_HEADER);
    CopyMemory(pCurr, key.data(), key.size());

    if (!CryptAcquireContext(&hProv, nullptr, NULL, Constants::BIGN_CSP_TYPE,
                             CRYPT_VERIFYCONTEXT | dev_type_))
    {
        error = GetLastError();
        goto Exit;
    }


    if (!CryptImportKey(hProv, blob.data(), blob.size(), 0, 0, &hSecKey))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!iv.empty())
    {
        DWORD param = (alg_id == CALG_BELT_KEYWRAP) ? KP_KEY_HEADER : KP_IV;
        if (!CryptSetKeyParam(hSecKey, param, iv.data(), 0))
        {
            error = GetLastError();
            goto Exit;
        }
    }

    in_len = static_cast<DWORD>(input_val.size());
    buffer_len = in_len;
    if (!CryptEncrypt(hSecKey, NULL, TRUE, 0, nullptr, &buffer_len, buffer_len))
    {
        error = GetLastError();
        goto Exit;
    }

    if (data_for_enc.size() < buffer_len)
    {
        data_for_enc.resize(buffer_len);
    }

    if (!CryptEncrypt(hSecKey, NULL, TRUE, 0, data_for_enc.data(), &in_len, buffer_len))
    {
        error = GetLastError();
        goto Exit;
    }


    is_test_passed = CheckResult(data_for_enc.data(), static_cast<DWORD>(data_for_enc.size()), exp_val.data(),
        static_cast<DWORD>(exp_val.size()));
    error = is_test_passed ? ERROR_SUCCESS : NTE_FAIL;

Exit:

    if (hSecKey)
    {
        CryptDestroyKey(hSecKey);
    }

    if (hProv)
    {
        CryptReleaseContext(hProv, NULL);
    }

    return error;
}

DWORD Test::LaunchDecryptTest(const ALG_ID alg_id, const std::vector<BYTE>& input_val,
    const std::vector<BYTE>& key, const std::vector<BYTE>&iv, const std::vector<BYTE>& exp_val)
{
    DWORD error = NTE_FAIL;
    HCRYPTPROV hProv = NULL;
    HCRYPTKEY hKey = NULL;
    HCRYPTKEY hSecKey = NULL;
    DWORD buffer_len = 0;
    bool is_test_passed = false;
    std::vector<BYTE> data_for_enc = input_val;

    BLOBHEADER blob_header;
    TZI_SYMM_KEY_BLOB_HEADER tzi_header;
    std::vector<BYTE> blob;
    BYTE* pCurr = nullptr;

    blob_header.aiKeyAlg = alg_id;
    blob_header.bType = SIMPLEBLOB;
    blob_header.bVersion = CUR_BLOB_VERSION;
    blob_header.reserved = 0;

    tzi_header.magic = Constants::BELT_KEY_MAGIC;
    tzi_header.length = static_cast<DWORD>(key.size());

    blob.resize(sizeof(BLOBHEADER)+sizeof(TZI_SYMM_KEY_BLOB_HEADER)+key.size());
    pCurr = blob.data();
    CopyMemory(pCurr, &blob_header, sizeof(BLOBHEADER));
    pCurr += sizeof(BLOBHEADER);
    CopyMemory(pCurr, &tzi_header, sizeof(TZI_SYMM_KEY_BLOB_HEADER));
    pCurr += sizeof(TZI_SYMM_KEY_BLOB_HEADER);
    CopyMemory(pCurr, key.data(), key.size());

    if (!CryptAcquireContext(&hProv, nullptr, NULL, Constants::BIGN_CSP_TYPE,
                             CRYPT_VERIFYCONTEXT | dev_type_))
    {
        error = GetLastError();
        goto Exit;
    }


    if (!CryptImportKey(hProv, blob.data(), blob.size(), hKey, 0, &hSecKey))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!iv.empty())
    {
        DWORD param = (alg_id == CALG_BELT_KEYWRAP) ? KP_KEY_HEADER : KP_IV;
        if (!CryptSetKeyParam(hSecKey, param, iv.data(), 0))
        {
            error = GetLastError();
            goto Exit;
        }
    }

    buffer_len = data_for_enc.size();
    if (!CryptDecrypt(hSecKey, NULL, TRUE, 0, data_for_enc.data(), &buffer_len))
    {
        error = GetLastError();
        goto Exit;
    }

    data_for_enc.resize(buffer_len);
    is_test_passed = CheckResult(data_for_enc.data(), static_cast<DWORD>(data_for_enc.size()), exp_val.data(),
        static_cast<DWORD>(exp_val.size()));

    error = is_test_passed ? ERROR_SUCCESS : NTE_FAIL;

Exit:

    if (hSecKey)
    {
        CryptDestroyKey(hSecKey);
    }

    if (hKey)
    {
        CryptDestroyKey(hKey);
    }

    if (hProv)
    {
        CryptReleaseContext(hProv, NULL);
    }

    return error;
}

DWORD Test::Stb45_bs11()
{
    HCRYPTPROV hProv = NULL;
    HCRYPTKEY hKey = NULL;
    HCRYPTHASH hHash = NULL;
    DWORD error = 0;
    DWORD priv_blob_len = 0;
    std::vector<BYTE> priv_blob;
    DWORD sign_len = 0;
    std::vector<BYTE> sign_val;

    if (!CryptAcquireContext(&hProv, nullptr, NULL, Constants::BIGN_CSP_TYPE, dev_type_))
    {
        error = GetLastError();
        goto Exit;
    }

    CreatePrivBlob(&priv_blob, &priv_blob_len);
    if (!CryptImportKey(hProv, priv_blob.data(), priv_blob_len, 0, 0, &hKey))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptCreateHash(hProv, CALG_BELT_HASH_256, 0, 0, &hHash))
    {
        error = GetLastError();
        goto Exit;
    }


    if (!CryptHashData(hHash, stb45_bs11_mess_val.data(), static_cast<DWORD>(stb45_bs11_mess_val.size()), 0))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptSignHash(hHash, AT_SIGNATURE, nullptr, 0, nullptr, &sign_len))
    {
        error = GetLastError();
        goto Exit;
    }

    sign_val.resize(sign_len);
    if (!CryptSignHash(hHash, AT_SIGNATURE, nullptr, 0, sign_val.data(), &sign_len))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptVerifySignature(hHash, sign_val.data(), sign_len, hKey, nullptr, 0))
    {
        error = GetLastError();
        goto Exit;
    }

    error = ERROR_SUCCESS;

Exit:
    if (hKey)
    {
        CryptDestroyKey(hKey);
    }

    if (hHash)
    {
        CryptDestroyHash(hHash);
    }

    if (hProv)
    {
        CryptReleaseContext(hProv, NULL);
    }

    return error;
}

DWORD Test::Stb45_BS12Test()
{
    HCRYPTPROV hProv = NULL;
    HCRYPTKEY hKey = NULL;
    HCRYPTHASH hHash = NULL;
    DWORD error = 0;
    DWORD priv_blob_len = 0;
    std::vector<BYTE> priv_blob;
    std::vector<BYTE> sign_val = stb45_bs12_sign_val;

    if (!CryptAcquireContext(&hProv, nullptr, NULL, Constants::BIGN_CSP_TYPE, dev_type_))
    {
        error = GetLastError();
        goto Exit;
    }

    CreatePrivBlob(&priv_blob, &priv_blob_len);
    if (!CryptImportKey(hProv, priv_blob.data(), priv_blob_len, 0, 0, &hKey))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptCreateHash(hProv, CALG_BELT_HASH_256, 0, 0, &hHash))
    {
        error = GetLastError();
        goto Exit;
    }


    if (!CryptHashData(hHash, stb45_bs12_mess_val.data(),
                       static_cast<DWORD>(stb45_bs12_mess_val.size()), 0))
    {
        error = GetLastError();
        goto Exit;
    }

    std::reverse(sign_val.begin(), sign_val.end());
    if (!CryptVerifySignature(hHash, sign_val.data(), static_cast<DWORD>(sign_val.size()),
        hKey, nullptr, 0))
    {
        error = GetLastError();
        goto Exit;
    }

    error = ERROR_SUCCESS;

Exit:

    if (hKey)
    {
        CryptDestroyKey(hKey);
    }

    if (hHash)
    {
        CryptDestroyHash(hHash);
    }

    if (hProv)
    {
        CryptReleaseContext(hProv, NULL);
    }

    return error;
}

DWORD Test::Stb45_BS13Test()
{
    HCRYPTPROV hProv = NULL;
    HCRYPTKEY hKey = NULL;
    HCRYPTKEY hSecKey = NULL;
    DWORD error = NTE_FAIL;
    DWORD priv_blob_len = 0;
    std::vector<BYTE> priv_blob;
    DWORD sec_blob_len = 0;
    std::vector<BYTE> sec_blob;
    TZI_SYMM_KEY_BLOB_HEADER tzi_header;
    BLOBHEADER sec_hdr;
    BYTE* pCurr = nullptr;
    std::vector<BYTE> key_token_blob;
    DWORD key_token_blob_len = 0;

    if (!CryptAcquireContext(&hProv, nullptr, nullptr, Constants::BIGN_CSP_TYPE, dev_type_))
    {
        error = GetLastError();
        goto Exit;
    }

    CreatePrivBlob(&priv_blob, &priv_blob_len);
    if (!CryptImportKey(hProv, priv_blob.data(), priv_blob_len, 0, 0, &hKey))
    {
        error = GetLastError();
        goto Exit;
    }

    sec_hdr.aiKeyAlg = 0;
    sec_hdr.bType = SIMPLEBLOB;
    sec_hdr.bVersion = CUR_BLOB_VERSION;
    sec_hdr.reserved = 0;
    tzi_header.length = static_cast<DWORD>(stb45_bs13_trans_key.size());
    tzi_header.magic = 0;
    sec_blob_len = sizeof(BLOBHEADER)+sizeof(TZI_SYMM_KEY_BLOB_HEADER)+
            static_cast<DWORD>(stb45_bs13_trans_key.size());
    sec_blob.resize(sec_blob_len);
    pCurr = sec_blob.data();
    CopyMemory(pCurr, &sec_hdr, sizeof(sec_hdr));
    pCurr += sizeof(sec_hdr);
    CopyMemory(pCurr, &tzi_header, sizeof(tzi_header));
    pCurr += sizeof(tzi_header);
    CopyMemory(pCurr, stb45_bs13_trans_key.data(), stb45_bs13_trans_key.size());

    if (!CryptImportKey(hProv, sec_blob.data(), sec_blob_len, 0, 0, &hSecKey))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptSetKeyParam(hSecKey, KP_KEY_HEADER, stb45_bs13_key_hdr.data(), 0))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptExportKey(hSecKey, hKey, SYMMETRICWRAPKEYBLOB, 0, nullptr, &key_token_blob_len))
    {
        error = GetLastError();
        goto Exit;
    }

    key_token_blob.resize(key_token_blob_len);
    if (!CryptExportKey(hSecKey, hKey, SYMMETRICWRAPKEYBLOB, 0, key_token_blob.data(), &key_token_blob_len))
    {
        error = GetLastError();
        goto Exit;
    }

    error = ERROR_SUCCESS;

Exit:
    if (hKey)
    {
        CryptDestroyKey(hKey);
    }

    if (hSecKey)
    {
        CryptDestroyKey(hSecKey);
    }

    if (hProv)
    {
        CryptReleaseContext(hProv, NULL);
    }

    return error;
}

DWORD Test::Stb45_BS14Test()
{
    HCRYPTPROV hProv = NULL;
    HCRYPTKEY hKey = NULL;
    HCRYPTKEY hSecKey = NULL;
    DWORD error = NTE_FAIL;
    DWORD priv_blob_len = 0;
    std::vector<BYTE> priv_blob;
    BYTE* pCurr = nullptr;
    BIGN_KEY_TRANS_BLOB_HEADER trans_blob_hdr;
    std::vector<BYTE> token_blob;
    DWORD token_blob_len = 0;
    bool is_test_passed = false;
    DWORD sec_blob_len = 0;
    std::vector<BYTE> sec_blob_val;
    BLOBHEADER* sec_blob_hdr = nullptr;
    TZI_SYMM_KEY_BLOB_HEADER* tzi_sec_hdr = nullptr;
    BYTE* sec_key_val = nullptr;

    if (!CryptAcquireContext(&hProv, nullptr, nullptr, Constants::BIGN_CSP_TYPE, dev_type_))
    {
        error = GetLastError();
        goto Exit;
    }

    CreatePrivBlob(&priv_blob, &priv_blob_len);
    if (!CryptImportKey(hProv, priv_blob.data(), priv_blob_len, 0, 0, &hKey))
    {
        error = GetLastError();
        goto Exit;
    }

    token_blob_len = sizeof(BIGN_KEY_TRANS_BLOB_HEADER)+static_cast<DWORD>(stb45_bs14_key_hdr_val.size()) +
        static_cast<DWORD>(stb45_bs14_token_val.size());
    trans_blob_hdr.key_hdr_len = static_cast<DWORD>(stb45_bs14_key_hdr_val.size());
    trans_blob_hdr.token_len = static_cast<DWORD>(stb45_bs14_token_val.size());
    token_blob.resize(token_blob_len);
    pCurr = token_blob.data();
    CopyMemory(pCurr, &trans_blob_hdr, sizeof(trans_blob_hdr));
    pCurr += sizeof(trans_blob_hdr);
    CopyMemory(pCurr, stb45_bs14_token_val.data(), stb45_bs14_token_val.size());
    pCurr += stb45_bs14_token_val.size();
    CopyMemory(pCurr, stb45_bs14_key_hdr_val.data(), stb45_bs14_key_hdr_val.size());

    if (!CryptImportKey(hProv, token_blob.data(), token_blob_len, hKey, CRYPT_EXPORTABLE, &hSecKey))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptExportKey(hSecKey, 0, SIMPLEBLOB, 0, nullptr, &sec_blob_len))
    {
        error = GetLastError();
        goto Exit;
    }

    sec_blob_val.resize(sec_blob_len);
    if (!CryptExportKey(hSecKey, 0, SIMPLEBLOB, 0, sec_blob_val.data(), &sec_blob_len))
    {
        error = GetLastError();
        goto Exit;
    }

    sec_blob_hdr = reinterpret_cast<BLOBHEADER*>(sec_blob_val.data());
    tzi_sec_hdr = reinterpret_cast<TZI_SYMM_KEY_BLOB_HEADER*>(sec_blob_hdr + 1);
    sec_key_val = sec_blob_val.data() + sizeof(BLOBHEADER)+sizeof(TZI_SYMM_KEY_BLOB_HEADER);
    is_test_passed = CheckResult(sec_key_val, tzi_sec_hdr->length, stb45_bs14_exp_key_val.data(),
        static_cast<DWORD>(stb45_bs14_exp_key_val.size()));
    error = is_test_passed ? ERROR_SUCCESS : NTE_FAIL;

Exit:

    if (hKey)
    {
        CryptDestroyKey(hKey);
    }

    if (hSecKey)
    {
        CryptDestroyKey(hSecKey);
    }

    if (hProv)
    {
        CryptReleaseContext(hProv, NULL);
    }

    return error;
}

DWORD Test::Stb45_BS18Test()
{
    HCRYPTPROV hProv = NULL;
    HCRYPTHASH hHash = NULL;
    HCRYPTKEY hSecKey = NULL;
    DWORD error = NTE_FAIL;
    DATA_BLOB pass = { 0 };
    DATA_BLOB salt = {
        static_cast<DWORD>(stb45_bs18_salt_val.size()),
        const_cast<BYTE*>(stb45_bs18_salt_val.data())
    };
    DWORD it_count = 10000;
    std::vector<BYTE> blob;
    DWORD blob_len = 0;
    BYTE* drv_key = nullptr;
    BLOBHEADER* blob_header = nullptr;
    TZI_SYMM_KEY_BLOB_HEADER* tzi_header = nullptr;
    std::vector<BYTE> priv_blob;
    bool is_test_passed = false;

    if (!CryptAcquireContext(&hProv, nullptr, nullptr, Constants::BIGN_CSP_TYPE, dev_type_))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptCreateHash(hProv, CALG_BELT_HASH_256, 0, 0, &hHash))
    {
        error = GetLastError();
        goto Exit;
    }

    pass.pbData = const_cast<BYTE*>(stb45_bs18_pass_val.data());
    pass.cbData = static_cast<DWORD>(stb45_bs18_pass_val.size());

    if (!CryptSetHashParam(hHash, HP_PASS_VAL, reinterpret_cast<BYTE*>(&pass), 0))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptSetHashParam(hHash, HP_SALT_EX, reinterpret_cast<BYTE*>(&salt), 0))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptSetHashParam(hHash, HP_ITERATION_COUNT, reinterpret_cast<BYTE*>(&it_count), 0))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptDeriveKey(hProv, CALG_BELT_KEYWRAP, hHash, CRYPT_EXPORTABLE, &hSecKey))
    {
        error = GetLastError();
        goto Exit;
    }

    if (!CryptExportKey(hSecKey, 0, SIMPLEBLOB, 0, nullptr, &blob_len))
    {
        error = GetLastError();
        goto Exit;
    }

    blob.resize(blob_len);
    if (!CryptExportKey(hSecKey, 0, SIMPLEBLOB, 0, blob.data(), &blob_len))
    {
        error = GetLastError();
        goto Exit;
    }

    blob_header = reinterpret_cast<BLOBHEADER*>(blob.data());
    tzi_header = reinterpret_cast<TZI_SYMM_KEY_BLOB_HEADER*>(blob_header + 1);
    drv_key = blob.data() + sizeof(BLOBHEADER)+sizeof(TZI_SYMM_KEY_BLOB_HEADER);
    is_test_passed = CheckResult(drv_key, tzi_header->length, stb45_bs18_exp_drv_key.data(),
        static_cast<DWORD>(stb45_bs18_exp_drv_key.size()));
    error = is_test_passed ? ERROR_SUCCESS : NTE_FAIL;

Exit:

    if (hSecKey)
    {
        CryptDestroyKey(hSecKey);
    }

    if (hProv)
    {
        CryptReleaseContext(hProv, NULL);
    }

    return error;
}

void Test::CreatePrivBlob(std::vector<BYTE>* blob_val, DWORD* blob_val_len)
{
    BLOBHEADER blob_header = { 0 };
    TZI_ASYMM_KEY_BLOB_HEADER tzi_header;
    BYTE* pCurr = nullptr;

    *blob_val_len = sizeof(BLOBHEADER)+sizeof(TZI_ASYMM_KEY_BLOB_HEADER)+static_cast<DWORD>(priv_key_val.size()) +
        static_cast<DWORD>(pub_key_val.size());
    blob_val->resize(*blob_val_len);
    blob_header.aiKeyAlg = CALG_BIGN;
    blob_header.bType = PRIVATEKEYBLOB;
    blob_header.bVersion = CUR_BLOB_VERSION;
    blob_header.reserved = 0;
    tzi_header.magic = Constants::BIGN_PRIVATE_CURVE256_MAGIC;
    tzi_header.pub_key_len = static_cast<DWORD>(pub_key_val.size());
    tzi_header.priv_key_len = static_cast<DWORD>(priv_key_val.size());
    pCurr = blob_val->data();
    CopyMemory(pCurr, &blob_header, sizeof(blob_header));
    pCurr += sizeof(blob_header);
    CopyMemory(pCurr, &tzi_header, sizeof(tzi_header));
    pCurr += sizeof(tzi_header);
    CopyMemory(pCurr, pub_key_val.data(), pub_key_val.size());
    pCurr += pub_key_val.size();
    CopyMemory(pCurr, priv_key_val.data(), priv_key_val.size());
}

bool Test::CheckResult(const BYTE* result, const DWORD result_len,
                                 const BYTE* expect, const DWORD expect_len)
{
    if (result_len != expect_len)
    {
        return false;
    }

    for (DWORD i = 0; i < result_len; ++i)
    {

        if (result[i] != expect[i])
        {
            return false;
        }
    }

    return true;
}

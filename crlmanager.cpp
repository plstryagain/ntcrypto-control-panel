#include "icertificatemanager.h"
#include "asn1bel/Asn1Decoder.h"

CrlManager::CrlManager(PCCRL_CONTEXT pCrl, bool is_need_free_ctx)
    : pCrl_(pCrl), is_need_free_ctx_(is_need_free_ctx)
{

}

CrlManager::~CrlManager()
{
    if(is_need_free_ctx_)
    {
        CertFreeCRLContext(pCrl_);
    }
}

//void CrlManager::ShowUICrl()
//{
//    CryptUIDlgViewContext(CERT_STORE_CRL_CONTEXT, crl_ctx_, NULL, L"CRL", 0, nullptr);
//}

const void* CrlManager::GetRawContextPtr() const
{
    return pCrl_;
}

QString CrlManager::GetVersion() const
{
    switch(pCrl_->pCrlInfo->dwVersion){
    case 0:
        return "V1";
    case 1:
        return "V2";
    case 2:
        return "V3";
    default:
        return "Версия не поддерживается";
    }
}

QString CrlManager::GetSerialNumber() const
{
    return "";
}

QString CrlManager::GetSignatureAlgName() const
{
    PCCRYPT_OID_INFO pInfo =  CryptFindOIDInfo(CRYPT_OID_INFO_OID_KEY,
                                               const_cast<char*>(pCrl_->pCrlInfo->SignatureAlgorithm.pszObjId),
                                               0);
    if(!pInfo){
        return QString::fromLatin1(pCrl_->pCrlInfo->SignatureAlgorithm.pszObjId);
    }
    return QString::fromWCharArray(pInfo->pwszName);
}

QString CrlManager::GetHashAlgName() const
{
    PCCRYPT_OID_INFO pInfo =  CryptFindOIDInfo(CRYPT_OID_INFO_OID_KEY,
                                               const_cast<char*>(pCrl_->pCrlInfo->SignatureAlgorithm.pszObjId),
                                               0);
    if(!pInfo){
        return QString::fromLatin1(pCrl_->pCrlInfo->SignatureAlgorithm.pszObjId);
    }
    PCCRYPT_OID_INFO pHashInfo = CryptFindOIDInfo(CRYPT_OID_INFO_ALGID_KEY,
                                                  const_cast<ALG_ID*>(&pInfo->Algid),
                                                  0);
    if(!pHashInfo){
        return "";
    }
    return QString::fromWCharArray(pHashInfo->pwszName);
}

QString CrlManager::GetCertIssuerName() const
{
    DWORD size = CertNameToStr(X509_ASN_ENCODING,
                               &pCrl_->pCrlInfo->Issuer,
                               CERT_X500_NAME_STR | CERT_NAME_STR_CRLF_FLAG,
                               nullptr,
                               0);
    QVector<wchar_t> name(size);
    CertNameToStr(X509_ASN_ENCODING,
                  &pCrl_->pCrlInfo->Issuer,
                  CERT_X500_NAME_STR | CERT_NAME_STR_CRLF_FLAG,
                  name.data(),
                  name.size());
    return QString::fromWCharArray(name.data());
}

QString CrlManager::GetCertIssuerSimpleName() const
{
    DWORD size = CertNameToStr(X509_ASN_ENCODING,
                               &pCrl_->pCrlInfo->Issuer,
                               CERT_X500_NAME_STR | CERT_NAME_STR_CRLF_FLAG,
                               nullptr,
                               0);
    QVector<wchar_t> name(size);
    CertNameToStr(X509_ASN_ENCODING,
                  &pCrl_->pCrlInfo->Issuer,
                  CERT_X500_NAME_STR | CERT_NAME_STR_CRLF_FLAG,
                  name.data(),
                  name.size());
    QString full_name = QString::fromWCharArray(name.data());
    int idx = full_name.indexOf("CN");
    if(idx == -1){
        return full_name;
    }
    int idx2 = full_name.indexOf("\n", idx);
    full_name = full_name.mid(idx + 3, idx2 - idx - 3);
    return full_name;
}

QString CrlManager::GetNotBefore() const
{
    FILETIME ft = pCrl_->pCrlInfo->ThisUpdate;
    SYSTEMTIME st;
    FileTimeToSystemTime(&ft, &st);
    QString time = QString("%1.%2.%3 %4:%5:%6").arg(st.wDay, 2, 10, QChar('0')).
                                                arg(st.wMonth, 2, 10, QChar('0')).
                                                arg(st.wYear, 2, 10, QChar('0')).
                                                arg(st.wHour, 2, 10, QChar('0')).
                                                arg(st.wMinute, 2, 10, QChar('0')).
                                                arg(st.wSecond, 2, 10, QChar('0'));
    return time;
}

QString CrlManager::GetNotAfter() const
{
    FILETIME ft = pCrl_->pCrlInfo->NextUpdate;
    SYSTEMTIME st;
    FileTimeToSystemTime(&ft, &st);
    QString time = QString("%1.%2.%3 %4:%5:%6").arg(st.wDay, 2, 10, QChar('0')).
                                                arg(st.wMonth, 2, 10, QChar('0')).
                                                arg(st.wYear, 2, 10, QChar('0')).
                                                arg(st.wHour, 2, 10, QChar('0')).
                                                arg(st.wMinute, 2, 10, QChar('0')).
                                                arg(st.wSecond, 2, 10, QChar('0'));
    return time;
}

std::vector<std::vector<unsigned char>> CrlManager::GetExtensionOidList() const
{
    std::vector<std::vector<unsigned char>> oid_list;
    for(DWORD i = 0; i < pCrl_->pCrlInfo->cExtension; ++i){
        CERT_EXTENSION ext = pCrl_->pCrlInfo->rgExtension[i];
        auto len = strlen(ext.pszObjId) + 1;
        std::vector<unsigned char> boid;
        boid.insert(boid.begin(), reinterpret_cast<const unsigned char*>(ext.pszObjId),
                    reinterpret_cast<const unsigned char*>(ext.pszObjId) + len);
        oid_list.push_back(std::move(boid));
    }
    return oid_list;
}

QString CrlManager::GetExtansionNameString(const std::vector<unsigned char>& oid) const
{
    PCCRYPT_OID_INFO pOid = CryptFindOIDInfo(CRYPT_OID_INFO_OID_KEY, const_cast<unsigned char*>(oid.data()), 0);
    if(!pOid){
        return QString::fromLatin1(reinterpret_cast<const char*>(oid.data()));
    }
    return QString::fromWCharArray(pOid->pwszName);
}

QString CrlManager::GetExtensionValueString(const std::vector<unsigned char>& oid) const
{
    QString val = "";
    LPCSTR soid = reinterpret_cast<const char*>(oid.data());
    CRYPT_DECODE_PARA cdp = { 0 };
    void* dec_struct = nullptr;
    DWORD dec_struct_len = 0;
    BYTE* ptr = nullptr;
    DWORD len = 0;
    for(DWORD i = 0; i < pCrl_->pCrlInfo->cExtension; ++i){
        CERT_EXTENSION ext = pCrl_->pCrlInfo->rgExtension[i];
        if(strcmp(ext.pszObjId, soid) == 0){
            ptr = ext.Value.pbData;
            len = ext.Value.cbData;
        }
    }
    if(!CryptDecodeObjectEx(X509_ASN_ENCODING, soid, ptr, len, CRYPT_DECODE_ALLOC_FLAG, &cdp, &dec_struct,
                            &dec_struct_len)){
        if(GetLastError() != ERROR_FILE_NOT_FOUND){
            goto Exit;
        }
    }
    if(dec_struct){
        if(strcmp(soid, szOID_AUTHORITY_KEY_IDENTIFIER2) == 0){
            CERT_AUTHORITY_KEY_ID2_INFO* pInfo = static_cast<CERT_AUTHORITY_KEY_ID2_INFO*>(dec_struct);
            if(pInfo->KeyId.pbData){
                val = QString(QByteArray::fromRawData(reinterpret_cast<char*>(pInfo->KeyId.pbData),
                                                      pInfo->KeyId.cbData).toHex());
            }
        } else if (strcmp(soid, szOID_CRL_NUMBER) == 0){
            int num = *static_cast<int*>(dec_struct);
            val = QString::number(num);
        } else if(strcmp(soid, szOID_BASIC_CONSTRAINTS2) == 0){
            CERT_BASIC_CONSTRAINTS2_INFO* pInfo = static_cast<CERT_BASIC_CONSTRAINTS2_INFO*>(dec_struct);
            if(pInfo->fCA){
                val.append("Тип субъекта: Центр сертификации \n");
            } else {
                val.append("Тип субъекта: Конечный субъект \n");
            }
            if(!pInfo->fPathLenConstraint){
                val.append("Ограничение на длину пути: Отсутствует \n");
            } else {
                val.append("Ограничение на длину пути: "+QString::number(pInfo->dwPathLenConstraint)+"\n");
            }
        } else if(strcmp(soid, szOID_KEY_USAGE) == 0){
            CERT_KEY_ATTRIBUTES_INFO* pInfo = static_cast<CERT_KEY_ATTRIBUTES_INFO*>(dec_struct);
            /* YES, BYTE* to BYTE! */
            BYTE b = reinterpret_cast<BYTE>(pInfo->IntendedKeyUsage.pbData);
            if((b >> 7) & 1U){
                val.append("Цифровая подпись \n");
            }
            if((b >> 6) & 1U){
                val.append("Невозможность отказа \n");
            }
            if((b >> 5) & 1U){
                val.append("Шифрование ключей \n");
            }
            if((b >> 4) & 1U){
                val.append("Шифрование данных \n");
            }
            if((b >> 3) & 1U){
                val.append("Согласование ключей \n");
            }
            if((b >> 2) & 1U){
                val.append("Подписание сертификатов \n");
            }
            if((b >> 1) & 1U){
                val.append("Подписание списка отозванных сертификатов \n");
            }
            if((b >> 0) & 1U){
                val.append("Только шифрование\n");
            }
        }
    } else {
        std::unique_ptr<Asn1Decoder> dec = Asn1Decoder::Create(ptr, len, ASN1_ENCODING::DER);
        dec->ParseAsnTree();
        if(dec->IsTagAsExpected(0,ASN1_UNIVERSAL_TAG_NUMBER::BMPString)){
            std::vector<byte> bs;
            dec->GetBmpString(0, &bs);
            std::wstring unp = dec->DecodeBmpString(bs);
            val.append(QString::fromStdWString(unp));
        }
    }

Exit:
    if(dec_struct){
        LocalFree(dec_struct);
    }
    return val;
}

PCCERT_CHAIN_CONTEXT CrlManager::GetCertificateChain()
{
    return nullptr;
}

QString CrlManager::CertTrustStatusToString(const CERT_TRUST_STATUS& cts) const
{
    Q_UNUSED(cts);
    return "";
}

bool CrlManager::IsExpired() const
{
    FILETIME ft1 = pCrl_->pCrlInfo->NextUpdate;
    SYSTEMTIME st = {0};
    GetLocalTime(&st);
    FILETIME ft2 = {0};
    if(!SystemTimeToFileTime(&st, &ft2))
    {
        return true;
    }
    return (CompareFileTime(&ft1, &ft2) == -1) ? true : false;
}

DWORD CrlManager::GetCrlEntryCount() const
{
    return pCrl_->pCrlInfo->cCRLEntry;
}

QString CrlManager::GetCrlEntrySerialNumber(const DWORD idx) const
{
    if(idx >= pCrl_->pCrlInfo->cCRLEntry){
        return "";
    }
    return QString(QByteArray::fromRawData(reinterpret_cast<const char*>(pCrl_->pCrlInfo->rgCRLEntry[idx].SerialNumber.pbData),
                                                                         pCrl_->pCrlInfo->rgCRLEntry[idx].SerialNumber.cbData).toHex());
}

QString CrlManager::GetCrlEntryRevokeDate(const DWORD idx) const
{
    if(idx >= pCrl_->pCrlInfo->cCRLEntry){
        return "";
    }
    FILETIME ft = pCrl_->pCrlInfo->rgCRLEntry[idx].RevocationDate;
    SYSTEMTIME st;
    FileTimeToSystemTime(&ft, &st);
    QString time = QString("%1.%2.%3 %4:%5:%6").arg(st.wDay, 2, 10, QChar('0')).
                                                arg(st.wMonth, 2, 10, QChar('0')).
                                                arg(st.wYear, 2, 10, QChar('0')).
                                                arg(st.wHour, 2, 10, QChar('0')).
                                                arg(st.wMinute, 2, 10, QChar('0')).
                                                arg(st.wSecond, 2, 10, QChar('0'));
    return time;
}

QString CrlManager::GetCrlEntryRevokeReason(const DWORD idx) const
{
    if(idx >= pCrl_->pCrlInfo->cCRLEntry){
        return "";
    }

    BYTE* ptr = nullptr;
    DWORD len = 0;
    for(DWORD i = 0; i < pCrl_->pCrlInfo->rgCRLEntry[idx].cExtension; ++i){
        if(strcmp(pCrl_->pCrlInfo->rgCRLEntry[idx].rgExtension[i].pszObjId, szOID_CRL_REASON_CODE) == 0){
            ptr = pCrl_->pCrlInfo->rgCRLEntry[idx].rgExtension[i].Value.pbData;
            len = pCrl_->pCrlInfo->rgCRLEntry[idx].rgExtension[i].Value.cbData;
            break;
        }
    }

    if(!ptr){
        return "";
    }

    CRYPT_DECODE_PARA cdp = { 0 };
    void* rcode = nullptr;
    DWORD rcode_len = 0;
    QString reason = "";
    if(!CryptDecodeObjectEx(X509_ASN_ENCODING, szOID_CRL_REASON_CODE, ptr, len, CRYPT_DECODE_ALLOC_FLAG, &cdp,
                            &rcode, &rcode_len)){
        goto Exit;
    }
    int code = *static_cast<int*>(rcode);
    switch(code){
    case 0:
        reason = "Необозначена ("+QString::number(code)+")";
        break;
    case 1:
        reason = "Компрометация ключа ("+QString::number(code)+")";
        break;
    case 2:
        reason = "Компрометация ЦС ("+QString::number(code)+")";
        break;
    case 3:
        reason = "Изменение атрибутов (владельца или издателя) сертификата ("+QString::number(code)+")";
        break;
    case 4:
        reason = "Сертификат заменён ("+QString::number(code)+")";
        break;
    case 5:
        reason = "Прекращение действия ("+QString::number(code)+")";
        break;
    case 6:
        reason = "Действие сертификата приостановлено ("+QString::number(code)+")";
        break;
    case 9:
        reason = "Привилегии отозваны ("+QString::number(code)+")";
        break;
    case 17:
        reason = "Невозможность использования ключа ("+QString::number(code)+")";
        break;
    case 18:
        reason = "Неверное заполнение атрибутов ("+QString::number(code)+")";
        break;
    default:
        break;
    }

Exit:
    if(rcode){
        LocalFree(rcode);
    }
    return reason;
}


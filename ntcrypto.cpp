#include "ntcrypto.h"
#include <QVector>
#include "Constants.h"

#pragma comment(lib, "crypt32")
#pragma comment(lib, "advapi32")



NTCryptoManager::NTCryptoManager(const QString& container, const DWORD prov_type, const DWORD flags)
    : hProv_(0), hKey_(0), hHash_(0), container_(container), prov_type_(prov_type), flags_(flags)
{

}

NTCryptoManager::~NTCryptoManager()
{
    if(hKey_)
    {
        CryptDestroyKey(hKey_);
    }

    if(hHash_)
    {
        CryptDestroyHash(hHash_);
    }

    if(hProv_)
    {
        CryptReleaseContext(hProv_, 0);
    }
}

int NTCryptoManager::Init()
{
    if(hProv_)
    {
        return 0;
    }

    QVector<wchar_t> wcontainer((container_.size() + 1)*sizeof(wchar_t));
    container_.toWCharArray(wcontainer.data());
    //return CryptAcquireContext(&hProv_, wcontainer.data(), nullptr, prov_type_, flags_);
    std::wstring wstr = container_.toStdWString().data();
    return CryptAcquireContext(&hProv_, wstr.data(), nullptr, prov_type_, flags_);
}

int NTCryptoManager::GetProvParam(const DWORD param, BYTE* data, DWORD* data_len, const DWORD flag)
{
    if(!hProv_)
    {
        return 0;
    }

    return CryptGetProvParam(hProv_, param, data, data_len, flag);
}

int NTCryptoManager::GetUserKey(const DWORD spec)
{
    if(!hProv_)
    {
        return 0;
    }

    return CryptGetUserKey(hProv_, spec, &hKey_);
}

int NTCryptoManager::SetKeyParam(const DWORD param, const BYTE* data, const DWORD data_len)
{
    if(!hKey_)
    {
        return 0;
    }

    return CryptSetKeyParam(hKey_, param, data, data_len);
}

int NTCryptoManager::GetKeyParam(const DWORD param, BYTE *data, DWORD *data_len, const DWORD flags)
{
    if(!hKey_)
    {
        return 0;
    }

    return CryptGetKeyParam(hKey_, param, data, data_len, flags);
}

int NTCryptoManager::ExportKey(const DWORD blob_type, const DWORD flags, BYTE* key_value, DWORD* key_value_len)
{
    if(!hKey_)
    {
        return 0;
    }

    return CryptExportKey(hKey_, 0, blob_type, flags, key_value, key_value_len);
}

bool NTCryptoManager::CalculateHash(const ALG_ID alg_id,
                                    const BYTE* data,
                                    const DWORD data_len,
                                    BYTE* hash_val,
                                    DWORD* hash_val_len)
{
    if(!CryptCreateHash(hProv_, alg_id, 0, 0, &hHash_))
    {
        return false;
    }

    if(!hash_val)
    {
        DWORD temp = sizeof(*hash_val_len);
        if(!CryptGetHashParam(hHash_, HP_HASHSIZE, reinterpret_cast<BYTE*>(hash_val_len), &temp, 0))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    if(!CryptHashData(hHash_, data, data_len, 0))
    {
        return false;
    }

    if(!CryptGetHashParam(hHash_, HP_HASHVAL, hash_val, hash_val_len, 0))
    {
        return false;
    }

    return true;
}

int NTCryptoManager::GenerateKeyPair(const ALG_ID alg_id, const DWORD flags)
{
    if(!hProv_)
    {
        return 0;
    }

    return CryptGenKey(hProv_, alg_id, flags, &hKey_);
}

int NTCryptoManager::ExportPublicKeyInfo(const DWORD key_spec, CERT_PUBLIC_KEY_INFO* pub_key_info,
                                           DWORD* pub_key_info_len)
{
    return CryptExportPublicKeyInfo(hProv_, key_spec, PKCS_7_ASN_ENCODING | X509_ASN_ENCODING,
                                    pub_key_info, pub_key_info_len);
}

int NTCryptoManager::SignAndEncodeCertificate(const DWORD key_spec, const char* struct_type,
    const void* struct_info, PCRYPT_ALGORITHM_IDENTIFIER sign_alg, BYTE* enc_cert,
    DWORD* enc_cert_len)
{
    return CryptSignAndEncodeCertificate(hProv_,
                                         key_spec,
                                         PKCS_7_ASN_ENCODING | X509_ASN_ENCODING,
                                         struct_type,
                                         struct_info,
                                         sign_alg,
                                         nullptr,
                                         enc_cert,
                                         enc_cert_len);
}

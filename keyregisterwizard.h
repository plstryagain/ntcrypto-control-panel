#ifndef KEYREGISTERWIZARD_H
#define KEYREGISTERWIZARD_H

#include <QObject>
#include <QDialog>
#include <QFormLayout>
#include <QComboBox>
#include <QPushButton>
#include <QDialogButtonBox>

class KeyRegisterWizard : public QDialog
{
    Q_OBJECT

signals:
    void RegUnregKeyFinish();

public:
    KeyRegisterWizard(const bool is_machine_keyset);
    void Init();
    void UnregisterKey(const QString& container_name);

private slots:
    void EnumKeys();
    void btn_ok_clicked();
    void btn_cancel_clicked();
    bool IsRegistered(const QString &container_name);
    unsigned long RegisterKey(const QString& container_name);

private:
    QFormLayout layout_;
    QComboBox cb_containers_;
    QDialogButtonBox btn_box_;
    QComboBox cb_devices_;
    unsigned int slot_id_;
    unsigned long device_type_;
    bool is_machine_keyset_;
};

#endif // KEYREGISTERWIZARD_H

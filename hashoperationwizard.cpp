#include "hashoperationwizard.h"
#include "ui_hashoperationwizard.h"
#include "ntcrypto.h"
#include "Constants.h"
#include "dialogpleasewait.h"
#include <QDebug>
#include <QThread>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QToolTip>
#include "cryptooperationsmanager.h"
#include <memory>

HashOperationWizard::HashOperationWizard(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HashOperationWizard)
{
    ui->setupUi(this);
    ui->rb_calc_hash->setChecked(true);
    ui->ln_select_file_two->setVisible(false);
    ui->lb_select_file_two->setVisible(false);
    ui->btn_select_file_two->setVisible(false);
    ui->btn_start_operation->setText(tr("Вычислить"));

    ui->cb_alg->addItem("СТБ 34.101.31 (belt-hash256)", QVariant(CALG_BELT_HASH_256));
    ui->cb_alg->addItem("СТБ 34.101.77 (bash256)", QVariant(CALG_BASH_256));
    ui->cb_alg->addItem("СТБ 34.101.77 (bash384)", QVariant(CALG_BASH_384));
    ui->cb_alg->addItem("СТБ 34.101.77 (bash512)", QVariant(CALG_BASH_512));

    connect(ui->ln_select_file_one, SIGNAL(textChanged(QString)), this, SLOT(ChangeLineBorderColor(QString)));
    connect(ui->ln_select_file_two, SIGNAL(textChanged(QString)), this, SLOT(ChangeLineBorderColor(QString)));
}

HashOperationWizard::~HashOperationWizard()
{
    delete ui;
}

void HashOperationWizard::on_rb_calc_hash_toggled(bool checked)
{
    Q_UNUSED(checked)
    ui->ln_select_file_two->setVisible(false);
    ui->lb_select_file_two->setVisible(false);
    ui->btn_select_file_two->setVisible(false);
    ui->check_save_to_file->setEnabled(true);
    ui->btn_start_operation->setText(tr("Вычислить"));
    ui->cb_fil_or_dir->setEnabled(true);
}

void HashOperationWizard::on_rb_compare_files_toggled(bool checked)
{
    Q_UNUSED(checked)
    ui->ln_select_file_two->setVisible(true);
    ui->lb_select_file_two->setVisible(true);
    ui->btn_select_file_two->setVisible(true);
    ui->check_save_to_file->setEnabled(false);
    ui->btn_start_operation->setText(tr("Сравнить"));
    ui->cb_fil_or_dir->setEnabled(false);
}

void HashOperationWizard::on_cb_alg_currentIndexChanged(int index)
{
    ui->cb_impl->clear();
    switch(static_cast<HASH_ALG>(index)){
    case HASH_ALG::belt_hash256:
        ui->cb_impl->addItem("Программная", QVariant(static_cast<const uint>(Constants::SOFT_IMPL_FLAG)));
        ui->cb_impl->addItem("СКЗИ \"Сигма\"", QVariant(static_cast<const uint>(Constants::SIGMA_IMPL_FLAG)));
        ui->cb_impl->addItem("СКЗИ NTStore", QVariant(static_cast<const uint>(Constants::NTSTORE_IMPL_FLAG)));
        break;
    case HASH_ALG::bash256:
    case HASH_ALG::bash384:
    case HASH_ALG::bash512:
        ui->cb_impl->addItem("Программная", QVariant(static_cast<const uint>(Constants::SOFT_IMPL_FLAG)));
        ui->cb_impl->addItem("СКЗИ NTStore",QVariant(static_cast<const uint>(Constants::NTSTORE_IMPL_FLAG)));
        break;
    default:
        return;
    }
}

void HashOperationWizard::on_cb_fil_or_dir_currentIndexChanged(int index)
{
    if(index == 0){
        ui->lb_select_file_one->setText("Выберите файл");
        ui->rb_compare_files->setEnabled(true);
        ui->ln_select_file_one->clear();
    } else {
        ui->lb_select_file_one->setText("Выберите папку");
        ui->rb_compare_files->setEnabled(false);
        ui->ln_select_file_one->clear();
    }
}

void HashOperationWizard::on_btn_select_file_two_clicked()
{
    QString new_path = QFileDialog::getOpenFileName(this,
                                                tr("Выберите файл для сравнения"),
                                                "",
                                                tr("(*.*)"));
    if(!new_path.isEmpty())
    {
        ui->ln_select_file_two->setText(new_path);
    }
}

void HashOperationWizard::on_btn_select_file_one_clicked()
{
    QString new_path;
    if(ui->cb_fil_or_dir->currentIndex() == 0){
        new_path = QFileDialog::getOpenFileName(this,
                                                tr("Выберите файл для хэширования"),
                                                "",
                                                tr("(*.*)"));
    } else {
        new_path = QFileDialog::getExistingDirectory(this,
                                                     tr("Выберите папку"),
                                                     "",
                                                     QFileDialog::ShowDirsOnly
                                                     | QFileDialog::DontResolveSymlinks);
    }
    if(!new_path.isEmpty())
    {
        ui->ln_select_file_one->setText(new_path);
    }
}


void HashOperationWizard::on_btn_start_operation_clicked()
{
    is_op_success_ = true;
    ui->tb_result->clear();
    if(ui->rb_calc_hash->isChecked()){
        return CalcHashOperation();
    } else {
        return CompareOperation();
    }
}

void HashOperationWizard::CalcHashOperation()
{
    if(ui->ln_select_file_one->text().isEmpty()){
        if(ui->cb_fil_or_dir->currentIndex() == 0){
            ShowEmptyLineNotification(ui->ln_select_file_one, "Выберите файл для хэширования");
            return;
        } else {
            ShowEmptyLineNotification(ui->ln_select_file_one, "Выберите папку");
            return;
        }
    }
    int amount_of_files = 1;
    if(ui->cb_fil_or_dir->currentIndex() == 1){
        QDir dir(ui->ln_select_file_one->text());
        dir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot );
        amount_of_files = dir.count();
    }
    DialogPleaseWait dpw(this);
    dpw.SetAmountOfFiles(amount_of_files);
    ALG_ID alg_id = ui->cb_alg->currentData().value<ALG_ID>();
    DWORD impl = ui->cb_impl->currentData().value<uint>();
    QThread* thread = new QThread();
    std::unique_ptr<CryptoOperationsManager> comgr =
            std::unique_ptr<CryptoOperationsManager>(new CryptoOperationsManager());
    bool is_dir = ui->cb_fil_or_dir->currentIndex() == 1;
    comgr->InitCalculateHashOperation(alg_id, impl, ui->ln_select_file_one->text(), is_dir);
    comgr->moveToThread(thread);
    connect(thread, SIGNAL(started()), comgr.get(), SLOT(CalculateHash()));
    connect(comgr.get(), SIGNAL(signalCalculated(QString, QByteArray)), this, SLOT(Calculated(QString, QByteArray)));
    connect(comgr.get(), SIGNAL(signalReportError(QString)), this, SLOT(Error(QString)));
    connect(comgr.get(), SIGNAL(signalFinish()), thread, SLOT(quit()));
    connect(comgr.get(), SIGNAL(signalFileProcessed()), &dpw, SLOT(ProgressUpdate()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), &dpw, SLOT(accept()));
    if(ui->check_save_to_file->isChecked()){
        connect(thread, SIGNAL(finished()), this, SLOT(SaveToFile()));
    }
    thread->start();
    dpw.exec();
    if(is_op_success_){
        QMessageBox::information(this, "Информация", "Операция выполнена успешно", QMessageBox::Ok);
    } else {
        QMessageBox::critical(this, "Ошибка", "Операция не выполнена", QMessageBox::Ok);
    }
}

void HashOperationWizard::CompareOperation()
{
    is_op_success_ = true;
    if(ui->ln_select_file_one->text().isEmpty()){
        ShowEmptyLineNotification(ui->ln_select_file_one, "Выберите файл");
        return;
    }
    if(ui->rb_compare_files->isChecked() && ui->ln_select_file_two->text().isEmpty()){
        ShowEmptyLineNotification(ui->ln_select_file_two, "Выберите файл для сравнения");
        return;
    }
    DialogPleaseWait dpw(this);
    ALG_ID alg_id = ui->cb_alg->currentData().value<ALG_ID>();
    DWORD impl = ui->cb_impl->currentData().value<uint>();
    QThread* thread = new QThread();
    std::unique_ptr<CryptoOperationsManager> comgr =
            std::unique_ptr<CryptoOperationsManager>(new CryptoOperationsManager());
    comgr->InitCompareOperation(alg_id, impl, ui->ln_select_file_one->text(), ui->ln_select_file_two->text());
    comgr->moveToThread(thread);
    connect(thread, SIGNAL(started()), comgr.get(), SLOT(CompareFiles()));
    connect(comgr.get(), SIGNAL(signalReportError(QString)), this, SLOT(Error(QString)));
    connect(comgr.get(), SIGNAL(signalFinish()), thread, SLOT(quit()));
    connect(comgr.get(), SIGNAL(signalCompared(QString)), this, SLOT(Compared(QString)));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), &dpw, SLOT(accept()));
    thread->start();
    dpw.exec();
    if(is_op_success_){
        QMessageBox::information(this, "Информация", "Операция выполнена успешно", QMessageBox::Ok);
    } else {
        QMessageBox::critical(this, "Ошибка", "Операция не выполнена", QMessageBox::Ok);
    }
}

void HashOperationWizard::Calculated(QString file_name, QByteArray hash_val)
{
    ui->tb_result->append("Файл: "+file_name);
    ui->tb_result->append("Хэш-значение: "+QString(hash_val.toHex()));
    ui->tb_result->append("----------------------------------------------------------------"
                          "------------------------------------------------------------\n");
}

void HashOperationWizard::Compared(QString result)
{
    ui->tb_result->append(result);
}

void HashOperationWizard::Error(QString error)
{
    is_op_success_ = false;
    ui->tb_result->append(error);
}

void HashOperationWizard::SaveToFile()
{
    if(is_op_success_){
        QString path = QFileDialog::getSaveFileName(this,
                                                    tr("Сохранить файл"),
                                                    "",
                                                    tr("(*.*)"));
        QFile file(path);
        if(!file.open(QFile::WriteOnly)){
            QMessageBox::critical(this, "Ошибка", "Не удалось сохранить файл", QMessageBox::Ok);
            is_op_success_ = false;
            return;
        }
        file.write(ui->tb_result->toPlainText().toUtf8());
        file.close();
    }
}

void HashOperationWizard::ShowEmptyLineNotification(QLineEdit *line, const QString& text)
{
    line->setFocus();
    line->setStyleSheet("QLineEdit {"//border-style: solid;"
                               //"border-width: 1px;"
                               "border-color: red;}");
    QToolTip::showText(line->mapToGlobal(QPoint()), text);
    return;
}

void HashOperationWizard::ChangeLineBorderColor(QString)
{

    static_cast<QLineEdit*>(QObject::sender())->setStyleSheet(
                            "QLineEdit {border-style: solid;"
                               "border-width: 1px;"
                               "border-color: gray;}");
}

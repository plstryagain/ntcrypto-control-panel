#ifndef CRLUI_H
#define CRLUI_H

#include <QWidget>
#include <memory>

class CrlManager;

namespace Ui {
class CrlUI;
}

class CrlUI : public QWidget
{
    Q_OBJECT

public:
    CrlUI(QWidget *parent, const CrlManager* crlmgr);
    ~CrlUI();

protected:
    void showEvent(QShowEvent *event) override;

private slots:
    void on_tabw_about_crl_currentChanged(int index);

    void on_tw_crl_list_itemSelectionChanged();

    void on_btn_ok_clicked();

    void on_tw_crl_element_itemSelectionChanged();

    void on_tw_composition_itemSelectionChanged();

private:
    void FillCrlTable();

private:
    Ui::CrlUI *ui;
    const CrlManager* crlmgr_;
    bool is_crl_table_already_fill_ = false;
};

#endif // CRLUI_H

#ifndef NTSERVICEMANAGER
#define NTSERVICEMANAGER

#include <Windows.h>
#include <QString>

class NTServiceManager
{
public:
    NTServiceManager();
    ~NTServiceManager();

public:
    DWORD OpenWinSCManager(const DWORD desired_access);
    DWORD OpenNTService(const QString& srvc_name, const DWORD desired_access);
    DWORD GetServiceState();
    DWORD ControlNTService(const DWORD ctrl);
    DWORD StartNTService();

private:
    SC_HANDLE hSCManager_;
    SC_HANDLE hService_;
};

#endif // NTSERVICEMANAGER


#include "icertificatemanager.h"
#include <QVector>
#include <algorithm>
#include "asn1bel/Asn1Decoder.h"
#include "asn1bel/OidConstants.h"
#include <QDebug>

SystemCertificateManager::SystemCertificateManager(PCCERT_CONTEXT ctx, const bool is_need_free_ctx)
    : pCert_(ctx), is_need_free_ctx_(is_need_free_ctx)
{

}

SystemCertificateManager::~SystemCertificateManager()
{
    if(is_need_free_ctx_){
        CertFreeCertificateContext(pCert_);
    }
}

const void* SystemCertificateManager::GetRawContextPtr() const
{
    return pCert_;
}

QString SystemCertificateManager::GetVersion() const
{
    switch(pCert_->pCertInfo->dwVersion){
    case 0:
        return "V1";
    case 1:
        return "V2";
    case 2:
        return "V3";
    default:
        return "Версия не поддерживается";
    }
}

QString SystemCertificateManager::GetSerialNumber() const
{
    QByteArray ba = QByteArray::fromRawData(reinterpret_cast<char*>(pCert_->pCertInfo->SerialNumber.pbData),
                                            pCert_->pCertInfo->SerialNumber.cbData);
    std::reverse(ba.begin(), ba.end());
    return QString(ba.toHex());
}

QString SystemCertificateManager::GetSignatureAlgName() const
{
    PCCRYPT_OID_INFO pInfo =  CryptFindOIDInfo(CRYPT_OID_INFO_OID_KEY,
                                               const_cast<char*>(pCert_->pCertInfo->SignatureAlgorithm.pszObjId),
                                               0);
    if(!pInfo){
        return QString::fromLatin1(pCert_->pCertInfo->SignatureAlgorithm.pszObjId);
    }
    return QString::fromWCharArray(pInfo->pwszName);
}

QString SystemCertificateManager::GetHashAlgName() const
{
    PCCRYPT_OID_INFO pInfo =  CryptFindOIDInfo(CRYPT_OID_INFO_OID_KEY,
                                               const_cast<char*>(pCert_->pCertInfo->SignatureAlgorithm.pszObjId),
                                               0);
    if(!pInfo){
        return QString::fromLatin1(pCert_->pCertInfo->SignatureAlgorithm.pszObjId);
    }
    PCCRYPT_OID_INFO pHashInfo = CryptFindOIDInfo(CRYPT_OID_INFO_ALGID_KEY,
                                                  const_cast<ALG_ID*>(&pInfo->Algid),
                                                  0);
    if(!pHashInfo){
        return "";
    }
    return QString::fromWCharArray(pHashInfo->pwszName);
}

QString SystemCertificateManager::GetCertSubjectName() const
{
    DWORD size = CertNameToStr(X509_ASN_ENCODING,
                               &pCert_->pCertInfo->Subject,
                               CERT_X500_NAME_STR | CERT_NAME_STR_CRLF_FLAG,
                               nullptr,
                               0);
    QVector<wchar_t> name(size);
    CertNameToStr(X509_ASN_ENCODING,
                  &pCert_->pCertInfo->Subject,
                  CERT_X500_NAME_STR | CERT_NAME_STR_CRLF_FLAG,
                  name.data(),
                  name.size());
    return QString::fromWCharArray(name.data());
}

QString SystemCertificateManager::GetCertIssuerName() const
{
    DWORD size = CertNameToStr(X509_ASN_ENCODING,
                               &pCert_->pCertInfo->Issuer,
                               CERT_X500_NAME_STR | CERT_NAME_STR_CRLF_FLAG,
                               nullptr,
                               0);
    QVector<wchar_t> name(size);
    CertNameToStr(X509_ASN_ENCODING,
                  &pCert_->pCertInfo->Issuer,
                  CERT_X500_NAME_STR | CERT_NAME_STR_CRLF_FLAG,
                  name.data(),
                  name.size());
    return QString::fromWCharArray(name.data());
}

QString SystemCertificateManager::GetCertSubjectSimpleName() const
{
    QVector<wchar_t> name;
    DWORD name_size = CertGetNameString(pCert_, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, nullptr, 0);
    name.resize(name_size);
    CertGetNameString(pCert_, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, name.data(), name_size);
    return QString::fromWCharArray(name.data());
}

QString SystemCertificateManager::GetCertIssuerSimpleName() const
{
    QVector<wchar_t> name;
    DWORD name_size = CertGetNameString(pCert_, CERT_NAME_SIMPLE_DISPLAY_TYPE, CERT_NAME_ISSUER_FLAG,
                                        NULL, nullptr, 0);
    name.resize(name_size);
    CertGetNameString(pCert_, CERT_NAME_SIMPLE_DISPLAY_TYPE, CERT_NAME_ISSUER_FLAG, NULL,
                      name.data(), name_size);
    return QString::fromWCharArray(name.data());
}

QString SystemCertificateManager::GetNotBefore() const
{
    FILETIME ft = pCert_->pCertInfo->NotBefore;
    SYSTEMTIME st;
    FileTimeToSystemTime(&ft, &st);
    QString time = QString("%1.%2.%3 %4:%5:%6").arg(st.wDay, 2, 10, QChar('0')).
                                                arg(st.wMonth, 2, 10, QChar('0')).
                                                arg(st.wYear, 2, 10, QChar('0')).
                                                arg(st.wHour, 2, 10, QChar('0')).
                                                arg(st.wMinute, 2, 10, QChar('0')).
                                                arg(st.wSecond, 2, 10, QChar('0'));
    return time;
}

QString SystemCertificateManager::GetNotAfter() const
{
    FILETIME ft = pCert_->pCertInfo->NotAfter;
    SYSTEMTIME st;
    FileTimeToSystemTime(&ft, &st);
    QString time = QString("%1.%2.%3 %4:%5:%6").arg(st.wDay, 2, 10, QChar('0')).
                                                arg(st.wMonth, 2, 10, QChar('0')).
                                                arg(st.wYear, 2, 10, QChar('0')).
                                                arg(st.wHour, 2, 10, QChar('0')).
                                                arg(st.wMinute, 2, 10, QChar('0')).
                                                arg(st.wSecond, 2, 10, QChar('0'));
    return time;
}

std::vector<std::vector<unsigned char>> SystemCertificateManager::GetExtensionOidList() const
{
    std::vector<std::vector<unsigned char>> oid_list;
    for(DWORD i = 0; i < pCert_->pCertInfo->cExtension; ++i){
        CERT_EXTENSION ext = pCert_->pCertInfo->rgExtension[i];
        auto len = strlen(ext.pszObjId) + 1;
        std::vector<unsigned char> boid;
        boid.insert(boid.begin(), reinterpret_cast<const unsigned char*>(ext.pszObjId),
                    reinterpret_cast<const unsigned char*>(ext.pszObjId) + len);
        oid_list.push_back(std::move(boid));
    }
    return oid_list;
}

QString SystemCertificateManager::GetExtansionNameString(const std::vector<unsigned char>& oid) const
{
    PCCRYPT_OID_INFO pOid = CryptFindOIDInfo(CRYPT_OID_INFO_OID_KEY, const_cast<unsigned char*>(oid.data()), 0);
    if(!pOid){
        return QString::fromLatin1(reinterpret_cast<const char*>(oid.data()));
    }
    return QString::fromWCharArray(pOid->pwszName);
}

QString SystemCertificateManager::GetExtensionValueString(const std::vector<unsigned char>& oid) const
{
    QString val = "";
    LPCSTR soid = reinterpret_cast<const char*>(oid.data());
    CRYPT_DECODE_PARA cdp = { 0 };
    void* dec_struct = nullptr;
    DWORD dec_struct_len = 0;
    BYTE* ptr = nullptr;
    DWORD len = 0;
    for(DWORD i = 0; i < pCert_->pCertInfo->cExtension; ++i){
        CERT_EXTENSION ext = pCert_->pCertInfo->rgExtension[i];
        if(strcmp(ext.pszObjId, soid) == 0){
            ptr = ext.Value.pbData;
            len = ext.Value.cbData;
        }
    }
    if(!CryptDecodeObjectEx(X509_ASN_ENCODING, soid, ptr, len, CRYPT_DECODE_ALLOC_FLAG, &cdp, &dec_struct,
                            &dec_struct_len)){
        if(GetLastError() != ERROR_FILE_NOT_FOUND){
            goto Exit;
        }
    }
    if(dec_struct){
        if(strcmp(soid, szOID_BASIC_CONSTRAINTS2) == 0){
            CERT_BASIC_CONSTRAINTS2_INFO* pInfo = static_cast<CERT_BASIC_CONSTRAINTS2_INFO*>(dec_struct);
            if(pInfo->fCA){
                val.append("Тип субъекта: Центр сертификации \n");
            } else {
                val.append("Тип субъекта: Конечный субъект \n");
            }
            if(!pInfo->fPathLenConstraint){
                val.append("Ограничение на длину пути: Отсутствует \n");
            } else {
                val.append("Ограничение на длину пути: "+QString::number(pInfo->dwPathLenConstraint)+"\n");
            }
        } else if(strcmp(soid, szOID_AUTHORITY_KEY_IDENTIFIER2) == 0){
            CERT_AUTHORITY_KEY_ID2_INFO* pInfo = static_cast<CERT_AUTHORITY_KEY_ID2_INFO*>(dec_struct);
            if(pInfo->KeyId.pbData){
                val = QString(QByteArray::fromRawData(reinterpret_cast<char*>(pInfo->KeyId.pbData),
                                                      pInfo->KeyId.cbData).toHex());
            }
        } else if (strcmp(soid, szOID_SUBJECT_KEY_IDENTIFIER) == 0){
            CRYPT_DATA_BLOB* pInfo = static_cast<CRYPT_DATA_BLOB*>(dec_struct);
            val = QString(QByteArray::fromRawData(reinterpret_cast<char*>(pInfo->pbData), pInfo->cbData).toHex());
        } else if(strcmp(soid, szOID_KEY_USAGE) == 0){
            CERT_KEY_ATTRIBUTES_INFO* pInfo = static_cast<CERT_KEY_ATTRIBUTES_INFO*>(dec_struct);
            /* YES, BYTE* to BYTE! */
            BYTE b = reinterpret_cast<BYTE>(pInfo->IntendedKeyUsage.pbData);
            if((b >> 7) & 1U){
                val.append("Цифровая подпись \n");
            }
            if((b >> 6) & 1U){
                val.append("Невозможность отказа \n");
            }
            if((b >> 5) & 1U){
                val.append("Шифрование ключей \n");
            }
            if((b >> 4) & 1U){
                val.append("Шифрование данных \n");
            }
            if((b >> 3) & 1U){
                val.append("Согласование ключей \n");
            }
            if((b >> 2) & 1U){
                val.append("Подписание сертификатов \n");
            }
            if((b >> 1) & 1U){
                val.append("Подписание списка отозванных сертификатов \n");
            }
            if((b >> 0) & 1U){
                val.append("Только шифрование\n");
            }
        } else if(strcmp(soid, szOID_ENHANCED_KEY_USAGE) == 0){
            CERT_ENHKEY_USAGE* pInfo = static_cast<CERT_ENHKEY_USAGE*>(dec_struct);
            for(DWORD i = 0; i < pInfo->cUsageIdentifier; ++i){
                PCCRYPT_OID_INFO pOid = CryptFindOIDInfo(CRYPT_OID_INFO_OID_KEY,
                                                         pInfo->rgpszUsageIdentifier[i], 0);
                if(!pOid){
                    val.append(QString::fromLatin1(pInfo->rgpszUsageIdentifier[i]));
                } else {
                    val.append(QString::fromWCharArray(pOid->pwszName) + "\n");
                }
            }
        }
    } else {
        std::unique_ptr<Asn1Decoder> dec = Asn1Decoder::Create(ptr, len, ASN1_ENCODING::DER);
        dec->ParseAsnTree();
        if(dec->IsTagAsExpected(0,ASN1_UNIVERSAL_TAG_NUMBER::BMPString)){
            std::vector<byte> bs;
            dec->GetBmpString(0, &bs);
            std::wstring unp = dec->DecodeBmpString(bs);
            val.append(QString::fromStdWString(unp));
        }
    }


Exit:
    if(dec_struct){
        LocalFree(dec_struct);
    }
    return val;
}

PCCERT_CHAIN_CONTEXT SystemCertificateManager::GetCertificateChain()
{
    CERT_CHAIN_PARA chainPara;
    chainPara.cbSize = sizeof(CERT_CHAIN_PARA);
    chainPara.RequestedUsage.dwType = USAGE_MATCH_TYPE_AND;
    chainPara.RequestedUsage.Usage.cUsageIdentifier = 0;
    if(!CertGetCertificateChain(NULL, pCert_, NULL, NULL, &chainPara, CERT_CHAIN_CACHE_END_CERT |
                                CERT_CHAIN_CACHE_ONLY_URL_RETRIEVAL |
                                CERT_CHAIN_DISABLE_MY_PEER_TRUST |
                                CERT_CHAIN_ENABLE_PEER_TRUST |
                                CERT_CHAIN_REVOCATION_ACCUMULATIVE_TIMEOUT |
                                CERT_CHAIN_REVOCATION_CHECK_CHAIN_EXCLUDE_ROOT, NULL, &pChain_)){
        return nullptr;
    } else {
        return pChain_;
    }
}

 QString SystemCertificateManager::CertTrustStatusToString(const CERT_TRUST_STATUS& cts) const
 {
     QString status = "";
     if(cts.dwErrorStatus == CERT_TRUST_NO_ERROR){
         status.append("Этот сертификат действителен.");
         return status;
     }
     if(cts.dwErrorStatus & CERT_TRUST_IS_NOT_TIME_VALID){
         status.append("Срок действия этого сертификата уже истёк или ещё не наступил.\n");
     }
     if(cts.dwErrorStatus & CERT_TRUST_IS_REVOKED){
         status.append("Этот сертификат был отозван.\n");
     }
     if(cts.dwErrorStatus & CERT_TRUST_IS_NOT_SIGNATURE_VALID){
         status.append("Этот сертификат содержит недействительную цифровую подпись.\n");
     }
     //     if(cts.dwErrorStatus & CERT_TRUST_IS_NOT_VALID_FOR_USAGE){
     //         status.append("Этот сертификат "))
     if(cts.dwErrorStatus & CERT_TRUST_IS_UNTRUSTED_ROOT){
         status.append("Нет доверия к корневому сертификату центра сертификации.\n");
     }
     if(cts.dwErrorStatus & CERT_TRUST_REVOCATION_STATUS_UNKNOWN){
         status.append("Статус отзыва сертификата неизвестен.\n");
     }
     if(cts.dwErrorStatus & CERT_TRUST_IS_PARTIAL_CHAIN){
         status.append("Недостаточно информации для проверки этого сертификата.\n");
     }
     if(status.isEmpty()){
         status.append("При проверке сертификата возникла неизвестная ошибка.\n");
     }
     return status;
 }

 bool SystemCertificateManager::IsPrivateKeyForCertExist() const
 {
     DWORD len = 0;
     return CertGetCertificateContextProperty(pCert_, CERT_KEY_PROV_INFO_PROP_ID, NULL, &len) ? true : false;
 }

 QString SystemCertificateManager::GetPrivKeyCSPName() const
 {
     QVector<BYTE> data;
     DWORD data_len = 0;
     QString csp_name;
     if(!CertGetCertificateContextProperty(pCert_, CERT_KEY_PROV_INFO_PROP_ID, nullptr, &data_len))
     {
         return csp_name;
     }

     data.resize(data_len);
     if(!CertGetCertificateContextProperty(pCert_, CERT_KEY_PROV_INFO_PROP_ID, data.data(), &data_len))
     {
         return csp_name;
     }

     CRYPT_KEY_PROV_INFO* key_info = reinterpret_cast<CRYPT_KEY_PROV_INFO*>(data.data());
     return QString::fromWCharArray(key_info->pwszProvName);
 }

 QString SystemCertificateManager::GetPrivKeyName() const
 {
     QVector<BYTE> data;
     DWORD data_len = 0;
     QString csp_name;
     if(!CertGetCertificateContextProperty(pCert_, CERT_KEY_PROV_INFO_PROP_ID, nullptr, &data_len))
     {
         return csp_name;
     }

     data.resize(data_len);
     if(!CertGetCertificateContextProperty(pCert_, CERT_KEY_PROV_INFO_PROP_ID, data.data(), &data_len))
     {
         return csp_name;
     }

     CRYPT_KEY_PROV_INFO* key_info = reinterpret_cast<CRYPT_KEY_PROV_INFO*>(data.data());
     return QString::fromWCharArray(key_info->pwszContainerName);
 }


 QString SystemCertificateManager::GetPublicKeyAlgName() const
 {
     PCCRYPT_OID_INFO pInfo =  CryptFindOIDInfo(CRYPT_OID_INFO_OID_KEY,
                                                const_cast<char*>(pCert_->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId),
                                                0);
     if(!pInfo){
         return QString::fromLatin1(pCert_->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId);
     }
     return QString::fromWCharArray(pInfo->pwszName);
 }

 QString SystemCertificateManager::GetPublicKeyValueString() const
 {
     QByteArray ba = QByteArray::fromRawData(
                 reinterpret_cast<char*>(pCert_->pCertInfo->SubjectPublicKeyInfo.PublicKey.pbData),
                 pCert_->pCertInfo->SubjectPublicKeyInfo.PublicKey.cbData);
     return QString(ba.toHex());
 }

 QString SystemCertificateManager::GetPublicKeyParamString() const
 {
     QByteArray ba = QByteArray::fromRawData(
                 reinterpret_cast<char*>(pCert_->pCertInfo->SubjectPublicKeyInfo.Algorithm.Parameters.pbData),
                 pCert_->pCertInfo->SubjectPublicKeyInfo.Algorithm.Parameters.cbData);
     return QString(ba.toHex());
 }

 bool SystemCertificateManager::IsRootCertificate() const
 {
     if(CertCompareCertificateName(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, &pCert_->pCertInfo->Issuer,
                                   &pCert_->pCertInfo->Subject)){
         return true;
     } else {
         for (DWORD i = 0; i < pCert_->pCertInfo->cExtension; ++i) {
             PCERT_EXTENSION pExt = &pCert_->pCertInfo->rgExtension[i];
             if (strcmp(pExt->pszObjId, szOID_BASIC_CONSTRAINTS2) == 0) {
                 void* ptr = nullptr;
                 DWORD len = 0;
                 if (!CryptDecodeObjectEx(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, X509_BASIC_CONSTRAINTS2,
                                          pExt->Value.pbData, pExt->Value.cbData, CRYPT_DECODE_ALLOC_FLAG,
                                          nullptr, &ptr, &len)) {
                     return false;
                 } else {
                     PCERT_BASIC_CONSTRAINTS2_INFO bsinfo = static_cast<PCERT_BASIC_CONSTRAINTS2_INFO>(ptr);
                     bool is_ca = bsinfo->fCA;
                     LocalFree(ptr);
                     return is_ca;
                 }
             }
         }
         return false;
     }
 }

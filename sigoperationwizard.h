#ifndef SIGOPERATIONWIZARD_H
#define SIGOPERATIONWIZARD_H

#include <QWidget>
#include <memory>

class SystemCertificateManager;
struct _CERT_CONTEXT;

namespace Ui {
class SigOperationWizard;
}

class SigOperationWizard : public QWidget
{
    Q_OBJECT

public:
    explicit SigOperationWizard(QWidget *parent = 0);
    ~SigOperationWizard();

private slots:
    void on_btn_select_sig_clicked();
    void on_btn_sign_clicked();
    void on_btn_select_cert_clicked();
    void on_btn_add_file_clicked();
    void on_btn_rem_file_clicked();
    void Error(QString error);

private:
    void ShowEmptyLineNotification(QWidget* w, const QString& text);

private:
    Ui::SigOperationWizard *ui;
    QStringList cont_files_;
    const _CERT_CONTEXT* cert_ = nullptr;
    bool is_op_success_ = true;
};

#endif // SIGOPERATIONWIZARD_H

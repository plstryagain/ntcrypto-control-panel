#include "wordautomation.h"
#include <string>
#include <QAxObject>
#include <QMessageBox>
#include <QFile>
#include <QStandardPaths>
#include <QFileInfo>

WordAutomation::WordAutomation(QObject *parent) : QObject(parent)
{

}

WordAutomation::~WordAutomation()
{
    if(QFileInfo::exists(temp_)){
        QFile::remove(temp_);
    }

    delete pBms_;
    delete pDoc_;
    delete pDocs_;
    delete pWord_;
}

bool WordAutomation::Init()
{
    temp_ = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).join("")
            + "/_ac_temp_.dotx";
    if(!QFileInfo::exists(temp_)){
        if(!QFile::copy("tmpl\\ac.dotx", temp_)){
            return false;
        }
    }
    pWord_ = new QAxObject("Word.Application", nullptr);
    if(!pWord_){
        return false;
    }
    //pWord_->dynamicCall("Visible", 1);
    pDocs_ = pWord_->querySubObject("Documents");
    if(!pDocs_){
        return false;
    }
    pDoc_ = pDocs_->querySubObject("Open(const QString&)", temp_);
    if(!pDoc_){
        return false;
    }
    pBms_ = pDoc_->querySubObject("Bookmarks");
    if(!pBms_){
        return false;
    }
    return true;
}


HRESULT WordAutomation::FillField(const QString& data, const QString& tag)
{
    int i = 1;
    for(const auto& c : data){
        QString bm = tag + QString::number(i);
        QAxObject* pBm = pBms_->querySubObject("Item(const QString&)", bm);
        if(!pBm){
            return NTE_FAIL;
        }
        QAxObject* pRng = pBm->querySubObject("Range");
        if(!pRng){
            return NTE_FAIL;
        }
        pRng->dynamicCall("Text", c);
        delete pRng;
        delete pBm;
        ++i;
    }
    return 0;
}

HRESULT WordAutomation::SaveAs(const QString& path)
{
    pDoc_->dynamicCall("SaveAs2(const QString&, int)", path, 16);
    return ERROR_SUCCESS;
}

void WordAutomation::Close()
{
    if(pDoc_){
        pDoc_->dynamicCall("Close()");
        pDoc_ = nullptr;
    }
    if(pWord_){
        pWord_->dynamicCall("Quit()");
        pWord_ = nullptr;
    }
}

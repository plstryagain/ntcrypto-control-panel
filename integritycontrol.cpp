#include "integritycontrol.h"
#include <Windows.h>
#include "ntservicemanager.h"

const QString key_iso_srvc_name = "NTKeyIsoSrvc";
const QString cng_key_iso_srvc_name = "NTCNGKeyIsoSrvc";
const std::wstring shared_mem_name = L"Global\\ntcryptosharedmemory";
const std::wstring cng_shared_mem_name = L"Global\\ntcryptocngsharedmemory";


IntegrityControl::IntegrityControl(QObject *parent) : QObject(parent)
{

}

COMPARE_RESULT IntegrityControl::GetResult() const
{
    return cr_;
}

CNG_COMPARE_RESULT IntegrityControl::GetCngResult() const
{
    return ccr_;
}

unsigned long IntegrityControl::GetStatus() const
{
    return status_;
}

void IntegrityControl::CheckIntegrity()
{
    DWORD error = SubmitRequestToSrvc(key_iso_srvc_name, shared_mem_name.data(), &cr_,
                                      sizeof(COMPARE_RESULT));
    if(error != ERROR_SUCCESS){
        status_ = error;
        emit signalFinished();
    }
    error = SubmitRequestToSrvc(cng_key_iso_srvc_name, cng_shared_mem_name.data(), &ccr_,
                                sizeof(CNG_COMPARE_RESULT));
    if(error != ERROR_SUCCESS){
        status_ = error;
        emit signalFinished();
    }
    emit signalFinished();
}

unsigned long IntegrityControl::SubmitRequestToSrvc(const QString& srvc_name,
                                                    const wchar_t* mem_name, void* cr, const int cr_len)
{
    NTServiceManager scmgr;
    DWORD error = scmgr.OpenWinSCManager(SC_MANAGER_ENUMERATE_SERVICE);
    if(error != ERROR_SUCCESS)
    {
        return error;
    }

    error = scmgr.OpenNTService(srvc_name, SERVICE_USER_DEFINED_CONTROL);
    if(error != ERROR_SUCCESS)
    {
        return error;
    }

    error = scmgr.ControlNTService(SERVICE_CONTROL_INTEGRITY_CHECK);
    if(error == NTE_FAIL)
    {
        return error;
    }

    error = ReadSharedMemory(mem_name, cr, cr_len);
    if(error != ERROR_SUCCESS)
    {
        return error;
    }

    scmgr.ControlNTService(SERVICE_CONTROL_CLOSE_SHARED_HANDLE);
    return ERROR_SUCCESS;
}

unsigned long IntegrityControl::ReadSharedMemory(const wchar_t* mem_name, void* cr, const int cr_len)
{
    HANDLE hMapFile = NULL;
    DWORD error = NTE_FAIL;
    BYTE* buff = nullptr;

    hMapFile = OpenFileMapping(
                FILE_MAP_READ,   // read/write access
                FALSE,                 // do not inherit the name
                mem_name);               // name of mapping object
    if (!hMapFile)
    {
        error = GetLastError();
        goto Exit;
    }

    buff = static_cast<BYTE*>(MapViewOfFile(hMapFile, // handle to map object
                  FILE_MAP_READ,  // read/write permission
                  0,
                  0,
                  cr_len));
    if(!buff)
    {
        error = GetLastError();
        goto Exit;
    }

    CopyMemory(cr, buff, cr_len);
    UnmapViewOfFile(buff);
    error = ERROR_SUCCESS;

Exit:
    if(hMapFile)
    {
        CloseHandle(hMapFile);
    }
    return error;
}



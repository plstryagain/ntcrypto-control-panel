#ifndef DIALOGPLEASEWAIT_H
#define DIALOGPLEASEWAIT_H

#include <QDialog>

namespace Ui {
class DialogPleaseWait;
}

class DialogPleaseWait : public QDialog
{
    Q_OBJECT

public:
    explicit DialogPleaseWait(QWidget *parent = 0);
    ~DialogPleaseWait();
    void SetAmountOfFiles(const int amout_of_files);

protected:
    void showEvent(QShowEvent *event) override;
    void keyPressEvent(QKeyEvent *e) override;

public slots:
    void ProgressUpdate();

private:
    Ui::DialogPleaseWait *ui;
    QWidget* parent_;
    int amount_of_files_ = 0;
    int num_of_proc_files_ = 0;
};

#endif // DIALOGPLEASEWAIT_H

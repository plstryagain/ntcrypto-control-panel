#include "icertificatemanager.h"

ICertificateManager::ICertificateManager()
{

}

std::unique_ptr<ICertificateManager> ICertificateManager::Create(const CERTIFICATE_TYPE &ct,
                                                                 const void *ctx,
                                                                 const bool is_need_free_ctx)
{
    switch(ct){
    case CERTIFICATE_TYPE::SystemCertificate:
        return std::unique_ptr<ICertificateManager>(
                    new SystemCertificateManager(static_cast<PCCERT_CONTEXT>(ctx), is_need_free_ctx));
    case CERTIFICATE_TYPE::CRL:
        return std::unique_ptr<ICertificateManager>(
                    new CrlManager(static_cast<PCCRL_CONTEXT>(ctx), is_need_free_ctx));
    case CERTIFICATE_TYPE::AttributeCertificate:
        return std::unique_ptr<ICertificateManager>(
                    new AttributeCertificateManager(static_cast<const QByteArray*>(ctx)));
    default:
        return nullptr;
    }
}

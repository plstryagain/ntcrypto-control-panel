#ifndef REGISTRY_H
#define REGISTRY_H

#include <QObject>
#include <QVector>
#include <windows.h>

namespace RegConstants
{
    extern const QString DEFAULT_PROVIDER_OPTIONS_REG_PATH;
    extern const QString PROVIDER_KEYS_REG_PATH;
    extern const QString DEFAULT_STORAGE_OPTION;
    extern const QString LOG_FILE_PATH_OPTION;
    extern const QString CNG_LOG_FILE_PATH_OPTION;
    extern const QString KEY_STORAGE_OPTION;
    extern const QString KEY_STORAGE_SLOT_ID_OPTION;
    extern const QString INSTALLED_PATH_OPTION;
    extern const QString CRLDP_URLS_OPTION;
}

class RegistryManager : public QObject
{
    Q_OBJECT
public:
    explicit RegistryManager();
    ~RegistryManager();

signals:

public slots:
    NTSTATUS OpenCurrentUser(const DWORD access);
    NTSTATUS OpenDefaultUser();
    NTSTATUS OpenKey(const QString &sub_path, const DWORD access);
    NTSTATUS CreateKey(const QString &sub_path);
    NTSTATUS QueryValue(const QString &value_name, BYTE* data, DWORD* data_len);
    NTSTATUS SetKeyValue(const QString& sub_path, const QString& val_name,
                                          const DWORD type, const BYTE* data, const DWORD data_len);
    NTSTATUS EnumKeyEx(const DWORD index, QVector<wchar_t> *name, DWORD* name_len);
    NTSTATUS DeleteKey(const QString& sub_path);

private:
    HKEY hCurrentUser_;
    HKEY hKey_;
    QString sub_path_;
};

#endif // REGISTRY_H

#include "certificatestoremanager.h"

CertificateStoreManager::CertificateStoreManager()
{

}

std::unique_ptr<CertificateStoreManager> CertificateStoreManager::Create(const CERTIFICATE_STORE_TYPE cst)
{
    switch(cst){
    case CERTIFICATE_STORE_TYPE::MY:
    case CERTIFICATE_STORE_TYPE::ROOT:
        return std::unique_ptr<CertificateStoreManager>(new SystemCertificateStoreManager(cst));
    case CERTIFICATE_STORE_TYPE::SIGMA:
        return std::unique_ptr<CertificateStoreManager>(new SigmaCertificateStoreManager());
    case CERTIFICATE_STORE_TYPE::NTSTORE:
        return std::unique_ptr<CertificateStoreManager>(new NtstoreCertificateStoreManager());
    case CERTIFICATE_STORE_TYPE::CRL:
        return std::unique_ptr<CertificateStoreManager>(new CrlStoreManager());
    case CERTIFICATE_STORE_TYPE::ATTRIBUTE:
        return std::unique_ptr<CertificateStoreManager>(new AttributeCertificateStoreManager());
    default:
        return nullptr;
    }
}

const std::vector<std::unique_ptr<ICertificateManager>>& CertificateStoreManager::GetCertificateManagerList() const
{
    return crt_mgr_list_;
}

const std::unique_ptr<ICertificateManager>& CertificateStoreManager::GetCertificateManagerByIndex(
        const quint32 idx) const
{
    return crt_mgr_list_.at(idx);
}

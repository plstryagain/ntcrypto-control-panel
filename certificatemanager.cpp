#include "certificatemanager.h"

CertificateManager::CertificateManager(const PCCERT_CONTEXT &cert_ctx, bool need_free_ctx) :
    cert_ctx_(cert_ctx), need_free_ctx_(need_free_ctx)
{

}

CertificateManager::~CertificateManager()
{
    if(need_free_ctx_)
    {
        CertFreeCertificateContext(cert_ctx_);
    }
}

QString CertificateManager::GetCertSubjectName()
{
    QVector<wchar_t> name;
    DWORD name_size = CertGetNameString(cert_ctx_, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, nullptr, 0);
    name.resize(name_size);
    CertGetNameString(cert_ctx_, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, name.data(), name_size);
    return QString::fromWCharArray(name.data());
}

QString CertificateManager::GetCertIssuerName()
{
    QVector<wchar_t> name;
    DWORD name_size = CertGetNameString(cert_ctx_, CERT_NAME_SIMPLE_DISPLAY_TYPE, CERT_NAME_ISSUER_FLAG, NULL, nullptr, 0);
    name.resize(name_size);
    CertGetNameString(cert_ctx_, CERT_NAME_SIMPLE_DISPLAY_TYPE, CERT_NAME_ISSUER_FLAG, NULL, name.data(), name_size);
    return QString::fromWCharArray(name.data());
}

bool CertificateManager::IsPrivKeyExist()
{
    DWORD len = 0;
    return CertGetCertificateContextProperty(cert_ctx_, CERT_KEY_PROV_INFO_PROP_ID, NULL, &len) ? true : false;
}

void CertificateManager::ShowUICertificate()
{
    CRYPTUI_VIEWCERTIFICATE_STRUCT view_struct;
    ZeroMemory( &view_struct, sizeof( CRYPTUI_VIEWCERTIFICATE_STRUCT ) );

    view_struct.dwSize = sizeof( CRYPTUI_VIEWCERTIFICATE_STRUCT );
    view_struct.dwFlags = 0;
    view_struct.pCertContext = cert_ctx_;
    view_struct.szTitle = L"Certificate";

    BOOL changed = FALSE;
    CryptUIDlgViewCertificate(&view_struct, &changed);
}

QString CertificateManager::GetKeyCSPName()
{
    QVector<BYTE> data;
    DWORD data_len = 0;
    QString csp_name;
    if(!CertGetCertificateContextProperty(cert_ctx_, CERT_KEY_PROV_INFO_PROP_ID, nullptr, &data_len))
    {
        return csp_name;
    }

    data.resize(data_len);
    if(!CertGetCertificateContextProperty(cert_ctx_, CERT_KEY_PROV_INFO_PROP_ID, data.data(), &data_len))
    {
        return csp_name;
    }

    CRYPT_KEY_PROV_INFO* key_info = reinterpret_cast<CRYPT_KEY_PROV_INFO*>(data.data());
    return QString::fromWCharArray(key_info->pwszProvName);
}

QString CertificateManager::GetKeyContainerName()
{
    QVector<BYTE> data;
    DWORD data_len = 0;
    QString csp_name;
    if(!CertGetCertificateContextProperty(cert_ctx_, CERT_KEY_PROV_INFO_PROP_ID, nullptr, &data_len))
    {
        return csp_name;
    }

    data.resize(data_len);
    if(!CertGetCertificateContextProperty(cert_ctx_, CERT_KEY_PROV_INFO_PROP_ID, data.data(), &data_len))
    {
        return csp_name;
    }

    CRYPT_KEY_PROV_INFO* key_info = reinterpret_cast<CRYPT_KEY_PROV_INFO*>(data.data());
    return QString::fromWCharArray(key_info->pwszContainerName);
}

int CertificateManager::SetCertCtxProperty(const DWORD prop_id, const DWORD flags, const void *data)
{
    return CertSetCertificateContextProperty(cert_ctx_, prop_id, flags, data);
}

PCCERT_CONTEXT CertificateManager::GetCertCtx()
{
    return cert_ctx_;
}

QString CertificateManager::GetNotBefore()
{
    FILETIME ft = cert_ctx_->pCertInfo->NotBefore;
    SYSTEMTIME st;
    FileTimeToSystemTime(&ft, &st);
    QString time = QString("%1.%2.%3 %4:%5:%6").arg(st.wDay, 2, 10, QChar('0')).
                                                arg(st.wMonth, 2, 10, QChar('0')).
                                                arg(st.wYear, 2, 10, QChar('0')).
                                                arg(st.wHour, 2, 10, QChar('0')).
                                                arg(st.wMinute, 2, 10, QChar('0')).
                                                arg(st.wSecond, 2, 10, QChar('0'));
    return time;
}

QString CertificateManager::GetNotAfter()
{
    FILETIME ft = cert_ctx_->pCertInfo->NotAfter;
    SYSTEMTIME st;
    FileTimeToSystemTime(&ft, &st);
    QString time = QString("%1.%2.%3 %4:%5:%6").arg(st.wDay, 2, 10, QChar('0')).
                                                arg(st.wMonth, 2, 10, QChar('0')).
                                                arg(st.wYear, 2, 10, QChar('0')).
                                                arg(st.wHour, 2, 10, QChar('0')).
                                                arg(st.wMinute, 2, 10, QChar('0')).
                                                arg(st.wSecond, 2, 10, QChar('0'));
    return time;
}

bool CertificateManager::IsRootCertificate()
{
    if(CertCompareCertificateName(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, &cert_ctx_->pCertInfo->Issuer,
                                  &cert_ctx_->pCertInfo->Subject)){
        return true;
    } else {
        for (DWORD i = 0; i < cert_ctx_->pCertInfo->cExtension; ++i) {
            PCERT_EXTENSION pExt = &cert_ctx_->pCertInfo->rgExtension[i];
            if (strcmp(pExt->pszObjId, szOID_BASIC_CONSTRAINTS2) == 0) {
                void* ptr = nullptr;
                DWORD len = 0;
                if (!CryptDecodeObjectEx(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, X509_BASIC_CONSTRAINTS2,
                                         pExt->Value.pbData, pExt->Value.cbData, CRYPT_DECODE_ALLOC_FLAG,
                                         nullptr, &ptr, &len)) {
                    return false;
                } else {
                    PCERT_BASIC_CONSTRAINTS2_INFO bsinfo = static_cast<PCERT_BASIC_CONSTRAINTS2_INFO>(ptr);
                    bool is_ca = bsinfo->fCA;
                    LocalFree(ptr);
                    return is_ca;
                }
            }
        }
        return false;
    }
}
